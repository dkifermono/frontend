# Development Readme

### Development Setup

- Latest Visual Studio 2017 Version (with the latest Update)
- If you use Resharper - Latest Version because of .NET Standard / .NET Core
- Node.JS (Latest LTS-Version)
- NPM (node install -g npm@latest)
- Use powershell or cmd.exe for invoking commands - As an alternative cmder.net

#### Frameworks

- Angular5
- Moment.js
- Feather SVG Icons
- .NET Core 2.1
- Swagger for REST-Documentation
- NSwag for client generation
- Orleans as Service Layer

### Architecture

Basically the internal logic is hosted within a so called "Orleans Silo" (we call that "Orleans Service Layer"). 

The Frontend is calling the methods defined within the Messaging projects.


For Testing: the latest Version of the Service Layer is up an running at "192.168.201.141".

This can be configured within the appsettings.json and within the "OrleansDependencyConfiguration" class.

If needed, there is already the schema implemented for mocking the Service Layer, or only parts of it.


- m2trust.Messaging.* NuGet Packages defining all messages against "Orleans Service Layer"
- The Service Layer is completly asynchronous, every Call has to be awaited and to be configured (.ConfigureAwait(false) most of the time)

### Usage

- "**dotnet watch run**" (In Folder m2trust.Frontend) within the m2trust.Frontend folder will start the development server and watching for changes (No breakpoints possible), if you want to debug the project start it with Visual Studio or Visual Studio Core (Not with IIS Express - select the project within the "Run" section).
- "**npm install**" Ensure all packages configured are downloaded and available
- "**npm run debug**" (In Folder ClientApp) will run the Angular-CLI development server which is getting the requests of the dotnet core server via a proxy which is implemented in .NET Core
- "**npm run swagger**" (In Folder ClientApp) will generate the Typescript Client API according to the current, local WebAPI project. The .NET Core server has to be up and running (dotnet watch run)


### Deployment

Just commit and push your changes, an automatic build is getting started and will deploy everything on the test system.

Testing URL is **http://m2trust-app.alpha.ecom.ag:10000**

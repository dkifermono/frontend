﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Models;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using System.Collections.Generic;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;

namespace m2trust.Frontend.Web.Configuration
{
    /// <summary>
    /// Reinstate dci configuration.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Web.Configuration.SettingsProviderConfiguration" />
    /// <seealso cref="m2trust.Frontend.Common.Configuration.IReinstateDCIConfiguration" />
    public class ReinstateDCIConfiguration : SettingsProviderConfiguration, IReinstateDCIConfiguration
    {
        private readonly Common.Configuration.Models.Configuration defaultConfiguration;

        #region Constructors

        public ReinstateDCIConfiguration(ISettingsProviderFactory settingsProviderFactory, IServiceConfigurationStore serviceConfigurationStore) : base(settingsProviderFactory, serviceConfigurationStore)
        {
            this.defaultConfiguration = new Common.Configuration.Models.Configuration();
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets the dci profile templates.
        /// </summary>
        /// <value>The dci profile templates.</value>
        public Task<IEnumerable<ProfileTemplate>> DCIProfileTemplates => GetSettingAsync<IEnumerable<ProfileTemplate>>(SettingKeys.DCIProfileTemplatesKey, defaultConfiguration.DCIProfileTemplates);

        #endregion Properties
    }
}
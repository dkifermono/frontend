﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;
using m2trust.Messaging.Configuration.Models;
using System;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.Configuration
{
    public class FrontendConfigurationService : IFrontendConfigurationService
    {
        private readonly IServiceConfigurationStore serviceConfigurationStore;
        private readonly ICacheService cacheService;
        private readonly ISettingsProviderFactory settingsProviderFactory;

 

        public FrontendConfigurationService(
            IServiceConfigurationStore serviceConfigurationStore,
            ICacheService cacheService,
            ISettingsProviderFactory settingsProviderFactory) 
        {
            this.serviceConfigurationStore = serviceConfigurationStore;
            this.cacheService = cacheService;
            this.settingsProviderFactory = settingsProviderFactory;
        }

        #region ServiceConfiguration
        public Task<ServiceConfiguration> GetServiceConfigurationAsync()
        {
            return serviceConfigurationStore.GetConfiguration();
        }
        public async Task SaveServiceConfigurationAsync(ServiceConfiguration configuration)
        {
            await this.serviceConfigurationStore.SaveConfiguration(configuration);
        }
        #endregion ServiceConfiguration

        #region FrontendConfiguration
        public async Task<Common.Configuration.Models.Configuration> GetFrontendConfigurationAsync()
        {
            var prov = await settingsProviderFactory.CreateAsync();
            var configuration = prov.ApplicationSettings.ReadSetting(SettingKeys.FrontendConfigKey,
                new Common.Configuration.Models.Configuration());
            return configuration;
        }

        public async Task SaveFrontendConfigurationAsync(Common.Configuration.Models.Configuration configuration, Guid userId)
        {
            var provider = await settingsProviderFactory.CreateAsync();
            var newConfiguration = provider
                .ApplicationSettings
                .WriteSetting(SettingKeys.FrontendConfigKey, configuration);
            await provider.SaveApplicationSettings(newConfiguration);

            foreach (var propertyInfo in configuration.Activities.GetType().GetProperties())
            {
                var key = $"{userId}.{propertyInfo.Name}.IsAuthorized";
                await this.cacheService.RemoveKeyAsync(key);
            }
        }
        #endregion FrontendConfiguration

    }
}

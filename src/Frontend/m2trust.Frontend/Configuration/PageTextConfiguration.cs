﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using System.Threading.Tasks;
using m2trust.Frontend.Common.Configuration.Models;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;

namespace m2trust.Frontend.Web.Configuration
{
    /// <summary>
    /// Page text configuration.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Web.Configuration.SettingsProviderConfiguration" />
    /// <seealso cref="m2trust.Frontend.Common.Configuration.IPageTextConfiguration" />
    public class PageTextConfiguration : SettingsProviderConfiguration, IPageTextConfiguration
    {
        private readonly Common.Configuration.Models.Configuration defaultConfiguration;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PageTextConfiguration" /> class.
        /// </summary>
        /// <param name="settingsProviderFactory">The settings provider factory.</param>
        /// <param name="serviceConfigurationStore"></param>
        public PageTextConfiguration(ISettingsProviderFactory settingsProviderFactory, IServiceConfigurationStore serviceConfigurationStore) : base(settingsProviderFactory, serviceConfigurationStore)
        {
            this.defaultConfiguration = new Common.Configuration.Models.Configuration();
        }

        #endregion Constructors

        #region Properties
        public Task<PageTextItem> ContactAddress => GetSettingAsync<PageTextItem>(SettingKeys.ContactAddressKey, defaultConfiguration.ContactAddressText);

        public Task<PageTextItem> FooterText => GetSettingAsync<PageTextItem>(SettingKeys.FooterTextKey, defaultConfiguration.FooterText);

        public Task<PageTextItem> HeaderText => GetSettingAsync<PageTextItem>(SettingKeys.HeaderTextKey, defaultConfiguration.HeaderText);

        public Task<PageTextItem> HelpDeskInstruction => GetSettingAsync<PageTextItem>(SettingKeys.HelpDeskInstructionKey, defaultConfiguration.HelpDeskInstruction);

        public Task<PageTextItem> WelcomeText => GetSettingAsync<PageTextItem>(SettingKeys.WelcomeTextKey, defaultConfiguration.WelcomeText);

        #endregion Properties
    }



}
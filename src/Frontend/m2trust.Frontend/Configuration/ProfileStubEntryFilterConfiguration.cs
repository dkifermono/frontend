﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using System.Collections.Generic;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;

namespace m2trust.Frontend.Web.Configuration
{
    /// <summary>
    /// Profile stub entry filter configuration.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Web.Configuration.SettingsProviderConfiguration" />
    /// <seealso cref="m2trust.Frontend.Common.Configuration.IProfileStubEntryFilterConfiguration" />
    public class ProfileStubEntryFilterConfiguration : SettingsProviderConfiguration, IProfileStubEntryFilterConfiguration
    {
        private readonly Common.Configuration.Models.Configuration defaultConfiguration;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfileStubEntryFilterConfiguration" /> class.
        /// </summary>
        /// <param name="settingsProviderFactory">The settings provider factory.</param>
        /// <param name="serviceConfigurationStore"></param>
        public ProfileStubEntryFilterConfiguration(ISettingsProviderFactory settingsProviderFactory, IServiceConfigurationStore serviceConfigurationStore) : base(settingsProviderFactory, serviceConfigurationStore)
        {
            this.defaultConfiguration = new Common.Configuration.Models.Configuration();
        }

        #endregion Constructors

        #region Properties
        
        public Task<IEnumerable<string>> BlacklistedProfileTemplates => GetSettingAsync<IEnumerable<string>>(SettingKeys.BlacklistedProfileTemplatesKey, defaultConfiguration.BlacklistedProfileTemplates);

        public Task<IEnumerable<string>> ExcludedProfileTemplates => GetSettingAsync<IEnumerable<string>>(SettingKeys.ExcludedProfileTemplatesKey, defaultConfiguration.ExcludedProfileTemplates);

        #endregion Properties
    }
}
﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using m2trust.Frontend.Common.Configuration.Models;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;
using Microsoft.Extensions.Options;

namespace m2trust.Frontend.Web.Configuration
{
    public class ActivityConfiguration : SettingsProviderConfiguration, IActivityConfiguration
    {
        public readonly IOptions<AdministrationConfig> administrationConfiguration;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityConfiguration" /> class.
        /// </summary>
        /// <param name="settingsProviderFactory"></param>
        /// <param name="administrationConfiguration"></param>
        /// <param name="serviceConfigurationStore"></param>
        public ActivityConfiguration(ISettingsProviderFactory settingsProviderFactory, IOptions<AdministrationConfig> administrationConfiguration, IServiceConfigurationStore serviceConfigurationStore) : base(settingsProviderFactory, serviceConfigurationStore)
        {
            this.administrationConfiguration = administrationConfiguration;
        }

        #endregion Constructors

        #region Methods

        public async Task<string[]> GetActivitiesAsync()
        {
            var defaultActivities = GetDefaultActivities();
            var config = this.administrationConfiguration.Value;
            Activity response = await GetSettingAsync<Activity>(SettingKeys.Activities, defaultActivities);
 
            response.Administration = new BaseActivity
            {
                Name = config.Name,
                AuthorizedGroups = new List<string>(config.Groups)
            };

            var properties = response.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(prop => !prop.GetIndexParameters().Any());
            var activities = properties.Select(x => x.Name).ToArray();
            return activities;
        }

        public async Task<bool?> GetShowCommentsAsync(string activityName)
        {
            var defaultActivities = GetDefaultActivities();
            Activity response = await GetSettingAsync<Activity>(SettingKeys.Activities, defaultActivities);
            return response[activityName].ShowComments;
        }

        public async Task<bool?> GetContinueAsync(string activityName)
        {
            var defaultActivities = GetDefaultActivities();
            Activity response = await GetSettingAsync<Activity>(SettingKeys.Activities, defaultActivities);
            return response[activityName].Continue;
        }

        /// <summary>
        /// Gets the names of AD Groups that are authorized to perform the given activity.
        /// </summary>
        /// <param name="activityName">The name of the activity.</param>
        /// <returns></returns>
        public async Task<IEnumerable<string>> GetAuthorizedADGroups(string activityName)
        {
            var defaultActivities = GetDefaultActivities();
            var config = this.administrationConfiguration.Value;
            Activity response = await GetSettingAsync<Activity>(SettingKeys.Activities, defaultActivities);
            response.Administration = new BaseActivity
            {
                Name = config.Name,
                AuthorizedGroups = new List<string>(config.Groups)
            };
            var authorizedGroups = response[activityName].AuthorizedGroups;
            return authorizedGroups;
        }

        /// <summary>
        /// Returns a value indicating whether or not the activity is registered.
        /// </summary>
        /// <param name="activityName">The name of the activity to check</param>
        /// <returns></returns>
        public async Task<bool> IsActivityPermissionRegistered(string activityName)
        {
            var activities = await GetActivitiesAsync();
            return activities.Contains(activityName, StringComparer.InvariantCulture);
        }

        private Activity GetDefaultActivities()
        {
            var configuration = new m2trust.Frontend.Common.Configuration.Models.Configuration();
            return configuration.Activities;
        }

        #endregion Methods
    }

   
}
﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using System.Collections.Generic;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;

namespace m2trust.Frontend.Web.Configuration
{
    /// <summary>
    /// Search form configuration.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Web.Configuration.SettingsProviderConfiguration" />
    /// <seealso cref="m2trust.Frontend.Common.Configuration.ISearchFormConfiguration" />
    public class SearchFormConfiguration : SettingsProviderConfiguration, ISearchFormConfiguration
    {
        private readonly Common.Configuration.Models.Configuration defaultConfiguration;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchFormConfiguration" /> class.
        /// </summary>
        /// <param name="settingsProviderFactory">The settings provider factory.</param>
        public SearchFormConfiguration(ISettingsProviderFactory settingsProviderFactory, IServiceConfigurationStore serviceConfigurationStore) : base(settingsProviderFactory, serviceConfigurationStore)
        {
            this.defaultConfiguration = new Common.Configuration.Models.Configuration();
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets the size of the search users page.
        /// </summary>
        /// <value>The size of the search users page.</value>
        public Task<int> SearchUsersPageSize => GetSettingAsync(SettingKeys.SearchUsersPageSizeKey, defaultConfiguration.SearchUsersPageSize);

        /// <summary>
        /// Gets the user display properties.
        /// </summary>
        /// <value>The user display properties.</value>
        public Task<IEnumerable<string>> UserDisplayProperties => GetSettingAsync<IEnumerable<string>>(SettingKeys.UserDisplayPropertiesKey, defaultConfiguration.UserDisplayProperties);

        /// <summary>
        /// Gets the user search properties.
        /// </summary>
        /// <returns>The user search properties.</returns>
        public Task<IEnumerable<string>> UserSearchProperties => GetSettingAsync<IEnumerable<string>>(SettingKeys.UserSearchPropertiesKey, defaultConfiguration.UserSearchProperties);

        #endregion Properties
    }
}
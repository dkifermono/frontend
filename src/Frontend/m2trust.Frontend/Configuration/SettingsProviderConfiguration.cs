﻿using m2trust.Frontend.Web.Configuration.AppConfiguration;
using System.Threading.Tasks;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;
using m2trust.Frontend.Common;

namespace m2trust.Frontend.Web.Configuration
{
    /// <summary>
    /// Settings provider configuration base class.
    /// </summary>
    public abstract class SettingsProviderConfiguration
    {
        #region Fields

        private readonly ISettingsProviderFactory settingsProviderFactory;
        private readonly IServiceConfigurationStore _serviceConfigurationStore;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsProviderConfiguration" /> class.
        /// </summary>
        /// <param name="settingsProviderFactory">The settings provider factory.</param>
        /// <param name="serviceConfigurationStore"></param>
        public SettingsProviderConfiguration(ISettingsProviderFactory settingsProviderFactory, IServiceConfigurationStore serviceConfigurationStore)
        {
            this.settingsProviderFactory = settingsProviderFactory;
            _serviceConfigurationStore = serviceConfigurationStore;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Gets the setting asynchronous.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="fallBackValue">The fall back value.</param>
        /// <returns></returns>
        public virtual async Task<T> GetSettingAsync<T>(string key, T fallBackValue)
        {
            var provider = await settingsProviderFactory.CreateAsync(); 
            if (provider?.ApplicationSettings != null)
            {
                var config = provider.ApplicationSettings.ReadSetting(SettingKeys.FrontendConfigKey, new Common.Configuration.Models.Configuration());
                var result = config?.GetType().GetProperty(key).GetValue(config, null);
                if (result == null)
                {
                    return fallBackValue;
                }
                return (T)result;
            }
            return fallBackValue;
        }

        public async Task<string> GetSignatureDataCollectionItemName()
        {
            var fimConfig = await this._serviceConfigurationStore.GetFimConfiguration();
            return fimConfig.SignatureDciName;
        }

        public async Task<bool> GetRejectInvalidSigningCertificate()
        {
            var fimConfig = await this._serviceConfigurationStore.GetFimConfiguration();
            return fimConfig.RejectInvalidSigningCertificate;
        }
        #endregion Methods
    }
}
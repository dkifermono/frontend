﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;

namespace m2trust.Frontend.Web.Configuration
{
    /// <summary>
    /// Signature configuration.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Web.Configuration.SettingsProviderConfiguration" />
    /// <seealso cref="m2trust.Frontend.Common.Configuration.ISignatureConfiguration" />
    public class SignatureConfiguration : SettingsProviderConfiguration, ISignatureConfiguration
    {
        private readonly Common.Configuration.Models.Configuration defaultConfiguration;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SignatureConfiguration" /> class.
        /// </summary>
        /// <param name="settingsProviderFactory">The settings provider factory.</param>
        public SignatureConfiguration(ISettingsProviderFactory settingsProviderFactory, IServiceConfigurationStore serviceConfigurationStore) : base(settingsProviderFactory, serviceConfigurationStore)
        {
            this.defaultConfiguration = new Common.Configuration.Models.Configuration();
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets the reject invalid signing certificate.
        /// </summary>
        /// <value>The reject invalid signing certificate.</value>
        public Task<bool> RejectInvalidSigningCertificate => GetRejectInvalidSigningCertificate();

        /// <summary>
        /// Gets the name of the signature data collection item.
        /// </summary>
        /// <value>The name of the signature data collection item.</value>
        public Task<string> SignatureDataCollectionItemName => GetSignatureDataCollectionItemName();

        #endregion Properties
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using m2trust.Messaging.Configuration.API;
using m2trust.Messaging.Settings;
using m2trust.Settings.Client;
using m2trust.Settings.Internal;

namespace m2trust.Frontend.Web.Configuration.AppConfiguration
{
    public class ConfigurationProvider : SettingsProviderBase
    {
        private readonly IConfigurationStore configStore;

        public ConfigurationProvider(string appKey, string userKey, IConfigurationStore configStore) : base(appKey, userKey)
        {
            this.configStore = configStore;
        }

        protected override Task<ISettingsStore> GetSettingsStoreAsync()
        {
            return Task.FromResult<ISettingsStore>(configStore);
        }
    }
}

﻿using Autofac.Features.Indexed;
using m2trust.Frontend.Web.Configuration.AppConfiguration.Keys;
using m2trust.Messaging.Settings;
using m2trust.Settings;
using m2trust.Settings.Service;
using System;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;
using Orleans;

namespace m2trust.Frontend.Web.Configuration.AppConfiguration
{
    /// <summary>
    /// Settings provider factory.
    /// </summary>
    public class SettingsProviderFactory : ISettingsProviderFactory
    {
        #region Fields

        private readonly IAppKeyProvider appKeyProvider;
        private readonly IUserKeyProvider userKeyProvider;
        private readonly IConfigurationStore configStore;

        #endregion Fields


        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsProviderFactory" /> class.
        /// </summary>
        /// <param name="appKeyProvider">The application key provider.</param>
        /// <param name="userKeyProvider">The user key provider.</param>
        /// <param name="configStore"></param>
        public SettingsProviderFactory(IAppKeyProvider appKeyProvider, IUserKeyProvider userKeyProvider, IConfigurationStore configStore)
        {
            this.appKeyProvider = appKeyProvider;
            this.userKeyProvider = userKeyProvider;
            this.configStore = configStore;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns></returns>
        public async Task<ISettingsProvider> CreateAsync()
        {
            var appKey = await appKeyProvider.GetKeyAsync();
            var userKey = await userKeyProvider.GetKeyAsync();
            if (appKey == null)
            {
                throw new Exception("App key doesn't exist");
            }
            if (userKey == null)
            {
                throw new Exception("User key doesn't exist");
            }
            var settingsProvider = new ConfigurationProvider(appKey, userKey, configStore);
            await settingsProvider.Initialize();
            return settingsProvider;
        }

        #endregion Methods
    }
}
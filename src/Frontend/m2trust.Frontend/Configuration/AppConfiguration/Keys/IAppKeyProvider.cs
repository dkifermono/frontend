﻿using System.Threading.Tasks;

namespace m2trust.Frontend.Web.Configuration.AppConfiguration.Keys
{
    /// <summary>
    /// App key provider contract.
    /// </summary>
    public interface IAppKeyProvider
    {
        #region Methods

        /// <summary>
        /// Gets the key.
        /// </summary>
        /// <returns></returns>
        Task<string> GetKeyAsync();

        #endregion Methods
    }
}
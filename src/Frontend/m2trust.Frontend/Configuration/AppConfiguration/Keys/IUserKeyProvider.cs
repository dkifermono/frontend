﻿using System.Threading.Tasks;

namespace m2trust.Frontend.Web.Configuration.AppConfiguration.Keys
{
    /// <summary>
    /// User key provider contract.
    /// </summary>
    public interface IUserKeyProvider
    {
        #region Methods

        /// <summary>
        /// Gets the key asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<string> GetKeyAsync();

        #endregion Methods
    }
}
﻿using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.Configuration.AppConfiguration.Keys
{
    /// <summary>
    /// App key provider
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Web.Configuration.AppConfiguration.Keys.IAppKeyProvider" />
    public class AppKeyProvider : IAppKeyProvider
    {
        #region Fields


        private string appKey;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppKeyProvider" /> class.
        /// </summary>
        /// <param name="appKey">The application key.</param>
        public AppKeyProvider(string appKey)
        {
            this.appKey = appKey;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Gets the key.
        /// </summary>
        /// <returns></returns>
        public Task<string> GetKeyAsync()
        {
            if (string.IsNullOrEmpty(appKey))
            {
                return null;
            }
            else return Task.FromResult(appKey);
            
        }

        #endregion Methods
    }
}
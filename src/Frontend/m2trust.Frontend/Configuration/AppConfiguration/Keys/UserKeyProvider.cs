﻿using m2trust.Frontend.Web.Infrastructure;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.Configuration.AppConfiguration.Keys
{
    /// <summary>
    /// User key provider
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Web.Configuration.AppConfiguration.Keys.IUserKeyProvider" />
    public class UserKeyProvider : IUserKeyProvider
    {
        #region Fields

        private readonly IApplicationUserContext applicationUserContext;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserKeyProvider" /> class.
        /// </summary>
        /// <param name="applicationUserContext">The application user context.</param>
        public UserKeyProvider(IApplicationUserContext applicationUserContext)
        {
            this.applicationUserContext = applicationUserContext;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Gets the key asynchronous.
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetKeyAsync()
        {
            var user = await applicationUserContext.GetCurrentUser();
            if (user != null)
            {
                return user.UniqueIdentifier.ToString();
            }
            else
            {
                return null;
            }
        }

        #endregion Methods
    }
}
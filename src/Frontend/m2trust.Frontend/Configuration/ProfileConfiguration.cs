﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;

namespace m2trust.Frontend.Web.Configuration
{
    /// <summary>
    /// Profile configuration.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Web.Configuration.SettingsProviderConfiguration" />
    /// <seealso cref="m2trust.Frontend.Common.Configuration.IProfileConfiguration" />
    public class ProfileConfiguration : SettingsProviderConfiguration, IProfileConfiguration
    {
        private readonly Common.Configuration.Models.Configuration defaultConfiguration;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfileConfiguration" /> class.
        /// </summary>
        /// <param name="settingsProviderFactory">The settings provider factory.</param>
        /// <param name="serviceConfigurationStore"></param>
        public ProfileConfiguration(ISettingsProviderFactory settingsProviderFactory, IServiceConfigurationStore serviceConfigurationStore) : base(settingsProviderFactory, serviceConfigurationStore)
        {
            this.defaultConfiguration = new Common.Configuration.Models.Configuration();
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets a value indicating whether [select default profile].
        /// </summary>
        /// <value><c>true</c> if [select default profile]; otherwise, <c>false</c>.</value>
        public Task<bool> SelectDefaultProfile => GetSettingAsync(SettingKeys.SelectDefaultProfileKey, defaultConfiguration.SelectDefaultProfile);
        

        #endregion Properties
    }
}
﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;

namespace m2trust.Frontend.Web.Configuration
{
    /// <summary>
    /// Profile state filter configuration.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Web.Configuration.SettingsProviderConfiguration" />
    /// <seealso cref="m2trust.Frontend.Common.Configuration.IProfileStateFilterConfiguration" />
    public class ProfileStateFilterConfiguration : SettingsProviderConfiguration, IProfileStateFilterConfiguration
    {
        private readonly Common.Configuration.Models.Configuration defaultConfiguration;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfileStateFilterConfiguration" /> class.
        /// </summary>
        /// <param name="settingsProviderFactory">The settings provider factory.</param>
        /// <param name="serviceConfigurationStore"></param>
        public ProfileStateFilterConfiguration(ISettingsProviderFactory settingsProviderFactory, IServiceConfigurationStore serviceConfigurationStore) : base(settingsProviderFactory, serviceConfigurationStore)
        {
            this.defaultConfiguration = new Common.Configuration.Models.Configuration();
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets a value indicating whether [show disabled profiles].
        /// </summary>
        /// <value><c>true</c> if [show disabled profiles]; otherwise, <c>false</c>.</value>
        public Task<bool> ShowDisabledProfiles => GetSettingAsync(SettingKeys.ShowDisabledProfilesKey, defaultConfiguration.ShowDisabledProfiles);

        #endregion Properties
    }
}
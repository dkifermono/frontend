import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, ErrorHandler } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { API_BASE_URL } from './common';
import { RouteComponentsModule } from './route-components';
import { RoutingModule } from './routing.module';
import { AppCommonModule } from './common/common.module';
import { ComponentsModule } from './components/components.module';
import {SimpleNotificationsModule} from 'angular2-notifications';
import { AppConfig, APP_CONFIG } from './app.config';
import {ErrorsHandler} from './common/services/error-handler';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import {CustomHttpInterceptorService} from './common/services';


export function loadConfigService(configService: AppConfig): Function {
  return () => configService.load();
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppCommonModule,
    ComponentsModule,
    RouteComponentsModule,
    RoutingModule,
    SimpleNotificationsModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [
    AppConfig,
    { provide: APP_INITIALIZER,
      useFactory: loadConfigService,
      deps: [AppConfig], multi: true } ,
      {provide: API_BASE_URL,
        useFactory: () => {
          return APP_CONFIG.restApiBaseUrl.replace(/\/+$/, '');
        } },
        { provide: HTTP_INTERCEPTORS, useClass: CustomHttpInterceptorService, multi: true },
        {provide: ErrorHandler,
        useClass: ErrorsHandler}
 ],
  bootstrap: [AppComponent]
})



export class AppModule {
}

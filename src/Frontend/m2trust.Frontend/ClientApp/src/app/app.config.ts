
import {throwError as observableThrowError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';

// tslint:disable-next-line:class-name
export class settings {
  readonly production: boolean;
  readonly hmr: string;
  readonly restApiBaseUrl: string;
}

export let APP_CONFIG: settings;

@Injectable()
export class AppConfig {

  constructor(private http: HttpClient) { }

  public load() {
    return new Promise((resolve, reject) => {
      const random = (moment().format('MMMMMDoYYYYhhmmss'));
      const confName = 'appConfig.json?v=' + random;
      this.http.get('assets/config/' + confName).pipe(map(res => res as any), catchError((error: any): any => {
        reject(true);
        return observableThrowError('Server error');
      }), ).subscribe((envResponse: any) => {
        const t = new settings();
        APP_CONFIG = Object.assign(t, envResponse);
        resolve(true);
      });
    });
  }
}

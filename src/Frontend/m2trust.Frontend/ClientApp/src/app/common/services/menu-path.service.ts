import { Injectable } from '@angular/core';
import { ActivityConstants, AdministrationActivityConstants } from '../constants';

@Injectable()
export class MenuPathService {

  constructor(
      private activityConstants: ActivityConstants
  ) { }

  public getTranslationKeys(url: string): string[] {
    const translation: string[] = [];

    const activityConstants = this.activityConstants;
    const administrationConstants = AdministrationActivityConstants;
    const paths = this.getPaths(url);

    if (paths.includes(activityConstants.Administration.path)) {
      translation.push(activityConstants.Administration.translationKey);
      paths.shift();
      translation.push(administrationConstants.find(i => i.path === paths[0]).translationKey);
    }
    return translation;
  }

  private getPaths(url: string): string[] {
      const result: string[] = [];
      const splitResult = url.split('/').filter(Boolean);
      let i: number;
      result[0] = '/' + splitResult[0];
      for (i = 1; i < splitResult.length; i++) {
        result[i] = result[i - 1] + '/' + splitResult[i];
      }
    return result;
  }
}



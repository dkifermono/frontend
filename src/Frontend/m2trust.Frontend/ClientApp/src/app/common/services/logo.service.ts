import { Injectable } from '@angular/core';
import { ConfigurationService } from '../restclient';
import {of as observableOf, Observable } from 'rxjs';

@Injectable({
  // Singleton service
  providedIn: 'root'
})
export class LogoService {
  private _imagePath: string = null;

  constructor(
    private configurationClient: ConfigurationService
  ) { }

  async getImagePath(page: string = null): Promise<string> {
    if (page === 'home' && this._imagePath === null) {
      setTimeout(() => {}, 1000);
    }
    if (this._imagePath === null) {
      this._imagePath = await this.configurationClient.getLogoImage().toPromise();
    }
    return this._imagePath;
  }

}


import { Injectable } from '@angular/core';
import { Subject } from '../../../../node_modules/rxjs';
import { SignedRequestDto } from '..';

@Injectable()
export class ValidationService {
  private messageSource = new Subject<SignedRequestDto[]>();

  currentMessage = this.messageSource.asObservable();

  private isValidatingSource = new Subject<boolean>();

  isValidating = this.isValidatingSource.asObservable();

  private isSearchFormSubmittedSource = new Subject<boolean>();

  isSearchFormSubmitted = this.isSearchFormSubmittedSource.asObservable();



  changeMessage(message: SignedRequestDto[]) {
    this.messageSource.next(message);
  }

  changeValidatingState(value: boolean) {
    this.isValidatingSource.next(value);
  }

  changeSearchFormSubmittedState(value: boolean) {
    this.isSearchFormSubmittedSource.next(value);
  }

}









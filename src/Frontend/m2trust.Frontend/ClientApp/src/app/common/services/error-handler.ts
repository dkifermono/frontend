import {ErrorHandler, Injectable} from '@angular/core';
import { NotificationService } from './notification.service';
import { TranslateService } from '@ngx-translate/core';


@Injectable()
export class ErrorsHandler implements ErrorHandler {

  constructor(
    private notificationService: NotificationService,
    private translateService: TranslateService
  ) {}
  handleError(error) {
    console.error('It happens:', error);
      this.notificationService.error(this.translateService.instant('ActivityFailed'), error.message);
  }
}

/// <reference types="winrt-uwp" />
import { SigningResult } from '../models/signing-result.model';
import { DciDataDto, DataCollectionItemDto } from '../restclient';

//TODO: the following two declarations should be in @types definitions
declare namespace m2trust {
  namespace Frontend {
    namespace CertificateUtils {
      class X509UserStorageHelper {
        public static getCertificateThumbprintHexStringsByTemplateOid(certificateTemplateOid: string): Windows.Foundation.Collections.IVectorView<string>;
        public static hexStringToByteArray(hexString: string): number[];
      }
      class SignatureHelper {
        public static buildSignatureString(base64Certificate: string, base64SignatureDigest: string): string;
        public static getDataToSignForProfileTemplateRequest(signatureDciName: string, userId: string, profileTemplateId: string, originatorId: string, dataCollectionItems: Windows.Foundation.Collections.PropertySet): string;
        public static getDataToSignForProfileRequest(signatureDciName: string, profileId: string, originatorId: string, dataCollectionItems: Windows.Foundation.Collections.PropertySet): string;
      }
    }
  }
}

export class CreateSignatureService {
  signingResult: SigningResult;
  certificateList: Windows.Foundation.Collections.IIterable<Windows.Security.Cryptography.Certificates.Certificate>;
  certs: Windows.Security.Cryptography.Certificates.Certificate[];
  signerInfo: Windows.Security.Cryptography.Certificates.CmsSignerInfo;
  signerInfoList: Array<Windows.Security.Cryptography.Certificates.CmsSignerInfo>;
  constructor() {
    this.signingResult = new SigningResult();
  }
  async getSignatureFromTemplateEkuOid(ekuOid: string, signatureDciKey: string, dciDataDto: DciDataDto, dataCollection: DataCollectionItemDto[]) {
    const dts = this.getDataToSign(signatureDciKey, dciDataDto, dataCollection);
    return this.getSignatureResultFromTemplateEkuOid(ekuOid, dts);
  }
  async getSignature(templateOid: string, signatureDciKey: string, dciDataDto: DciDataDto, dataCollection: DataCollectionItemDto[]) {
    const dts = this.getDataToSign(signatureDciKey, dciDataDto, dataCollection);
    return this.getSignatureResult(templateOid, dts);
  }
  getDataToSign(signatureDciKey: string, dciDataDto: DciDataDto, dataCollection: DataCollectionItemDto[]): string {
    var dc = new Windows.Foundation.Collections.PropertySet();
    for (let dci of dataCollection) {
      dc[dci.name] = dci.value;
    }
    return m2trust.Frontend.CertificateUtils.SignatureHelper.getDataToSignForProfileTemplateRequest(signatureDciKey, dciDataDto.userId, dciDataDto.profileTemplateId, dciDataDto.originatorId, dc);
  }
  async getSignatureResultFromTemplateEkuOid(ekuOid: string, dataToSign: string) {
    const certificates = await this.getCertificatesForEkuOid(ekuOid);
    if (certificates.length < 1 || certificates.length > 1) {
      this.signingResult.returnCode = -certificates.length;
    } else {
      return this.getSignatureResultFromCertificate(certificates[0], dataToSign);
    }
    return this.signingResult;
  }
  async getSignatureResult(templateOid: string, dataToSign: string) {
    const certificates = await this.getCertificates(templateOid);
    if (certificates.length < 1 || certificates.length > 1) {
      this.signingResult.returnCode = -certificates.length;
    } else {
      return this.getSignatureResultFromCertificate(certificates[0], dataToSign);
    }
    return this.signingResult;
  }
  async getSignatureResultFromCertificate(certificate: Windows.Security.Cryptography.Certificates.Certificate, dataToSign: string) {
    const signature = await this.createSignature(certificate, dataToSign);
    this.signingResult.returnCode = 1;
    this.signingResult.signedData = signature;
    return this.signingResult;
  }

  async getCertificates(templateOid: string) {
    const thumbprintStrings = m2trust.Frontend.CertificateUtils.X509UserStorageHelper.getCertificateThumbprintHexStringsByTemplateOid(templateOid);
    const rVal : Windows.Security.Cryptography.Certificates.Certificate[] = [];
    for (const thumbprintString of thumbprintStrings) {
      let query = new Windows.Security.Cryptography.Certificates.CertificateQuery();
      (<any>query).thumbprint = m2trust.Frontend.CertificateUtils.X509UserStorageHelper.hexStringToByteArray(thumbprintString);
      const certs = await Windows.Security.Cryptography.Certificates.CertificateStores.findAllAsync(query);
      rVal.push(...certs.map((c) => c as Windows.Security.Cryptography.Certificates.Certificate));
    }
    return rVal;
  }
  async getCertificatesForEkuOid(ekuOid: string) {
    let query = new Windows.Security.Cryptography.Certificates.CertificateQuery();
    query.enhancedKeyUsages.push(ekuOid);
    const certs = await Windows.Security.Cryptography.Certificates.CertificateStores.findAllAsync(query);
    return certs.map((c) => c as Windows.Security.Cryptography.Certificates.Certificate);//copy to array
  }

  async createSignature(certificate: Windows.Security.Cryptography.Certificates.Certificate, dataToSign: string) {
    const signedPrivateKey = await this.signPrivateKey(certificate, dataToSign);
    const encodedCertificate = await this.encodeCertificateAsBase64(certificate);
    return m2trust.Frontend.CertificateUtils.SignatureHelper.buildSignatureString(encodedCertificate, signedPrivateKey);
  }

  async signPrivateKey(certificate: Windows.Security.Cryptography.Certificates.Certificate, dataToSign: string) {
    const sha = Windows.Security.Cryptography.Core.HashAlgorithmNames.sha512;
    const rsa = Windows.Security.Cryptography.Core.CryptographicPadding.rsaPkcs1V15;
    const privateKey = await Windows.Security.Cryptography.Core.PersistedKeyProvider.openKeyPairFromCertificateAsync(certificate, sha, rsa);
    const encodedBuffer = Windows.Security.Cryptography.CryptographicBuffer.convertStringToBinary(dataToSign, Windows.Security.Cryptography.BinaryStringEncoding.utf8);
    const sign = await Windows.Security.Cryptography.Core.CryptographicEngine.signAsync(privateKey, encodedBuffer);
    const signature = Windows.Security.Cryptography.CryptographicBuffer.encodeToBase64String(sign);
    return signature;
  }

  async encodeCertificateAsBase64(certificate: Windows.Security.Cryptography.Certificates.Certificate) {
    this.signerInfo = new Windows.Security.Cryptography.Certificates.CmsSignerInfo();
    const blob = certificate.getCertificateBlob();
    this.signerInfo.certificate = certificate;
    this.signerInfo.hashAlgorithmName = Windows.Security.Cryptography.Core.HashAlgorithmNames.md5;
    const signature = await (Windows.Security.Cryptography.Certificates.CmsAttachedSignature as any).generateSignatureAsync(blob, [this.signerInfo], null);
    return Windows.Security.Cryptography.CryptographicBuffer.encodeToBase64String(signature);
  }
}


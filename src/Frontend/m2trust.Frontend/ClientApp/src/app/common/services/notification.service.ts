import { Injectable } from '@angular/core';
import {NotificationsService as AngularNotificationService } from 'angular2-notifications';

@Injectable()
export class NotificationService {
  constructor(
    private notificationsService: AngularNotificationService,
  ) {}

  error(title: string, message: string) {
    this.notificationsService.error(title, message,
      {
        animate: 'fromRight'
    });
  }

  success(message: string) {
    this.notificationsService.success('Success', message,
  {
    timeOut: 5000,
    animate: 'fromRight'
  });
  }

  alert(title: string, message: string) {
    this.notificationsService.alert(title, message, {
      timeOut: 3000
    });
  }

  warn(title: string, message: string) {
    this.notificationsService.warn(title, message);
  }
}

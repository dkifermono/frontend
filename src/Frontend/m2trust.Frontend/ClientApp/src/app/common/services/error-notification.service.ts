import { Injectable } from '@angular/core';
import { NotificationService } from './notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class ErrorNotificationService {
  constructor(
    private notificationsService: NotificationService,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService
  ) {}

  notificationError(error) {
    if (error.status === 403) {
      this.notificationsService.error(this.translate.instant('ErrorMessage.AuthorizationFailed'), this.translate.instant('ErrorMessage.NoPermission'));
      this.router.navigate(['/home']);
    } else {
      this.notificationsService.error(this.translate.instant('ActivityFailed'), this.getErrorMessage(error));
    }
  }

  getErrorMessage(error): string {
    let result = this.translate.instant('ActivityFailed');
    if (error.status === 404) {
      result = this.translate.instant('ErrorMessage.ObjectNotFound');
    } else if (error.status === 400) {
      result = this.translate.instant('ErrorMessage.BadRequest');
    } else {
      if (error.errorId) {
        result = this.translate.instant('ErrorMessage.ReferenceNumber') + error.errorId;
      } else if (error.message) {
        result = error.message;
      }
    }
    return result;
  }
}

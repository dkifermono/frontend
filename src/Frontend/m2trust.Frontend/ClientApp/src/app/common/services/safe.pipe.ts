import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeStyle } from '../../../../node_modules/@angular/platform-browser';

@Pipe({
  name: 'safe'
})

export class SafePipe implements PipeTransform {
  constructor(protected sanitizer: DomSanitizer) { }

  public transform(value: any, type: string): SafeStyle {
    return this.sanitizer.bypassSecurityTrustStyle(value);
  }
}

import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'localizedDate',
  pure: false
})
export class LocalizedDatePipe implements PipeTransform {
  longOptions: any = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: '2-digit',
    minute: '2-digit'
  };
  smallOptions = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
  };

  constructor(private translateService: TranslateService) {
}

  transform(value: any, type: string): any {
    const date = new Date(value);
    let options = this.longOptions;
    if (type === 'small') {
      options = this.smallOptions;
    }
    return date.toLocaleString(this.translateService.currentLang, options);
  }

}

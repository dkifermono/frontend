import { Injectable } from '@angular/core';
import { FormControl, FormGroup, FormArray, ValidatorFn } from '@angular/forms';
import { BaseConfigService } from './base-config-form.service';
import { duplicateArrayItemValidator } from '../..';

@Injectable()
export class ConfigFormService extends BaseConfigService {

  constructor() {
    super();
  }

  public validateAllFormFields(formGroup: any) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsDirty();
      } else if (control instanceof FormGroup || control instanceof FormArray) {
        this.validateAllFormFields(control);
      }
    });
  }

  public createFormArrayControl(array: string[] = [], validators: ValidatorFn[] = []): FormArray {
    const formArray = new FormArray([], duplicateArrayItemValidator);
    if (array.length > 0) {
      array.forEach(item => {
        formArray.push(new FormControl(item, validators));
      });
    }
    return formArray;
  }
}

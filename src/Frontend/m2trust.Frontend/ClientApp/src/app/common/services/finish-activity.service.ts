import { AuditResult, ContinueModel } from '../models';

export class FinishActivityService {
  public auditResult: AuditResult;
  public continueModel: ContinueModel;
}

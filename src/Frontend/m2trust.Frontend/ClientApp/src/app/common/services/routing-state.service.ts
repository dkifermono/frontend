import { Injectable } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Activities } from '../constants';

@Injectable()
export class RoutingStateService {
  private history = [];
  private index = 0;
  private isBack = false;
  private nextUrl = '';

  constructor(
    private router: Router
  ) { }

  public loadRouting(): void {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({urlAfterRedirects}: NavigationEnd) => {
        if (this.isBack) {
          this.index = Math.max(this.index - 1, 0);
          this.isBack = false;
        } else {
          this.history = [...this.history.slice(0, this.index + 1), urlAfterRedirects];
          this.index = this.history.length - 1;
        }
      });
    this.router.events
      .pipe(filter(event => event instanceof NavigationStart))
      .subscribe(({url}: NavigationStart) => {
      this.nextUrl = url;
    });
  }

  public back(): void {
    this.isBack = true;
  }

  public getHistory(): string[] {
    return this.history;
  }

  public getPreviousUrl(): string {
    return this.history[this.index - 1] || '/home';
  }

  public getCurrentUrl(): string {
    return this.history[this.index] || '/home';
  }

  public getNextUrl(): string {
    return this.nextUrl;
  }

  public getRootUrl(): string {
    const currentUrl = this.getCurrentUrl();
    const root = Activities.find(a => currentUrl.includes(a.path));
    if (root) {
      return root.path;
    } else {
      return null;
    }
  }

  public isUrlRootSame(): boolean {
    const rootUrl = this.getRootUrl();
    if (rootUrl) {
      return this.getNextUrl().indexOf(rootUrl) === 0;
    } else {
      return false;
    }
  }
}

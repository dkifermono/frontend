import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SecretDto } from '../restclient';

@Injectable()
export class ManageRequestsDistributeSecretsService {
  private messageSource = new Subject<SecretDto[]>();
  currentMessage = this.messageSource.asObservable();

  changeMessage(message: SecretDto[]) {
    this.messageSource.next(message);
  }
}

import { Injectable } from '@angular/core';
import { BaseConfigService } from './base-config-form.service';
import { ProfileTemplateDto, ProfileTemplateItemDto, DCIDto, guidValidator, duplicateArrayItemValidator, ProfileTemplatesConfigurationDto } from '../..';
import { FormGroup, FormControl, ValidatorFn, Validators, FormArray } from '@angular/forms';

@Injectable()
export class ProfileTemplateConfigService extends BaseConfigService {
  profileTemplateValidators: ValidatorFn[] = [Validators.required, guidValidator];
  constructor() {
    super();
  }

  public createProfileTemplateGroup(item?: ProfileTemplateItemDto): FormGroup {
    if (!item) {
      item = new ProfileTemplateItemDto();
      item.reinstateDCIs = [ ];
    }
    return new FormGroup({
      profileTemplateId: new FormControl(item.profileTemplateId, this.profileTemplateValidators),
      reinstateDCIs: this.createReinstateDcisArray(item.reinstateDCIs)
    });
  }

  public createReinstateDcisArray(array: DCIDto[] = []): FormArray {
    const formArray = new FormArray([]);
    if (array.length > 0) {
      array.forEach(item => {
        formArray.push(this.createNameValueGroup(item));
      });
    }
    return formArray;
  }

  public createNameValueGroup(item: DCIDto = new DCIDto()): FormGroup {

    const nameValueGroup = new FormGroup({
      name: new FormControl(item.name, Validators.required),
      value: new FormControl(item.value, Validators.required)
    });
    return nameValueGroup;
  }

  public createProfileTemplateIdForm(item: ProfileTemplateDto, validators: ValidatorFn[]): FormGroup {
    const form = new FormGroup({
      name: new FormControl(item.name || item.profileTemplateId),
      id: new FormControl(item.profileTemplateId, validators)
    });
    return form;
  }

  public createProfileTemplatesArray(array: ProfileTemplateItemDto[] = []): FormArray {
    const formArray = new FormArray([]);
    if (array.length > 0) {
      array.forEach(item => {
        formArray.push(this.createProfileTemplateGroup(item));
      });
    }
    return formArray;
  }

  public createProfileTemplateFormArray(items: ProfileTemplateDto[], validators: ValidatorFn[] = []): FormArray {
    const array = new FormArray([], duplicateArrayItemValidator);
    if (!items) {
      items = [];
    }
    items.forEach(item => {
      array.push(this.createProfileTemplateIdForm(item, validators));
    });
    return array;
  }
}

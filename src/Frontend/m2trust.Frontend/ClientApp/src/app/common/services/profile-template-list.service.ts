import { Injectable } from '@angular/core';
import { ProfileTemplateDto } from '..';

@Injectable({
  // Singleton service
  providedIn: 'root'
})
export class ProfileTemplateListService {
  private suggestedProfileList: { blacklisted: ProfileTemplateDto[], excluded: ProfileTemplateDto[] } = { blacklisted: [], excluded: []};
  private suggested: { blacklisted: string[], excluded: string[] } = { blacklisted: [], excluded: []};

  readonly blacklistedType = 'blacklisted';
  readonly excludedType = 'excluded';

  constructor() {  }

  profileTemplateLookup(list: ProfileTemplateDto[], type: string = null) {
    if (type) {
      this.suggestedProfileList[type] = list;
    } else {
      this.suggestedProfileList.blacklisted = list;
      this.suggestedProfileList.excluded = list;
    }
  }

  getProfileTemplateLookup(type: string): ProfileTemplateDto[] {
    return this.suggestedProfileList[type];
  }

  getSuggestionList(event: string, type: string): string[] {
    if (this.suggestedProfileList[type] && event) {
      this.suggested[type] = this.suggestedProfileList[type].reduce((array, value) => {
          const match = value.name.toUpperCase().indexOf(event.toUpperCase()) > -1;
          if (match) {
              array.push(value.name);
          }
          return array;
      }, []);
    } else {
      this.suggested[type] = [];
    }
    return this.suggested[type];
  }




  // message part

  removeFromSuggestedList(message: ProfileTemplateMessage) {
    if (this.suggestedProfileList[message.type].findIndex(p => p.profileTemplateId === message.profile.profileTemplateId) >= 0) {
      this.suggestedProfileList[message.type] = this.suggestedProfileList[message.type].filter(p => p.profileTemplateId !== message.profile.profileTemplateId);
    }
  }

  addToSuggestedList(message: ProfileTemplateMessage) {
    if (this.suggestedProfileList[message.type].findIndex(p => p.profileTemplateId === message.profile.profileTemplateId) < 0) {
      this.suggestedProfileList[message.type].push(message.profile);
    }
  }

}

export class ProfileTemplateMessage {
  profile: ProfileTemplateDto;
  type: string;

  constructor(profile: ProfileTemplateDto, type: string) {
    this.type = type;
    this.profile = new ProfileTemplateDto({name: profile.name, profileTemplateId: profile.profileTemplateId});
  }
}

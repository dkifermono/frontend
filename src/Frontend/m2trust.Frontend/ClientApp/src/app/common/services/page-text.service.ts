import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class PageTextService {
  private messageSource = new Subject<string>();
  currentMessage = this.messageSource.asObservable();

  changeMessage(message: string) {
    this.messageSource.next(message);
  }
}

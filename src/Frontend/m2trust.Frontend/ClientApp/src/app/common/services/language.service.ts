import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class LanguageService {
  private messageSource = new Subject<string>();
  public initLanguage = localStorage.getItem('selectedLanguage');
  currentMessage = this.messageSource.asObservable();

  changeMessage(message: string) {
    this.initLanguage = message;
    this.messageSource.next(message);
  }
}

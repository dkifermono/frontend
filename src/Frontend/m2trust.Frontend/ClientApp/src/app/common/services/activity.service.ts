
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ActivityDto } from '../restclient';
import { Activity, ActivityClient, ActivityConstants, WizardData } from '..';

import { Observable } from 'rxjs';
import { NotificationService } from './notification.service';

@Injectable()
export class ActivityService {
  constructor(
    private activityClient: ActivityClient,
    private activityConstants: ActivityConstants,
    private wizardData: WizardData,
    private notificationService: NotificationService
  ) {}

  getActivitiesAsync(): Observable<Activity[]> {
      return this.activityClient.getActivities().pipe(
      map(activities => {
        this.wizardData.setActivities(Object.values(activities));
        return this.sortAndFiltertActivityDto(Object.values(activities));
      }));
  }

sortAndFiltertActivityDto(items: ActivityDto[]) {
  return items.map(this.createActivity, this).filter(activity => activity !== undefined && activity !== null).
        sort((previous, next): number => {
          if (previous.sortOrder < next.sortOrder) {
            return -1;
          }
          if (previous.sortOrder > next.sortOrder) {
            return 1;
          }
          return 0;
        });
}

  getActivity(actionName: string): Promise<any> {
      return this.activityClient.getActivities().pipe(
      map(activities => {
        this.wizardData.setActivities(Object.values(activities));
        return Object.values(activities).map(this.createActivity, this).find(activity => activity.name === actionName);
      })).toPromise().catch(er => {
        this.notificationService.error('Error', er.message);
      }
      );
  }

  private createActivity(activityDto: ActivityDto): Activity {
    const activityInfo = this.activityConstants[activityDto.activityName];
    if (activityInfo === null || activityInfo === undefined) {
      return;
    }
    return {
      name: activityDto.activityName,
      path: activityInfo.path,
      translationKey: activityInfo.translationKey,
      sortOrder : activityInfo.sortOrder
    };
  }
}

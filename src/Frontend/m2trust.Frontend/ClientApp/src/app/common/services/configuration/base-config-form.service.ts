import { Injectable } from '@angular/core';

@Injectable()
export class BaseConfigService {

  constructor() { }

  public getStringValue(value: string): any {
    if (value) {
      value = value.trim();
    }
    return value ? value : null;
  }
}


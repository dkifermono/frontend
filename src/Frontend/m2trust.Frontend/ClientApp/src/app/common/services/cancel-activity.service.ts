import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class CancelActivityService {
  private messageSourceTryNavigation = new Subject<boolean>();
  isNavigationStart = this.messageSourceTryNavigation.asObservable();

  private messageSourceConfirm = new Subject<boolean>();
  isConfirmed = this.messageSourceConfirm.asObservable();

  private messageSourceNavigate = new Subject<string>();
  canNavigate = this.messageSourceNavigate.asObservable();

  private messageSourceCancelButton = new Subject<boolean>();
  isCancelButton = this.messageSourceCancelButton.asObservable();

  tryNavigation(message: boolean) {
    this.messageSourceTryNavigation.next(message);
  }

  changeConfirmation(message: boolean) {
    this.messageSourceConfirm.next(message);
  }

  changeNavigation(message: string) {
    this.messageSourceNavigate.next(message);
  }

  cancelButtonPressed(message: boolean) {
    this.messageSourceCancelButton.next(message);
  }
}


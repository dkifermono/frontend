import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginatorModule } from 'primeng/paginator';
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { CalendarModule } from 'primeng/calendar';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { MultiSelectModule } from 'primeng/multiselect';
import { PickListModule } from 'primeng/picklist';
import { ColorPickerModule } from 'primeng/colorpicker';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {
  ActivityClient,
  IconsModule,
  ProfileClient,
  RequestClient,
  LoggingClient,
  UserClient,
  DataCollectionItemClient,
  FinishRequestClient,
  SmartcardClient
} from '.';
import { HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  ManageRequestsDistributeSecretsService,
  ActivityService,
  RoutingStateService,
  NotificationService,
  ValidationService,
  ErrorNotificationService,
  SafePipe,
  PageTextService,
  CreateSignatureService,
  LanguageService,
  LocalizedDatePipe,
  FinishActivityService,
  CustomHttpInterceptorService,
  CancelActivityService,
  ProfileTemplateListService,
  LogoService,
  MenuPathService,
  ConfigFormService,
  ProfileTemplateConfigService
} from './services';
import {
  AuthorizationGuard,
  EnrollGuard,
  DisableSmartcardGuard,
  UnblockSmartcardGuard,
  RecoverCertificateGuard,
  ReinstateSmartcardGuard,
  RetireTemporarySmartcardGuard,
  RevokeCertificateGuard,
  SuspendSmartcardGuard,
  RewriteSmartcardGuard,
  ManageRequestsGuard,
  RenewCertificateGuard,
  AdministrationGuard
} from './guards';
import { WizardData } from './providers';
import { ActivityConstants } from './constants';
import { StickyDirective, SidebarDirective } from './directives';
import { ConfigurationService } from './restclient';
import { CanDeactivateGuard } from './guards-can-deactivate';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    StickyDirective,
    SidebarDirective,
    SafePipe,
    LocalizedDatePipe
  ],
  imports: [
    CommonModule,
    PaginatorModule,
    DialogModule,
    ConfirmDialogModule,
    CalendarModule,
    IconsModule,
    AutoCompleteModule,
    MultiSelectModule,
    PickListModule,
    ColorPickerModule,
    AngularFontAwesomeModule,
    Ng4LoadingSpinnerModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      }
    })
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PaginatorModule,
    DialogModule,
    ConfirmDialogModule,
    CalendarModule,
    IconsModule,
    Ng4LoadingSpinnerModule,
    TranslateModule,
    StickyDirective,
    SidebarDirective,
    SafePipe,
    LocalizedDatePipe,
    AutoCompleteModule,
    MultiSelectModule,
    PickListModule,
    ColorPickerModule,
    AngularFontAwesomeModule
  ],
  providers: [
    ManageRequestsDistributeSecretsService,
    CancelActivityService,
    ValidationService,
    ActivityConstants,
    ActivityService,
    NotificationService,
    LoggingClient,
    ActivityClient,
    RequestClient,
    UserClient,
    ConfirmationService,
    ProfileClient,
    DataCollectionItemClient,
    FinishRequestClient,
    SmartcardClient,
    RoutingStateService,
    ErrorNotificationService,
    WizardData,
    AuthorizationGuard,
    EnrollGuard,
    DisableSmartcardGuard,
    UnblockSmartcardGuard,
    RecoverCertificateGuard,
    ReinstateSmartcardGuard,
    RetireTemporarySmartcardGuard,
    RevokeCertificateGuard,
    SuspendSmartcardGuard,
    RewriteSmartcardGuard,
    ManageRequestsGuard,
    RenewCertificateGuard,
    AdministrationGuard,
    ConfigurationService,
    PageTextService,
    CreateSignatureService,
    LanguageService,
    FinishActivityService,
    CustomHttpInterceptorService,
    CanDeactivateGuard,
    LogoService,
    ProfileTemplateListService,
    MenuPathService,
    ConfigFormService,
    ProfileTemplateConfigService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
}

)

export class AppCommonModule { }

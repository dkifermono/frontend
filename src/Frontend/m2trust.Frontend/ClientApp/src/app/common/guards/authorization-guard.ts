import { Injectable } from '@angular/core';
import {ActivityService} from '../services';
import { CanActivate } from '@angular/router';
import {NotificationService} from '../services';

@Injectable()
export class AuthorizationGuard implements CanActivate {
  constructor(
    private activityService: ActivityService,
    private notificationService: NotificationService
  ) { }

 async canActivate() {
    const activity = await this.activityService.getActivity(this.getName());
    if (activity != null) {
      return true;
    }
    this.notificationService.error('Authorization failed', 'You don`t have permission for this action');
    return false;
  }

  getName(): string {
    return '';
  }
}

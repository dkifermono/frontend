import { Injectable } from '@angular/core';
import {ActivityService} from '../services';
import { AuthorizationGuard } from './authorization-guard';
import {NotificationService} from '../services';

@Injectable()
export class ReinstateSmartcardGuard extends AuthorizationGuard {
  constructor(
    activityService: ActivityService,
    notificationService: NotificationService
  ) {
  super(activityService, notificationService);
  }

  getName(): string {
    return 'ReinstateSmartcard';
  }
}

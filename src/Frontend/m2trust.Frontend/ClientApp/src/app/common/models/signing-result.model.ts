export class SigningResult {
  signedData: string;
  returnCode: number;
}

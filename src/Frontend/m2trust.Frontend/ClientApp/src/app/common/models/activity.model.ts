export class Activity {
  name: string;
  path: string;
  translationKey: string;
  sortOrder: number;
}

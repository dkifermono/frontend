export class UISettingsForm {
  menuBackgroundColor: string;
  menuFontColor: string;
  menuSelectedItemBackgroundColor: string;
  menuSelectedItemFontColor: string;
  pageTitleBackgroundColor: string;
  pageTitleFontColor: string;
  tableRowSelectedBackgroundColor: string;
  tableRowSelectedFontColor: string;
  pageBackgroundColor: string;
  tableRowFontColor: string;

  constructor(
    menuBacgroundColor: string,
    menuFontColor: string,
    menuSelectedItemBackgroundColor: string,
    menuSelectedItemFontColor: string,
    pageTitleBackgroundColor: string,
    pageTitleFontColor: string,
    tableRowSelectedBackgroundColor: string,
    tableRowSelectedFontColor: string,
    pageBackgroundColor: string,
    tableRowFontColor: string
  ) {
    this.menuBackgroundColor = menuBacgroundColor;
    this.menuFontColor = menuFontColor;
    this.menuSelectedItemBackgroundColor = menuSelectedItemBackgroundColor;
    this.menuSelectedItemFontColor = menuSelectedItemFontColor;
    this.pageTitleBackgroundColor = pageTitleBackgroundColor;
    this.pageTitleFontColor = pageTitleFontColor;
    this.tableRowSelectedBackgroundColor = tableRowSelectedBackgroundColor;
    this.tableRowSelectedFontColor = tableRowSelectedFontColor;
    this.pageBackgroundColor = pageBackgroundColor;
    this.tableRowFontColor = tableRowFontColor;
  }
}

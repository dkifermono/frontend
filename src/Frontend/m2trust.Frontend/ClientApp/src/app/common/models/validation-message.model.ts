export class ValidationMessage {
  key: string;
  params: {};

  constructor(key: string, params?: {}) {
    this.key = key;

    if (params) {
      this.params = params;
    }
  }
}

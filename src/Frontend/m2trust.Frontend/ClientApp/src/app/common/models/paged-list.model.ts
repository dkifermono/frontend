export interface PagedList<T> {
  items: T[];
  page: number;
  pageSize: number;
  totalCount: number;
}

export class PageEvent {
  first: number;
  page: number;
  pageCount: number;
  rows: number;
}

export class Page {

  rows: number;
  totalRecords: number;
  pageNumber: number;
  pageSize: number;

  getFirstItemIndex(): number {
    return this.pageNumber === 1 ? 0 : (this.pageNumber - 1) * this.rows;
  }
}

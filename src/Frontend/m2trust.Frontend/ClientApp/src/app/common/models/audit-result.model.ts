export class AuditResult {
  otp?: string;
  description: string;
  url: string;
  isSuccessful: boolean;

  constructor(description: string, url: string, isSuccessful: boolean, otp?: string) {
    this.description = description;
    this.url = url;
    this.isSuccessful = isSuccessful;
    this.otp = otp;
  }
}

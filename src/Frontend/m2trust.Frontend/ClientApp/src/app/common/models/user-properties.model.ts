export class UserProperty {
  name: string;
  id: string;

  constructor(name: string, id: string) {
    this.name = name;
    this.id = id;
  }
}

export class UserPropertiesPicklist {
  options: UserProperty[];
  target: UserProperty[];

  constructor(options: UserProperty[], target: UserProperty[]) {
    this.options = options;
    this.target = target;
  }
}

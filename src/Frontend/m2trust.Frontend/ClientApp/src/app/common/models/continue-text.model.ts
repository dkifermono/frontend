export class ContinueModel {
  description: string;
  url: string;
  title: string;
  userId: string;
  isSuccessful: boolean;
  otp: string;

  constructor(title: string, description: string, url: string, userId: string, isSuccessful: boolean, otp: string) {
    this.title = title;
    this.description = description;
    this.url = url;
    this.userId = userId;
    this.isSuccessful = isSuccessful;
    this.otp = otp;
  }
}

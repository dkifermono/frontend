import { ValidationDto, SignedRequestDto, RequestForm, RequestState } from '../restclient';
import { Moment } from 'moment';

export class RequestValidation {
  request: SignedRequestDto;
    validation: ValidationDto;
    requestId: string;
    constructor(request: SignedRequestDto, validation: ValidationDto) {
      this.request = request;
      this.validation = validation;
      this.requestId = request.requestId;
    }
}

export class RequestSearchForm {
  requestForm: RequestForm;
  requestState: RequestState;
  originatorUserName: string;
  targetUserName: string;
  submittedFrom: Moment;
  submittedTo: Moment;
}

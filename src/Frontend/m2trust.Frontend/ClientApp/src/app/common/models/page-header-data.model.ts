export class PageHeaderData {
  userName: string;
  profileTemplateName: string;
  profileName: string;
  constructor(userName: string, profileTemplateName: string = null, profileName: string = null) {
    this.userName = userName;
    this.profileTemplateName = profileTemplateName;
    this.profileName = profileName;
  }
}

import { Injectable } from '@angular/core';
import { DataCollectionItemDto, ActivityDto, DataCollectionDto } from '../restclient';
import { PageHeaderData } from '../models/page-header-data.model';

@Injectable()
export class WizardData {
    private dataCollectionKey = 'DataCollection';
    private errorKey = 'FinishRequestError';
    private ProfileHeaderKey = 'ProfileHeader';
    private activitiesKey = 'Activities';
    private profileTemplateIdKey = 'ProfileTemplateId';
    private userListDataKey = 'UserListData';
    private auditCommentKey = 'AuditComment';
    public constructor() { }

    setAuditComment(comment: string) {
      sessionStorage.setItem(this.auditCommentKey, comment);
    }

    getAuditComment(): string {
      return sessionStorage.getItem(this.auditCommentKey);
    }

    clearAuditComment() {
      sessionStorage.removeItem(this.auditCommentKey);
    }

    setDataCollection(item: DataCollectionDto) {
      sessionStorage.setItem(this.dataCollectionKey, JSON.stringify(item));
    }

    getDataCollection(): DataCollectionDto {
      const retrievedData = sessionStorage.getItem(this.dataCollectionKey);
      const cachedData = JSON.parse(retrievedData) as DataCollectionDto;
      return cachedData;
    }

    clearDataCollection() {
      sessionStorage.removeItem(this.dataCollectionKey);
    }

    setProfileTemplateId(profileTemplateId: string) {
      sessionStorage.setItem(this.profileTemplateIdKey, profileTemplateId);
    }

    getProfileTemplateId(): string {
      return sessionStorage.getItem(this.profileTemplateIdKey);
    }

    clearProfileTemplateId() {
      sessionStorage.removeItem(this.profileTemplateIdKey);
    }

    setUserListData(data: UserListData) {
      sessionStorage.setItem(this.userListDataKey, JSON.stringify(data));
    }

    getUserListData(): UserListData {
      const retrievedData = sessionStorage.getItem(this.userListDataKey);
      const cachedData = JSON.parse(retrievedData) as UserListData;
      return cachedData;
    }

    clearUserListData() {
      sessionStorage.removeItem(this.userListDataKey);
    }

    setPageHeaderData(data: PageHeaderData): void {
      sessionStorage.setItem(this.ProfileHeaderKey, JSON.stringify(data));
    }

    getPageHeaderData(): PageHeaderData {
      const retrievedData = sessionStorage.getItem(this.ProfileHeaderKey);
      const cachedData = JSON.parse(retrievedData) as PageHeaderData;
      return cachedData;
    }

    setErrorData(error: any) {
      sessionStorage.setItem(this.errorKey, JSON.stringify(error));
    }

    getErrorData() {
      const retrievedData = sessionStorage.getItem(this.errorKey);
      return retrievedData as string;
    }

    setActivities(activities: ActivityDto[]) {
      sessionStorage.setItem(this.activitiesKey, JSON.stringify(activities));
    }

    getActivites() {
      const retrievedData = sessionStorage.getItem(this.activitiesKey);
      const cachedData = JSON.parse(retrievedData) as ActivityDto[];
      return cachedData;
    }

    getActivity(activityName: string) {
      const activities = this.getActivites();
      return activities.find(activity => activity.activityName.toLowerCase() === activityName.toLowerCase());
    }

    clear() {
      sessionStorage.removeItem(this.dataCollectionKey);
      sessionStorage.removeItem(this.errorKey);
      sessionStorage.removeItem(this.ProfileHeaderKey);
      sessionStorage.removeItem(this.activitiesKey);
      sessionStorage.removeItem(this.profileTemplateIdKey);
      sessionStorage.removeItem(this.userListDataKey);
    }

    clearWizard() {
      sessionStorage.removeItem(this.dataCollectionKey);
      sessionStorage.removeItem(this.profileTemplateIdKey);
      sessionStorage.removeItem(this.userListDataKey);
      sessionStorage.removeItem(this.auditCommentKey);
    }
}

export class UserListData {
  searchFor: string;
  domain: string;
  searchIn: string;
  userId: string;
  page: number;

  constructor(searchFor: string = null, domain: string = null, searchIn: string = null , page: number = 1, userId: string = null) {
    this.searchFor = searchFor;
    this.domain = domain;
    this.searchIn = searchIn;
    this.page = page;
    this.userId = userId;
  }
}

import { AbstractControl, ValidationErrors } from '@angular/forms';

export const requiredValidator = (c: AbstractControl): ValidationErrors => {
  return testIfEmpty(c.value) ? null : {
    required: true
  };
};

function testIfEmpty (value: any): any {
  if (typeof value === 'boolean') {
    if (value === null || value === undefined) {
      return null;
    } else {
      return true;
    }
  } else {
    if (!value || (typeof value === 'string' && value.trim() === '') || (value instanceof Array && value.length < 1)) {
      return null;
    } else {
      return true;
    }
  }
}

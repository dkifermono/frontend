import { AbstractControl, ValidatorFn } from '@angular/forms';
import * as moment from 'moment';


export function dateValidator(dateFormat: string): ValidatorFn {
  return (c: AbstractControl): any => {
    if (c.value !== null && !moment(c.value, dateFormat, true).isValid()) {
      return {
        dateFormat: true
      };
    } else {
      return null;
    }
  };
}

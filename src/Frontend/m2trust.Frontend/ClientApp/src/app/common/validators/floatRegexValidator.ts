import { AbstractControl, ValidationErrors } from '@angular/forms';

const floatRegex = /^\s*$|^([-+]?\d*\.?\d+)(?:[eE]([-+]?\d+))?$/;


export const floatRegexValidator = (c: AbstractControl): ValidationErrors => {
  return testRegex(c.value) ? null : {
    floatRegex: true
  };
};

function testRegex (n: string): any {
  if (n != null && !floatRegex.test(n)) {
    return null;
  } else {
    return true;
  }
}

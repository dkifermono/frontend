import { AbstractControl, ValidationErrors } from '@angular/forms';

const intRegex = /^\s*$|^[0-9]*$/;


export const intRegexValidator = (c: AbstractControl): ValidationErrors => {
  return testRegex(c.value) ? null : {
    intRegex: true
  };
};

function testRegex (n: string): any {
  if (n != null && !intRegex.test(n)) {
    return null;
  } else {
    return true;
  }
}

import { AbstractControl, ValidationErrors } from '@angular/forms';

const oidRegex =  /^\s*$|^(\d+\.)+(\d+)$/;

export const simpleOidValidator = (c: AbstractControl): ValidationErrors => {
  return testOid(c.value) ? null : {
    oidRegex: true
  };
};

function testOid (oid: string): any {
  if (oid != null && oid !== '' && !oidRegex.test(oid)) {
    return null;
  } else {
    return true;
  }
}

import { AbstractControl, ValidationErrors } from '@angular/forms';

export const numberValidator = (c: AbstractControl): ValidationErrors => {
  return ((parseFloat(c.value) === parseInt(c.value, 10)) && !isNaN(c.value) || c.value === null) ? null : {
    numberValidator: {
      valid: false
    }
  };
};

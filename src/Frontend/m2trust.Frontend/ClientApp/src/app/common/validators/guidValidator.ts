import { AbstractControl, ValidationErrors } from '@angular/forms';

const guidRegex =  /^\s*$|\b[a-fA-F0-9]{8}(?:-[a-fA-F0-9]{4}){3}-[a-fA-F0-9]{12}\b/;

export const guidValidator = (c: AbstractControl): ValidationErrors => {
  return testGuid(c.value) ? null : {
    guidRegex: true
  };
};

function testGuid (guid: string): any {
  if ((guid != null && !guidRegex.test(guid)) || guid === '00000000-0000-0000-0000-000000000000') {
    return null;
  } else {
    return true;
  }
}

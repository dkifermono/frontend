import { AbstractControl, ValidationErrors, FormArray } from '@angular/forms';

export const duplicateArrayItemValidator = (c: AbstractControl): ValidationErrors => {
  return testItems(<FormArray> c);
};

function testItems (array: FormArray): any {
  const length = array.length;
  if (length <= 0) {
    return null;
  }
  const arrayValues = array.getRawValue();
  const count = values =>
  values.reduce((a, b) => ({ ...a,
    [b]: (a[b] || 0) + 1
  }), {});
  const duplicates = dict =>
  Object.keys(dict).filter((a) => dict[a] > 1);

  const duplicateValues = duplicates(count(arrayValues));

  for (let i = 0; i < length; i++) {
    if (duplicateValues.findIndex(item => item === array.controls[i].value) >= 0) {
      setDuplicateError(true, array.controls[i]);
    } else {
      setDuplicateError(false, array.controls[i]);
    }
    array.controls[i].markAsDirty({onlySelf: true});
  }
  return null;
}

function setDuplicateError(isDuplicate: boolean, control: AbstractControl) {
  let controlErrors = (control.errors && Object.entries(control.errors).length !== 0) ? control.errors : null;
  if (isDuplicate) {
    controlErrors = controlErrors ? controlErrors : {};
    Object.assign(controlErrors, {duplicate: true});
    control.setErrors(controlErrors);
  } else {
    if (controlErrors) {
      delete control.errors.duplicate;
      if (Object.entries(control.errors).length === 0) {
        control.setErrors(null);
      }
    }
  }
}

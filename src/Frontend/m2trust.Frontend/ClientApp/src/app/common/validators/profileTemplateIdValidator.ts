import { AbstractControl, ValidationErrors } from '@angular/forms';

const guidRegex =  /^\s*$|\b[a-fA-F0-9]{8}(?:-[a-fA-F0-9]{4}){3}-[a-fA-F0-9]{12}\b/;

export const profileTemplateIdValidator = (c: AbstractControl): ValidationErrors => {
  return testProfileTemplate(c.value) ? null : {
    profileTemplateIdRegex: true
  };
};

function testProfileTemplate (guid: string): any {
  if ((guid != null && !guidRegex.test(guid)) || guid === '00000000-0000-0000-0000-000000000000') {
    return null;
  } else {
    return true;
  }
}

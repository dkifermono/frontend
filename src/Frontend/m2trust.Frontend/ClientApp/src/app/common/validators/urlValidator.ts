import { AbstractControl, ValidationErrors } from '@angular/forms';

const urlRegex =  /^\s*$|^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\/\\]*)?$/;

export const urlValidator = (c: AbstractControl): ValidationErrors => {
  return testUrl(c.value) ? null : {
    urlRegex: true
  };
};

function testUrl (url: string): any {
  if (url != null && !urlRegex.test(url)) {
    return null;
  } else {
    return true;
  }
}

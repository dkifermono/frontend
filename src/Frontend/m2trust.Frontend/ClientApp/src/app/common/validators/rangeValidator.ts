import { AbstractControl, ValidatorFn } from '@angular/forms';


export function rangeValidator(min: number, max: number = Number.MAX_VALUE): ValidatorFn {
    return (control: AbstractControl): any => {
      if (max === Number.MAX_VALUE) {
        if (control.value && control.value < min) {
          return {
              rangeMin: true
            };
        } else {
            return true;
        }
      } else {
        if (control.value) {
            if (control.value < min || control.value > max) {
              return {
                  range: true
                };
            } else {
                return true;
            }
          }
        }
    };
}

import { AbstractControl, ValidationErrors } from '@angular/forms';

const oidRegex =  /^\s*$|^([0-9]+\.)*[0-9]+$/;

export const oidValidator = (c: AbstractControl): ValidationErrors => {
  return testOid(c.value) ? null : {
    oidRegex: true
  };
};

function testOid (oid: string): any {
  if (oid != null && !oidRegex.test(oid)) {
    return null;
  } else {
    return true;
  }
}

import { Directive, ElementRef, HostListener, Input, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appClickOutside]'
})
export class SidebarDirective {

  constructor(private _elementRef: ElementRef) { }

  @Output('clickOutside') clickOutside: EventEmitter<any> = new EventEmitter();

  @HostListener('document:click', ['$event.target']) onMouseEnter(targetElement) {
    const clickedInside = this._elementRef.nativeElement.contains(targetElement) ||
    targetElement.className === 'nav--primary__link ng-star-inserted active' || targetElement.className === 'nav--primary__link ng-star-inserted' ||
    targetElement.className === 'nav--primary__link cursor--pointer' || targetElement.className === 'nav--primary__link cursor--pointer active' ||
    targetElement.className === 'nav--secondary__link cursor--pointer' || targetElement.className === 'nav--secondary__link cursor--pointer is-active' ||
    targetElement.className === 'nav__title' ||
    targetElement.className === 'layout--aside' ||
    targetElement.className === 'sidebar__footer';
    if (!clickedInside) {
      this.clickOutside.emit(null);
    }
  }


}

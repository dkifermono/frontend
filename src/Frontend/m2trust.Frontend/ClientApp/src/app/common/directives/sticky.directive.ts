import { Directive, ElementRef, Renderer2, OnInit, OnDestroy } from '@angular/core';

@Directive({ selector: '[appSticky]' })
export class StickyDirective implements OnInit, OnDestroy {
  refNode: any;

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) {  }

  ngOnInit() {
    this.refNode = document.createElement('div');
    this.el.nativeElement.parentNode.insertBefore(this.refNode, this.el.nativeElement);
    window.addEventListener('scroll', this.scroll, true);
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = (): void => {
    const isVisible = this.isElementVisible(this.refNode);
    if (!isVisible) {
      this.renderer.addClass(this.el.nativeElement, 'is-active');
    } else {
       this.renderer.removeClass(this.el.nativeElement, 'is-active');
    }
  }

  isElementVisible(el) {
    const rect = el.getBoundingClientRect();
    const result = rect.bottom > 0 &&
    rect.right > 0 &&
    rect.left < (window.innerWidth || document.documentElement.clientWidth) &&
    rect.top < (window.innerHeight || document.documentElement.clientHeight);
    return result;
  }


}

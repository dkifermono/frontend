
import {of as observableOf, throwError as observableThrowError,  Observable } from 'rxjs';

import {catchError, mergeMap} from 'rxjs/operators';
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { API_BASE_URL, ExceptionDto, SwaggerException, ConfigurationClient } from './restClient';


export class PostResultDto {
  colorsResult: boolean;
  imageResult: boolean;

  constructor(data?: PostResultDto) {
    if (data) {
        for (const property in data) {
            if (data.hasOwnProperty(property)) {
              (<any>this)[property] = (<any>data)[property];
            }
        }
    }
  }

  init(data?: any) {
    if (data) {
        this.colorsResult = data['colorsResult'];
        this.imageResult = data['imageResult'];
    }
  }

  // tslint:disable-next-line:member-ordering
  static fromJS(data: any): PostResultDto {
      data = typeof data === 'object' ? data : {};
      const result = new PostResultDto();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data['colorsResult'] = this.colorsResult;
      data['imageResult'] = this.imageResult;
      return data;
  }
}

@Injectable()
export class ConfigurationService extends ConfigurationClient {
  private http1: HttpClient;
  private baseUrl1: string;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
      super(http, baseUrl);
      this.http1 = http;
      this.baseUrl1 = baseUrl ? baseUrl : '';
  }

  postUISettingsAsync(form: FormData): Observable<PostResultDto> {
    let url_ = this.baseUrl1 + '/api/Configuration/PostUISettings?activityType=Administration';
        url_ = url_.replace(/[?&]$/, '');

    const content_ = form;
    const headers = new HttpHeaders();

    const options_: any = {
      body: content_,
      observe: 'response',
      responseType: 'blob',
      headers: headers
    };
    return this.http1.request('post', url_, options_).pipe(mergeMap((response_: any) => {
      return this.processUISettingsAsync(response_);
    }), catchError((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
            return this.processUISettingsAsync(<any>response_);
        } catch (e) {
            return <Observable<PostResultDto>><any>observableThrowError(e);
        }
    } else {
      return <Observable<PostResultDto>><any>observableThrowError(response_);
    }
    }), );
  }

  protected processUISettingsAsync(response: HttpResponseBase): Observable<PostResultDto> {
    const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        const _headers: any = {}; if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(mergeMap(_responseText => {
            let result200: any = null;
            const resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = resultData200 ? PostResultDto.fromJS(resultData200) : new PostResultDto();
            return observableOf(result200);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(mergeMap(_responseText => {
            let result500: any = null;
            const resultData500 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result500 = resultData500 ? ExceptionDto.fromJS(resultData500) : new ExceptionDto();
            return throwException('A server error occurred.', status, _responseText, _headers, result500);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(mergeMap(_responseText => {
            return throwException('An unexpected server error occurred.', status, _responseText, _headers);
            }));
        }
        return observableOf<PostResultDto>(<any>null);
  }
}
function blobToText(blob: any): Observable<string> {
  return new Observable<string>((observer: any) => {
      if (!blob) {
          observer.next('');
          observer.complete();
      } else {
          const reader = new FileReader();
          reader.onload = function() {
              observer.next(this.result);
              observer.complete();
          };
          reader.readAsText(blob);
      }
  });
}

function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
  if (result !== null && result !== undefined) {
      return observableThrowError(result);
  } else {
      return observableThrowError(new SwaggerException(message, status, response, headers, null));
  }
}



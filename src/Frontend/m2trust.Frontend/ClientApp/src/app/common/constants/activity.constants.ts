import { Injectable } from '@angular/core';

@Injectable()
export class ActivityConstants {
  readonly Administration = {path: '/administration', translationKey: 'Sidebar.Administration', sortOrder: 1};
  readonly ManageRequests = { path: '/manage-requests', translationKey: 'Sidebar.ManageRequests', sortOrder: 2};
  readonly Enroll = { path: '/enroll-user', translationKey: 'Sidebar.EnrollUser', sortOrder: 3};
  readonly DisableSmartcard = { path: '/disable-smartcard', translationKey: 'Sidebar.DisableSmartcard', sortOrder: 4 };
  readonly UnblockSmartcard = { path: '/unblock-smartcard', translationKey: 'Sidebar.UnblockSmartcard' , sortOrder: 5};
  readonly RecoverCertificate = { path: '/recover-certificate', translationKey: 'Sidebar.RecoverCertificate', sortOrder: 6 };
  readonly ReinstateSmartcard = { path: '/reinstate-smartcard', translationKey: 'Sidebar.ReinstateSmartcard', sortOrder: 7 };
  readonly RetireTemporarySmartcard = { path: '/retire-temporary-smartcard', translationKey: 'Sidebar.RetireTemporarySmartcard', sortOrder: 8 };
  readonly RevokeCertificate = { path: '/revoke-certificate', translationKey: 'Sidebar.RevokeCertificate', sortOrder: 9 };
  readonly RevokeProfile = { path: '/revoke-my-profile', translationKey: 'Sidebar.RevokeMyProfile' , sortOrder: 10};
  readonly SuspendSmartcard = { path: '/suspend-smartcard', translationKey: 'Sidebar.SuspendSmartcard', sortOrder: 11 };
  readonly RewriteSmartcard = { path: '/rewrite-smartcard', translationKey: 'Sidebar.RewriteSmartcard', sortOrder: 12};
  readonly RenewCertificate = { path: '/renew-certificate', translationKey: 'Sidebar.RenewCertificate', sortOrder: 13};
}

export const AdministrationActivityConstants = [
  { name: 'serviceConfiguration', path: '/administration/service-configuration', translationKey: 'ServiceConfiguration.PageTitle', sortOrder: 1, isActive: false },
  { name: 'profileTemplateConfiguration', path: '/administration/profile-template-configuration', translationKey: 'ProfileTemplateConfiguration.PageTitle', sortOrder: 2, isActive: false },
  { name: 'activitiesConfiguration', path: '/administration/activities-configuration', translationKey: 'ActivitiesConfiguration.PageTitle', sortOrder: 3, isActive: false },
  { name: 'userLookupConfiguration', path: '/administration/user-lookup-configuration', translationKey: 'UserLookupConfiguration.PageTitle', sortOrder: 4, isActive: false },
  { name: 'frontendConfiguration', path: '/administration/frontend-configuration', translationKey: 'FrontendConfiguration.PageTitle', sortOrder: 5, isActive: false },
  { name: 'UISettings', path: '/administration/ui-settings', translationKey: 'Sidebar.UISettings', sortOrder: 6, isActive: false },
  { name: 'logging', path: '/administration/logging', translationKey: 'Sidebar.Logging', sortOrder: 7, isActive: false }
];

export const Activities = [
  { name: 'enroll', path: '/enroll-user' },
  { name: 'disable', path: '/disable-smartcard' },
  { name: 'unblock', path: '/unblock-smartcard' },
  { name: 'recover', path: '/recover-certificate' },
  { name: 'reinstate', path: '/reinstate-smartcard' },
  { name: 'retire', path: '/retire-temporary-smartcard' },
  { name: 'revoke', path: '/revoke-certificate' },
  { name: 'suspend', path: '/suspend-smartcard' },
  { name: 'rewrite', path: '/rewrite-smartcard' },
  { name: 'renew', path: '/renew-certificate' }
];

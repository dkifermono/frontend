export const ActivityTypes = {
  smartcard: [
    'ReinstateSmartcard',
    'SuspendSmartcard',
    'RetireTemporarySmartcard',
    'RewriteSmartcard',
    'SuspendSmartcard',
    'DisableSmartcard',
    'UnblockSmartcard'
  ]
};

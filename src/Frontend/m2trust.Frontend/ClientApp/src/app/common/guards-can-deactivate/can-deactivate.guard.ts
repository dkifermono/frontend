import { Injectable, HostListener } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { CancelActivityService } from '../services/cancel-activity.service';
import { RoutingStateService } from '../services/routing-state.service';


export abstract class CanComponentDeactivate {
  form: FormGroup;
  confirmation = false;
  isFromOutside = false;
  isCancelButton = false;

  constructor(
    private cancelActivityService: CancelActivityService,
    private routingStateService: RoutingStateService
  ) {
    this.cancelActivityService.isConfirmed.subscribe(message => {
      this.confirmation = message;
      if (message) {
        this.cancelActivityService.changeNavigation(this.routingStateService.getNextUrl());
      }
    });
    this.cancelActivityService.isCancelButton.subscribe(message => {
      this.isCancelButton = message;
    });
  }

  canDeactivate() {
    let returnValue = false;
    // don't show confirmation dialog for next and previous buttons (but show it if cancel button is pressed)
    if (this.routingStateService.isUrlRootSame() && !this.isFromOutside && !this.isCancelButton) {
      returnValue = true;
    } else if (this.form) { // page contains form
      if (this.confirmation) {
        returnValue = true;
      } else {
        returnValue = !this.form.dirty;
      }
    } else { // page does not contain form, but confirmation dialog should be shown
      if (this.confirmation) {
        returnValue = true;
      } else {
        returnValue = false;
      }
    }
    this.confirmation = false;
    this.isFromOutside = false;
    this.isCancelButton = false;
    return returnValue;
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    this.isFromOutside = true;
    if (!this.canDeactivate()) {
      $event.returnValue = true;
    }
  }
}

@Injectable()
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {

  constructor(private cancelActivityService: CancelActivityService) {
  }


  canDeactivate(
    component: CanComponentDeactivate,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const canDeactivate = component.canDeactivate();
    if (!canDeactivate) {
      this.cancelActivityService.tryNavigation(true);
    } else {
      return canDeactivate;
    }
  }

}

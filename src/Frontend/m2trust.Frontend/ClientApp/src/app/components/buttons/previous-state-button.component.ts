import { Component, Input } from '@angular/core';
import { Location } from '@angular/common';
import { RoutingStateService } from '../../common/services';
import { Router, ActivatedRoute } from '@angular/router';
import { WizardData } from '../../common';

@Component({
  selector: 'app-previous-state-button',
  templateUrl: 'previous-state-button.component.html'
})

export class PreviousStateButtonComponent {

  constructor(
    private location: Location,
    private routingState: RoutingStateService,
    private router: Router,
    private route: ActivatedRoute,
    private wizardData: WizardData) { }
    @Input() isFirstStep = false;
    @Input() disabled = false;

  goBack() {
    if (this.isFirstStep) {
      this.wizardData.clear();
      this.router.navigate(['/home']);
    } else {
      this.routingState.back();
      this.router.navigate([this.routingState.getPreviousUrl()], {relativeTo: this.route});
    }
  }
}



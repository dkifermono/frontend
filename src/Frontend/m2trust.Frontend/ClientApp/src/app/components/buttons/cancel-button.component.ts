import { Component, Input, OnInit } from '@angular/core';
import {Router } from '@angular/router';
import { CancelActivityService } from '../../common/services';

@Component({
  selector: 'app-cancel-button',
  templateUrl: 'cancel-button.component.html'
})

export class CancelButtonComponent {

  @Input() url: string;

  constructor(
    private router: Router,
    private cancelActivityService: CancelActivityService) {}

  cancelCurrentActivity() {
    this.cancelActivityService.cancelButtonPressed(true);
    this.router.navigate([this.url]);
  }
}



import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {
  UserEntryDto,
  ProfileTemplateDto,
  AuditRequestDto,
  DataItemDto,
  DataCollectionDto
} from '../../common/restclient/restClient';
import { ActivatedRoute } from '@angular/router';
import { WizardData } from '../../common/providers/wizardData';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-reason',
  templateUrl: 'reason.component.html'
})

export class ReasonComponent implements OnInit {
  @Input() pageTitle: string;
  @Input() stepTitle: string;
  @Input() stepDescription: string;
  @Input() useHeader = true;
  @Input() firstStepUrl: string;
  @Output() effectiveRevocationChange = new EventEmitter<number>();
  @Output() navigateToNextPage = new EventEmitter<AuditRequestDto>();
  user: UserEntryDto;
  profileTemplate: ProfileTemplateDto;
  suspendReinstate: AuditRequestDto;
  cachedData: DataCollectionDto;
  dataItemDtoArray: DataItemDto[];

  private _effectiveRevocation: number;

  get effectiveRevocation() {
    return this._effectiveRevocation;
  }
  set effectiveRevocation(value: number) {
    if (value !== this._effectiveRevocation) {
      this._effectiveRevocation = value;
      this.effectiveRevocationChange.emit(value);
    }
  }

  constructor(
    private route: ActivatedRoute,
    private spinnerService: Ng4LoadingSpinnerService,
    private wizardData: WizardData,
  ) {}

ngOnInit() {
  this.route.params.subscribe(params => {
    this.spinnerService.show();
    const userId = params['userId'];
    const profileId = params['profileId'];
    this.cachedData = this.wizardData.getDataCollection();
      if (this.cachedData) {
        this.dataItemDtoArray = this.cachedData.dataCollectionItems.map(item => {
           return new DataItemDto( {name: item.name, value: item.value});
        });
        }

  this.suspendReinstate = new AuditRequestDto({
    userId: userId,
    profileId: profileId,
    dataCollections: this.dataItemDtoArray
  });
  this.spinnerService.hide();
});
}

submitRequest() {
  this.navigateToNextPage.emit(this.suspendReinstate);
  }
}

import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { LanguageService, LogoService } from '../../common/services';
@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public selectedLanguage: string;
  public availableLanguages: string[] = ['de', 'en'];
  menuVisible = true;
  isActive = false;
  languageKey = 'selectedLanguage';
  headerText =  '';
  imagePath: string;
  @Output() menuChangeVisibility = new EventEmitter<boolean>();

  constructor(private translate: TranslateService,
    private languageService: LanguageService,
    private logoService: LogoService) {

  }

  async ngOnInit() {
    const browserLang = this.translate.getBrowserLang();
    this.selectedLanguage = browserLang === 'de' ? browserLang : 'en';
    this.changeLanguage();
    if (localStorage.getItem(this.languageKey)) {
      this.selectedLanguage = localStorage.getItem(this.languageKey);
    } else {
      this.selectedLanguage = 'en';
      localStorage.setItem(this.languageKey, 'de');
    }

    this.imagePath = await this.logoService.getImagePath();
    this.languageService.currentMessage.subscribe(value => {
      this.getPageText();
    });
  }

  changeLanguage() {
    this.translate.setDefaultLang(this.selectedLanguage);
    this.translate.use(this.selectedLanguage);
    moment.locale(this.selectedLanguage);
    localStorage.setItem(this.languageKey, this.selectedLanguage);
    this.languageService.changeMessage(this.selectedLanguage);
  }

  getPageText() {
    const pageTextConfig = localStorage.getItem('pageTextConfiguration');
    if (pageTextConfig && JSON.parse(pageTextConfig).headerText) {
      this.headerText = JSON.parse(pageTextConfig).headerText[this.selectedLanguage];
    }
  }

  changeMenuVisibility() {
    this.menuVisible = !this.menuVisible;
    this.isActive = !this.isActive;
    this.menuChangeVisibility.emit(this.menuVisible);
  }

  onClickOutsideChangeMenuVisibility() {
    if (!this.menuVisible) {
      this.menuVisible = !this.menuVisible;
      this.isActive = !this.isActive;
      this.menuChangeVisibility.emit(this.menuVisible);
    }
  }

  private loadLogoImage() {
    const random = (moment().format('MMMMMDoYYYYhhmmss'));
    const imageElement = document.createElement('img');
    imageElement.src = 'assets/images/logo.png?v=' + random;
    imageElement.className = 'header__logo__image';
    document.getElementsByClassName('header__logo')[0].appendChild(imageElement);
  }

}

import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  DataCollectionItemClient,
  DataCollectionDto,
  SectionType,
  DataCollectionItemDtoDataType,
  DataCollectionItemDto,
  DataCollectionItemDtoValidationType,
  WizardData,
  intRegexValidator,
  numberValidator,
  dateValidator
} from '../../common';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {ErrorNotificationService, RoutingStateService} from '../../common/services';


@Component({
  selector: 'app-data-collection-items',
  templateUrl: 'data-collection-items.component.html'
})
export class DataCollectionItemsComponent implements OnInit {
  @Input() sectionType: SectionType;
  @Input() pageTitle: string;
  @Input() stepTitle: string;
  @Input() useHeader = true;
  @Input() activityType: string;
  @Input() firstStepUrl: string;
  @Output() navigateToNextPage = new EventEmitter();
  dataCollection: DataCollectionDto;
  dataCollectionItemDataType = DataCollectionItemDtoDataType;
  validationTypeDci = DataCollectionItemDtoValidationType;
  form: FormGroup;
  isLoading = true;
  isItemRequired = false;
  private userId: string;
  private profileId: string;
  private profileTemplateId: string;

  constructor(
    private route: ActivatedRoute,
    private dataCollectionItemClient: DataCollectionItemClient,
    private spinnerService: Ng4LoadingSpinnerService,
    private wizardData: WizardData,
    private routingStateService: RoutingStateService,
    private errorNotificationService: ErrorNotificationService
  ) {}

  ngOnInit() {
    this.wizardData.clearAuditComment();
    this.dataCollection = this.wizardData.getDataCollection();
    if (this.dataCollection == null) {
      this.getDataCollectionFromApi();
    } else {
      this.form = this.toFormGroup(
        this.dataCollection.dataCollectionItems
      );
      this.isLoading = false;
    }
  }

  getDataCollectionFromApi() {
    this.route.params.subscribe(params => {
      this.spinnerService.show();
      this.isLoading = true;
      this.userId = params['userId'];
      this.profileTemplateId = params['profileTemplateId'];
      this.profileId = params['profileId'];

      const dataCollectionItems = this.dataCollectionItemClient.getDataCollectionItems(
        this.activityType,
        this.profileId,
        this.profileTemplateId,
        this.sectionType
      ).subscribe(
        result => {
          this.dataCollection = result;
          this.form = this.toFormGroup(
            this.dataCollection.dataCollectionItems
          );
        },
        error => {
          this.errorNotificationService.notificationError(error);
          this.spinnerService.hide();
        },
        () => {
          this.isLoading = false;
          this.spinnerService.hide();
        }
      );
    });
  }

  toFormGroup(items: DataCollectionItemDto[]) {
    const group: any = {};
    items.forEach(item => {
      const validators = [];
    if (item.required) {
      validators.push(Validators.required);
      this.isItemRequired = true;
    }
      if (item.validationType === this.validationTypeDci.DataType) {
        if (item.dataType === this.dataCollectionItemDataType.Number) {
          validators.push(intRegexValidator);
        } else if (item.dataType === this.dataCollectionItemDataType.Date) {
          validators.push(dateValidator);
        }
      } else if (item.validationType === this.validationTypeDci.RegularExpression) {
        validators.push(Validators.pattern(item.validationData));
      }
      group[item.name] = new FormControl(item.value, validators);
    });
    return new FormGroup(group);
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        control.updateValueAndValidity();
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  isItemInvalid(key: string) {
    return !this.form.get(key).valid && this.form.get(key).touched;
  }

  goToNextPage() {
    this.validateAllFormFields(this.form);
    if (this.form.valid) {
      const formValues = this.form.getRawValue();
      this.dataCollection.dataCollectionItems.forEach(dci => {
        dci.value = formValues[dci.name];
      });
      this.wizardData.setDataCollection(this.dataCollection);
      this.navigateToNextPage.emit();
    } else {
      return;
    }
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { ContinueModel, WizardData } from '../../common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-continue-activity',
  templateUrl: 'continue-activity.component.html'
})

export class ContinueActivityComponent implements OnInit {
  error: any;
  @Input() model: ContinueModel;

  constructor(private router: Router, private wizardData: WizardData) { }

  ngOnInit() {
    this.wizardData.clearWizard();
    this.error = JSON.parse(this.wizardData.getErrorData());
  }

  continue() {
    this.router.navigate(['/enroll-user/select-profile-template', this.model.userId]);
  }
}

import { Component, OnInit} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import {
  Activity,
  WizardData,
  AdministrationActivityConstants,
  ConfigurationService
} from '../../common';
import {
  ActivityService,
  LanguageService,
  ErrorNotificationService
} from '../../common/services';
import { NotificationService } from '../../common/services';


@Component({
  selector: 'app-sidebar',
  templateUrl: 'sidebar.component.html'
})

export class SidebarComponent implements OnInit {
  activities: Activity[];
  administrationActivities = AdministrationActivityConstants;
  loading = false;
  selectedLanguage = 'en';
  footerText = '';
  isAdminMenuVisible = false;
  isAdministrationPageVisible = false;
  productVersion = '';

  constructor(
    private spinnerService: Ng4LoadingSpinnerService,
    private activityService: ActivityService,
    private notificationService: NotificationService,
    private wizardData: WizardData,
    private languageService: LanguageService,
    private router: Router,
    private errorNotificationService: ErrorNotificationService,
    private configurationClient: ConfigurationService
  ) {
    this.selectedLanguage = languageService.initLanguage;
    this.router.events.subscribe(evt => {
      if (evt instanceof NavigationEnd) {
        this.updateActiveMenuItem();
      }
    });
   }

  ngOnInit() {
    this.spinnerService.show();
    this.loading = true;
    this.languageService.currentMessage.subscribe(value => {
      this.selectedLanguage = value;
      this.getPageText();
    });

    this.activityService.getActivitiesAsync()
      .subscribe(activities => {
        this.activities = activities;
        this.isAdministrationPageVisible = this.getAdministration(activities);
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.spinnerService.hide();
        this.loading = false;

      },
      () => {
        this.spinnerService.hide();
        this.loading = false;
      });

    this.configurationClient.getProductVersion()
      .subscribe(response => {
        this.productVersion = response;
      },
      error => {
        this.errorNotificationService.notificationError(error);
      });
  }



  updateActiveMenuItem() {
    const url = this.router.url;
    if (url.includes('administration')) {
      this.isAdminMenuVisible = true;
      this.administrationActivities.forEach(item => {
        item.isActive = (url === item.path || url.includes(item.path));

      });
    } else {
      this.isAdminMenuVisible = false;
      this.administrationActivities.forEach(subItem => {
          subItem.isActive = false;
      });
    }

  }

  onAdministrationSubitemClick(item: any) {
    this.router.navigate([item.path]);
  }

  onActivityClick() {
    this.removeWizardData();
  }

  getPageText() {
    const pageTextConfig = localStorage.getItem('pageTextConfiguration');
    if (pageTextConfig && JSON.parse(pageTextConfig).footerText) {
      this.footerText = JSON.parse(pageTextConfig).footerText[this.selectedLanguage];
    }
  }

  removeWizardData() {
    this.wizardData.clear();
  }

  onAdministrationMenuClick() {
    this.isAdminMenuVisible = !this.isAdminMenuVisible;
    this.removeWizardData();
  }

  getAdministration(activites: Activity[]) {
    const name = activites.filter(a => a.name === 'Administration').length > 0;
    return name;
  }
}

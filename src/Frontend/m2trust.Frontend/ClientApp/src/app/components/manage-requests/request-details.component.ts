import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import {
  RequestClient,
  RequestDetailsDto,
  DataCollectionItem
} from '../../common';
import {ErrorNotificationService } from '../../common/services';
import * as moment from 'moment';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-request-details',
  templateUrl: 'request-details.component.html'
})
export class RequestDetailsComponent implements OnInit, OnDestroy {
  private _requestDetails: RequestDetailsDto;
  private _dataCollectionItems: DataCollectionItem[] = [];
  activityType: string;

  get requestDetails(): RequestDetailsDto {
    return this._requestDetails;
  }
  set requestDetails(requestDetails: RequestDetailsDto) {
    this._requestDetails = requestDetails;
  }

  get dataCollectionItems(): DataCollectionItem[] {
    return this._dataCollectionItems;
  }
  set dataCollectionItems(dataCollectionItems: DataCollectionItem[]) {
    this._dataCollectionItems = dataCollectionItems;
  }

  constructor(private requestClient: RequestClient,
    private errorNotificationService: ErrorNotificationService,
    private spinner: Ng4LoadingSpinnerService) {}

  @Input() requestId: string;
  @Input() display: boolean;

  @Output() displayChange = new EventEmitter();

  ngOnInit() {
    this.getRequestDetails();
  }

  private getRequestDetails() {
    this.activityType = 'ManageRequests';
    this.spinner.show();
    this.requestClient.getRequest(this.requestId, this.activityType).subscribe(
      response => {
        this.spinner.hide();
        this.requestDetails = response;
        if (this.requestDetails.dateCompleted.isBefore(moment(0))) {
          this.requestDetails.dateCompleted = null;
        }
        if (response.dataCollectionItems) {
          for (const key of Object.keys(response.dataCollectionItems)) {
            this.dataCollectionItems.push({
              key: key,
              value: response.dataCollectionItems[key]
            });
          }
        }
      },
      error => {
        this.spinner.hide();
        this.errorNotificationService.notificationError(error);
        this.onClose();
      },
      () => (this.display = true)
    );
  }

  onClose() {
    this.display = false;
    this.requestId = null;
    this.displayChange.emit(false);
  }

  ngOnDestroy() {
    this.displayChange.unsubscribe();
  }
}

import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-deny-popup',
  templateUrl: 'deny-popup.component.html'
})

export class DenyPopupComponent implements OnInit, OnDestroy {
  comment: string;

  constructor() { }

  @Input() showPannel: boolean;
  @Output() displayChange = new EventEmitter();

  ngOnInit() {
  }

  onClose() {
    this.showPannel = false;
    this.displayChange.emit();
  }

  onContinue() {
    if (!this.comment) {
      this.comment = '';
    }
    this.displayChange.emit(this.comment);
  }

  ngOnDestroy() {
    this.displayChange.unsubscribe();
  }
}

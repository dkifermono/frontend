import { Component, Output, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { RequestForm, RequestState, RequestSearchForm } from '../../common';

@Component({
  selector: 'app-request-search-form',
  templateUrl: 'request-search-form.component.html'
})
export class RequestSearchFormComponent implements OnDestroy, OnInit {
  private _searchForm: FormGroup;
  private _requestTypes: string[];
  private _requestStatuses: string[];
  private _searchRequest: RequestSearchForm;

  private readonly all = 'All';

  get searchForm(): FormGroup {
    return this._searchForm;
  }

  get requestTypes(): string[] {
    return this._requestTypes;
  }
  set requestTypes(requestTypes: string[]) {
    this._requestTypes = requestTypes;
  }

  get requestStatuses(): string[] {
    return this._requestStatuses;
  }
  set requestStatuses(requestStatuses: string[]) {
    this._requestStatuses = requestStatuses;
  }

  get searchRequest(): RequestSearchForm {
    return this._searchRequest;
  }
  set searchRequest(searchRequest: RequestSearchForm) {
    this._searchRequest = searchRequest;
  }

  constructor() {
  }

  @Output() submitEvent = new EventEmitter<RequestSearchForm>();
  @Output() formStatusChange = new EventEmitter<FormGroup>();

  ngOnDestroy(): void {
    this.submitEvent.unsubscribe();
  }

  ngOnInit(): void {
    this.setTypesAndStatuses();
    this.createForm();
  }

  private createForm() {
    const form = {
      originator: new FormControl(null),
      requestTypes: new FormControl(this.all),
      submittedFrom: new FormControl(null),
      requestStatuses: new FormControl('Pending'),
      targetUser: new FormControl(null),
      submittedTo: new FormControl(null)
    };
    this._searchForm = new FormGroup(form);
    this.formStatusChange.emit(this._searchForm);

    this._searchForm.statusChanges.subscribe(status => {
      this.formStatusChange.emit(this._searchForm);
    });
  }

  private setTypesAndStatuses() {
    this.requestTypes = Object.keys(RequestForm);
    const index = this.requestTypes.indexOf('None', 0);
    if (index > -1) {
      this.requestTypes.splice(index, 1);
    }
    this.requestTypes.push(this.all);

    this.requestStatuses = Object.keys(RequestState);
    this.requestStatuses.push(this.all);
  }

  onSubmit() {
    this.searchRequest = {
      requestForm:
        this.searchForm.value.requestTypes !== this.all
          ? this.searchForm.value.requestTypes
          : '',
      requestState:
        this.searchForm.value.requestStatuses !== this.all
          ? this.searchForm.value.requestStatuses
          : '',
      originatorUserName:
        this.searchForm.value.originator !== null
          ? this.searchForm.value.originator
          : '',
      targetUserName:
        this.searchForm.value.targetUser !== null
          ? this.searchForm.value.targetUser
          : '',
      submittedFrom: this.searchForm.value.submittedFrom,
      submittedTo: this.searchForm.value.submittedTo
    };
    this.submitEvent.emit(this.searchRequest);
  }
}

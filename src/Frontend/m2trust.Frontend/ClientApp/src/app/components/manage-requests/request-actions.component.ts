import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
  OnInit
} from '@angular/core';
import {
  IValidationDto,
  SignedRequestDto,
  ValidationDto,
  RequestClient,
  RESTPagedResultOfSignedRequestDto,
  SecretDto,
  IDenyDto,
  DenyDto,
  IRequestActionResponseDto,
  RequestValidation
} from '../../common';
import {ManageRequestsDistributeSecretsService} from '../../common/services';
import { Observable ,  forkJoin } from 'rxjs';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {ErrorNotificationService, NotificationService } from '../../common/services';
import { ValidationService } from '../../common/services/validation.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-request-actions',
  templateUrl: 'request-actions.component.html'
})
export class RequestActionsComponent implements OnInit, OnDestroy {
  activityType: string;
  observables: Observable<ValidationDto>[] = [];
  validatedRequests: RequestValidation[] = [];
  noSecrets = false;

  abandonResponse: IRequestActionResponseDto;
  cancelResponse: IRequestActionResponseDto;
  denyResponse: IRequestActionResponseDto;
  approveResponse: IRequestActionResponseDto;

  validation: IValidationDto = {
    approve: false,
    deny: false,
    distributeSecrets: false,
    cancel: false,
    abandon: false
  };
  selectedRows: SignedRequestDto[];
  showPopup = false;

  constructor(
    private requestClient: RequestClient,
    private spinnerService: Ng4LoadingSpinnerService,
    private distributeSecretsData: ManageRequestsDistributeSecretsService,
    private validationServiceData: ValidationService,
    private errorNotificationService: ErrorNotificationService,
    private notificationService: NotificationService,
    private translateService: TranslateService
  ) {}

  @Input() requestList: RESTPagedResultOfSignedRequestDto;
  @Input() secrets: SecretDto[] = [];
  @Input() isValidating: boolean;

  @Output() getListEvent = new EventEmitter<boolean>();

  ngOnInit() {
    this.activityType = 'ManageRequests';
    this.distributeSecretsData.currentMessage.subscribe(message => {
      this.secrets = message;
    });

    this.validationServiceData.currentMessage.subscribe(message => {
      this.selectedRows = message;
      if (this.selectedRows) {
        this.validateActions(this.selectedRows);
      }
      if (!this.selectedRows || this.selectedRows.length <= 0) {
        this.setValidationFalse();
      }
    });

    this.validationServiceData.isSearchFormSubmitted.subscribe(value => {
      if (value) {
        this.setValidationFalse();
      }
    });
  }

  ngOnDestroy(): void {
    this.getListEvent.unsubscribe();
  }

  // Methods used for button actions

  distributeSecretsAction() {
    this.setValidationFalse();
    if (!this.selectedRows || this.selectedRows.length <= 0) {
      return;
    }
    this.secrets = [];
    this.spinnerService.show();
    this.requestClient
      .distributeSecrets(this.selectedRows.map(r => r.requestId), this.activityType)
      .subscribe(
        response => {
          this.populateSecrets(response);
          this.spinnerService.hide();
        },
        error => {
          this.spinnerService.hide();
          this.errorNotificationService.notificationError(error);
        },
        () => this.spinnerService.hide()
      );
  }

  private populateSecrets(response: SecretDto[]) {
    if (response.length > 0) {
      this.distributeSecretsData.changeMessage(response);
    } else {
      this.notificationService.alert(this.translateService.instant('ManageRequests.DistributeSecrets'), this.translateService.instant('ManageRequests.NoSecrets'));
    }
    this.selectedRows = [];
  }

  abandonAction() {
    this.setValidationFalse();
    if (!this.selectedRows || this.selectedRows.length <= 0) {
      return;
    }
    this.spinnerService.show();
    this.requestClient
      .abandon(this.selectedRows.map(r => r.requestId), this.activityType)
      .subscribe(
        response => {
          this.abandonResponse = response;
          this.notificationService.success(this.translateService.instant('ManageRequests.AbandonSuccessMessage'));
          this.getRequestList();
        },
        error => {
          this.spinnerService.hide();
          this.errorNotificationService.notificationError(error);
        },
        () => this.spinnerService.hide()
      );
  }
  cancelAction() {
    this.setValidationFalse();
    if (!this.selectedRows || this.selectedRows.length <= 0) {
      return;
    }
    this.spinnerService.show();
    this.requestClient
      .cancel(this.selectedRows.map(r => r.requestId), this.activityType)
      .subscribe(
        response => {
          this.cancelResponse = response;
          this.notificationService.success(this.translateService.instant('ManageRequests.CancelSuccessMessage'));
          this.getRequestList();
        },
        error => {
          this.spinnerService.hide();
          this.errorNotificationService.notificationError(error);

        },
        () => this.spinnerService.hide()
      );
  }
  denyAction() {
    if (!this.selectedRows || this.selectedRows.length <= 0) {
      return;
    }
    this.showPopup = true;
  }

  onDialogClose($event: string) {
    this.showPopup = false;
    if ($event || $event === '') {
      this.setValidationFalse();
      this.spinnerService.show();
      const denyObj: IDenyDto = {
        requestIds: this.selectedRows.map(r => r.requestId),
        comment: $event === '' ? null : $event
      };
      this.requestClient.deny(new DenyDto(denyObj), this.activityType).subscribe(
        response => {
          this.denyResponse = response;
          this.notificationService.success(this.translateService.instant('ManageRequests.DenySuccessMessage'));
          this.getRequestList();
        },
        error => {
          this.spinnerService.hide();
          this.errorNotificationService.notificationError(error);
        },
        () => this.spinnerService.hide()
      );
    }
  }

  approveAction() {
    this.setValidationFalse();
    if (!this.selectedRows || this.selectedRows.length <= 0) {
      return;
    }
    this.secrets = [];
    this.spinnerService.show();
    this.requestClient
      .approve(this.selectedRows.map(r => r.requestId), this.activityType)
      .subscribe(
        response => {
          this.approveResponse = response.result;
          if (response.result.hasSecrets) {
            this.populateSecrets(response.secrets);
          }

          this.notificationService.success(this.translateService.instant('ManageRequests.ApproveSuccessMessage'));
          this.getRequestList();

        },
        error => {
          this.spinnerService.hide();
          this.errorNotificationService.notificationError(error);
        },
        () => this.spinnerService.hide()
      );
  }

  private getRequestList() {
    this.validatedRequests = [];
    this.getListEvent.emit(true);
  }

  // Methods used for validation

  private setValidationFalse() {
    this.validation = {
      approve: false,
      deny: false,
      distributeSecrets: false,
      cancel: false,
      abandon: false
    };
    this.noSecrets = false;
  }

  private changeValidatingState(value: boolean) {
    this.isValidating = value;
    this.validationServiceData.changeValidatingState(this.isValidating);
  }

  private validateActions(requests: SignedRequestDto[]) {
    this.observables = [];
    if (!requests || requests.length <= 0) {
      return;
    }
    this.changeValidatingState(true);
    // check if there is any data in already validated requests
    if (this.validatedRequests.length > 0) {
      // remove the request that is already validated
      requests = requests.filter(
        request =>
          this.validatedRequests.findIndex(
            valid => valid.requestId === request.requestId
          ) === -1
      );
    }

    // get validation for every not yet validated request
    requests.forEach(request => {
      const observable = this.requestClient.validateActions(
        request.requestId,
        this.activityType
      );
      if (! this.observables.includes(observable)) {
        this.observables.push(observable);
      }
    });
    if (this.observables.length > 0) {
      forkJoin(this.observables).subscribe(
        response => {
          response.forEach(validation => {
            const responseRequest = this.requestList.items.find(
              request => request.requestId === validation.requestId
            );
            if (responseRequest) {
              this.validatedRequests.push(
                new RequestValidation(responseRequest, validation)
              );
            }

          });
          this.createValidationObject();
          this.changeValidatingState(false);
        },
        error => {
          this.errorNotificationService.notificationError(error);
          this.createValidationObject();
          this.changeValidatingState(false);
        },
        () => {
          this.createValidationObject();
          this.changeValidatingState(false);
        }
      );
    } else {
      this.createValidationObject();
      this.changeValidatingState(false);
    }
  }

  private createValidationObject() {
    if (this.validatedRequests.length > 0 && this.selectedRows.length > 0) {
      let vR = [];
      vR = this.validatedRequests.filter(
        valid =>
          this.selectedRows.findIndex(
            selected => selected.requestId === valid.requestId
          ) !== -1
      );

      this.validation = {
        approve: vR.map(r => r.validation.approve).some(r => r === false)
          ? false
          : true,
        abandon: vR.map(r => r.validation.abandon).some(r => r === false)
          ? false
          : true,
        deny: vR.map(r => r.validation.deny).some(r => r === false)
          ? false
          : true,
        distributeSecrets: vR
          .map(r => r.validation.distributeSecrets)
          .some(r => r === false)
          ? false
          : true,
        cancel: vR.map(r => r.validation.cancel).some(r => r === false)
          ? false
          : true
      };
    } else {
      this.setValidationFalse();
    }
  }
}

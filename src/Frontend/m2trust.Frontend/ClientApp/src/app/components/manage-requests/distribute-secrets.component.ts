import { Component, OnInit } from '@angular/core';
import { ManageRequestsDistributeSecretsService } from '../../common/services';
import { SecretDto } from '../../common';


@Component({
  selector: 'app-distribute-secrets',
  templateUrl: 'distribute-secrets.component.html'
})

export class DistributeSecretsComponent implements OnInit {
  secrets: SecretDto[];
  canDistributeSecrets = false;

  constructor(
    private distributeSecretsData: ManageRequestsDistributeSecretsService,
  ) { }

  ngOnInit() {
    this.distributeSecretsData.currentMessage.subscribe(message => {
      this.secrets = message;
      this.canDistributeSecrets = this.secrets && this.secrets.length > 0;
    });
  }
}

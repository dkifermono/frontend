import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  Page,
  PageEvent,
  WizardData,
  UserClient,
  RESTPagedResultOfUserEntryDto,
  UserEntryDto,
  PageHeaderData,
  UserListData
} from '../../common';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {  ErrorNotificationService } from '../../common/services';

@Component({
  selector: 'app-user-list',
  templateUrl: 'user-list.component.html'
})
export class UserListComponent implements OnInit {
  @Input() url: string;
  @Input() pageTitle: string;
  @Input() stepTitle: string;
  @Input() displayProp: string[] = null;
  @Input() searchProp: string[] = null;
  @Input() activityType: string;
  @Output() statusChange = new EventEmitter<FormGroup>();

  public isLoading = false;
  private _searchInput: string;
  private _domain: string[];
  private _searchIn: string[];
  currentPaging: Page = new Page();
  selectedUser: UserEntryDto;
  response: RESTPagedResultOfUserEntryDto = null;
  private readonly allFields = 'AllFields';
  private readonly allDomains = 'SelectUser.Table.AllDomains';
  isInit = true;
  form: FormGroup;
  userListData: UserListData = null;

  get domain(): string[] {
    return this._domain;
  }
  set domain(domain: string[]) {
    this._domain = domain;
  }

  get searchInTypes(): string[] {
    return this._searchIn;
  }
  set searchInTypes(searchIn: string[]) {
    this._searchIn = searchIn;
  }

  get searchInput(): string {
    return this._searchInput;
  }
  set searchInput(searchInput: string) {
    this._searchInput = searchInput;
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private spinnerService: Ng4LoadingSpinnerService,
    private wizardData: WizardData,
    private userClient: UserClient,
    private errorNotificationService: ErrorNotificationService
  ) {

  }

  createForm() {
    const form = {
      searchInput: new FormControl(this.userListData ? this.userListData.searchFor : null, Validators.required),
      domain: new FormControl(this.userListData.domain ? this.userListData.domain : this.allDomains),
      searchInTypes: new FormControl(this.userListData.searchIn ? this.userListData.searchIn : this.allFields)
    };
    this.form = new FormGroup(form);
    this.statusChange.emit(this.form);

    this.form.statusChanges.subscribe(status => {
      this.statusChange.emit(this.form);
    });
  }

  ngOnInit() {
    this.wizardData.clearProfileTemplateId();
    this.wizardData.setPageHeaderData(null);
    const wizardDataList = this.wizardData.getUserListData();
    if (wizardDataList == null) {
      this.userListData = new UserListData();
    } else {
      this.userListData = wizardDataList;
    }
    this.createForm();
    this.spinnerService.show();
    this.userClient.getUserFilters().subscribe(response => {
      const searchItems = response;
      this.spinnerService.hide();
      if (searchItems.searchFields !== undefined) {
        searchItems.searchFields.splice(0, 0, this.allFields);
        if (this.searchProp) {
          searchItems.searchFields = searchItems.searchFields.concat(this.searchProp);
        }
        this.searchInTypes = searchItems.searchFields;
      }
      if (searchItems.domains !== undefined) {
          const domains = searchItems.domains.map(x => x.name);
          domains.splice(0, 0, this.allDomains);
          this.domain = domains;
      }
    });

    if (wizardDataList) {
      this.onSubmit(true);
    }
  }

  onSubmit(isFromPrevious: boolean = false) {
    this.getUsers(this.userListData ? this.userListData.page : 1);
    this.getUsersFromApi(isFromPrevious);
  }

  manageTable(event: PageEvent) {
    this.getUsers(event.page + 1);
  }

  selectUser(item: UserEntryDto) {
    this.selectedUser = item;
  }

  getUsers(page: number) {
  this.isInit = false;

  const searchUser =
    this.form.value.searchInput !== null
      ? this.form.value.searchInput
      : '';
  const searchIn =
    this.form.value.searchInTypes !== this.allFields
      ? this.form.value.searchInTypes
      : '';
  const domain = this.form.value.domain !== this.allDomains ? this.form.value.domain : '';

  this.userListData.searchFor = searchUser;
  this.userListData.searchIn = searchIn;
  this.userListData.domain = domain;
  this.userListData.page = page;

  }

  getUsersFromApi(isFromPrevious: boolean = false) {
    this.spinnerService.show();
    this.isLoading = true;
    this.userClient.getUsers(
      this.userListData.domain,
      this.userListData.page,
      this.userListData.searchIn,
      this.userListData.searchFor,
      this.displayProp,
      this.searchProp,
      this.activityType)
    .subscribe(
      responseData => {
        this.response = responseData;
        this.currentPaging.rows = this.response.pageSize;
        this.currentPaging.totalRecords = this.response.totalCount;
        this.currentPaging.pageNumber = this.response.pageNumber;
        if (isFromPrevious) {
          const user = this.response.items.filter(u => u.id === this.userListData.userId);
          if (user.length > 0) {
            this.selectUser(user[0]);
          }
        }
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.spinnerService.hide();
      },
      () => {
        this.spinnerService.hide();
        this.isLoading = false;
      }
    );
  }

  getColumnValue(columnName: string, item: UserEntryDto) {
    if (columnName === 'distinguishedName') {
      return item.directoryPath;
    } else if (item[columnName] !== undefined) {
      return item[columnName];
    } else if (item.properties != null) {
      if (columnName === 'memberOf' && item.properties[columnName.toLowerCase()] !== undefined) {
        if (Array.isArray(item.properties[columnName.toLowerCase()]) ) {
          return  item.properties[columnName.toLowerCase()].join('\r\n');
        } else {
          return  item.properties[columnName.toLowerCase()];
      }
      } else {
        return item.properties[columnName];
      }
    }
    return undefined;
  }

  goToNextPage() {
    if (this.selectedUser == null || this.url == null) {
      return;
    }
    this.wizardData.setPageHeaderData(new PageHeaderData(this.selectedUser.name));
    this.userListData.userId = this.selectedUser.id;
    this.userListData.domain = this.userListData.domain ? this.userListData.domain : this.allDomains;
    this.userListData.searchIn = this.userListData.searchIn ? this.userListData.searchIn : this.allFields;
    this.wizardData.setUserListData(this.userListData);
    this.router.navigate([this.url, this.selectedUser.id], {
      relativeTo: this.route
    });
  }
}

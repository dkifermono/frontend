import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-color-picker',
  templateUrl: 'color-picker.component.html'
})

export class ColorPickerComponent implements OnInit {

  @Input() controlName: string;
  @Input() formGroup: FormGroup;

  inputValue: string;

  ngOnInit() {
    this.inputValue = this.formGroup.get(this.controlName).value;
  }

  onColorChange() {
    this.inputValue = this.formGroup.get(this.controlName).value;
  }

  onInputChange($event) {
    this.formGroup.get(this.controlName).setValue($event.target.value);
  }
}


import { Component, OnInit } from '@angular/core';
import { MenuPathService } from '../../common/services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-path-subtitle',
  templateUrl: 'page-path-subtitle.component.html'
})

export class PagePathSubtitleComponent implements OnInit {

    translationKeys: string[];
  constructor(
      private menuPathService: MenuPathService,
      private router: Router
  ) { }

  ngOnInit() {
    this.translationKeys = this.menuPathService.getTranslationKeys(this.router.url);
  }
}



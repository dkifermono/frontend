import { NgModule } from '@angular/core';
import {
  AuditComponent,
  CancelButtonComponent,
  PreviousStateButtonComponent,
  DataCollectionItemsComponent,
  FinishActivityComponent,
  ContinueActivityComponent,
  PageHeaderComponent,
  RevokeProfilePageHeaderComponent,
  HeaderComponent,
  SelectProfileComponent,
  CertificateListComponent,
  SidebarComponent,
  UserListComponent,
  ValidationSummaryComponent,
  DistributeSecretsComponent,
  RequestDetailsComponent,
  RequestSearchFormComponent,
  RequestActionsComponent,
  ReasonComponent,
  LoggingDetailsComponent,
  ConfirmationDialogComponent,
  ColorPickerComponent
} from '.';
import { AppCommonModule } from '../common/common.module';
import { RouterModule } from '@angular/router';
import { DenyPopupComponent } from './manage-requests';
import { DatePickerComponent } from './date-picker';
import {
  FormArrayListComponent,
  ValidationErrorComponent,
  ProfileTemplateListComponent,
  NameValueListComponent,
  ActivityFormComponent,
  PageTextItemComponent,
  AddStringToArrayAutocompleteComponent,
  AutocompletePairComponent,
  PicklistComponent
} from './form-components';
import { PagePathSubtitleComponent } from './page-path-subtitle';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule
  ],
  declarations: [
    AuditComponent,
    CancelButtonComponent,
    PreviousStateButtonComponent,
    DataCollectionItemsComponent,
    FinishActivityComponent,
    ContinueActivityComponent,
    HeaderComponent,
    LoggingDetailsComponent,
    PageHeaderComponent,
    RevokeProfilePageHeaderComponent,
    SelectProfileComponent,
    CertificateListComponent,
    SidebarComponent,
    ConfirmationDialogComponent,
    UserListComponent,
    ValidationSummaryComponent,
    DistributeSecretsComponent,
    RequestDetailsComponent,
    RequestSearchFormComponent,
    RequestActionsComponent,
    ReasonComponent,
    DenyPopupComponent,
    DatePickerComponent,
    PageTextItemComponent,
    ActivityFormComponent,
    FormArrayListComponent,
    ValidationErrorComponent,
    ProfileTemplateListComponent,
    NameValueListComponent,
    AddStringToArrayAutocompleteComponent,
    AutocompletePairComponent,
    PicklistComponent,
    ColorPickerComponent,
    PagePathSubtitleComponent
  ],
  exports: [
    AuditComponent,
    CancelButtonComponent,
    PreviousStateButtonComponent,
    DataCollectionItemsComponent,
    FinishActivityComponent,
    ContinueActivityComponent,
    HeaderComponent,
    LoggingDetailsComponent,
    PageHeaderComponent,
    RevokeProfilePageHeaderComponent,
    SelectProfileComponent,
    CertificateListComponent,
    SidebarComponent,
    ConfirmationDialogComponent,
    UserListComponent,
    ValidationSummaryComponent,
    DistributeSecretsComponent,
    RequestDetailsComponent,
    RequestSearchFormComponent,
    RequestActionsComponent,
    ReasonComponent,
    DenyPopupComponent,
    DatePickerComponent,
    PageTextItemComponent,
    ActivityFormComponent,
    FormArrayListComponent,
    ValidationErrorComponent,
    ProfileTemplateListComponent,
    NameValueListComponent,
    AddStringToArrayAutocompleteComponent,
    AutocompletePairComponent,
    PicklistComponent,
    ColorPickerComponent,
    PagePathSubtitleComponent
  ],
  providers: [
  ]
})
export class ComponentsModule {}

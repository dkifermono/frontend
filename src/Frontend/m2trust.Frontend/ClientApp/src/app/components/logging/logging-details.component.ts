import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { LogEntryDto } from '../../common';

@Component({
  selector: 'app-logging-details',
  templateUrl: 'logging-details.component.html'
})

export class LoggingDetailsComponent implements OnDestroy {
  constructor() {}

  @Input() showPanel: boolean;
  @Input() logEntry: LogEntryDto;
  @Output() displayChange = new EventEmitter();

  onClose() {
    this.showPanel = false;
    this.displayChange.emit(false);
  }

  ngOnDestroy() {
    this.displayChange.unsubscribe();
  }
}

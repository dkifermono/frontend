import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-activity-form',
  templateUrl: 'activity-form.component.html'
})

export class ActivityFormComponent {
  @Input() group: FormGroup;
  @Input() labelText: string;
  @Input() hasShowCommentsSection = false;
  @Input() hasContinueSection = false;
  constructor() {
  }


}


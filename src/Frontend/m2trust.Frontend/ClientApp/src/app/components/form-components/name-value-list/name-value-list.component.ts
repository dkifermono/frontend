import { Component, Input } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { ProfileTemplateConfigService } from '../../../common/services';

@Component({
  selector: 'app-name-value-list',
  templateUrl: 'name-value-list.component.html'
})

export class NameValueListComponent {
  @Input() group: FormGroup;
  @Input() formArrayName: string;

  constructor(
    private profileTemplateConfigService: ProfileTemplateConfigService
  ) { }

  get formArray() {
    return (<FormArray>this.group.get(this.formArrayName));
  }

  addToList() {
    this.formArray.push(this.profileTemplateConfigService.createNameValueGroup());
  }

  removeFromList(index: number) {
    this.formArray.removeAt(index);
    this.group.markAsDirty();
  }

}

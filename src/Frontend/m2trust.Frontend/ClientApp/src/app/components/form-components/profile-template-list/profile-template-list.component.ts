import { Component, Input } from '@angular/core';
import { FormGroup, FormArray} from '@angular/forms';
import { ProfileTemplateConfigService } from '../../../common/services';

@Component({
  selector: 'app-profile-template-list',
  templateUrl: 'profile-template-list.component.html'
})

export class ProfileTemplateListComponent {

  @Input() configurationGroup: FormGroup;
  @Input() formArrayName: string;

  constructor(
    private profileTemplateConfigService: ProfileTemplateConfigService
  ) {  }

  get formArray() {
    return (<FormArray>this.configurationGroup.get(this.formArrayName));
  }

  addToList() {
    this.formArray.insert(0, this.profileTemplateConfigService.createProfileTemplateGroup());
  }

  removeFromList(index: number) {
    this.formArray.removeAt(index);
    this.configurationGroup.markAsDirty();
  }

  getControl(index: number) {
    return this.formArray.controls[index];
  }


}

import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-validation-error',
  templateUrl: 'validation-error.component.html'
})

export class ValidationErrorComponent implements OnInit {
  @Input() control: AbstractControl;
  @Input() minInterval: number = null;
  @Input() maxInterval: number = null;
  params: any;
  constructor() { }

  ngOnInit() {
    this.params = {
      min: this.minInterval,
      max: this.maxInterval
    };
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray, ValidatorFn, FormControl, Validators } from '@angular/forms';
import { ConfigFormService } from '../../../common/services';

@Component({
  selector: 'app-form-array-list',
  templateUrl: 'form-array-list.component.html'
})

export class FormArrayListComponent  {

  @Input() configurationGroup: FormGroup;
  @Input() labelText: string;
  @Input() formArrayName: string;
  @Input() validators: ValidatorFn[];

  constructor(
    private configurationFormService: ConfigFormService
  ) { }

  get formArray() {
    return (<FormArray>this.configurationGroup.get(this.formArrayName));
  }

  addToList(newItem: any) {
    if (!this.configurationFormService.getStringValue(newItem)) {
      return;
    }
    if (!this.validators) {
      this.validators = [Validators.required];
    }
    if (newItem) {
      const control = new FormControl(newItem, this.validators);
      this.formArray.insert(0, control);
      this.formArray.get('0').markAsDirty();
    }
  }

  removeFromList(index: number) {
    this.formArray.removeAt(index);
    this.configurationGroup.markAsDirty();
  }

  // needed for validation
  getControl(i: string) {
    return this.formArray.controls[i];
  }
}

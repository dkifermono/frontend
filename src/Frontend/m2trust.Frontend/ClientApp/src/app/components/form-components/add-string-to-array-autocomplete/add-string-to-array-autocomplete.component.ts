import { Component, Input } from '@angular/core';
import {FormGroup, FormArray,  ValidatorFn } from '@angular/forms';
import { ProfileTemplateDto } from '../../../common';
import { ProfileTemplateConfigService, ProfileTemplateListService, ProfileTemplateMessage, ConfigFormService } from '../../../common/services';

@Component({
  selector: 'app-add-string-to-array-autocomplete',
  templateUrl: 'add-string-to-array-autocomplete.component.html'
})

export class AddStringToArrayAutocompleteComponent {
  @Input() group: FormGroup | FormArray;
  @Input() listTitle: string;
  @Input() itemTitle: string;
  @Input() buttonTitle: string;
  @Input() ArrayName: string;
  @Input() validators: ValidatorFn[];
  @Input() type: string;

  constructor(
    private profileTemplateConfigService: ProfileTemplateConfigService,
    private profileTemplateMessageService: ProfileTemplateListService,
    private configFormService: ConfigFormService
  ) { }

  get stringFormArray() {
    const formArray = this.group.get(this.ArrayName);
    return (<FormArray>formArray);
  }

  getControl(index): FormGroup {
    return (<FormGroup>this.stringFormArray.controls[index]);
  }

  removeFromList(array: FormArray, index: number) {
    // on remove from profile template list, that profile template should be added to suggested list
    const profileTemplate = this.getControl(index).getRawValue();
    const profileDto = new ProfileTemplateDto({name: profileTemplate.name, profileTemplateId: profileTemplate.id});
    this.profileTemplateMessageService.addToSuggestedList(new ProfileTemplateMessage(profileTemplate, this.type));

    array.removeAt(index);
    this.configFormService.validateAllFormFields(this.stringFormArray);
  }

  addToList(array: FormArray) {
    array.push(this.profileTemplateConfigService.createProfileTemplateIdForm(new ProfileTemplateDto({name: null, profileTemplateId: null }), this.validators));
    this.configFormService.validateAllFormFields(this.stringFormArray);
  }
}


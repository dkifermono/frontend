import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { ProfileTemplateDto } from '../../../common';
import { ProfileTemplateListService, ProfileTemplateMessage } from '../../../common/services';

@Component({
  selector: 'app-autocomplete-pair',
  templateUrl: 'autocomplete-pair.component.html'
})

export class AutocompletePairComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() labelText: string;
  @Input() profileTemplates: ProfileTemplateDto[];
  @Input() formArray: FormArray;
  @Input() itemIndex: any;
  @Input() type: string;

  suggestions: string[];
  pairList: ProfileTemplateDto[];


  constructor(
    private profileTemplateService: ProfileTemplateListService
  ) { }

  ngOnInit() {
    this.suggestions = this.profileTemplateService.getSuggestionList(this.group.get('id').value, this.type);
    this.pairList = this.profileTemplateService.getProfileTemplateLookup(this.type);
  }

  onSelect(event) {
      this.setValue(event);
  }

  onBlur(event) {
      if (event.query) {
          this.suggestions = this.profileTemplateService.getSuggestionList(event.query, this.type);
      }
      this.setValue(event.target.value);
  }

  onGetSuggestionList(event) {
      this.suggestions = this.profileTemplateService.getSuggestionList(event.query, this.type);
  }

  private setValue(event): boolean {
    this.pairList = this.profileTemplateService.getProfileTemplateLookup(this.type);
    this.group.get('id').markAsTouched();
    if (this.pairList && this.pairList.length > 0) {
        const profileTemplate = this.pairList.find(pt => pt.name === event || pt.profileTemplateId === event);
        if (profileTemplate) {
            this.group.get('id').setValue(profileTemplate.profileTemplateId);
            this.profileTemplateService.removeFromSuggestedList(new ProfileTemplateMessage(profileTemplate, this.type));
            return true;
        } else {
            this.group.get('id').setValue(event);
            return false;
        }
    } else {
      this.group.get('id').setValue(event);
      return false;
    }
  }
}

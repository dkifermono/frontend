import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-page-text-item',
  templateUrl: 'page-text-item.component.html'
})

export class PageTextItemComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() labelText: FormGroup;
  constructor() { }

  ngOnInit() { }
}

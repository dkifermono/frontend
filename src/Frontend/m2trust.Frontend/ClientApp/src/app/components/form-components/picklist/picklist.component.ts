import { Component, OnInit, Input } from '@angular/core';
import { ObjectUnsubscribedError } from 'rxjs';

@Component({
  selector: 'app-picklist',
  templateUrl: 'picklist.component.html'
})

export class PicklistComponent implements OnInit {
  @Input() listTitle: string;
  @Input() source: any;
  @Input() target: any;
  @Input() filterBy: string[] = ['name'];
  @Input() sourceHeader: string = 'Available';
  @Input() targetHeader: string = 'Target';
  @Input() sourceFilterPlaceholder: string;
  @Input() targetFilterPlaceholder: string;
  @Input() dragdrop: boolean = false;
  @Input() labelField: string = 'name';
  @Input() showTargetControls: boolean = false;

  constructor() { }

  ngOnInit() {
    this.target = this.target ? this.target : [];
    this.source = this.source ? this.source : [];
  }

  sortSourceList() {
    this.source.sort((a, b) => a.name.localeCompare(b.name));
  }
}


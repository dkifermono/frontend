import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import {
  ProfileClient,
  ProfileStubDto,
  ProfileItemStubDto,
  SectionType2,
  WizardData,
  PageHeaderData,
  ActivityTypes
} from '../../common';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {ErrorNotificationService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-select-profile',
  templateUrl: 'select-profile.component.html'
})
export class SelectProfileComponent implements OnInit {
  @Input() noDataMessage: string;
  @Input() nextStepUrl: string;
  @Input() pageTitle: string;
  @Input() stepTitle: string;
  @Input() sectionType: SectionType2;
  @Input() activityType: string;
  @Input() firstStepUrl: string;
  profileStub: ProfileStubDto;
  selectedProfile;
  expandedProfiles: string[] = Array();
  private userId: string;
  private activeProfiles: ProfileItemStubDto[];
  public isLoading = true;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private spinnerService: Ng4LoadingSpinnerService,
    private errorNotificationService: ErrorNotificationService,
    private profileClient: ProfileClient,
    private wizardData: WizardData,
    private routingStateService: RoutingStateService,
  ) {}

  ngOnInit() {
    this.wizardData.clearDataCollection();
    this.route.params.subscribe(params => {
      this.spinnerService.show();
      this.isLoading = true;
      this.userId = params['userId'];
      this.profileClient.getProfileStubs(this.sectionType, this.userId, this.activityType).subscribe(
        result => {
          this.profileStub = result;
          if (this.profileStub && this.profileStub.profiles) {
            this.activeProfiles = this.profileStub.profiles
              .filter(x =>
                this.profileStub.eligibleProfilesStates.find(
                  y => y.toString() === x.status.toString()
                )
              );
          }

          if (this.activeProfiles && this.activeProfiles.length > 0) {
            const profile = this.activeProfiles[0];
            if ((this.activeProfiles.length === 1) || (this.activeProfiles.length > 1 && this.profileStub.selectDefaultProfile)) {
              this.selectedProfile = profile;
            }
          }

          this.setSelectedProfileFromWizard();
        },
        error => {
          this.errorNotificationService.notificationError(error);
          this.spinnerService.hide();
        },

        () => {
          this.spinnerService.hide();
          this.isLoading = false;
        }
      );
    });
    const pageHeaderData = this.wizardData.getPageHeaderData();
    this.wizardData.setPageHeaderData(new PageHeaderData(pageHeaderData.userName));


  }

  setSelectedProfileFromWizard() {
    const wizardProfileId = this.wizardData.getProfileTemplateId();
    if (wizardProfileId) {
      const selected = this.activeProfiles.filter(a => a.id === wizardProfileId);
      if (selected.length > 0) {
        this.onProfileSelectionChange(selected[0]);
      }
    }
  }

  onProfileSelectionChange(item: ProfileItemStubDto) {
    if (!this.isProfileDisabled(item)) {
      this.selectedProfile = item;
    }
  }

  toogleDetailsVisibility(item: ProfileItemStubDto, $event: Event) {
    $event.stopPropagation();
    const index = this.expandedProfiles.indexOf(item.id, 0);
    if (index > -1) {
      this.expandedProfiles.splice(index, 1);
    } else {
      this.expandedProfiles.push(item.id);
    }
  }

  isProfileSelected(item: ProfileItemStubDto) {
    if (this.selectedProfile) {
      return this.selectedProfile.id === item.id;
    }
    return false;
  }

  isProfileDisabled(item: ProfileItemStubDto) {
    return (
      this.profileStub.eligibleProfilesStates.filter(
        y => y.toString() === item.status.toString()
      ).length === 0
    );
  }

  goToNextStep(profile: any) {
    const profileId = ActivityTypes.smartcard.includes(this.activityType) ? profile.smartcardId : profile.id;
    if (this.userId == null || profileId == null) {
      return;
    }
    const pageHeaderData = this.wizardData.getPageHeaderData();
    this.wizardData.setProfileTemplateId(profile.id);
    this.wizardData.setPageHeaderData(new PageHeaderData(pageHeaderData.userName, this.selectedProfile.name));
    this.router.navigate([this.nextStepUrl, this.userId, profileId], { relativeTo: this.route });
  }
}

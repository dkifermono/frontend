import { Component, Input } from '@angular/core';
import { CertificateDto } from '../../common';

@Component({
    selector: 'app-certificate-list',
    templateUrl: 'certificate-list.component.html'
})


export class CertificateListComponent {
    @Input() certificates: CertificateDto[];

    constructor() { }
}

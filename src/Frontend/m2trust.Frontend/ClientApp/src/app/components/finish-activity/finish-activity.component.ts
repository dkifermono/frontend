import { Component, OnInit, Input } from '@angular/core';
import {Router } from '@angular/router';
import { WizardData, AuditResult } from '../../common';
import { ErrorNotificationService } from '../../common/services';

@Component({
  selector: 'app-finish-activity',
  templateUrl: 'finish-activity.component.html'
})

export class FinishActivityComponent implements OnInit {
  error: any;
  errorMessage: string;

  @Input() auditResult: AuditResult;

  constructor(
    private router: Router,
    private wizardData: WizardData,
    private errorNotificationService: ErrorNotificationService) { }

  ngOnInit() {
    this.wizardData.clearWizard();
    this.error = JSON.parse(this.wizardData.getErrorData());
    if (this.error) {
      this.errorMessage = this.errorNotificationService.getErrorMessage(this.error);
    }
  }

  goToBegining() {
    this.wizardData.clearWizard();
    this.router.navigate([this.auditResult.url]);
  }
}

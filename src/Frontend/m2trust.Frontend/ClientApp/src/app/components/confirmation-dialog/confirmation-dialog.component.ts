import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { WizardData,  } from '../../common';
import { CancelActivityService } from '../../common/services';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: 'confirmation-dialog.component.html'
})

export class ConfirmationDialogComponent implements OnInit {

  constructor(
    private router: Router,
    private confirmationService: ConfirmationService,
    private wizardData: WizardData,
    private cancelActivityService: CancelActivityService
    ) {}

    ngOnInit() {
      this.cancelActivityService.isNavigationStart.subscribe(message => {
        if (message) {
          this.cancelCurrentActivity();
        }
      });
      this.cancelActivityService.canNavigate.subscribe(message => {
        this.router.navigate([message]);
      });
    }


    cancelCurrentActivity() {
      this.confirmationService.confirm({
        header: '',
        icon: null,
        message: '',
        accept: () => {
          this.wizardData.clear();
          this.cancelActivityService.changeConfirmation(true);
        },
        reject: () => {
          this.cancelActivityService.changeConfirmation(false);
        }
      });
    }
}


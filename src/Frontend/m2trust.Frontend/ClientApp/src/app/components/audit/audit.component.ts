import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  FinishRequestClient,
  RequestSuccessDto,
  AuditRequestDto,
  DciDataDto,
  DciDataDtoSectionType,
  WizardData,
  DataItemDto,
  SigningResult,
  DataCollectionDto
} from '../../common';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {ErrorNotificationService, NotificationService, CreateSignatureService } from '../../common/services';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-audit',
  templateUrl: 'audit.component.html'
})
export class AuditComponent implements OnInit {
  @Input() sectionType: DciDataDtoSectionType;
  @Input() pageTitle: string;
  @Input() stepTitle: string;
  @Input() useHeader = true;
  @Input() activityType: string;
  @Input() firstStepUrl: string;
  @Output() submit = new EventEmitter<AuditRequestDto>();
  requestSuccessDto: RequestSuccessDto;
  auditRequestDto: AuditRequestDto;
  auditData: DciDataDto;
  isLoading = true;
  cachedData: DataCollectionDto;
  dataItemDtoArray: DataItemDto[];
  signingResult: SigningResult;
  templateOid: string;
  certificateEkuOid: string;
  isFinishDisabled = true;

  constructor(
    private route: ActivatedRoute,
    private finishRequestClient: FinishRequestClient,
    private spinnerService: Ng4LoadingSpinnerService,
    private wizardData: WizardData,
    private notificationService: NotificationService,
    private errorNotificationService: ErrorNotificationService,
    private createSignatureService: CreateSignatureService,
    private translate: TranslateService
  ) {}

    ngOnInit() {
    this.route.params.subscribe(params => {
      this.spinnerService.show();
      this.isLoading = true;
      const userId = params['userId'];
      const profileTemplateId = params['profileTemplateId'];
      const profileId = params['profileId'];
      this.cachedData = this.wizardData.getDataCollection();
      const temp = {};
      if (this.cachedData && this.cachedData.dataCollectionItems) {
        this.cachedData.dataCollectionItems.forEach(item => {
          temp[item.name] = item.value;
        });
        this.dataItemDtoArray = this.cachedData.dataCollectionItems.map(item => {
          return new DataItemDto( {name: item.name, value: item.value});
       });
      }
      this.auditData = new DciDataDto({
        userId: userId,
        profileId: profileId,
        profileTemplateId: profileTemplateId,
        items: temp,
        sectionType: this.sectionType,
        hasSignature: (this.cachedData && this.cachedData.hasSignature) ? this.cachedData.hasSignature : false
      });

      this.finishRequestClient
        .audit(this.auditData, this.activityType)
        .subscribe(response => {
          this.auditData = response;
          this.templateOid = response.signatureTemplateOid;
          this.certificateEkuOid = response.signatureEkuOid;
          this.auditRequestDto = new AuditRequestDto({
            userId: this.auditData.userId,
            profileTemplateId: this.auditData.profileTemplateId,
            profileId: this.auditData.profileId,
            dataCollections: this.dataItemDtoArray,
            originatorId: this.auditData.originatorId,
          });
         if (this.auditData.usesSignature) {
          this.signingResult = new SigningResult();
          if  (window['Windows'] === undefined) {
            this.notificationService.error(
              this.translate.instant('ErrorMessage.Signing.Title'),
              this.translate.instant('ErrorMessage.Signing.ClientApp')
            );
          } else {
            this.getSignature();
            this.isFinishDisabled = false;
          }
          } else {
           this.isFinishDisabled = false;
          }
          const wizardDataComment = this.wizardData.getAuditComment();
          if (wizardDataComment) {
            this.auditRequestDto.comment = wizardDataComment;
          }
        },
        error => {
          this.errorNotificationService.notificationError(error);
          this.spinnerService.hide();
        },
        () => {
          this.isLoading = false;
          this.spinnerService.hide();
        });
    });
  }

   submitRequest() {
     if (this.signingResult !== undefined) {
       if (this.signingResult.signedData != null) {
        this.auditRequestDto.signature = this.signingResult.signedData;
       }
     }
    this.wizardData.setAuditComment(this.auditRequestDto.comment);
    this.submit.emit(this.auditRequestDto);
  }

  async getSignature() {
    let result: SigningResult;
    if (this.templateOid && this.templateOid !== "") {
      result = await this.createSignatureService.getSignature(this.templateOid, this.cachedData.signatureDciKey, this.auditData, this.cachedData.dataCollectionItems);
    } else {
      result = await this.createSignatureService.getSignatureFromTemplateEkuOid(this.certificateEkuOid, this.cachedData.signatureDciKey, this.auditData, this.cachedData.dataCollectionItems);
    }
    this.signingResult = result;
    if (this.signingResult.returnCode === 0) {
      this.notificationService.error(
        this.translate.instant('ErrorMessage.Signing.Title'),
        this.translate.instant('ErrorMessage.Signing.NoCert'));
    } else if (this.signingResult.returnCode < 0) {
      this.notificationService.error(
        this.translate.instant('ErrorMessage.Signing.Title'),
        this.translate.instant('ErrorMessage.Signing.MultipleCert'));
    }
  }
}

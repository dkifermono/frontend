import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { LanguageService } from '../../common/services';
import { LocaleSettings, Calendar } from 'primeng/calendar';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { dateValidator } from '../../common';

@Component({
  selector: 'app-date-picker',
  templateUrl: 'date-picker.component.html'
})

export class DatePickerComponent implements OnInit {
  value: Date;

  language = 'en';

  primeCalendarFormat = {
    de: 'dd.mm.yy',
    en: 'mm/dd/yy'
  };
  momentDateFormat = {
    de: 'DD.MM.YY',
    en: 'MM/DD/YY'
  };
  pipeDateFormat = {
    de: 'dd.MM.yy',
    en: 'MM/dd/yy'
  };
  format = 'mm/dd/yy';

  isIE = false;

  enLocale: LocaleSettings = {
    firstDayOfWeek: 0,
    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    monthNames: [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ],
    monthNamesShort: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ],
    today: 'Today',
    clear: 'Clear'
  };

  deLocale: LocaleSettings = {
    firstDayOfWeek: 1,
    dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
    dayNamesShort: ['Son', 'Mon', 'Die', 'Mit', 'Don', 'Fre', 'Sam'],
    dayNamesMin:  ['S', 'M', 'D', 'M ', 'D', 'F ', 'S'],
    monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
    monthNamesShort: ['Jan', 'Feb', 'MÃ¤r', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
    today: 'Heute',
    clear: 'Klären'
  };

  locale = this.enLocale;

  @Input() control: FormControl;
  @Input() group: FormGroup;
  @Input() controlName = '';

  @ViewChild('myCalendar')
  private myCalendar: Calendar;

  dateModel: string;
  pipe: DatePipe;


  constructor(private languageService: LanguageService) {
    this.setLocale(languageService.initLanguage);
  }

  private setLocale(language: string) {
    this.language = language;
    if (language === 'en') {
      this.locale = this.enLocale;
      this.format = this.primeCalendarFormat.en;
    } else {
      this.locale = this.deLocale;
      this.format = this.primeCalendarFormat.de;
    }
    this.pipe = new DatePipe('en');
  }

  ngOnInit() {
    this.languageService.currentMessage.subscribe(language => {
        this.language = language;
        this.setLocale(language);
        this.adjustInput();
        this.control.markAsTouched();
        this.control.markAsUntouched();
    });
    this.isIE = this.isAgentIE();
    this.adjustInput();
   }


  onDateModelChange() {
    this.control.setValue(this.dateModel);
    this.control.updateValueAndValidity();
    this.control.markAsTouched();
    this.control.markAsDirty();
    if (this.control.valid) {
      this.control.setValue(moment(this.dateModel, this.momentDateFormat[this.language]));
    }
  }

  private adjustInput() {
    if (this.isIE) {
      this.dateModel = this.formatDate(this.control.value);
      this.setControlValidator();
    } else {
      setTimeout(() => {
        this.myCalendar.updateInputfield();
      });
    }
  }

  private setControlValidator() {
    setTimeout(() => {
      this.control.setValidators(dateValidator(this.momentDateFormat[this.language]));
    });
  }

  private formatDate(date: string): string | null {
    const previousFormat = this.language === 'en' ? this.momentDateFormat.de : this.momentDateFormat.en;
    if (date && (moment(date, this.momentDateFormat.en, true).isValid() || moment(date, this.momentDateFormat.de, true).isValid())) {
      return this.pipe.transform(moment(date, previousFormat), this.pipeDateFormat[this.language]);
    } else {
      return date;
    }
  }

  private isAgentIE(): boolean {
    const ua = window.navigator.userAgent;
    const isIE = /MSIE|Trident/.test(ua);
    return isIE;
  }




}


import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ValidationMessage } from '../../common';

@Component({
  selector: 'app-validation-summary',
  templateUrl: 'validation-summary.component.html'
})
export class ValidationSummaryComponent implements OnInit {
  @Input() form: FormGroup;
  errors: ValidationMessage[] = [];

  constructor() {}

  ngOnInit() {
    this.form.statusChanges.subscribe(status => {
      this.resetErrorMessages();
      this.generateErrorMessages(this.form);
    });
  }

  resetErrorMessages() {
    this.errors.length = 0;
  }

  generateErrorMessages(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(controlName => {
      const control = formGroup.controls[controlName];
      if (control.invalid && control.touched) {
        const errors = control.errors;
        if (errors === null || errors.count === 0) {
          return;
        }
        // Handle the 'required' case
        if (errors.required) {
          this.errors.push(
            new ValidationMessage('DciValidation.DciRequired', {
              controlName: controlName
            })
          );
        }
        if (errors.pattern) {
          this.errors.push(
            new ValidationMessage('DciValidation.DciRegexDoesNotMatch', {
              controlName: controlName
            })
          );
        }

        if (errors.numberValidator || errors.intRegex) {
          this.errors.push(
            new ValidationMessage('DciValidation.DciNotANumber', {
              controlName: controlName
            })
          );
        }
        if (errors.dateValidator) {
          this.errors.push(
            new ValidationMessage('DciValidation.DciNotADate', {
              controlName: controlName
            })
          );
        }
      }
    });
  }
}

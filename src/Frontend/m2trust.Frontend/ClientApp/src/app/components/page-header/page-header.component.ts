import { Component, Input, OnInit } from '@angular/core';
import {
  UserEntryDto,
  ProfileTemplateDto,
  PageHeaderData,
  WizardData
} from '../../common';

@Component({
  selector: 'app-page-header',
  templateUrl: 'page-header.component.html'
})
export class PageHeaderComponent implements OnInit {
  @Input() pageTitle: string;
  @Input() stepTitle: string;
  @Input() stepDescription: string;
  isLoading = true;
  user: UserEntryDto;
  profileTemplate: ProfileTemplateDto;
  headerData: PageHeaderData = null;

  constructor(
    private wizardData: WizardData
  ) {}

  ngOnInit() {
    this.headerData = this.wizardData.getPageHeaderData();
  }
}

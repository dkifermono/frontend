import { Component, OnInit, Input } from '@angular/core';
import { WizardData, PageHeaderData } from '../../common';

@Component({
  selector: 'app-rp-page-header',
  templateUrl: 'rp-page-header.component.html'
})

export class RevokeProfilePageHeaderComponent implements OnInit {
  headerData: PageHeaderData;
  constructor(
    private wizardData: WizardData
  ) { }

  @Input() stepTitle: string;
  @Input() headTitle: string;

  ngOnInit() {
    this.headerData = this.wizardData.getPageHeaderData();
  }
}

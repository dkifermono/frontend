import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import * as moment from 'moment';

import { WizardData, ConfigurationService } from './common';
import { RoutingStateService, ErrorNotificationService, LanguageService } from './common/services';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ WizardData, RoutingStateService]
})
export class AppComponent implements OnInit {
  isMenuVisible = true;
  public options = {
    position: ['bottom', 'right']
  };
  language: string;

  url = '/home';

  constructor(private routingState: RoutingStateService,
    private router: Router,
    private configurationService: ConfigurationService,
    private errorService: ErrorNotificationService,
    private languageService: LanguageService) {
    this.loadCssScript();
    this.navigate();
    routingState.loadRouting();
    this.getPageTextConfig();
  }


  ngOnInit(): void {
    this.languageService.currentMessage.subscribe(value => {
      this.language = value;
    });
    this.url = this.routingState.getNextUrl();
  }


  onMenuChangeVisibility(isVisible: boolean) {
    this.isMenuVisible = isVisible;
  }

  private loadCssScript() {
    const random = (moment().format('MMMMMDoYYYYhhmmss'));
    const scriptElement = document.createElement('link');
    scriptElement.type = 'text/css';
    scriptElement.href = 'api/Configuration/GetColorVariables';
    scriptElement.rel = 'stylesheet';

    document.getElementsByTagName('head')[0].appendChild(scriptElement);
  }

  private navigate() {
    const url = window.location.href;
    if (url) {
      const paths = this.router.config.map(p => p['path']);
      const rootPath = paths.find(path => path && url.includes(path));
      if (rootPath !== 'administration') {
        this.router.navigate(['/' + rootPath]);
      }
    } else {
      this.router.navigate(['/home']);
    }
  }


  private getPageTextConfig() {
    this.configurationService.getTextConfiguration()
      .subscribe(response => {
        localStorage.setItem('pageTextConfiguration', JSON.stringify(response));
        this.languageService.changeMessage(localStorage.getItem('selectedLanguage'));
      },
      error => this.errorService.notificationError(error));
  }

}

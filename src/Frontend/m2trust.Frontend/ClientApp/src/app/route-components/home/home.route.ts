import { Component, OnInit } from '@angular/core';
import { LanguageService, LogoService } from '../../common/services';

@Component({
  selector: 'app-home',
  templateUrl: './home.route.html'
})
export class HomeRouteComponent implements OnInit {
  welcomeText = '';
  helpDeskInstruction = '';
  contactAddress = '';
  selectedLanguage = localStorage.getItem('selectedLanguage');
  imagePath: string;

  constructor(private languageService: LanguageService,
    private logoService: LogoService) { }

  async ngOnInit() {
    this.languageService.currentMessage.subscribe(value => {
      this.selectedLanguage = value;
      this.getPageText();

    });
    this.getPageText();
    this.imagePath = await this.logoService.getImagePath('home');
  }

  getPageText() {
    const pageTextConfig = localStorage.getItem('pageTextConfiguration');
    if (pageTextConfig && JSON.parse(pageTextConfig).welcomeText) {
      const config = JSON.parse(pageTextConfig);
      if (config.welcomeText) {
        this.welcomeText = config.welcomeText[this.selectedLanguage];
      }
      if (config.helpDeskInstruction) {
        this.helpDeskInstruction = config.helpDeskInstruction[this.selectedLanguage];
      }
      if (config.contactAddress) {
        this.contactAddress = config.contactAddress[this.selectedLanguage];
      }
    }
  }
  }



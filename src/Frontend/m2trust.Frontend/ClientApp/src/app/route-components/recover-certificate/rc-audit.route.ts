import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  FinishRequestClient,
  DciDataDtoSectionType,
  WizardData,
  AuditRequestDto,
  IAuditRequestDto,
  AuditResult,
  CanComponentDeactivate
} from '../../common';
import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-recover-cert-audit',
  templateUrl: 'rc-audit.route.html'
})
export class RecoverCertAuditRouteComponent extends CanComponentDeactivate implements OnDestroy {
  sectionType = DciDataDtoSectionType.Recover;
  audit: IAuditRequestDto;
  activityType: string;
  firstStepUrl = '/recover-certificate';
  description = '';
  otp: string;
  isSuccessful = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private finishRequestClient: FinishRequestClient,
    private wizardData: WizardData,
    private finishService: FinishActivityService,
    private spinner: Ng4LoadingSpinnerService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  onSubmit($event) {
    this.spinner.show();
    this.activityType = 'RecoverCertificate';
    this.audit = {
      userId: $event.userId,
      profileTemplateId: $event.profileTemplateId,
      comment: $event.comment,
      profileId: $event.profileId,
      dataCollections: $event.dataCollections,
      signature: $event.signature
    };
    this.finishRequestClient
      .profileRecover(new AuditRequestDto(this.audit), this.activityType)
      .subscribe(
        response => {
          this.description = response.hasSignature ? 'FinishRequest.Recover.SuccessSignatureDescription' : 'FinishRequest.Recover.SuccessDescription';
          this.isSuccessful = true;
          this.otp = response.oneTimePassword;
          this.spinner.hide();
         this.navigateToFinish();

        },
        error => {
          this.wizardData.setErrorData(error);
          this.isSuccessful = false;
          this.description = 'FinishRequest.Recover.FailedDescription';
          this.spinner.hide();
          this.navigateToFinish();
        }
      );
  }

  private navigateToFinish() {
    this.router.navigate(['/recover-certificate/finish'], {
      relativeTo: this.route
    });
  }

  ngOnDestroy() {
    this.finishService.auditResult = new AuditResult(this.description, this.firstStepUrl, this.isSuccessful, this.otp);
  }
}

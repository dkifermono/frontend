import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CanComponentDeactivate } from '../../common';
import {
  CancelActivityService,
  RoutingStateService
} from '../../common/services';


@Component({
  selector: 'app-recover-cert-user-list',
  templateUrl: 'rc-user-list.route.html'
})

export class RecoverCertUserListRouteComponent extends CanComponentDeactivate {

  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  onFormStatusChange($event: FormGroup) {
    this.form = $event;
  }
}

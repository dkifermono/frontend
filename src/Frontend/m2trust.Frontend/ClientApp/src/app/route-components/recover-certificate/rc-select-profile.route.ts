import { Component, OnInit } from '@angular/core';
import {
  SectionType2,
  CanComponentDeactivate
} from '../../common';
import {
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-recover-cert-select-profile',
  templateUrl: 'rc-select-profile.route.html'
})

export class RecoverCertSelectProfileRouteComponent extends CanComponentDeactivate implements OnInit {
  sectionType = SectionType2.Recover;
  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  ngOnInit() {}
}

import { Component,  OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  IAuditRequestDto,
  AuditResult,
  WizardData,
  FinishRequestClient,
  AuditRequestDto,
  CanComponentDeactivate
} from '../../common';
import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-rvc-reason',
  templateUrl: 'rvc-reason.route.html'
})

export class RevokeCertificateReasonRouteComponent extends CanComponentDeactivate implements OnDestroy, OnInit {
  audit: IAuditRequestDto;
  activityType: string;
  firstStepUrl = '/revoke-certificate';
  description = '';
  isSuccessful = false;
  otp: string;
  effectiveRevocation: number;
  auditData: {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private finishRequestClient: FinishRequestClient,
    private wizardData: WizardData,
    private finishService: FinishActivityService,
    private spinner: Ng4LoadingSpinnerService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  ngOnInit() {
    this.route.paramMap.pipe(map(() => window.history.state)).subscribe(s => {
      this.auditData = s.audit;
    });
  }

  navigateToNextPage($event) {
    this.spinner.show();
    this.activityType = 'RevokeCertificate';
    this.audit = {
      userId: $event.userId,
      profileTemplateId: $event.profileTemplateId,
      comment: this.auditData['comment'],
      profileId: $event.profileId,
      dataCollections: $event.dataCollections,
      effectiveRevocationInHours: this.effectiveRevocation,
      signature: $event.signature
    };
    this.finishRequestClient
    .profileRevoke(new AuditRequestDto(this.audit), this.activityType)
    .subscribe(
      response => {
        this.otp = response.oneTimePassword;
        this.description = response.hasSignature ? 'FinishRequest.Revoke.SuccessSignatureDescription' : 'FinishRequest.Revoke.SuccessDescription';
        this.isSuccessful = true;
        this.spinner.hide();
        this.navigateToFinish();
      },
      error => {
        this.wizardData.setErrorData(error);
        this.isSuccessful = false;
        this.description = 'FinishRequest.Revoke.FailedDescription';
        this.spinner.hide();
        this.navigateToFinish();
      }
    );
  }

  private navigateToFinish() {
    this.router.navigate(['/revoke-certificate/finish'], {
      relativeTo: this.route
    });
  }


  ngOnDestroy() {
    this.finishService.auditResult = new AuditResult(this.description, this.firstStepUrl, this.isSuccessful, this.otp);
  }
}

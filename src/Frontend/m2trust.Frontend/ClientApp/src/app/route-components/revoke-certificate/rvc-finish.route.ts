import { Component, OnInit } from '@angular/core';
import { AuditResult } from '../../common';
import { FinishActivityService } from '../../common/services';

@Component({
  selector: 'app-rvc-finish',
  templateUrl: 'rvc-finish.route.html'
})

export class RevokeCertificateFinishRouteComponent implements OnInit {
  auditResult: AuditResult;
  loadComponent = false;
  constructor(private finishService: FinishActivityService) { }

  ngOnInit() {
    this.auditResult = this.finishService.auditResult;
    this.loadComponent = true;
   }
}

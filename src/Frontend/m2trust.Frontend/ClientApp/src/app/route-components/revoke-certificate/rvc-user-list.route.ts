import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CanComponentDeactivate } from '../../common';
import { CancelActivityService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-rvc-user-list',
  templateUrl: 'rvc-user-list.route.html'
})

export class RevokeCertificateUserListRouteComponent extends CanComponentDeactivate implements OnInit {
  additionalDisplayProperties = ['operatingSystem'];
  additionalSearchProperties = ['operatingSystem'];
  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  ngOnInit() { }

  onFormStatusChange($event: FormGroup) {
    this.form = $event;
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SectionType, CanComponentDeactivate } from '../../common';
import { CancelActivityService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-rvc-data-collection',
  templateUrl: 'rvc-data-collection.route.html'
})

export class RevokeCertificateDataCollectionComponent extends CanComponentDeactivate implements OnInit {
  sectionType = SectionType.Revoke;
  private userId: string;
  private profileId: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.userId = params['userId'];
      this.profileId = params['profileId'];
    });
  }

  onNavigateToNextPage() {
    this.router.navigate(
      [
        '/revoke-certificate/audit',
        this.userId,
        this.profileId
      ],
      { relativeTo: this.route }
    );
  }
}

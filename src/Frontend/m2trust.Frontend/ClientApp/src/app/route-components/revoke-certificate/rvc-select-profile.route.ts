import { Component, OnInit } from '@angular/core';
import { SectionType2, CanComponentDeactivate } from '../../common';
import { CancelActivityService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-rvc-select-profile.route.ts',
  templateUrl: 'rvc-select-profile.route.html'
})

export class RevokeCertificateSelectProfileRouteComponent extends CanComponentDeactivate implements OnInit {
  sectionType = SectionType2.Revoke;
  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  ngOnInit() { }
}

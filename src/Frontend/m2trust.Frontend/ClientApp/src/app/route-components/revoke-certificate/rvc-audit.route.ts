import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  DciDataDtoSectionType,
  IAuditRequestDto,
  FinishRequestClient,
  AuditRequestDto,
  WizardData,
  AuditResult,
  CanComponentDeactivate
} from '../../common';
import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-rvc-audit',
  templateUrl: 'rvc-audit.route.html'
})

export class RevokeCertificateAuditComponent extends CanComponentDeactivate implements OnDestroy {
  sectionType = DciDataDtoSectionType.Revoke;
  audit: IAuditRequestDto;
  activityType: string;
  firstStepUrl = '/revoke-certificate';
  description = '';
  isSuccessful = false;
  otp: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private finishRequestClient: FinishRequestClient,
    private wizardData: WizardData,
    private finishService: FinishActivityService,
    private spinner: Ng4LoadingSpinnerService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  onSubmit($event) {
    this.spinner.show();
    this.activityType = 'RevokeCertificate';
    this.audit = {
      userId: $event.userId,
      profileTemplateId: $event.profileTemplateId,
      comment: $event.comment,
      profileId: $event.profileId,
      dataCollections: $event.dataCollections,
      signature: $event.signature
    };

    this.finishRequestClient.allowSpecifyDelay(new AuditRequestDto(this.audit), this.activityType).subscribe(
      result => {
        if (result === false) {
          this.finishRequestClient.profileRevoke(new AuditRequestDto(this.audit), this.activityType).subscribe(
            response => {
              this.otp = response.oneTimePassword;
              this.description = response.hasSignature ? 'FinishRequest.Revoke.SuccessSignatureDescription' : 'FinishRequest.Revoke.SuccessDescription';
              this.isSuccessful = true;
              this.spinner.hide();
              this.navigateToFinish();
            },
            error => {
              this.wizardData.setErrorData(error);
              this.isSuccessful = false;
              this.description = 'FinishRequest.Revoke.FailedDescription';
              this.spinner.hide();
              this.navigateToFinish();
            }
          );
        } else {
          this.spinner.hide();
          if ($event.signature) {
            this.router.navigate(
              ['/revoke-certificate/reason',
              $event.userId,
              $event.profileId,
              $event.signature], {
                relativeTo: this.route,
                state: { audit: this.audit }
              }
            );
          } else {
            this.router.navigate(
              ['/revoke-certificate/reason',
              $event.userId,
              $event.profileId], {
                relativeTo: this.route,
                state: { audit: this.audit }
              }
            );
          }
        }
      }
    );
  }

  private navigateToFinish() {
    this.router.navigate(['/revoke-certificate/finish'], {
      relativeTo: this.route
    });
  }

  ngOnDestroy() {
    this.finishService.auditResult = new AuditResult(this.description, this.firstStepUrl, this.isSuccessful, this.otp);
  }
}

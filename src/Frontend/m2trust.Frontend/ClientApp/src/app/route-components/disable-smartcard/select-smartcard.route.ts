import { Component, OnInit} from '@angular/core';
import { SectionType2, CanComponentDeactivate } from '../../common';
import { CancelActivityService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-select-smartcard',
  templateUrl: 'select-smartcard.route.html'
})

export class SelectSmartcardRouteComponent extends CanComponentDeactivate implements OnInit {
  sectionType = SectionType2.Revoke;
  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService)
   }

  ngOnInit() {}
}

import { Component, OnInit } from '@angular/core';
import { SectionType, CanComponentDeactivate } from '../../common';
import { Router, ActivatedRoute } from '@angular/router';
import { CancelActivityService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-disable-smart-data-collection',
  templateUrl: 'ds-data-collection.route.html'
})

export class DisableSmartDataCollectionRouteComponent extends CanComponentDeactivate implements OnInit {
  sectionType = SectionType.Revoke;
  private userId: string;
  private profileId: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService)
   }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.userId = params['userId'];
      this.profileId = params['profileId'];
    });
  }

  onNavigateToNextPage() {
    this.router.navigate(
      [
        '/disable-smartcard/audit',
        this.userId,
        this.profileId
      ],
      { relativeTo: this.route }
    );
  }
}

import { Component} from '@angular/core';
import { CanComponentDeactivate } from '../../common/guards-can-deactivate';
import { FormGroup, FormControl } from '@angular/forms';
import { CancelActivityService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-disable-smartcard',
  templateUrl: './disable-smartcard.route.html'
})

export class DisableSmartcardRouteComponent extends CanComponentDeactivate {
  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
    ) {
    super(cancelActivityService, routingStateService);
  }

  onStatusChange($event: FormGroup) {
    this.form = $event;
  }

}



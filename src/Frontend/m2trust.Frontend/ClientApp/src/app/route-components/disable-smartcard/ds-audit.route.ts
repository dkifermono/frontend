import { Component, OnDestroy } from '@angular/core';
import { FinishRequestClient, DciDataDtoSectionType, AuditRequestDto, IAuditRequestDto, WizardData, AuditResult, ContinueModel, CanComponentDeactivate } from '../../common';
import {FinishActivityService, CancelActivityService, RoutingStateService } from '../../common/services';
import { ActivatedRoute, Router,  } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-disable-smart-audit',
  templateUrl: 'ds-audit.route.html'
})

export class DisableSmartAuditRouteComponent extends CanComponentDeactivate implements OnDestroy {
  sectionType = DciDataDtoSectionType.Revoke;
  audit: IAuditRequestDto;
  activityType: string;
  auditResult: AuditResult;

  isContinue = false;
  url = '/disable-smartcard';
  isSuccessful = false;
  description = '';
  otp: string;
  userId: string;
  title =  'FinishRequest.Disable.Continue.PageTitle';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private finishRequestClient: FinishRequestClient,
    private wizardData: WizardData,
    private finishService: FinishActivityService,
    private spinner: Ng4LoadingSpinnerService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  onSubmit($event) {
    this.spinner.show();
    this.activityType = 'DisableSmartcard';
    this.audit = {
      userId: $event.userId,
      profileTemplateId: $event.profileTemplateId,
      comment: $event.comment,
      profileId: $event.profileId,
      dataCollections: $event.dataCollections,
      signature: $event.signature
    };

    this.finishRequestClient.allowSpecifyDelay(new AuditRequestDto(this.audit), this.activityType).subscribe(
      result => {
        if (result === false) {
          this.finishRequestClient.disableSmartcard(new AuditRequestDto(this.audit), this.activityType).subscribe(
            response => {
              this.isSuccessful = true;
              this.otp = response.otpResult.oneTimePassword;
              if (response.continue) {
                this.description = response.otpResult.hasSignature ? 'FinishRequest.Disable.Continue.SuccessSignatureDescription' : 'FinishRequest.Disable.Continue.SuccessDescription';
                this.url = '/enroll-user/select-profile-template';
                this.userId =  $event.userId;
                this.spinner.hide();
                this.navigateToContinue();
              } else {
                this.url = '/disable-smartcard';
                this.description = response.otpResult.hasSignature ? 'FinishRequest.Disable.SuccessSignatureDescription' : 'FinishRequest.Disable.SuccessDescription';
                this.spinner.hide();
                this.navigateToFinish();
              }
            },
            error => {
              this.spinner.hide();
              this.wizardData.setErrorData(error);
              this.isSuccessful = false;
              this.description = 'FinishRequest.Disable.FailedDescription';
              this.navigateToFinish();
            }
          );
        } else {
          this.spinner.hide();
          if ($event.signature) {
            this.router.navigate([
              '/disable-smartcard/reason',
              $event.userId,
              $event.profileId,
              $event.signature], {
                relativeTo: this.route,
                state: { audit: this.audit }
              }
            );
          } else {
            this.router.navigate([
              '/disable-smartcard/reason',
              $event.userId,
              $event.profileId], {
                relativeTo: this.route,
                state: { audit: this.audit }
              }
            );
          }
        }
      }
    );
  }

  private navigateToFinish() {
    this.router.navigate(['/disable-smartcard/finish'], {
      relativeTo: this.route
    });
  }

  private navigateToContinue() {
    this.isContinue = true;
    this.router.navigate(['/disable-smartcard/continue'], {
      relativeTo: this.route
    });
  }

  ngOnDestroy() {
    if (this.isContinue) {
      this.finishService.continueModel = new ContinueModel(this.title, this.description, this.url, this.userId, this.isSuccessful, this.otp);
    } else {
      this.finishService.auditResult = new AuditResult(this.description, this.url, this.isSuccessful, this.otp);
    }
  }
}

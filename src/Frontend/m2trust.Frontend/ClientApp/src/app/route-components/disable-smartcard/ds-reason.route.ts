import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  IAuditRequestDto,
  WizardData,
  FinishRequestClient,
  AuditRequestDto,
  ContinueModel,
  AuditResult,
  CanComponentDeactivate
} from '../../common';
import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';


@Component({
  selector: 'app-ds-reason',
  templateUrl: 'ds-reason.route.html'
})

export class DisableReasonRouteComponent extends CanComponentDeactivate implements OnDestroy, OnInit {
  audit: IAuditRequestDto;
  activityType: string;
  cachedData = [];
  effectiveRevocation: number;
  userId: string;

  title =  'FinishRequest.Disable.Continue.PageTitle';
  firstStepUrl = '/suspend-smartcard';
  description = '';
  isSuccessful = false;
  otp: string;
  url = '/disable-smartcard';
  isContinue = false;
  auditData = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private finishRequestClient: FinishRequestClient,
    private wizardData: WizardData,
    private finishService: FinishActivityService,
    private spinner: Ng4LoadingSpinnerService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  ngOnInit() {
    this.route.paramMap.pipe(map(() => window.history.state)).subscribe(s => {
      this.auditData = s.audit;
    });
  }

  navigateToNextPage($event) {
    this.wizardData.clearWizard();
    this.spinner.show();
    this.activityType = 'DisableSmartcard';
    this.userId = $event.userId;
    this.audit = {
      userId: $event.userId,
      profileId: $event.profileId,
      dataCollections: $event.dataCollections,
      effectiveRevocationInHours: this.effectiveRevocation,
      signature: $event.signature,
      comment: this.auditData['comment']
    };
    this.finishRequestClient.disableSmartcard(new AuditRequestDto(this.audit), this.activityType).subscribe(
      response => {
        this.isSuccessful = true;
        this.otp = response.otpResult.oneTimePassword;
        if (response.continue) {
          this.description = response.otpResult.hasSignature ? 'FinishRequest.Disable.Continue.SuccessSignatureDescription' : 'FinishRequest.Disable.Continue.SuccessDescription';
          this.url = '/enroll-user/select-profile-template';
          this.userId =  $event.userId;
          this.spinner.hide();
          this.navigateToContinue();
        } else {
          this.url = '/disable-smartcard';
          this.description = response.otpResult.hasSignature ? 'FinishRequest.Disable.SuccessSignatureDescription' : 'FinishRequest.Disable.SuccessDescription';
          this.spinner.hide();
          this.navigateToFinish();
        }
      },
      error => {
        this.spinner.hide();
        this.wizardData.setErrorData(error);
        this.isSuccessful = false;
        this.description = 'FinishRequest.Disable.FailedDescription';
        this.navigateToFinish();
      }
    );
  }

  private navigateToFinish() {
    this.router.navigate(['/disable-smartcard/finish'], {
      relativeTo: this.route
    });
  }

  private navigateToContinue() {
    this.isContinue = true;
    this.router.navigate(['/disable-smartcard/continue'], {
      relativeTo: this.route
    });
  }

  ngOnDestroy() {
    if (this.isContinue) {
      this.finishService.continueModel = new ContinueModel(this.title, this.description, this.url, this.userId, this.isSuccessful, this.otp);
    } else {
      this.finishService.auditResult = new AuditResult(this.description, this.url, this.isSuccessful, this.otp);
    }
  }
}


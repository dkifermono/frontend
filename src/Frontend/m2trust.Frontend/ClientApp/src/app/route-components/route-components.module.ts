import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppCommonModule } from '../common/common.module';
import { ComponentsModule } from '../components/components.module';
import { HomeRouteComponent } from './home';
import { ManageRequestsRouteComponent } from './manage-requests';
import {
  SelectSmartcardRouteComponent,
  DisableSmartcardRouteComponent,
  DisableSmartAuditRouteComponent,
  DisableSmartDataCollectionRouteComponent,
  DisableSmartcardFinishRouteComponent,
  DisableContinueRouteComponent,
  DisableReasonRouteComponent
} from './disable-smartcard';
import {
  EnrollUserOrCertificateRouteComponent,
  DataCollectionRouteComponent,
  SelectProfileTemplateRouteComponent,
  EnrollAuditRouteComponent,
  EnrollFinishRouteComponent
} from './enroll-user';
import {
  UnblockSmartcardRouteComponent,
  UnblockSelectProfileRouteComponent,
  UnblockSmartAuditRouteComponent,
  UnblockSmartDataCollectionRouteComponent,
  UnblockSmartChallengeComponent,
  UnblockSmartResponseComponent,
  UnblockSmartFinishRouteComponent
} from './unblock-smartcard';
import {
  ReinstateSmartcardRouteComponent,
  ReinstateSelectProfileRouteComponent,
  ReinstateDataCollectionRouteComponent,
  ReinstateSmartAuditRouteComponent,
  ReinstateSmartReasonComponent,
  ReinstateFinishRouteComponent
 } from './reinstate-smartcard';
import {
  RetireTemporarySmartcardRouteComponent,
  RetireSelectProfileRouteComponent,
  RetireDataCollectionRouteComponent,
  RetireAuditRouteComponent,
  RetireFinishRouteComponent
} from './retire-temporary-smardcard';
import {
  RecoverCertSelectProfileRouteComponent,
  RecoverCertUserListRouteComponent,
  RecoverCertAuditRouteComponent,
  RecoverCertDataCollectionRouteComponent,
  RecoverCertFinishRouteComponent
} from './recover-certificate';
import {
  SuspendSmartcardRouteComponent,
  SuspendSelectProfileRouteComponent,
  SuspendDataCollectionRouteComponent,
  SuspendSmartAuditRouteComponent,
  SuspendReasonComponent,
  SuspendFinishRouteComponent
} from './suspend-smartcard';
import {
  RewriteSmartcardRouteComponent,
  RewriteSelectProfileRouteComponent,
  RewriteDataCollectionRouteComponent,
  RewriteAuditRouteComponent,
  RewriteFinishRouteComponent,
  RewriteContinueRouteComponent
} from './rewrite-smartcard';
import {
  RevokeCertificateUserListRouteComponent,
  RevokeCertificateSelectProfileRouteComponent,
  RevokeCertificateDataCollectionComponent,
  RevokeCertificateAuditComponent,
  RevokeCertificateFinishRouteComponent,
  RevokeCertificateReasonRouteComponent
} from './revoke-certificate';
import {
  RenewCertificateUserListRouteComponent,
  RenewCertificateSelectProfileComponent,
  RenewCertificateDataCollectionRouteComponent,
  RenewCertificateAuditRouteComponent,
  RenewCertificateFinishRouteComponent
} from './renew-certificate';
import { ColorPickerModule } from 'ngx-color-picker';
import {
  UISettingsRouteComponent,
  LoggingRouteComponent,
  ServiceConfigurationRouteComponent,
  ProfileTemplateConfigurationRouteComponent,
  ActivitiesConfigurationRouteComponent,
  UserPageConfigurationRouteComponent,
  TextConfigurationRouteComponent
} from './administration';
@NgModule({
  declarations: [
    HomeRouteComponent,
    ManageRequestsRouteComponent,
    LoggingRouteComponent,
    SelectSmartcardRouteComponent,
    DisableSmartcardRouteComponent,
    DisableReasonRouteComponent,
    SelectProfileTemplateRouteComponent,
    DataCollectionRouteComponent,
    EnrollAuditRouteComponent,
    EnrollUserOrCertificateRouteComponent,
    EnrollFinishRouteComponent,
    UnblockSelectProfileRouteComponent,
    UnblockSmartcardRouteComponent,
    ReinstateSmartcardRouteComponent,
    ReinstateSelectProfileRouteComponent,
    ReinstateDataCollectionRouteComponent,
    ReinstateFinishRouteComponent,
    RetireTemporarySmartcardRouteComponent,
    RetireSelectProfileRouteComponent,
    RetireDataCollectionRouteComponent,
    RetireAuditRouteComponent,
    RetireFinishRouteComponent,
    RecoverCertSelectProfileRouteComponent,
    RecoverCertDataCollectionRouteComponent,
    RecoverCertAuditRouteComponent,
    RecoverCertUserListRouteComponent,
    RecoverCertFinishRouteComponent,
    DisableSmartAuditRouteComponent,
    DisableSmartDataCollectionRouteComponent,
    DisableSmartcardFinishRouteComponent,
    DisableContinueRouteComponent,
    UnblockSmartAuditRouteComponent,
    UnblockSmartDataCollectionRouteComponent,
    UnblockSmartChallengeComponent,
    UnblockSmartResponseComponent,
    UnblockSmartResponseComponent,
    UnblockSmartFinishRouteComponent,
    ReinstateSmartAuditRouteComponent,
    ReinstateSmartReasonComponent,
    SuspendSmartcardRouteComponent,
    SuspendSelectProfileRouteComponent,
    SuspendDataCollectionRouteComponent,
    SuspendSmartAuditRouteComponent,
    SuspendReasonComponent,
    SuspendFinishRouteComponent,
    RevokeCertificateSelectProfileRouteComponent,
    RevokeCertificateUserListRouteComponent,
    RevokeCertificateDataCollectionComponent,
    RevokeCertificateAuditComponent,
    RevokeCertificateFinishRouteComponent,
    RevokeCertificateReasonRouteComponent,
    UISettingsRouteComponent,
    SuspendReasonComponent,
    RewriteSmartcardRouteComponent,
    RenewCertificateUserListRouteComponent,
    RenewCertificateSelectProfileComponent,
    RewriteSelectProfileRouteComponent,
    RenewCertificateDataCollectionRouteComponent,
    RenewCertificateFinishRouteComponent,
    RewriteDataCollectionRouteComponent,
    RewriteAuditRouteComponent,
    RewriteFinishRouteComponent,
    RewriteContinueRouteComponent,
    RenewCertificateAuditRouteComponent,
    ServiceConfigurationRouteComponent,
    ProfileTemplateConfigurationRouteComponent,
    ActivitiesConfigurationRouteComponent,
    UserPageConfigurationRouteComponent,
    TextConfigurationRouteComponent
  ],
  imports: [
    AppCommonModule,
    ComponentsModule,
    RouterModule,
    ColorPickerModule
  ]
})
export class RouteComponentsModule {}

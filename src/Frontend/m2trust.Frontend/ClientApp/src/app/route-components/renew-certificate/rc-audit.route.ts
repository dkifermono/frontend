import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  DciDataDtoSectionType,
  IAuditRequestDto,
  FinishRequestClient,
  WizardData,
  AuditRequestDto,
  AuditResult,
  CanComponentDeactivate
} from '../../common';
import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';


@Component({
  selector: 'app-rc-audit',
  templateUrl: 'rc-audit.route.html'
})

export class RenewCertificateAuditRouteComponent extends CanComponentDeactivate implements OnDestroy {
  sectionType = DciDataDtoSectionType.Renew;
  audit: IAuditRequestDto;
  activityType: string;

  firstStepUrl =  '/renew-certificate';
  description = '';
  isSuccessful = false;
  otp: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private finishRequestClient: FinishRequestClient,
    private spinnerService: Ng4LoadingSpinnerService,
    private wizardData: WizardData,
    private finishService: FinishActivityService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
    ) {
    super(cancelActivityService, routingStateService);
  }

  onSubmit($event) {
    this.activityType = 'RenewCertificate';
    this.spinnerService.show();
    this.audit = {
      userId: $event.userId,
      profileTemplateId: $event.profileTemplateId,
      comment: $event.comment,
      profileId: $event.profileId,
      dataCollections: $event.dataCollections,
      signature: $event.signature
    };
    this.finishRequestClient
      .renewProfile(new AuditRequestDto(this.audit), this.activityType)
      .subscribe(
        response => {
          this.spinnerService.hide();
          this.description = response.hasSignature ? 'FinishRequest.Renew.SuccessSignatureDescription' : 'FinishRequest.Renew.SuccessDescription';
          this.isSuccessful = true;
          this.otp = response.oneTimePassword;
          this.navigateToFinish();
        },
        error => {
          this.spinnerService.hide();
          this.wizardData.setErrorData(error);
          this.isSuccessful = false;
          this.description = 'FinishRequest.Renew.FailedDescription';
          this.navigateToFinish();
        }
      );
  }

  private navigateToFinish() {
    this.router.navigate(['/renew-certificate/finish'], {
      relativeTo: this.route
    });
  }

  ngOnDestroy() {
    this.finishService.auditResult = new AuditResult(this.description, this.firstStepUrl, this.isSuccessful, this.otp);
  }
}


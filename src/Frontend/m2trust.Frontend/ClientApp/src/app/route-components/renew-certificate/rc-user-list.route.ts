import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CanComponentDeactivate } from '../../common';
import { CancelActivityService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-rc-user-list',
  templateUrl: 'rc-user-list.route.html'
})

export class RenewCertificateUserListRouteComponent extends CanComponentDeactivate implements OnInit {
  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  ngOnInit() { }

  onFormStatusChange($event: FormGroup) {
    this.form = $event;
  }
}

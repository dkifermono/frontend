import { Component, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  SmartcardClient,
  DciDataDtoSectionType,
  AuditRequestDto,
  IAuditRequestDto,
  WizardData,
  ContinueModel,
  AuditResult,
  CanComponentDeactivate
} from '../../common';
import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-rewrite-smart-audit',
  templateUrl: 'rewrite-audit.route.html'
})

export class RewriteAuditRouteComponent extends CanComponentDeactivate implements OnDestroy {
  sectionType = DciDataDtoSectionType.Retire;
  audit: IAuditRequestDto;
  activityType: string;

  isContinue = false;

  url = '/rewrite-smartcard';
  description = '';
  isSuccessful = false;
  otp: string;
  userId: string;
  title =  'FinishRequest.Rewrite.Continue.PageTitle';

  constructor(
    private smartcardClient: SmartcardClient,
    private wizardData: WizardData,
    private finishService: FinishActivityService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: Ng4LoadingSpinnerService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
    ) {
      super(cancelActivityService, routingStateService);
    }

  onSubmit($event) {
    this.spinner.show();
    this.activityType = 'RewriteSmartcard';
    this.audit = {
      userId: $event.userId,
      profileTemplateId: $event.profileTemplateId,
      comment: $event.comment,
      profileId: $event.profileId,
      dataCollections: $event.dataCollections,
      signature: $event.signature
    };
    this.smartcardClient.retire(new AuditRequestDto(this.audit), this.activityType).subscribe(
      response => {
        this.isSuccessful = true;
        this.otp = response.otpResult.oneTimePassword;
        if (response.continue) {
          this.description = response.otpResult.hasSignature ? 'FinishRequest.Rewrite.Continue.SuccessSignatureDescription' : 'FinishRequest.Rewrite.Continue.SuccessDescription';
          this.url = '/enroll-user/select-profile-template';
          this.userId =  $event.userId;
          this.spinner.hide();
          this.navigateToContinue();
        } else {
          this.url = '/rewrite-smartcard';
          this.description = response.otpResult.hasSignature ? 'FinishRequest.Rewrite.SuccessSignatureDescription' : 'FinishRequest.Rewrite.SuccessDescription';
          this.spinner.hide();
          this.navigateToFinish();
        }
      },
      error => {
        this.wizardData.setErrorData(error);
        this.isSuccessful = false;
        this.description = 'FinishRequest.Rewrite.FailedDescription';
        this.url = '/rewrite-smartcard';
        this.spinner.hide();
        this.navigateToFinish();
      }
    );
  }

  private navigateToContinue() {
    this.isContinue = true;
    this.router.navigate(['/rewrite-smartcard/continue'], {
      relativeTo: this.route
    });
  }

  private navigateToFinish() {
    this.router.navigate(['/rewrite-smartcard/finish'], {
      relativeTo: this.route
    });
  }

  ngOnDestroy() {
      if (this.isContinue) {
        this.finishService.continueModel = new ContinueModel(this.title, this.description, this.url, this.userId, this.isSuccessful, this.otp);
      } else {
        this.finishService.auditResult = new AuditResult(this.description, this.url, this.isSuccessful);
      }
  }
}

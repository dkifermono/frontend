import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CanComponentDeactivate } from '../../common';
import {
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-rewrite-smartcard',
  templateUrl: './rewrite-smartcard.route.html'
})

export class RewriteSmartcardRouteComponent extends CanComponentDeactivate implements OnInit {

  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  ngOnInit() {}

  onFormStatusChange($event: FormGroup) {
    this.form = $event;
  }
}

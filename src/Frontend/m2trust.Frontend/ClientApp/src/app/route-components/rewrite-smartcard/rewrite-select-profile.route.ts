import { Component, OnInit} from '@angular/core';
import { SectionType2, CanComponentDeactivate } from '../../common';
import { CancelActivityService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-rewrite-select-profile',
  templateUrl: 'rewrite-select-profile.route.html'
})

export class RewriteSelectProfileRouteComponent extends CanComponentDeactivate implements OnInit {
  sectionType = SectionType2.Retire;
  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  ngOnInit() {}
}

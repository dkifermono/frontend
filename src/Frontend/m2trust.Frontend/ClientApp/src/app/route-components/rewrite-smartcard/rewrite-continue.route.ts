import { Component, OnInit } from '@angular/core';
import { ContinueModel } from '../../common';
import { FinishActivityService } from '../../common/services';

@Component({
  selector: 'app-rewrite-continue',
  templateUrl: 'rewrite-continue.route.html'
})

export class RewriteContinueRouteComponent implements OnInit {
  continueModel: ContinueModel;
  loadComponent = false;
  constructor(private finishService: FinishActivityService) { }

  ngOnInit() {
    this.continueModel = this.finishService.continueModel;
    this.loadComponent = true;
  }
}

import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  WizardData,
  UserClient,
  UserEntryDto,
  ProfileClient,
  ProfileTemplateDto,
  DataCollectionDto,
  PageHeaderData,
  CanComponentDeactivate
} from '../../common';
import {
  ErrorNotificationService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-select-profile-template',
  templateUrl: 'select-profile-template.route.html'
})


export class SelectProfileTemplateRouteComponent extends CanComponentDeactivate implements OnInit {

  user: UserEntryDto;
  profileTemplates: ProfileTemplateDto[];
  dataCollection: DataCollectionDto = new DataCollectionDto();
  loading = false;
  selectedProfileTemplate: ProfileTemplateDto = null;
  activityType: string;
  private readonly noTemplates: 'SelectProfileTemplate.NoProfileTemplateData';


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userClient: UserClient,
    private profileClient: ProfileClient,
    private spinnerService: Ng4LoadingSpinnerService,
    private wizardData: WizardData,
    private errorNotificationService: ErrorNotificationService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  ngOnInit() {
    this.wizardData.clearDataCollection();
    this.activityType = 'Enroll';
    this.route.params.subscribe(params => {
      this.spinnerService.show();
      this.loading = true;
      const userId = params['userId'];
      this.userClient.getUser(userId, this.activityType)
        .subscribe(response => {
          this.user = response;
        });

      this.profileClient.getProfileTemplates(userId, this.activityType)
        .subscribe(response => {
          this.profileTemplates = response;
          if (this.profileTemplates && this.profileTemplates.length === 1) {
            this.selectedProfileTemplate = response[0];
          }
          this.setSelectedProfileFromWizard();
          this.spinnerService.hide();
          this.loading = false;
        },
          error => {
            this.spinnerService.hide();
            this.loading = false;
            this.wizardData.setErrorData(error);
            this.errorNotificationService.notificationError(error);
          } ,
          () => {
            this.spinnerService.hide();
            this.loading = false;
          });
    });
    const pageHeaderData = this.wizardData.getPageHeaderData();
    this.wizardData.setPageHeaderData(new PageHeaderData(pageHeaderData.userName));
  }

  setSelectedProfileFromWizard() {
    const wizardProfileId = this.wizardData.getProfileTemplateId();
    if (wizardProfileId) {
      const selected = this.profileTemplates.filter(a => a.profileTemplateId === wizardProfileId);
      if (selected.length > 0) {
        this.onProfileTemplateSelectionChange(selected[0]);
      }
    }
  }

  isProfileSelected(item: ProfileTemplateDto) {
    if (this.selectedProfileTemplate) {
      return this.selectedProfileTemplate.profileTemplateId === item.profileTemplateId;
    }
    return false;
  }

  onProfileTemplateSelectionChange(item) {
    this.selectedProfileTemplate = item;
  }

  goToNextPage() {
    if (this.selectedProfileTemplate == null) {
      return;
    }
    const pageHeaderData = this.wizardData.getPageHeaderData();
    this.wizardData.setProfileTemplateId(this.selectedProfileTemplate.profileTemplateId);
    this.wizardData.setPageHeaderData(new PageHeaderData(pageHeaderData.userName, this.selectedProfileTemplate.name));
    this.router.navigate(['/enroll-user/collect-data-collection', this.user.id, this.selectedProfileTemplate.profileTemplateId], {relativeTo: this.route});
  }
}

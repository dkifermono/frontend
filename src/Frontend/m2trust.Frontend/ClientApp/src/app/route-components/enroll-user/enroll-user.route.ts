import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CanComponentDeactivate } from '../../common';
import { CancelActivityService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-enroll-user',
  templateUrl: './enroll-user.route.html'
})

export class EnrollUserOrCertificateRouteComponent extends CanComponentDeactivate implements OnInit {

  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  ngOnInit() {}

  onFormStatusChange($event: FormGroup) {
    this.form = $event;
  }
}

import { Component, OnInit } from '@angular/core';
import { FinishActivityService } from '../../common/services';
import { AuditResult } from '../../common';

@Component({
  selector: 'app-enroll-finish',
  templateUrl: 'enroll-finish.route.html'
})

export class EnrollFinishRouteComponent implements OnInit {
  auditResult: AuditResult;
  loadComponent = false;
  constructor(private finishService: FinishActivityService) { }

  ngOnInit() {
    this.auditResult = this.finishService.auditResult;
    this.loadComponent = true;
   }
}

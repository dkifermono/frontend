import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router,  } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  WizardData,
  FinishRequestClient,
  DciDataDtoSectionType,
  AuditRequestDto,
  IAuditRequestDto,
  AuditResult,
  CanComponentDeactivate
} from '../../common';
import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';


@Component({
  selector: 'app-enroll-audit',
  templateUrl: 'enroll-audit.route.html'
})
export class EnrollAuditRouteComponent extends CanComponentDeactivate implements OnDestroy {
  sectionType = DciDataDtoSectionType.Enroll;
  audit: IAuditRequestDto;
  activityType: string;
  description = '';
  isSuccessful = false;
  firstStepUrl = '/enroll-user';
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private finishRequestClient: FinishRequestClient,
    private wizardData: WizardData,
    private finishService: FinishActivityService,
    private spinner: Ng4LoadingSpinnerService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  onSubmit($event) {
    this.spinner.show();
    this.activityType = 'Enroll';
    this.audit = {
      userId: $event.userId,
      profileTemplateId: $event.profileTemplateId,
      comment: $event.comment,
      profileId: $event.profileId,
      dataCollections: $event.dataCollections,
      signature: $event.signature,
      originatorId: $event.originatorId,
    };
    this.finishRequestClient.enrollRequest(new AuditRequestDto(this.audit), this.activityType).subscribe(
      response => {
        this.spinner.hide();
        this.description = response.hasSignature ? 'FinishRequest.Enroll.SuccessSignatureDescription' : 'FinishRequest.Enroll.SuccessDescription';
        this.isSuccessful = true;
        this.navigateToFinish();
      },
      error => {
        this.spinner.hide();
        this.wizardData.setErrorData(error);
        this.description = 'FinishRequest.Enroll.FailedDescription';
        this.isSuccessful = false;
        this.navigateToFinish();
      }
    );
  }

  private navigateToFinish() {
    this.router.navigate(
      ['/enroll-user/finish'], {
      relativeTo: this.route
    });
  }

  ngOnDestroy() {
    this.finishService.auditResult = new AuditResult(this.description, this.firstStepUrl, this.isSuccessful);
  }
}

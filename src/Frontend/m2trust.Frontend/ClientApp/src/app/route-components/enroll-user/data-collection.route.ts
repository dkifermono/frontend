import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SectionType, CanComponentDeactivate } from '../../common';
import { CancelActivityService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-data-collection',
  templateUrl: 'data-collection.route.html'
})

export class DataCollectionRouteComponent extends CanComponentDeactivate implements OnInit {
  sectionType = SectionType.Enroll;
  private userId: string;
  private profileTemplateId: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
    ) {
      super(cancelActivityService, routingStateService);
    }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.userId = params['userId'];
      this.profileTemplateId = params['profileTemplateId'];
    });
  }

    onNavigateToNextPage() {
      this.router.navigate(
        [
          '/enroll-user/audit',
          this.userId,
          this.profileTemplateId
        ],
        { relativeTo: this.route }
      );
    }

}



import { Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { map } from 'rxjs/operators';
import {
  SmartcardClient,
  IAuditRequestDto,
  AuditRequestDto,
  AuditRequestDtoReason
} from '../../common/restclient';
import { WizardData } from '../../common/providers';
import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';
import { AuditResult, CanComponentDeactivate } from '../../common';


@Component({
  selector: 'app-suspend-smartcard-reason',
  templateUrl: 'suspend-reason.component.html',
})

export class SuspendReasonComponent extends CanComponentDeactivate implements OnDestroy, OnInit {
audit: IAuditRequestDto;
activityType: string;
cachedData = [];
reinstateReason = AuditRequestDtoReason.CertificateReinstate;
private _effectiveRevocation: number;

firstStepUrl = '/suspend-smartcard';
description = '';
isSuccessful = false;
otp: string;
auditData: {};


get effectiveRevocation() {
  return this._effectiveRevocation;
}
set effectiveRevocation(value: number) {
  this._effectiveRevocation = value;
}

constructor(
  private route: ActivatedRoute,
  private router: Router,
  private smartcardClient: SmartcardClient,
  private wizardData: WizardData,
  private finishService: FinishActivityService,
  private spinner: Ng4LoadingSpinnerService,
  cancelActivityService: CancelActivityService,
  routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

ngOnInit() {
  this.route.paramMap.pipe(map(() => window.history.state)).subscribe(s => {
    this.auditData = s.audit;
  });
}

navigateToNextPage($event) {
  this.spinner.show();
  this.activityType = 'SuspendSmartcard';
  this.audit = {
  userId: $event.userId,
  profileId: $event.profileId,
  dataCollections: $event.dataCollections,
  effectiveRevocationInHours: this.effectiveRevocation,
  signature: $event.signature,
  comment: this.auditData['comment']
};
this.smartcardClient.suspend(new AuditRequestDto(this.audit), this.activityType).subscribe(
  response => {
    this.description = response.hasSignature ? 'FinishRequest.Suspend.SuccessSignatureDescription' : 'FinishRequest.Suspend.SuccessDescription';
    this.isSuccessful = true;
    this.otp = response.oneTimePassword;
    this.spinner.hide();
    this.navigateToFinish();
  },
  error => {
    this.wizardData.setErrorData(error);
    this.isSuccessful = false;
    this.description = 'FinishRequest.Suspend.FailedDescription';
    this.spinner.hide();
    this.navigateToFinish();
  }
);
}
  private navigateToFinish() {
    this.router.navigate(['/suspend-smartcard/finish'], {
      relativeTo: this.route
    });
  }

  ngOnDestroy() {
    this.finishService.auditResult = new AuditResult(this.description, this.firstStepUrl, this.isSuccessful, this.otp);
  }
}

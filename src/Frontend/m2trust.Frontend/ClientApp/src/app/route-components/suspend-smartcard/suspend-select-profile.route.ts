import { Component, OnInit} from '@angular/core';
import { SectionType2, CanComponentDeactivate } from '../../common';
import { CancelActivityService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-suspend-select-profile',
  templateUrl: 'suspend-select-profile.route.html'
})

export class SuspendSelectProfileRouteComponent extends CanComponentDeactivate implements OnInit {
  sectionType = SectionType2.Suspend;
  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  ngOnInit() {}
}

import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router,  } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  WizardData,
  FinishRequestClient,
  DciDataDtoSectionType,
  AuditRequestDto,
  SmartcardClient,
  IAuditRequestDto,
  AuditResult,
  CanComponentDeactivate
} from '../../common';

import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';
  @Component({
    selector: 'app-suspend-smart-audit',
    templateUrl: 'suspend-audit.route.html'
  })

  export class SuspendSmartAuditRouteComponent extends CanComponentDeactivate implements OnDestroy {
    sectionType = DciDataDtoSectionType.Suspend;
    audit: IAuditRequestDto;
    activityType: string;

    firstStepUrl = '/suspend-smartcard';
    description = '';
    isSuccessful = false;
    otp: string;

    constructor(
      private route: ActivatedRoute,
      private router: Router,
      private finishRequestClient: FinishRequestClient,
      private smartcardClient: SmartcardClient,
      private wizardData: WizardData,
      private finishService: FinishActivityService,
      private spinner: Ng4LoadingSpinnerService,
      cancelActivityService: CancelActivityService,
      routingStateService: RoutingStateService
    ) {
      super(cancelActivityService, routingStateService);
    }

    onSubmit($event) {
      this.spinner.show();
      this.activityType = 'SuspendSmartcard';
      this.audit = {
        userId: $event.userId,
        profileId: $event.profileId,
        profileTemplateId: $event.profileTemplateId,
        comment: $event.comment,
        dataCollections: $event.dataCollections,
        signature: $event.signature
      };
      this.finishRequestClient.allowSpecifyDelay(new AuditRequestDto(this.audit), this.activityType).subscribe(
        result => {
          if (result === false) {
            this.smartcardClient.suspend(new AuditRequestDto(this.audit), this.activityType).subscribe(
              response => {
                this.description = response.hasSignature ? 'FinishRequest.Suspend.SuccessSignatureDescription' : 'FinishRequest.Suspend.SuccessDescription';
                this.isSuccessful = true;
                this.otp = response.oneTimePassword;
                this.spinner.hide();
                this.navigateToFinish();
              },
              error => {
                this.wizardData.setErrorData(error);
                this.isSuccessful = false;
                this.description = 'FinishRequest.Suspend.FailedDescription';
                this.spinner.hide();
                this.navigateToFinish();
              }
            );
          } else {
            this.spinner.hide();
            const reasonUrl = '/suspend-smartcard/reason';
            if ($event.signature) {
              this.router.navigate(
                [reasonUrl,
                $event.userId,
                $event.profileId,
                $event.signature], {
                  relativeTo: this.route,
                  state: { audit: this.audit }
                }
              );
            } else {
              this.router.navigate(
                [reasonUrl,
                $event.userId,
                $event.profileId], {
                  relativeTo: this.route,
                  state: { audit: this.audit }
                }
              );
            }
          }
        }
      );
    }
    private navigateToFinish() {
      this.router.navigate(['/suspend-smartcard/finish'], {
        relativeTo: this.route
      });
    }

    ngOnDestroy() {
      this.finishService.auditResult = new AuditResult(this.description, this.firstStepUrl, this.isSuccessful, this.otp);
    }
  }

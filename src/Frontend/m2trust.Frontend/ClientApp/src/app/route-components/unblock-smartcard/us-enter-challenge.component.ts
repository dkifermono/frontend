import { Component, OnInit } from '@angular/core';
import {
  FinishRequestClient,
  IChallengeResponseRequestDto,
  ChallengeResponseRequestDto,
  UserClient,
  UserEntryDto
} from '../../common/restclient/restClient';
import { ActivatedRoute, Router } from '@angular/router';
import { WizardData } from '../../common/providers/wizardData';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { FormControl, Validators } from '@angular/forms';
import {ErrorNotificationService} from '../../common/services';

@Component({
  selector: 'app-unblock-smart-enter-challenge',
  templateUrl: 'us-enter-challenge.component.html'
})
export class UnblockSmartChallengeComponent implements OnInit {
  user: UserEntryDto;
  challengeResponse: IChallengeResponseRequestDto;
  userId = '';
  profileId = '';
  profileName = '';
  requestId = '';
  errorMessage = '';
  activityType: string;
  challenge = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9a-fA-F]{16}$')
  ]);
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userClient: UserClient,
    private finishRequestClient: FinishRequestClient,
    private spinnerService: Ng4LoadingSpinnerService,
    private wizardData: WizardData,
    private errorNotificationService: ErrorNotificationService
  ) {}

  ngOnInit() {
    this.spinnerService.show();
    this.activityType = 'UnblockSmartcard';
    this.route.params.subscribe(params => {
      this.spinnerService.show();
      this.userId = params['userId'];
      this.profileId = params['profileId'];
      this.requestId = this.route.snapshot.queryParamMap.get('requestId');
      this.userClient.getUser(this.userId, this.activityType).subscribe(response => {
        this.spinnerService.hide();
        this.user = response;
      });
      this.spinnerService.hide();
    });
  }

  goToNextPage() {
    this.spinnerService.show();
    this.activityType = 'UnblockSmartcard';
    this.challengeResponse = {
      profileId: this.profileId,
      profileName: this.profileName,
      challenge: this.challenge.value,
      userId: this.userId,
      requestGuid: this.requestId
    };
    if (this.challenge.valid) {
      this.finishRequestClient
        .enterChallenge(new ChallengeResponseRequestDto(this.challengeResponse), this.activityType)
        .subscribe(response => {
          this.spinnerService.hide();
          this.router.navigate(
            [
              '/unblock-smartcard/challenge-response',
              this.userId,
              this.profileId,
              this.profileName
            ],
            {
              relativeTo: this.route,
              queryParams: {
                challengeName: response
              }
            }
          );
        },
        error => {
          this.spinnerService.hide();
          this.wizardData.setErrorData(error);
          this.errorNotificationService.notificationError(error);
          this.router.navigate(
            ['/unblock-smartcard'], {
            relativeTo: this.route
          }
        );
        },
      );
    } else {
      this.spinnerService.hide();
      this.challenge.markAsTouched({ onlySelf: true });
    }
  }
}

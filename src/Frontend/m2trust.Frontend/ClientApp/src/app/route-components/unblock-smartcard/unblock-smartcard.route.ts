import { Component} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CanComponentDeactivate } from '../../common';
import {
  CancelActivityService,
  RoutingStateService
} from '../../common/services';


@Component({
  selector: 'app-unblock-smartcard',
  templateUrl: './unblock-smartcard.route.html'
})

export class UnblockSmartcardRouteComponent extends CanComponentDeactivate {

  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  onFormStatusChange($event: FormGroup) {
    this.form = $event;
  }
}

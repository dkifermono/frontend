import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router,  } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import {
  WizardData,
  FinishRequestClient,
  DciDataDtoSectionType,
  IAuditRequestDto,
  AuditRequestDto,
  PageHeaderData,
  AuditResult,
  CanComponentDeactivate
} from '../../common';
import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-unblock-smart-audit',
  templateUrl: 'us-audit.route.html'
})

export class UnblockSmartAuditRouteComponent extends CanComponentDeactivate implements OnDestroy {
  sectionType = DciDataDtoSectionType.OfflineUnblock;
  audit: IAuditRequestDto;
  activityType: string;

  firstStepUrl =  '/unblock-smartcard';
  description = '';
  isSuccessful = false;
  otp: string;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private finishRequestClient: FinishRequestClient,
    private wizardData: WizardData,
    private finishService: FinishActivityService,
    private spinner: Ng4LoadingSpinnerService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  onSubmit($event) {
    this.spinner.show();
    this.activityType = 'UnblockSmartcard';
    this.audit = {
      userId: $event.userId,
      profileTemplateId: $event.profileTemplateId,
      comment: $event.comment,
      profileId: $event.profileId,
      dataCollections: $event.dataCollections,
      signature: $event.signature
    };
    this.finishRequestClient.initiateOfflineUnblock(new AuditRequestDto(this.audit), this.activityType).subscribe(
      response => {
        this.spinner.hide();
          this.router.navigate([
            '/unblock-smartcard/enter-challenge',
            $event.userId,
            $event.profileId
          ], {
            relativeTo: this.route,
            queryParams: {
              requestId: response.requestGuid
            }
          });
          const pageHeaderData = this.wizardData.getPageHeaderData();
          this.wizardData.setPageHeaderData(new PageHeaderData(pageHeaderData.userName, null, response.profileName));
      },
      error => {
        this.spinner.hide();
        this.wizardData.setErrorData(error);
        this.navigateToFinish();
      }
    );

  }

  private navigateToFinish() {
    this.router.navigate(['/unblock-smartcard/finish'], {
      relativeTo: this.route
    });
  }

  ngOnDestroy() {
    this.finishService.auditResult = new AuditResult(this.description, this.firstStepUrl, this.isSuccessful, this.otp);
  }
}



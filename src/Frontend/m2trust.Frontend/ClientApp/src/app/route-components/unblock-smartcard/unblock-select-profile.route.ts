import { Component, OnInit} from '@angular/core';
import { FormGroup } from '@angular/forms';
import {
  SectionType2,
  CanComponentDeactivate
} from '../../common';
import {
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-unblock-select-profile',
  templateUrl: 'unblock-select-profile.route.html'
})

export class UnblockSelectProfileRouteComponent extends CanComponentDeactivate implements OnInit {
  sectionType = SectionType2.OfflineUnblock;
  constructor(cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  ngOnInit() {}
}

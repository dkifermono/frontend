import { Component, OnInit } from '@angular/core';
import { UserClient, UserEntryDto  } from '../../common/restclient/restClient';
import { ActivatedRoute, Router} from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { WizardData } from '../../common';

@Component({
  selector: 'app-unblock-smart-challenge-response',
  templateUrl: 'us-challenge-response.component.html'
})

export class UnblockSmartResponseComponent implements OnInit {
  user: UserEntryDto;
  userId = '';
  profileId = '';
  profileName = '';
  response = '';
  activityType: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userClient: UserClient,
    private spinnerService: Ng4LoadingSpinnerService,
    private wizardData: WizardData
  ) { }

  ngOnInit() {
    this.spinnerService.show();
    this.activityType = 'UnblockSmartcard';
    this.route.params.subscribe(params => {
      this.spinnerService.show();
      this.userId = params['userId'];
      this.profileId = params['profileId'];
      this.response = this.route.snapshot.queryParamMap.get('challengeName');
      this.userClient.getUser(this.userId, this.activityType)
            .subscribe(response => {
              this.spinnerService.hide();
              this.user = response;
            });
      this.spinnerService.hide();
    });
  }

  goToNextPage() {
    this.wizardData.clearWizard();
    this.router.navigate([
      '/unblock-smartcard'
    ], {
      relativeTo: this.route
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as cloneDeep from 'lodash/cloneDeep';

import {
  CancelActivityService,
  RoutingStateService,
  NotificationService,
  ErrorNotificationService,
  ConfigFormService
} from '../../../common/services';

import {
  CanComponentDeactivate,
  ConfigurationService,
  UserPageConfigurationDto,
  UserPageConfigurationDtoUserDisplayPropertiesEnum,
  UserPageConfigurationDtoUserSearchPropertiesEnum,
  UserPropertiesPicklist,
  UserProperty,
  intRegexValidator,
  rangeValidator
} from '../../../common';

@Component({
  selector: 'app-user-page-configuration.route.ts',
  templateUrl: 'user-page-configuration.route.html'
})

export class UserPageConfigurationRouteComponent extends CanComponentDeactivate implements OnInit {
  configurationForm: FormGroup;
  configuration: UserPageConfigurationDto	;
  initialConfiguration: UserPageConfigurationDto;
  userDisplayProps: UserPropertiesPicklist;
  userSearchProps: UserPropertiesPicklist;
  activityType = 'Administration';

  constructor(
    private spinner: Ng4LoadingSpinnerService,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private errorNotificationService: ErrorNotificationService,
    private configurationClient: ConfigurationService,
    private configFormService: ConfigFormService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  ngOnInit() {
    this.getConfiguration();
  }

  onSubmit() {
    this.configFormService.validateAllFormFields(this.configurationForm);
    if (!this.configurationForm.valid) {
      this.translateService.get('FrontendConfiguration.CheckValidation').subscribe(text => {
        this.notificationService.alert('', text);
      });
      return;
    }
    this.spinner.show();
    const configuration = this.createUserPageConfigurationDto();

    this.configurationClient.postUserPageConfiguration(configuration, this.activityType)
      .subscribe(response => {
        this.initialConfiguration = cloneDeep(configuration);
        this.configuration = cloneDeep(configuration);
        this.configurationForm.reset();
        this.createForm();
        this.translateService.get('FrontendConfiguration.SaveConfigurationSuccess').subscribe(text => {
          this.notificationService.success(text);
        });
        setTimeout(() => {
          location.reload();
        }, 5000);
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.spinner.hide();
      },
      () => this.spinner.hide());
  }

  resetForm() {
    this.createForm();
    this.fillUserPropertyPicklists(this.initialConfiguration);
    this.configuration = cloneDeep(this.initialConfiguration);
  }

  private createUserPageConfigurationDto(): UserPageConfigurationDto {
    const configuration =  this.configurationForm.getRawValue();
    configuration.userDisplayProperties = this.createUserPropsArray(this.userDisplayProps.target);
    configuration.userSearchProperties = this.createUserPropsArray(this.userSearchProps.target);
    return configuration;
  }

  private createUserPropsArray(target: UserProperty[]): string[] {
    const returnValue = [];
    if (target && target.length > 0) {
      target.forEach(t => {
        returnValue.push(t.id);
      });
    }
    return returnValue;
  }

  private getConfiguration() {
    this.spinner.show();
    this.configurationClient.getUserPageConfiguration()
      .subscribe(response => {
        this.configuration = cloneDeep(response);
        this.initialConfiguration = cloneDeep(response);
        this.fillUserPropertyPicklists(response);
        this.createForm();
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.spinner.hide();
      },
      () => this.spinner.hide());
  }

  private createForm() {
    const form = {
      searchUsersPageSize: new FormControl(this.configuration.searchUsersPageSize, [Validators.required, intRegexValidator, rangeValidator(0)])
    };
    this.configurationForm = new FormGroup(form);
    this.form = this.configurationForm;
  }

  private fillUserPropertyPicklists(config: UserPageConfigurationDto) {
    const displayKeys = Object.keys(UserPageConfigurationDtoUserDisplayPropertiesEnum);
    const searchKeys = Object.keys(UserPageConfigurationDtoUserSearchPropertiesEnum);
    this.userDisplayProps = this.createUserPropertyPicklist(config.userDisplayProperties, displayKeys.map(k => UserPageConfigurationDtoUserDisplayPropertiesEnum[k as any]));
    this.userSearchProps = this.createUserPropertyPicklist(config.userSearchProperties, searchKeys.map(k => UserPageConfigurationDtoUserDisplayPropertiesEnum[k as any]));
  }

  private createUserPropertyPicklist(configProps: string[], enumProps: string[]): UserPropertiesPicklist {
    const userProperties: UserProperty[] = []; // props from enum
    enumProps.forEach(p => {
      const prop = new UserProperty(p, p);
      userProperties.push(prop);
    });
    if (userProperties && userProperties.length > 0) {
      userProperties.sort((a, b) => a.name.localeCompare(b.name));
    }
    const picklist = new UserPropertiesPicklist(userProperties, []);
    configProps.forEach(c => {
      const up = userProperties.filter(p => p.id.toLowerCase() === c.toLowerCase());
      if (up.length > 0) {
        picklist.options = picklist.options.filter(o => o !== up[0]);
        picklist.target.push(up[0]);
      }
    });
    return picklist;
  }
}

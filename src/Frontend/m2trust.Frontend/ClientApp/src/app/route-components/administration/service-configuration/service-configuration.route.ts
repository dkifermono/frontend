import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import * as cloneDeep from 'lodash/cloneDeep';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import {
  CanComponentDeactivate,
  simpleOidValidator,
  urlValidator,
  ServiceConfigurationDto,
  ConfigurationService,
  MimConfigurationDto,
  AdImpersonationConfigurationDto,
  LoggingConfigurationDto
} from '../../../common';
import {
  NotificationService,
  ErrorNotificationService,
  CancelActivityService,
  RoutingStateService,
  ConfigFormService
} from '../../../common/services';

@Component({
  selector: 'app-service-configuration',
  templateUrl: 'service-configuration.route.html'
})

export class ServiceConfigurationRouteComponent  extends CanComponentDeactivate implements OnInit {
  activityType = 'Administration';
  configuration: ServiceConfigurationDto;
  initialConfiguration: ServiceConfigurationDto;
  configurationForm: FormGroup;

  constructor(
    private spinner: Ng4LoadingSpinnerService,
    private notificationService: NotificationService,
    private configurationClient: ConfigurationService,
    private configFormService: ConfigFormService,
    private translateService: TranslateService,
    private errorNotificationService: ErrorNotificationService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  ngOnInit() {
    this.getServiceConfiguration();
   }

   private getServiceConfiguration() {
    this.spinner.show();
    this.configurationClient.getServiceConfiguration(this.activityType)
      .subscribe(response => {
        this.configuration = cloneDeep(response);
        this.initialConfiguration = cloneDeep(response);
        this.createForm();
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.spinner.hide();
      },
      () => this.spinner.hide());
  }

  private createForm() {
    const form = {
      signatureTemplateOid: new FormControl(this.configuration.mimConfiguration.signatureTemplateOid, simpleOidValidator),
      signatureEkuOid: new FormControl(this.configuration.mimConfiguration.signatureEkuOid, simpleOidValidator),
      signatureDCIName: new FormControl(this.configuration.mimConfiguration.signatureDciName),
      rejectInvalidSigningCertificate: new FormControl(this.configuration.mimConfiguration.rejectInvalidSigningCertificate),

      mimProviderMimFrontendBaseUrl: new FormControl(this.configuration.mimConfiguration.mimProviderMimFrontendBaseUrl, [Validators.required, urlValidator]),
      mimProviderRequestOperationsEndpoint: new FormControl(this.configuration.mimConfiguration.mimProviderRequestOperationsEndpoint, [Validators.required, urlValidator]),
      mimProviderFindOperationsEndpoint: new  FormControl(this.configuration.mimConfiguration.mimProviderFindOperationsEndpoint, [Validators.required, urlValidator]),
      mimProviderPermissionOperationsEndpoint: new FormControl(this.configuration.mimConfiguration.mimProviderPermissionOperationsEndpoint, [Validators.required, urlValidator]),
      mimProviderExecuteOperationsEndpoint: new FormControl(this.configuration.mimConfiguration.mimProviderExecuteOperationsEndpoint, [Validators.required, urlValidator]),

      activeDirectoryUserUserName: new FormControl(this.configuration.adImpersonationConfiguration.activeDirectoryUserUserName),
      activeDirectoryUserDomain: new FormControl(this.configuration.adImpersonationConfiguration.activeDirectoryUserDomain),
      activeDirectoryUserPassword: new FormControl(this.configuration.adImpersonationConfiguration.activeDirectoryUserPassword),
      activeDirectoryUserDisableLoadProfile: new FormControl(this.configuration.adImpersonationConfiguration.activeDirectoryUserDisableLoadProfile),

      loggingSqlAddress: new FormControl(this.configuration.loggingConfiguration.loggingSqlAddress),

    };
    this.configurationForm = new FormGroup(form);
    this.form = this.configurationForm;
  }

  onSubmit() {
    this.configFormService.validateAllFormFields(this.configurationForm);
    if (!this.configurationForm.valid) {
      this.translateService.get('FrontendConfiguration.CheckValidation').subscribe(text => {
        this.notificationService.alert('', text);
      });
      return;
    }
    this.spinner.show();
    const configuration = this.createServiceConfigurationDto(this.configurationForm);
    this.configurationClient.postServiceConfiguration(configuration, this.activityType)
      .subscribe(response => {
        this.initialConfiguration = cloneDeep(configuration);
        this.configuration = cloneDeep(configuration);
        this.configurationForm.reset();
        this.createForm();
        this.translateService.get('FrontendConfiguration.SaveConfigurationSuccess').subscribe(text => {
          this.notificationService.success(text);
        });
        setTimeout(() => {
          location.reload();
        }, 5000);
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.spinner.hide();
      },
      () => this.spinner.hide());
  }

  resetForm() {
    this.createForm();
    this.configuration = cloneDeep(this.initialConfiguration);
  }

  private createServiceConfigurationDto(form: FormGroup): ServiceConfigurationDto {
    const values = form.getRawValue();
    const result = new ServiceConfigurationDto(form.getRawValue());
    result.mimConfiguration = new MimConfigurationDto();
    result.adImpersonationConfiguration = new AdImpersonationConfigurationDto();
    result.loggingConfiguration = new LoggingConfigurationDto();

    result.mimConfiguration.signatureTemplateOid = this.configFormService.getStringValue(values.signatureTemplateOid);
    result.mimConfiguration.signatureEkuOid = this.configFormService.getStringValue(values.signatureEkuOid);
    result.mimConfiguration.signatureDciName = this.configFormService.getStringValue(values.signatureDCIName);
    result.mimConfiguration.rejectInvalidSigningCertificate = values.rejectInvalidSigningCertificate;
    result.mimConfiguration.mimProviderMimFrontendBaseUrl = this.configFormService.getStringValue(values.mimProviderMimFrontendBaseUrl);
    result.mimConfiguration.mimProviderRequestOperationsEndpoint = this.configFormService.getStringValue(values.mimProviderRequestOperationsEndpoint);
    result.mimConfiguration.mimProviderFindOperationsEndpoint = this.configFormService.getStringValue(values.mimProviderFindOperationsEndpoint);
    result.mimConfiguration.mimProviderPermissionOperationsEndpoint = this.configFormService.getStringValue(values.mimProviderPermissionOperationsEndpoint);
    result.mimConfiguration.mimProviderExecuteOperationsEndpoint = this.configFormService.getStringValue(values.mimProviderExecuteOperationsEndpoint);

    result.adImpersonationConfiguration.activeDirectoryUserUserName = this.configFormService.getStringValue(values.activeDirectoryUserUserName);
    result.adImpersonationConfiguration.activeDirectoryUserDomain = this.configFormService.getStringValue(values.activeDirectoryUserDomain);
    // password can contain only spaces - not defined
    result.adImpersonationConfiguration.activeDirectoryUserPassword = values.activeDirectoryUserPassword;
    result.adImpersonationConfiguration.activeDirectoryUserDisableLoadProfile = values.activeDirectoryUserDisableLoadProfile;

    result.loggingConfiguration.loggingSqlAddress = this.configFormService.getStringValue(values.loggingSqlAddress);
    return result;
  }
}

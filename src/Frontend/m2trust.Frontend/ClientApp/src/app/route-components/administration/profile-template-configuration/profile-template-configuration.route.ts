import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as cloneDeep from 'lodash/cloneDeep';

import {
  CancelActivityService,
  RoutingStateService,
  NotificationService,
  ErrorNotificationService,
  ConfigFormService,
  ProfileTemplateConfigService,
  ProfileTemplateListService
} from '../../../common/services';
import {
  CanComponentDeactivate,
  ConfigurationService,
  ProfileTemplatesConfigurationDto,
  guidValidator,
  ProfileTemplateDto,
  ProfileTemplateItemDto,
  DCIDto
} from '../../../common';

@Component({
  selector: 'app-profile-template-configuration',
  templateUrl: 'profile-template-configuration.route.html'
})

export class ProfileTemplateConfigurationRouteComponent extends CanComponentDeactivate implements OnInit {
  configurationForm: FormGroup;
  configuration: ProfileTemplatesConfigurationDto;
  initialConfiguration: ProfileTemplatesConfigurationDto;
  activityType = 'Administration';
  profileTemplateValidators: ValidatorFn[] = [Validators.required, guidValidator];

  blacklistedOptions: ProfileTemplateDto[] = [];
  blacklistedTarget: ProfileTemplateDto[] = [];
  excludedOptions: ProfileTemplateDto[] = [];
  excludedTarget: ProfileTemplateDto[] = [];

  constructor(
    private spinner: Ng4LoadingSpinnerService,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private errorNotificationService: ErrorNotificationService,
    private configurationClient: ConfigurationService,
    private configFormService: ConfigFormService,
    private profileTemplateConfigService: ProfileTemplateConfigService,
    private profileTemplateListService: ProfileTemplateListService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  ngOnInit() {
    this.getConfiguration();
  }

  onSubmit() {
    this.configFormService.validateAllFormFields(this.configurationForm);
    if (!this.configurationForm.valid) {
      this.translateService.get('FrontendConfiguration.CheckValidation').subscribe(text => {
        this.notificationService.alert('', text);
      });
      return;
    }
    this.spinner.show();
    const configuration = this.createProfileTemplateConfigurationDto();
    this.configurationClient.postProfileTemplatesConfiguration(configuration, this.activityType)
      .subscribe(response => {
        this.initialConfiguration = cloneDeep(configuration);
        this.configuration = cloneDeep(configuration);
        this.createForm();
        this.translateService.get('FrontendConfiguration.SaveConfigurationSuccess').subscribe(text => {
          this.notificationService.success(text);
        });
        setTimeout(() => {
          location.reload();
        }, 5000);
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.spinner.hide();
      },
      () => this.spinner.hide());
  }

  resetForm() {
    this.createForm();
    this.fillProfileTemplatePicklists(this.initialConfiguration);
    this.configuration = cloneDeep(this.initialConfiguration);
  }

  private createProfileTemplateConfigurationDto(): ProfileTemplatesConfigurationDto {
    const values = this.configurationForm.getRawValue();
    const result = new ProfileTemplatesConfigurationDto(this.configurationForm.getRawValue());
    result.blacklistedProfileTemplates = this.fillProfileTemplateList(this.blacklistedTarget);
    result.excludedProfileTemplates = this.fillProfileTemplateList(this.excludedTarget);

    result.dciProfileTemplates = [];

    values.dciProfileTemplates.forEach(item => {
      const profileTemplate = new ProfileTemplateItemDto();
      profileTemplate.profileTemplateId = item.profileTemplateId;
      profileTemplate.reinstateDCIs  = [];
      item.reinstateDCIs.forEach(dci => {
        const mappedDci = new DCIDto(dci);
        profileTemplate.reinstateDCIs.push(mappedDci);
      });
      result.dciProfileTemplates.push(profileTemplate);
    });

    return result;
  }

  private getConfiguration() {
    this.spinner.show();
    this.configurationClient.getProfileTemplatesConfiguration(this.activityType)
      .subscribe(response => {
        this.configuration = cloneDeep(response);
        this.initialConfiguration = cloneDeep(response);
        this.fillProfileTemplatePicklists(response);
        this.profileTemplateListService.profileTemplateLookup(response.profileTemplateLookup);
        this.createForm();
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.notificationService.error(this.translateService.instant('FrontendConfiguration.CheckServiceConfig'), this.translateService.instant('ProfileTemplateConfiguration.ErrorMessage'));
        this.spinner.hide();
      },
      () => this.spinner.hide());
  }

  private createForm() {
    const form = {
      selectDefaultProfile : new FormControl(this.configuration.selectDefaultProfile),
      showDisabledProfiles : new FormControl(this.configuration.showDisabledProfiles),
      excludedProfileTemplates: this.profileTemplateConfigService.createProfileTemplateFormArray(this.configuration.blacklistedProfileTemplates, this.profileTemplateValidators),
      blacklistedProfileTemplates: this.profileTemplateConfigService.createProfileTemplateFormArray(this.configuration.excludedProfileTemplates, this.profileTemplateValidators),
      dciProfileTemplates: this.profileTemplateConfigService.createProfileTemplatesArray(this.configuration.dciProfileTemplates),
    };
    this.configurationForm = new FormGroup(form);
    this.form = this.configurationForm;
  }

  private fillProfileTemplatePicklists(config: ProfileTemplatesConfigurationDto) {
    this.excludedOptions = cloneDeep(config.profileTemplateLookup);
    this.excludedTarget = cloneDeep(config.excludedProfileTemplates);
    if (this.excludedTarget && this.excludedTarget.length > 0 && this.excludedOptions && this.excludedOptions.length > 0) {
      this.excludedOptions = this.fillPicklistOptions(this.excludedOptions, this.excludedTarget);
    }
    if (this.excludedOptions && this.excludedOptions.length > 0) {
      this.excludedOptions.sort((a, b) => a.name.localeCompare(b.name));
    }

    this.blacklistedOptions = cloneDeep(config.profileTemplateLookup);
    this.blacklistedTarget = cloneDeep(config.blacklistedProfileTemplates);
    if (this.blacklistedTarget.length > 0) {
      this.blacklistedOptions = this.fillPicklistOptions(this.blacklistedOptions, this.blacklistedTarget);
    }
    if (this.blacklistedOptions) {
      this.blacklistedOptions.sort((a, b) => a.name.localeCompare(b.name));
    }
  }

  private fillPicklistOptions(optionList: ProfileTemplateDto[], targetList: ProfileTemplateDto[]): ProfileTemplateDto[]  {
    for (let i = optionList.length - 1; i >= 0; i--) {
      for (let j = 0; j < targetList.length; j++) {
        if (optionList[i] && (optionList[i].profileTemplateId === targetList[j].profileTemplateId)) {
          optionList.splice(i, 1);
        }
      }
    }
    return optionList;
  }

  fillProfileTemplateList(target: ProfileTemplateDto[]): ProfileTemplateDto[] {
    const profileLookup = this.initialConfiguration.profileTemplateLookup;
    if (target && target.length > 0) {
      target.filter(p => {
        return profileLookup.indexOf(p) > 0;
      });
      return target;
    } else {
      return [];
    }
  }
}

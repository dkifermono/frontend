import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as moment from 'moment';
import {
  Page,
  PageEvent,
  LoggingClient,
  LogEntryDto,
  LogLevel,
  RESTPagedResultOfLogEntryDto,
  CanComponentDeactivate
} from '../../../common';
import {
  NotificationService,
  CancelActivityService,
  RoutingStateService,
  ErrorNotificationService
} from '../../../common/services';


@Component({
  selector: 'app-logging',
  templateUrl: './logging.route.html'
})

export class LoggingRouteComponent extends CanComponentDeactivate implements OnInit {
  response: RESTPagedResultOfLogEntryDto;
  logEntry: LogEntryDto;
  display = false;
  fromDate: Date = new Date();
  toDate: Date = new Date();
  logLevelValues = LogLevel;
  public isLoading = true;
  activityType: string;
  currentPaging: Page = new Page();
  private _searchForm: FormGroup;


  get searchForm(): FormGroup {
    return this._searchForm;
  }

  constructor(
    private loggingClient: LoggingClient,
    private spinnerService: Ng4LoadingSpinnerService,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private errorNotificationService: ErrorNotificationService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService,
  ) {
    super(cancelActivityService, routingStateService);
    this.currentPaging.pageSize = 25;
    this.createForm();
  }

  createForm() {
    this.fromDate.setDate(new Date().getDate() - 7);
    this.toDate.setDate(new Date().getDate() + 1);
    const form = {
      searchInput: new FormControl(null),
      from: new FormControl(this.fromDate),
      to: new FormControl(this.toDate),
      logLevel: new FormControl(''),
      auditLogs: new FormControl('')
    };
    this._searchForm = new FormGroup(form);
    this.form = this._searchForm;
  }

  ngOnInit() {
    this.getLogs(1);
  }



  onSubmit() {
    this.display = false;
    this.getLogs(1);
  }

  manageTable(event: PageEvent) {
    this.display = false;
    this.getLogs(event.page + 1);
  }

  getLogs(page: number) {
    if (this.form.invalid) {
      return;
    }
    this.activityType = 'Administration';
    this.isLoading = true;
    this.spinnerService.show();
      const searchTerm =
        this.searchForm.value.searchInput !== null
          ? this.searchForm.value.searchInput
          : '';
      const fromDate = this.searchForm.value.from;
      const toDate = this.searchForm.value.to;
      const logLevel = this.searchForm.value.logLevel;
      const auditLogs = this.searchForm.value.auditLogs;
      this.loggingClient.getLogEntries(fromDate, logLevel, page, this.currentPaging.pageSize, searchTerm, auditLogs, toDate, this.activityType)
      .subscribe(responseData => {
        this.response = responseData;
        this.currentPaging.rows = this.response.pageSize;
        this.currentPaging.totalRecords = this.response.totalCount;
        this.currentPaging.pageNumber = this.response.pageNumber;
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.notificationService.error(this.translateService.instant('FrontendConfiguration.CheckServiceConfig'), this.translateService.instant('Logging.ErrorMessage'));
        this.spinnerService.hide();
        this.isLoading = false;
      },
      () => {
        this.spinnerService.hide();
        this.isLoading = false;
      });
  }

  logLevelKeys(): Array<string> {
    return Object.keys(this.logLevelValues);
  }

  showDetails(entry: LogEntryDto, $event: Event) {
    $event.stopPropagation();
    this.display = true;
    this.logEntry = entry;
  }

  onDialogClose($event: boolean) {
    this.display = $event;
  }

  trackLogByDate(index: number, log: LogEntryDto): moment.Moment {
    return log.logDate;
  }
}

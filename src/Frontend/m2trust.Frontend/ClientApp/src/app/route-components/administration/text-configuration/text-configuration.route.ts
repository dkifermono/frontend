import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as cloneDeep from 'lodash/cloneDeep';
import {
  CancelActivityService,
  RoutingStateService,
  NotificationService,
  ErrorNotificationService,
  ConfigFormService
} from '../../../common/services';
import {
  CanComponentDeactivate,
  ConfigurationService,
  TextConfigurationDto,
  TextItemDto
} from '../../../common';


@Component({
  selector: 'app-text-configuration',
  templateUrl: 'text-configuration.route.html'
})
export class TextConfigurationRouteComponent extends CanComponentDeactivate implements OnInit {
  configurationForm: FormGroup;
  configuration: TextConfigurationDto;
  initialConfiguration: TextConfigurationDto;
  activityType = 'Administration';

  welcomeTextForm: FormGroup;
  footerTextForm: FormGroup;
  headerTextForm: FormGroup;
  contactAddressForm: FormGroup;
  helpDeskTextForm: FormGroup;

  constructor(
    private spinner: Ng4LoadingSpinnerService,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private errorNotificationService: ErrorNotificationService,
    private configurationClient: ConfigurationService,
    private configFormService: ConfigFormService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  ngOnInit() {
    this.getConfiguration();
  }

  onSubmit() {
    this.configFormService.validateAllFormFields(this.configurationForm);
    if (!this.configurationForm.valid) {
      this.translateService.get('FrontendConfiguration.CheckValidation').subscribe(text => {
        this.notificationService.alert('', text);
      });
      return;
    }
    this.spinner.show();
    const configuration = this.createTextConfigurationDto();
    this.configurationClient.postTextConfiguration(configuration, this.activityType)
      .subscribe(response => {
        this.initialConfiguration = cloneDeep(configuration);
        this.configuration = cloneDeep(configuration);
        this.configurationForm.reset();
        this.createForm();
        this.translateService.get('FrontendConfiguration.SaveConfigurationSuccess').subscribe(text => {
          this.notificationService.success(text);
        });
        setTimeout(() => {
          location.reload();
        }, 5000);
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.spinner.hide();
      },
      () => this.spinner.hide());
  }

  resetForm() {
    this.createForm();
    this.configuration = cloneDeep(this.initialConfiguration);
  }

  private createTextConfigurationDto(): TextConfigurationDto {
    const values = this.configurationForm.getRawValue();
    const result = new TextConfigurationDto();
    result.contactAddressText = this.getPageTextItem(values.contactAddressText);
    result.footerText = this.getPageTextItem(values.footerText);
    result.headerText = this.getPageTextItem(values.headerText);
    result.helpDeskInstruction = this.getPageTextItem(values.helpDeskText);
    result.welcomeText = this.getPageTextItem(values.welcomeText);
    return result;
  }

  private getPageTextItem(item: any): TextItemDto {
    item.en = this.configFormService.getStringValue(item.en);
    item.de = this.configFormService.getStringValue(item.de);
    return new TextItemDto(item);
  }

  private getConfiguration() {
    this.spinner.show();
    this.configurationClient.getTextConfiguration()
      .subscribe(response => {
        this.configuration = cloneDeep(response);
        this.initialConfiguration = cloneDeep(response);
        this.createForm();
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.spinner.hide();
      },
      () => this.spinner.hide());
  }

  private createForm() {
    this.welcomeTextForm = this.createPageTextItemFormGroup(this.configuration.welcomeText);
    this.footerTextForm = this.createPageTextItemFormGroup(this.configuration.footerText);
    this.headerTextForm = this.createPageTextItemFormGroup(this.configuration.headerText);
    this.contactAddressForm = this.createPageTextItemFormGroup(this.configuration.contactAddressText);
    this.helpDeskTextForm = this.createPageTextItemFormGroup(this.configuration.helpDeskInstruction);
    const form = {
      welcomeText: this.welcomeTextForm,
      footerText: this.footerTextForm,
      headerText: this.headerTextForm,
      contactAddressText: this.contactAddressForm,
      helpDeskText: this.helpDeskTextForm
    };
    this.configurationForm = new FormGroup(form);
    this.form = this.configurationForm;
  }

  private createPageTextItemFormGroup(item: TextItemDto): FormGroup {
    return new FormGroup({
      en: new FormControl(item.en),
      de: new FormControl(item.de)
    });
  }
}

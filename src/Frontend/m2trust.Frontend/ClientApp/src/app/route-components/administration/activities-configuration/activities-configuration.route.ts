import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as cloneDeep from 'lodash/cloneDeep';

import {
  CancelActivityService,
  RoutingStateService,
  NotificationService,
  ErrorNotificationService,
  ConfigFormService
} from '../../../common/services';

import {
  CanComponentDeactivate,
  ConfigurationService,
  ActivitiesConfigurationDto,
  BaseActivityDto
} from '../../../common';

@Component({
  selector: 'app-activities-configuration',
  templateUrl: 'activities-configuration.route.html'
})

export class ActivitiesConfigurationRouteComponent extends CanComponentDeactivate implements OnInit {
  configurationForm: FormGroup;
  configuration: ActivitiesConfigurationDto;
  initialConfiguration: ActivitiesConfigurationDto;
  activityType = 'Administration';


  manageRequestsForm: FormGroup;
  disableSmartcardForm: FormGroup;
  enrollForm: FormGroup;
  unblockSmartcardForm: FormGroup;
  recoverCertificateForm: FormGroup;
  reinstateForm: FormGroup;
  retireTemporarySmartcardForm: FormGroup;
  revokeCertificateForm: FormGroup;
  suspendSmartcardForm: FormGroup;
  rewriteSmartcardForm: FormGroup;
  renewCertificateForm: FormGroup;
  loggingForm: FormGroup;
  uiSettingsForm: FormGroup;

  constructor(
    private spinner: Ng4LoadingSpinnerService,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private errorNotificationService: ErrorNotificationService,
    private configurationClient: ConfigurationService,
    private configFormService: ConfigFormService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  ngOnInit() {
    this.getConfiguration();
  }

  onSubmit() {
    this.configFormService.validateAllFormFields(this.configurationForm);
    if (!this.configurationForm.valid) {
      this.translateService.get('FrontendConfiguration.CheckValidation').subscribe(text => {
        this.notificationService.alert('', text);
      });
      return;
    }
    this.spinner.show();
    const configuration = this.createActivitiesConfigurationDto();
    this.configurationClient.postActivitiesConfiguration(configuration, this.activityType)
      .subscribe(response => {
        this.initialConfiguration = cloneDeep(configuration);
        this.configuration = cloneDeep(configuration);
        this.createForm();
        this.translateService.get('FrontendConfiguration.SaveConfigurationSuccess').subscribe(text => {
          this.notificationService.success(text);
        });
        setTimeout(() => {
          location.reload();
        }, 5000);
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.spinner.hide();
      },
      () => this.spinner.hide());
  }

  resetForm() {
    this.createForm();
    this.configuration = cloneDeep(this.initialConfiguration);
  }

  private createActivitiesConfigurationDto(): ActivitiesConfigurationDto {
    const activities = new ActivitiesConfigurationDto();
    const values = this.configurationForm.getRawValue();
    const keyNames = Object.keys(this.initialConfiguration);
    keyNames.forEach(keyName => {
      activities[keyName] = this.createActivity(values[keyName], keyName, this.initialConfiguration);
    });
    return activities;
  }

  private createActivity(fromForm: any, keyName: string, initialActivities: ActivitiesConfigurationDto): BaseActivityDto {
    const result = new BaseActivityDto();
    if (keyName !==  'administration') {
      result.continue = fromForm.continue;
      result.showComments = fromForm.showComments;
      result.name = initialActivities[keyName].name;
      result.authorizedGroups = fromForm.authorizedGroups;
    }
    return result;
  }

  private getConfiguration() {
    this.spinner.show();
    this.configurationClient.getActivitiesConfiguration()
      .subscribe(response => {
        this.configuration = cloneDeep(response);
        this.initialConfiguration = cloneDeep(response);
        this.createForm();
      },
      error => {
        this.errorNotificationService.notificationError(error);
        this.spinner.hide();
      },
      () => this.spinner.hide());
  }

  private createForm() {
    this.manageRequestsForm = this.createActivitiesGroup(this.configuration.manageRequests);
    this.disableSmartcardForm = this.createActivitiesGroup(this.configuration.disableSmartcard);
    this.enrollForm = this.createActivitiesGroup(this.configuration.enroll);
    this.unblockSmartcardForm = this.createActivitiesGroup(this.configuration.unblockSmartcard);
    this.recoverCertificateForm = this.createActivitiesGroup(this.configuration.recoverCertificate);
    this.reinstateForm = this.createActivitiesGroup(this.configuration.reinstateSmartcard);
    this.retireTemporarySmartcardForm = this.createActivitiesGroup(this.configuration.retireTemporarySmartcard);
    this.revokeCertificateForm = this.createActivitiesGroup(this.configuration.revokeCertificate);
    this.suspendSmartcardForm = this.createActivitiesGroup(this.configuration.suspendSmartcard);
    this.rewriteSmartcardForm = this.createActivitiesGroup(this.configuration.rewriteSmartcard);
    this.renewCertificateForm = this.createActivitiesGroup(this.configuration.renewCertificate);
    const form = {
      manageRequests: this.manageRequestsForm,
      disableSmartcard: this.disableSmartcardForm,
      enroll: this.enrollForm,
      unblockSmartcard: this.unblockSmartcardForm,
      recoverCertificate: this.recoverCertificateForm,
      reinstateSmartcard: this.reinstateForm,
      retireTemporarySmartcard: this.retireTemporarySmartcardForm,
      revokeCertificate: this.revokeCertificateForm,
      suspendSmartcard: this.suspendSmartcardForm,
      rewriteSmartcard: this.rewriteSmartcardForm,
      renewCertificate: this.renewCertificateForm,
    };
    this.configurationForm = new FormGroup(form);
    this.form = this.configurationForm;
  }

  private createActivitiesGroup(item: BaseActivityDto): FormGroup {
    return new FormGroup({
      showComments: new FormControl(item.showComments),
      continue: new FormControl(item.continue),
      authorizedGroups: this.configFormService.createFormArrayControl(item.authorizedGroups)
    });
  }
}

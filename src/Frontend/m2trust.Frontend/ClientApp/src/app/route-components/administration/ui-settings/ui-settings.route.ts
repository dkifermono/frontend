import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  NotificationService,
  CancelActivityService,
  RoutingStateService,
  ErrorNotificationService
} from '../../../common/services';
import { ColorsDto, CanComponentDeactivate } from '../../../common';
import { ConfigurationService } from '../../../common/restclient';

class ValidationMessage {
  type: string;
  text: string;
  controlName?: string;

  constructor(type: string, text: string, controlName?: string) {
    this.type = type;
    this.text = text;
    this.controlName = controlName;
  }
}

enum ControlNameIdentifier {
  menuBackgroundColor = 'Menu backgorund color',
  menuFontColor = 'Menu font color',
  pageBackgroundColor = 'Page background color',
  pageTitleBackgroundColor = 'Page title background color',
  pageTitleFontColor = 'Page title font color',
  selectedMenuItemBackgroundColor = 'Selected menu item background color',
  selectedMenuItemFontColor = 'Selected menu item font color',
  selectedTableRowBackgroundColor = 'Selected table row background color',
  selectedTableRowFontColor = 'Selected table row font color',
  tableRowFontColor = 'Table row font color'
}

@Component({
  selector: 'app-ui-settings',
  templateUrl: 'ui-settings.route.html'
})
export class UISettingsRouteComponent extends CanComponentDeactivate implements OnInit {
  private _settings: ColorsDto;
  private _settingsForm: FormGroup;
  private _file: File;
  isLoaded = false;
  activityType = 'Administration';
  hexRegex = '^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$';
  errorList: ValidationMessage[] = [];
  readonly imageType = 'image';
  readonly colorsType = 'colors';
  hasColors = false;
  hasImage = false;
  fileLabel = 'Select image';
  isFormDirty = false;
  initialSettings: ColorsDto;

  constructor(
    private spinner: Ng4LoadingSpinnerService,
    private notificationService: NotificationService,
    private configurationService: ConfigurationService,
    private translateService: TranslateService,
    private errorNotificationService: ErrorNotificationService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  get settingsForm(): FormGroup {
    return this._settingsForm;
  }

  get settings(): ColorsDto {
    return this._settings;
  }
  set settings(settings: ColorsDto) {
    this._settings = settings;
  }

  get file(): File {
    return this._file;
  }

  set file(file: File) {
    this._file = file;
  }

  ngOnInit() {
    this.getInitialSettings();
  }

  private createForm() {
    const form = {
      menuBackgroundColor: new FormControl(this.settings.menuBackgroundColor, [Validators.pattern(this.hexRegex)]),
      innerMenuBackgroundColor: new FormControl(this.settings.innerMenuBackgroundColor, [Validators.pattern(this.hexRegex)]),
      menuFontColor: new FormControl(this.settings.menuFontColor, [Validators.pattern(this.hexRegex)]),
      menuHoverFontColor: new FormControl(this.settings.menuHoverFontColor, [Validators.pattern(this.hexRegex)]),
      pageBackgroundColor: new FormControl(this.settings.pageBackgroundColor, [Validators.pattern(this.hexRegex)]),
      pageTitleBackgroundColor: new FormControl(this.settings.pageTitleBackgroundColor, [Validators.pattern(this.hexRegex)]),
      pageTitleFontColor: new FormControl(this.settings.pageTitleFontColor, [Validators.pattern(this.hexRegex)]),
      selectedMenuItemBackgroundColor: new FormControl(this.settings.selectedMenuItemBackgroundColor, [Validators.pattern(this.hexRegex)]),
      selectedMenuItemFontColor: new FormControl(this.settings.selectedMenuItemFontColor, [Validators.pattern(this.hexRegex)]),
      selectedTableRowBackgroundColor: new FormControl(this.settings.selectedTableRowBackgroundColor, [Validators.pattern(this.hexRegex)]),
      selectedTableRowFontColor: new FormControl(this.settings.selectedTableRowFontColor, [Validators.pattern(this.hexRegex)]),
      tableRowFontColor: new FormControl(this.settings.tableRowFontColor, [Validators.pattern(this.hexRegex)])
    };
    this._settingsForm = new FormGroup(form);
    this.form = this._settingsForm;
  }

  onSubmitSettings() {
    if (this.settingsForm.invalid) {
      return;
    }
    this.isFormDirty = false;
    this._settingsForm.markAsPristine();
    if (this.errorList.length === 0) {
      this.spinner.show();
      const formData = this.getFormData();
      if (this.file) {
        this.hasImage = true;
        formData.append('image', this.file);
      }
      this.configurationService.postUISettingsAsync(formData)
        .subscribe(response => {
          this.spinner.hide();
          const isColorOk = response.colorsResult === this.hasColors;
          const isImageOk = response.imageResult === this.hasImage;
          if (isColorOk && isImageOk) {
            this.translateService.get('FrontendConfiguration.SaveConfigurationSuccess').subscribe(text => {
              this.notificationService.success(text);
            });
            setTimeout(() => {
              location.reload();
            }, 5000);
          } else {
            let message = '';
            if (!isColorOk) {
              message = message + 'Colors are not set';
            }
            if (!isImageOk) {
              message = message + ' Image is not set';
            }
            this.notificationService.error(this.translateService.instant('ActivityFailed'), message);
          }
        },
        error => {
          this.resetForm();
          this.errorNotificationService.notificationError(error);
          this.spinner.hide();
        },
        () => this.spinner.hide());
    }

  }

  onColorChange(controlName: string) {
    this.errorList = this.errorList.filter(e => e.controlName !== controlName);
    this._settingsForm.markAsDirty();
    this.isFormDirty = true;
  }

  private getInitialSettings() {
    this.spinner.show();
    this.configurationService.getColors(this.activityType)
      .subscribe(response => {
        this.settings = response;
        this.initialSettings = JSON.parse(JSON.stringify(this.settings));
        this.createForm();
        this.isLoaded = true;
      },
    error => {
      this.errorNotificationService.notificationError(error);
      this.spinner.hide();
    },
  () => this.spinner.hide());
  }

  resetForm() {
    this.isFormDirty = false;
    this.createForm();
    this.settings = JSON.parse(JSON.stringify(this.initialSettings));
    this.file = null;
    this.fileLabel = this.translateService.instant('UISettings.SelectImage');
  }

  upload(event)  {
    this.isFormDirty = true;
    this.file = null;
    const uploadedFile = File = event.target.files[0];
      this.file = File = event.target.files[0];
      this.fileLabel = this.file.name;


  }
  private getFormData(): FormData {
    const formData = new FormData();
    this.settings = this.settingsForm.getRawValue();
    Object.keys(this.settingsForm.controls).forEach(controlName => {
      const control = this.settingsForm.controls[controlName];
      if (control.touched) {
        this.hasColors = true;
        formData.append('colors[' + controlName + ']', this.settings[controlName]);
      }
    });
    return formData;
  }
}




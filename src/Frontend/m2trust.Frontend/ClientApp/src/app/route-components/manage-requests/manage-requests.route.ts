import { Component, OnInit } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { FormGroup } from '@angular/forms';
import {
  RequestClient,
  RESTPagedResultOfSignedRequestDto,
  SignedRequestDto,
  IValidationDto,
  SecretDto,
  RequestSearchForm,
  Page,
  PageEvent,
  CanComponentDeactivate
} from '../../common';
import {
  ManageRequestsDistributeSecretsService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';
import {ErrorNotificationService} from '../../common/services';
import { ValidationService } from '../../common/services/validation.service';

@Component({
  selector: 'app-manage-requests',
  templateUrl: './manage-requests.route.html'
})
export class ManageRequestsRouteComponent extends CanComponentDeactivate implements OnInit {
  selectedRows: SignedRequestDto[] = [];
  searchRequest: RequestSearchForm;
  activityType: string;
  requestId: string;
  response: RESTPagedResultOfSignedRequestDto = null;
  validation: IValidationDto = {
    approve: false,
    deny: false,
    distributeSecrets: false,
    cancel: false,
    abandon: false
  };
  loading = false;

  display = false;
  isValidating: boolean;
  isChecked = false;

  currentPaging: Page = new Page();

  noDataMessage = 'ManageRequests.NoDataInit';

  // sorting properties
  byTargetUser = 'TargetUser';
  targetUserAsc = false;
  byOriginator = 'Originator';
  originatorAsc = false;
  byType = 'Type';
  typeAsc = false;
  byStatus = 'Status';
  statusAsc = false;
  bySubmitted = 'Submitted';
  submittedAsc = false;
  byTargetType = 'TargetType';
  targetTypeAsc = false;

  // search form properties
  page = 1;
  orderBy = 'Submitted';
  ascending = false;

  secrets: SecretDto[];


  constructor(
    private requestClient: RequestClient,
    private spinnerService: Ng4LoadingSpinnerService,
    private distributeSecretsData: ManageRequestsDistributeSecretsService,
    private validationService: ValidationService,
    private errorNotificationService: ErrorNotificationService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  ngOnInit(): void {
    this.validationService.isValidating.subscribe(value => {
      this.isValidating = value;
    });
    this.validationService.currentMessage.subscribe(value => {
      this.selectedRows = value;
    });
  }

  onFormStatusChange($event: FormGroup) {
    this.form = $event;
  }

  receiveGetListEvent($event: boolean) {
    this.getRequestList();
    this.secrets = [];
  }

  // Methods used for getting requests

  receiveSearchRequest($event: RequestSearchForm) {
    this.searchRequest = $event;
    this.validationService.changeSearchFormSubmittedState(true);
    this.getRequestList();
  }

  private getRequestList() {
    this.selectedRows = [];
    this.validationService.changeMessage(this.selectedRows);
    this.activityType = 'ManageRequests';
    this.distributeSecretsData.changeMessage(this.secrets);
    this.spinnerService.show();
    this.loading = true;
    this.requestClient
      .getRequestList(
        this.ascending,
        this.orderBy,
        this.searchRequest.originatorUserName,
        this.page,
        this.searchRequest.requestForm,
        this.searchRequest.requestState,
        this.searchRequest.submittedFrom,
        this.searchRequest.submittedTo,
        this.searchRequest.targetUserName,
        this.activityType
      )
      .subscribe(
        response => {
          this.response = response;
          this.currentPaging.rows = response.pageSize;
          this.currentPaging.totalRecords = response.totalCount;
          this.currentPaging.pageNumber = response.pageNumber;
          if (response.totalCount <= 0) {
            this.noDataMessage = 'ManageRequests.NoData';
          }
        },
        error => {
          this.errorNotificationService.notificationError(error);
          this.spinnerService.hide();
          this.loading = false;
        },
        () => {
          this.spinnerService.hide();
          this.loading = false;
        }
      );
  }

  getRequestsPageChange($event: PageEvent) {
    this.page = $event.page + 1;
    this.selectedRows = [];
    this.getRequestList();
  }

  sort(orderBy: string, asc: boolean) {
    this.orderBy = orderBy;
    this.ascending = asc;
    this.getRequestList();
  }

  // Methods used for displaying request details

  showDetails(id: string, $event: Event) {
    $event.stopPropagation();
    this.display = true;
    this.requestId = id;
  }

  onDialogClose($event: boolean) {
    this.display = $event;
  }


  // Methods used for selecting rows

  markAll() {
    this.isChecked = !this.isChecked;
    let rows = JSON.parse(JSON.stringify(this.response.items));
    if (this.isChecked) {
      rows.forEach(
        item =>
          (this.response.items[rows.indexOf(item)][
            'isActive'
          ] = true)
      );
    } else {
      rows.forEach(item => {
        this.response.items[rows.indexOf(item)][
          'isActive'
        ] = false;
      });
      rows = [];
    }
    this.selectedRows = rows;
    this.validationService.changeMessage(this.selectedRows);
  }
  selectRequest(item: SignedRequestDto) {
    if (item['isActive']) {
      const index: number = this.selectedRows.map(e => e.requestId).indexOf(item.requestId);
      if (index !== -1) {
        this.selectedRows.splice(index, 1);
      }
      item['isActive'] = false;
    } else {
      this.selectedRows.push(item);
      item['isActive'] = true;
    }
    this.selectedRows = this.selectedRows.slice();
    this.validationService.changeMessage(this.selectedRows);
  }

  trackRequestById(index: number, item: SignedRequestDto) {
    return item.requestId;
  }
}

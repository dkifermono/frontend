import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  SectionType,
  CanComponentDeactivate,
  ProfileClient,
  ProfileDto,
  ProfileItemDto,
  WizardData,
  PageHeaderData,
  DataCollectionItemClient,
  DataCollectionDto
} from '../../common';
import {
  ErrorNotificationService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-retire-select-profile',
  templateUrl: 'retire-select-profile.route.html'
})

export class RetireSelectProfileRouteComponent extends CanComponentDeactivate implements OnInit {
  profileDto: ProfileDto;
  selectedProfile;
  dataCollection: DataCollectionDto = new DataCollectionDto();
  private activityType: string;
  private userId: string;
  private activeProfiles: ProfileItemDto[];
  public isLoading = true;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private spinnerService: Ng4LoadingSpinnerService,
    private errorNotificationService: ErrorNotificationService,
    private dataCollectionItemClient: DataCollectionItemClient,
    private profileClient: ProfileClient,
    private wizardData: WizardData,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  ngOnInit() {
  this.activityType = 'RetireTemporarySmartcard';
  this.wizardData.clearDataCollection();
  this.route.params.subscribe(params => {
  this.spinnerService.show();
  this.isLoading = true;
  this.userId = params['userId'];
  this.profileClient.getProfiles(this.userId, this.activityType).subscribe(
    result => {
      this.profileDto = result;
      if (this.profileDto && this.profileDto.profiles) {
        this.activeProfiles = this.profileDto.profiles
        .filter(x =>
        this.profileDto.eligibleProfilesStates.find(
          y => y.toString() === x.status.toString()
        )
      );
    }

    if (this.activeProfiles && this.activeProfiles.length > 0) {
      const profile = this.activeProfiles[0];
      if ((this.activeProfiles.length === 1)) {
        this.selectedProfile = profile;
      }
    }
    this.setSelectedProfileFromWizard();
    },
    error => {
      this.errorNotificationService.notificationError(error);
      this.spinnerService.hide();
    },

    () => {
      this.spinnerService.hide();
      this.isLoading = false;
    }
  );
});
  const pageHeaderData = this.wizardData.getPageHeaderData();
    this.wizardData.setPageHeaderData(new PageHeaderData(pageHeaderData.userName));
}

setSelectedProfileFromWizard() {
  const wizardProfileId = this.wizardData.getProfileTemplateId();
  if (wizardProfileId) {
    const selected = this.activeProfiles.filter(a => a.id === wizardProfileId);
    if (selected.length > 0) {
      this.onProfileSelectionChange(selected[0]);
    }
  }
}

onProfileSelectionChange(item: ProfileItemDto) {
  if (!this.isProfileDisabled(item)) {
    this.selectedProfile = item;
  }
}

isProfileSelected(item: ProfileItemDto) {
  if (this.selectedProfile) {
    return this.selectedProfile.id === item.id;
  }
  return false;
}

isProfileDisabled(item: ProfileItemDto) {
  return (
    this.profileDto.eligibleProfilesStates.filter(
      y => y.toString() === item.status.toString()
    ).length === 0
  );
}

  goToNextStep(profileId: string) {
    if (this.userId == null || profileId == null) {
      return;
    }

    const pageHeaderData = this.wizardData.getPageHeaderData();
    this.wizardData.setPageHeaderData(new PageHeaderData(pageHeaderData.userName, this.selectedProfile.profileTemplate.name));
    this.wizardData.setProfileTemplateId(this.selectedProfile.id);
    this.router.navigate(['/retire-temporary-smartcard/collect-data-collection', this.userId, this.selectedProfile.id], {relativeTo: this.route});

  }
}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  SectionType,
  CanComponentDeactivate
} from '../../common';
import {
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-retire-smart-data-collection',
  templateUrl: 'retire-data-collection.route.html'
})

export class RetireDataCollectionRouteComponent extends CanComponentDeactivate implements OnInit {
  sectionType = SectionType.Retire;
  private userId: string;
  private profileId: string;
  constructor (
    private route: ActivatedRoute,
    private router: Router,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

    ngOnInit() {
      this.route.params.subscribe(params => {
        this.userId = params['userId'];
        this.profileId = params['profileId'];
      });
}
onNavigateToNextPage() {
  this.router.navigate(
    [
      '/retire-temporary-smartcard/audit',
      this.userId,
      this.profileId
    ],
    { relativeTo: this.route }
  );
}
}

import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router,  } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  SmartcardClient,
  DciDataDtoSectionType,
  AuditRequestDto,
  IAuditRequestDto,
  WizardData,
  AuditResult,
  CanComponentDeactivate
} from '../../common';
import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-retire-audit',
  templateUrl: 'retire-audit.route.html'
})

export class RetireAuditRouteComponent extends CanComponentDeactivate implements OnDestroy {
  sectionType = DciDataDtoSectionType.Retire;
  audit: IAuditRequestDto;
  activityType: string;
  firstStepUrl = '/retire-temporary-smartcard';
  description = '';
  isSuccessful = false;
  otp: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private smartcardClient: SmartcardClient,
    private wizardData: WizardData,
    private finishService: FinishActivityService,
    private spinner: Ng4LoadingSpinnerService,
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  onSubmit($event) {
    this.spinner.show();
    this.activityType = 'RetireTemporarySmartcard';
    this.audit = {
      userId: $event.userId,
      profileTemplateId: $event.profileTemplateId,
      comment: $event.comment,
      profileId: $event.profileId,
      dataCollections: $event.dataCollections,
      signature: $event.signature
    };
    this.smartcardClient.retireAndReinstate(new AuditRequestDto(this.audit), this.activityType).subscribe(
      response => {
        if (response.permanentCardId === null || response.automaticReinstateEnabled === true) {
          this.description = response.otpResult.hasSignature ? 'FinishRequest.Retire.SuccessSignatureDescription' : 'FinishRequest.Retire.SuccessDescription';
          this.isSuccessful = true;
          this.otp = response.otpResult.oneTimePassword;
          this.spinner.hide();
          this.navigateToFinish();
        } else {
          this.spinner.hide();
          this.router.navigate(
            ['/reinstate-smartcard/collect-data-collection',
            $event.userId,
            response.permanentCardId], {
              relativeTo: this.route
          });
        }
      },
      error => {
        this.wizardData.setErrorData(error);
        this.isSuccessful = false;
        this.description = 'FinishRequest.Retire.FailedDescription';
        this.spinner.hide();
        this.navigateToFinish();
      }
    );
  }
  private navigateToFinish() {
    this.router.navigate(['/retire-temporary-smartcard/finish'], {
      relativeTo: this.route
    });
  }

  ngOnDestroy() {
    this.finishService.auditResult = new AuditResult(this.description, this.firstStepUrl, this.isSuccessful, this.otp);
  }
}

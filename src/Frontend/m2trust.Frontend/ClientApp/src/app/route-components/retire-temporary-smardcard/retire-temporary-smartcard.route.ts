import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CanComponentDeactivate } from '../../common';
import { CancelActivityService, RoutingStateService } from '../../common/services';

@Component({
  selector: 'app-retire-temporary-smartcard',
  templateUrl: './retire-temporary-smartcard.route.html'
})

export class RetireTemporarySmartcardRouteComponent extends CanComponentDeactivate {

  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

  onFormStatusChange($event: FormGroup) {
    this.form = $event;
  }
}

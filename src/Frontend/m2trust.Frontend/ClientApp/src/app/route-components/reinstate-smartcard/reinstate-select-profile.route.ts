import { Component, OnInit} from '@angular/core';
import {
  SectionType2,
  CanComponentDeactivate
} from '../../common';
import {
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

@Component({
  selector: 'app-reinstate-select-profile',
  templateUrl: 'reinstate-select-profile.route.html'
})

export class ReinstateSelectProfileRouteComponent extends CanComponentDeactivate implements OnInit {
  sectionType = SectionType2.Reinstate;
  constructor(
    cancelActivityService: CancelActivityService,
    routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
   }

  ngOnInit() {}
}

import { Component, OnInit } from '@angular/core';
import { AuditResult } from '../../common';
import { FinishActivityService } from '../../common/services';

@Component({
  selector: 'app-reinstate-finish',
  templateUrl: 'reinstate-finish.route.html'
})

export class ReinstateFinishRouteComponent implements OnInit {
  auditResult: AuditResult;
  loadComponent = false;
  constructor(private finishService: FinishActivityService) { }

  ngOnInit() {
    this.auditResult = this.finishService.auditResult;
    this.loadComponent = true;
  }
}

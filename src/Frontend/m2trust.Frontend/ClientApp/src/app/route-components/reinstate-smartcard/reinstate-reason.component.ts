import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { map } from 'rxjs/operators';
import {
  SmartcardClient,
  IAuditRequestDto,
  AuditRequestDto,
  AuditRequestDtoReason
} from '../../common/restclient/restClient';
import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';
import { AuditResult, CanComponentDeactivate } from '../../common';
import { WizardData } from '../../common/providers/wizardData';

@Component({
  selector: 'app-reinstate-smartcard-reason',
  templateUrl: 'reinstate-reason.component.html',
})

export class ReinstateSmartReasonComponent extends CanComponentDeactivate implements OnDestroy, OnInit {
suspendReinstate: IAuditRequestDto;
activityType: string;
cachedData = [];
reinstateReason = AuditRequestDtoReason.CertificateReinstate;
private _effectiveRevocation: number;
auditData: {};

firstStepUrl = '/reinstate-smartcard';
  description = '';
  isSuccessful = false;
  otp: string;

get effectiveRevocation() {
  return this._effectiveRevocation;
}
set effectiveRevocation(value: number) {
  this._effectiveRevocation = value;
}

constructor(
  private route: ActivatedRoute,
  private router: Router,
  private smartcardClient: SmartcardClient,
  private wizardData: WizardData,
  private finishService: FinishActivityService,
  private spinner: Ng4LoadingSpinnerService,
  cancelActivityService: CancelActivityService,
  routingStateService: RoutingStateService
  ) {
    super(cancelActivityService, routingStateService);
  }

ngOnInit() {
  this.route.paramMap.pipe(map(() => window.history.state)).subscribe(s => {
    this.auditData = s.audit;
  });
}

navigateToNextPage($event) {
  this.wizardData.clearWizard();
  this.spinner.show();
  this.activityType = 'ReinstateSmartcard';
  this.suspendReinstate = {
  userId: $event.userId,
  profileId: $event.profileId,
  dataCollections: $event.dataCollections,
  effectiveRevocationInHours: this.effectiveRevocation,
  signature: $event.signature,
  comment: this.auditData['comment']
};
this.smartcardClient.reinstate(new AuditRequestDto(this.suspendReinstate), this.activityType).subscribe(
  response => {
    this.description = response.hasSignature ? 'FinishRequest.Reinstate.SuccessSignatureDescription' : 'FinishRequest.Reinstate.SuccessDescription';
    this.isSuccessful = true;
    this.otp = response.oneTimePassword;
    this.spinner.hide();
    this.navigateToFinish();

  },
  error => {
    this.wizardData.setErrorData(error);
    this.isSuccessful = false;
    this.description = 'FinishRequest.Disable.FailedDescription';
    this.spinner.hide();
    this.navigateToFinish();
  }
);
}

private navigateToFinish() {
  this.router.navigate(['/reinstate-smartcard/finish'], {
    relativeTo: this.route
  });
}

ngOnDestroy() {
  this.finishService.auditResult = new AuditResult(this.description, this.firstStepUrl, this.isSuccessful, this.otp);
}
}

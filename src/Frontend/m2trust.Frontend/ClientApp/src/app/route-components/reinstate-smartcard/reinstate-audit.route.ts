import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router,  } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
  WizardData,
  FinishRequestClient,
  DciDataDtoSectionType,
  IAuditRequestDto,
  AuditRequestDto,
  SmartcardClient,
  AuditResult,
  CanComponentDeactivate
} from '../../common';

import {
  FinishActivityService,
  CancelActivityService,
  RoutingStateService
} from '../../common/services';

  @Component({
    selector: 'app-reinstate-smart-audit',
    templateUrl: 'reinstate-audit.route.html'
  })

  export class ReinstateSmartAuditRouteComponent extends CanComponentDeactivate implements OnDestroy {
    sectionType = DciDataDtoSectionType.Reinstate;
    audit: IAuditRequestDto;
    activityType: string;

    firstStepUrl = '/reinstate-smartcard';
    description = '';
    isSuccessful = false;
    otp: string;
    constructor(
      private route: ActivatedRoute,
      private router: Router,
      private finishRequestClient: FinishRequestClient,
      private smartcardClient: SmartcardClient,
      private wizardData: WizardData,
      private finishService: FinishActivityService,
      private spinner: Ng4LoadingSpinnerService,
      cancelActivityService: CancelActivityService,
      routingStateService: RoutingStateService
    ) {
      super(cancelActivityService, routingStateService);
    }

    onSubmit($event) {
      this.spinner.show();
      this.activityType = 'ReinstateSmartcard';
      this.audit = {
        userId: $event.userId,
        profileId: $event.profileId,
        profileTemplateId: $event.profileTemplateId,
        comment: $event.comment,
        dataCollections: $event.dataCollections,
        signature: $event.signature
      };
      this.finishRequestClient.allowSpecifyDelay(new AuditRequestDto(this.audit), this.activityType).subscribe(
          result => {
            if (result === false) {
              this.smartcardClient.reinstate(new AuditRequestDto(this.audit), this.activityType).subscribe(
                response => {
                  this.description = response.hasSignature ? 'FinishRequest.Reinstate.SuccessSignatureDescription' : 'FinishRequest.Reinstate.SuccessDescription';
                  this.isSuccessful = true;
                  this.otp = response.oneTimePassword;
                  this.spinner.hide();
                  this.navigateToFinish();
              },
              error => {
                this.wizardData.setErrorData(error);
                this.isSuccessful = false;
                this.description = 'FinishRequest.Disable.FailedDescription';
                this.spinner.hide();
                this.navigateToFinish();
              }
              );
            } else {
              this.spinner.hide();
              if ($event.signature) {
                this.router.navigate(
                  ['/reinstate-smartcard/reason',
                  $event.userId,
                  $event.profileId,
                  $event.signature], {
                   relativeTo: this.route,
                   state: {audit: this.audit }
                  }
                );
              } else {
                this.router.navigate(
                  ['/reinstate-smartcard/reason',
                  $event.userId,
                  $event.profileId], {
                   relativeTo: this.route,
                   state: {audit: this.audit }
                  }
                );
              }
             }
          }
      );
    }
    private navigateToFinish() {
      this.router.navigate(['/reinstate-smartcard/finish'], {
        relativeTo: this.route
      });
    }

    ngOnDestroy() {
      this.finishService.auditResult = new AuditResult(this.description, this.firstStepUrl, this.isSuccessful, this.otp);
    }
  }

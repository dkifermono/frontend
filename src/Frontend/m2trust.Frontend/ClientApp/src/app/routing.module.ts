import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppCommonModule } from './common/common.module';
import { ComponentsModule } from './components/components.module';
import { HomeRouteComponent } from './route-components/home';
import { ManageRequestsRouteComponent } from './route-components/manage-requests';
import {
  SelectSmartcardRouteComponent,
  DisableSmartcardRouteComponent,
  DisableSmartAuditRouteComponent,
  DisableSmartDataCollectionRouteComponent,
  DisableSmartcardFinishRouteComponent,
  DisableContinueRouteComponent,
  DisableReasonRouteComponent
} from './route-components/disable-smartcard';
import {
  EnrollUserOrCertificateRouteComponent,
  DataCollectionRouteComponent,
  SelectProfileTemplateRouteComponent,
  EnrollAuditRouteComponent,
  EnrollFinishRouteComponent
} from './route-components/enroll-user';
import {
  UnblockSmartcardRouteComponent,
  UnblockSelectProfileRouteComponent,
  UnblockSmartAuditRouteComponent,
  UnblockSmartDataCollectionRouteComponent,
  UnblockSmartChallengeComponent,
  UnblockSmartResponseComponent,
  UnblockSmartFinishRouteComponent
} from './route-components/unblock-smartcard';
import {
  ReinstateSmartcardRouteComponent,
  ReinstateSelectProfileRouteComponent,
  ReinstateDataCollectionRouteComponent,
  ReinstateSmartAuditRouteComponent,
  ReinstateSmartReasonComponent,
  ReinstateFinishRouteComponent
} from './route-components/reinstate-smartcard';
import {
   RetireTemporarySmartcardRouteComponent,
   RetireSelectProfileRouteComponent,
   RetireDataCollectionRouteComponent,
   RetireAuditRouteComponent,
   RetireFinishRouteComponent
  } from './route-components/retire-temporary-smardcard';
import {
  RecoverCertSelectProfileRouteComponent,
  RecoverCertUserListRouteComponent,
  RecoverCertAuditRouteComponent,
  RecoverCertDataCollectionRouteComponent,
  RecoverCertFinishRouteComponent
} from './route-components/recover-certificate';
import {
  SuspendSmartcardRouteComponent,
  SuspendSelectProfileRouteComponent,
  SuspendDataCollectionRouteComponent,
  SuspendSmartAuditRouteComponent,
  SuspendReasonComponent,
  SuspendFinishRouteComponent
} from './route-components/suspend-smartcard';
import {
  UISettingsRouteComponent,
  LoggingRouteComponent,
  ServiceConfigurationRouteComponent,
  ProfileTemplateConfigurationRouteComponent,
  ActivitiesConfigurationRouteComponent,
  UserPageConfigurationRouteComponent,
  TextConfigurationRouteComponent,
} from './route-components';
import {
  RevokeCertificateSelectProfileRouteComponent,
  RevokeCertificateUserListRouteComponent,
  RevokeCertificateDataCollectionComponent,
  RevokeCertificateAuditComponent,
  RevokeCertificateFinishRouteComponent,
  RevokeCertificateReasonRouteComponent
} from './route-components/revoke-certificate';
import {
  RewriteSmartcardRouteComponent,
  RewriteSelectProfileRouteComponent,
  RewriteDataCollectionRouteComponent,
  RewriteAuditRouteComponent,
  RewriteFinishRouteComponent,
  RewriteContinueRouteComponent
} from './route-components/rewrite-smartcard';
import {
  RenewCertificateUserListRouteComponent,
  RenewCertificateSelectProfileComponent,
  RenewCertificateDataCollectionRouteComponent,
  RenewCertificateAuditRouteComponent,
  RenewCertificateFinishRouteComponent
} from './route-components/renew-certificate';

import {
  EnrollGuard,
  DisableSmartcardGuard,
  UnblockSmartcardGuard,
  RecoverCertificateGuard,
  ReinstateSmartcardGuard,
  RetireTemporarySmartcardGuard,
  RevokeCertificateGuard,
  SuspendSmartcardGuard,
  RewriteSmartcardGuard,
  ManageRequestsGuard,
  RenewCertificateGuard,
  AdministrationGuard
} from './common/guards';
import { CanDeactivateGuard } from './common';

const routes: Routes = [
  { path: 'home', component: HomeRouteComponent },
  {
    path: 'manage-requests',
    component: ManageRequestsRouteComponent,
    canActivate: [ManageRequestsGuard],
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'administration',
    canActivate: [AdministrationGuard],
    children: [
      {
        path: 'service-configuration',
        component: ServiceConfigurationRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'profile-template-configuration',
        component: ProfileTemplateConfigurationRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'activities-configuration',
        component: ActivitiesConfigurationRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'user-lookup-configuration',
        component: UserPageConfigurationRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'frontend-configuration',
        component: TextConfigurationRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'ui-settings',
        component: UISettingsRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'logging',
        component: LoggingRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      }
    ]
  },
  {
    path: 'disable-smartcard',
    canActivate: [DisableSmartcardGuard],
    children: [
      {
        path: 'select-smartcard/:userId',
        component: SelectSmartcardRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'collect-data-collection/:userId/:profileId',
        component: DisableSmartDataCollectionRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'audit/:userId/:profileId',
        component: DisableSmartAuditRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'reason/:userId/:profileId',
        component: DisableReasonRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'reason/:userId/:profileId/:signature',
        component: DisableReasonRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'finish',
        component: DisableSmartcardFinishRouteComponent
      },
      {
        path: 'continue',
        component: DisableContinueRouteComponent
      },
      {
        path: '',
        component: DisableSmartcardRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      }
    ]
  },
  {
    path: 'enroll-user',
    canActivate: [EnrollGuard],
    children: [
      {
        path: 'select-profile-template/:userId',
        component: SelectProfileTemplateRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'collect-data-collection/:userId/:profileTemplateId',
        component: DataCollectionRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'audit/:userId/:profileTemplateId',
        component: EnrollAuditRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'finish',
        component: EnrollFinishRouteComponent
      },
      {
        path: '',
        component: EnrollUserOrCertificateRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      }
    ]
  },
  {
    path: 'unblock-smartcard',
    canActivate: [UnblockSmartcardGuard],
    children: [
      {
        path: 'unblock-select-profile/:userId',
        component: UnblockSelectProfileRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'collect-data-collection/:userId/:profileId',
        component: UnblockSmartDataCollectionRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'audit/:userId/:profileId',
        component: UnblockSmartAuditRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'enter-challenge/:userId/:profileId',
        component: UnblockSmartChallengeComponent
      },
      {
        path: 'challenge-response/:userId/:profileId/:profileName',
        component: UnblockSmartResponseComponent
      },
      {
        path: 'finish',
        component: UnblockSmartFinishRouteComponent
      },
      {
        path: '',
        component: UnblockSmartcardRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      }
    ]
  },
  {
    path: 'reinstate-smartcard',
    canActivate: [ReinstateSmartcardGuard],
    children: [
      {
        path: 'reinstate-select-profile/:userId',
        component: ReinstateSelectProfileRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'collect-data-collection/:userId/:profileId',
        component: ReinstateDataCollectionRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'audit/:userId/:profileId',
        component: ReinstateSmartAuditRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'reason/:userId/:profileId',
        component: ReinstateSmartReasonComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'reason/:userId/:profileId/:signature',
        component: ReinstateSmartReasonComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'finish',
        component: ReinstateFinishRouteComponent
      },
      {
        path: '',
        component: ReinstateSmartcardRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      }
    ]
  },
  {
    path: 'suspend-smartcard',
    canActivate: [SuspendSmartcardGuard],
    children: [
      {
        path: '',
        component: SuspendSmartcardRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'suspend-select-profile/:userId',
        component: SuspendSelectProfileRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'collect-data-collection/:userId/:profileId',
        component: SuspendDataCollectionRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'audit/:userId/:profileId',
        component: SuspendSmartAuditRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'finish',
        component: SuspendFinishRouteComponent
      },
      {
        path: 'reason/:userId/:profileId',
        component: SuspendReasonComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'reason/:userId/:profileId/:signature',
        component: SuspendReasonComponent,
        canDeactivate: [CanDeactivateGuard]
      }
    ]
  },
  {
    path: 'retire-temporary-smartcard',
    canActivate: [RetireTemporarySmartcardGuard],
    children: [
      {
        path: '',
        component: RetireTemporarySmartcardRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'retire-select-profile/:userId',
        component: RetireSelectProfileRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'collect-data-collection/:userId/:profileId',
        component: RetireDataCollectionRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'audit/:userId/:profileId',
        component: RetireAuditRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'finish',
        component: RetireFinishRouteComponent
      }
    ]
  },
  {
    path: 'recover-certificate',
    canActivate: [RecoverCertificateGuard],
    children: [
      {
        path: 'select-profile/:userId',
        component: RecoverCertSelectProfileRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'collect-data-collection/:userId/:profileId',
        component: RecoverCertDataCollectionRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'audit/:userId/:profileId',
        component: RecoverCertAuditRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'finish',
        component: RecoverCertFinishRouteComponent
      },
      {
        path: '',
        component: RecoverCertUserListRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      }
    ]
  },
  {
    path: 'revoke-certificate',
    canActivate: [RevokeCertificateGuard],
    children: [
      {
        path: 'select-profile/:userId',
        component: RevokeCertificateSelectProfileRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'collect-data-collection/:userId/:profileId',
        component: RevokeCertificateDataCollectionComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'reason/:userId/:profileId',
        component: RevokeCertificateReasonRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'reason/:userId/:profileId/:signature',
        component: RevokeCertificateReasonRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'audit/:userId/:profileId',
        component: RevokeCertificateAuditComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'finish',
        component: RevokeCertificateFinishRouteComponent
      },
      {
        path: '',
        component: RevokeCertificateUserListRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      }
    ]
  },
  {
    path: 'renew-certificate',
    canActivate: [RenewCertificateGuard],
    children: [
      {
        path: 'select-profile/:userId',
        component: RenewCertificateSelectProfileComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'collect-data-collection/:userId/:profileId',
        component: RenewCertificateDataCollectionRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'audit/:userId/:profileId',
        component: RenewCertificateAuditRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'finish',
        component: RenewCertificateFinishRouteComponent
      },
      {
        path: '',
        component: RenewCertificateUserListRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      }
    ]
  },
  {
    path: 'rewrite-smartcard',
    canActivate: [RewriteSmartcardGuard],
    children: [
      {
        path: 'select-profile/:userId',
        component: RewriteSelectProfileRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'collect-data-collection/:userId/:profileId',
        component: RewriteDataCollectionRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'audit/:userId/:profileId',
        component: RewriteAuditRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'finish',
        component: RewriteFinishRouteComponent
      },
      {
        path: 'continue',
        component: RewriteContinueRouteComponent
      },
      {
        path: '',
        component: RewriteSmartcardRouteComponent,
        canDeactivate: [CanDeactivateGuard]
      }
    ]
  },
  { path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [
    AppCommonModule,
    ComponentsModule,
    RouterModule.forRoot(routes, { enableTracing: false })
  ],
  exports: [RouterModule]
})
export class RoutingModule {}

﻿using m2trust.Frontend.Web.RestModels;

namespace m2trust.Frontend.Web.Infrastructure.MapperFactories
{
    public interface IFrontendConfigurationFactory
    {
        Common.Configuration.Models.Configuration MapConfiguration(TextConfigurationDto configurationDto, Common.Configuration.Models.Configuration configuration);

        Common.Configuration.Models.Configuration MapConfiguration(UserPageConfigurationDto configurationDto, Common.Configuration.Models.Configuration configuration);

        Common.Configuration.Models.Configuration MapConfiguration(ActivitiesConfigurationDto configurationDto, Common.Configuration.Models.Configuration configuration);
    }
}

﻿using m2trust.Frontend.Web.RestModels.Configuration;
using m2trust.Messaging.Configuration.Models;

namespace m2trust.Frontend.Web.Infrastructure.MapperFactories
{
    public interface IServiceConfigurationFactory
    {
        ServiceConfiguration MapConfiguration(ServiceConfigurationDto configurationDto);

        ServiceConfigurationDto MapConfigurationDto(ServiceConfiguration configuration);
    }
}

﻿using m2trust.Frontend.Common.Configuration.Models;
using m2trust.Frontend.Web.RestModels;

namespace m2trust.Frontend.Web.Infrastructure.MapperFactories
{
    public class FrontendConfigurationFactory : IFrontendConfigurationFactory
    {

        #region TextConfiguration
        public Common.Configuration.Models.Configuration MapConfiguration(TextConfigurationDto dto, Common.Configuration.Models.Configuration configuration)
        {
            configuration.WelcomeText = MapPageTextItem(dto.WelcomeText);
            configuration.FooterText = MapPageTextItem(dto.FooterText);
            configuration.HeaderText = MapPageTextItem(dto.HeaderText);
            configuration.ContactAddressText = MapPageTextItem(dto.ContactAddressText);
            configuration.HelpDeskInstruction = MapPageTextItem(dto.HelpDeskInstruction);

            return configuration;
        }
        private PageTextItem MapPageTextItem(TextItemDto item)
        {
            return new PageTextItem() { En = item.En, De = item.De };
        }
        #endregion TextConfiguration

        #region UserPageConfiguration
        public Common.Configuration.Models.Configuration MapConfiguration(UserPageConfigurationDto dto, Common.Configuration.Models.Configuration configuration)
        {
            configuration.UserDisplayProperties = dto.UserDisplayProperties;
            configuration.UserSearchProperties = dto.UserSearchProperties;
            configuration.SearchUsersPageSize = dto.SearchUsersPageSize;

            return configuration;
        }
        #endregion UserPageConfiguration

        #region ActivitiesConfiguration
        public Common.Configuration.Models.Configuration MapConfiguration(ActivitiesConfigurationDto dto, Common.Configuration.Models.Configuration configuration)
        {
            configuration.Activities = new Activity()
            {
                DisableSmartcard = MapBaseActivity(dto.DisableSmartcard),
                Enroll = MapBaseActivity(dto.Enroll),
                ManageRequests = MapBaseActivity(dto.ManageRequests),
                RecoverCertificate = MapBaseActivity(dto.RecoverCertificate),
                ReinstateSmartcard = MapBaseActivity(dto.ReinstateSmartcard),
                RenewCertificate = MapBaseActivity(dto.RenewCertificate),
                RetireTemporarySmartcard = MapBaseActivity(dto.RetireTemporarySmartcard),
                RevokeCertificate = MapBaseActivity(dto.RevokeCertificate),
                RewriteSmartcard = MapBaseActivity(dto.RewriteSmartcard),
                SuspendSmartcard = MapBaseActivity(dto.SuspendSmartcard),
                UnblockSmartcard = MapBaseActivity(dto.UnblockSmartcard)
            };
            return configuration;
        }

        private BaseActivity MapBaseActivity(BaseActivityDto dto)
        {
            return new BaseActivity()
            {
                AuthorizedGroups = dto.AuthorizedGroups,
                Name = dto.Name,
                ShowComments = dto.ShowComments,
                Continue = dto.Continue
            };
        }



        #endregion ActivitiesConfiguration
    }
}

﻿using AutoMapper;
using m2trust.Frontend.Web.RestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using m2trust.Frontend.Model;
using m2trust.Frontend.Common.Models;

namespace m2trust.Frontend.Web.Infrastructure.MapperFactories
{
    public class ProfileTemplateConfigurationFactory : IProfileTemplateConfigurationFactory
    {
        #region Fields

        private readonly IMapper mapper;

        #endregion Fields

        #region Constructors

        public ProfileTemplateConfigurationFactory(IMapper mapper)
        {
            this.mapper = mapper;
        }

        #endregion Constructors

        #region  Methods

        public Common.Configuration.Models.Configuration MapConfiguration(ProfileTemplatesConfigurationDto configurationDto, IEnumerable<ProfileTemplateEntry> profileTemplateLookup, Common.Configuration.Models.Configuration configuration)
        {
            configuration.DCIProfileTemplates = configurationDto.DCIProfileTemplates.Select(dci => new ProfileTemplate() { ProfileTemplateId = dci.ProfileTemplateId, ReinstateDCIs = MapReinstateDCIs(dci) });

            configuration.SelectDefaultProfile = configurationDto.SelectDefaultProfile;
            configuration.ShowDisabledProfiles = configurationDto.ShowDisabledProfiles;

            configuration.BlacklistedProfileTemplates = MapProfileTemplates(configurationDto.BlacklistedProfileTemplates, profileTemplateLookup);
            configuration.ExcludedProfileTemplates = MapProfileTemplates(configurationDto.ExcludedProfileTemplates, profileTemplateLookup);

            return configuration;
        }

        private List<DCI> MapReinstateDCIs(ProfileTemplateItemDto dto)
        {
            return dto.ReinstateDCIs.Select(d => new DCI() { Name = d.Name, Value = d.Value }).ToList();
        }

        private IEnumerable<string> MapProfileTemplates(IEnumerable<ProfileTemplateDto> profileTemplateListDto, IEnumerable<ProfileTemplateEntry> profileTemplateLookup)
        {
            var profileTemplatesList = profileTemplateListDto != null
                ? profileTemplateListDto.Select(p => p.Name).ToList()
                : new List<string>();
            if (profileTemplatesList.Any())
            {
                var toRemove = new List<string>();
                profileTemplatesList.ForEach(p =>
                {
                    if (!profileTemplateLookup.Any(l => l.Name == p))
                    {
                        toRemove.Add(p);
                    }
                });
                profileTemplatesList.RemoveAll(p => toRemove.Exists(s => s == p));
            }
            return profileTemplatesList;
        }

        public ProfileTemplatesConfigurationDto MapConfigurationDto(Common.Configuration.Models.Configuration configuration, IEnumerable<ProfileTemplateEntry> profileTemplateLookup)
        {
            var configDto = new ProfileTemplatesConfigurationDto(configuration)
            {
                ProfileTemplateLookup = mapper.Map<IEnumerable<ProfileTemplateDto>>(profileTemplateLookup),

                BlacklistedProfileTemplates =
                MapProfileTemplateDto(configuration.BlacklistedProfileTemplates, profileTemplateLookup),

                ExcludedProfileTemplates =
                MapProfileTemplateDto(configuration.ExcludedProfileTemplates, profileTemplateLookup)
            };

            return configDto;
        }

        private IEnumerable<ProfileTemplateDto> MapProfileTemplateDto(IEnumerable<string> profileTemplates, IEnumerable<ProfileTemplateEntry> profileTemplateLookup)
        {
            var profileTemplatesDto = profileTemplates != null
                ? profileTemplates.Select(p => new ProfileTemplateDto()
                {
                    Name = p,
                    ProfileTemplateId = profileTemplateLookup.Where(x => x.Name == p).Select(q => q.Id).FirstOrDefault()
                })
                : new List<ProfileTemplateDto>();
            return profileTemplatesDto;
        }

        #endregion methods
    }
}

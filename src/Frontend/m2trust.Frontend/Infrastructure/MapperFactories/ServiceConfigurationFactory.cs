﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using m2trust.Frontend.Web.RestModels.Configuration;
using m2trust.Messaging.Configuration.Models;

namespace m2trust.Frontend.Web.Infrastructure.MapperFactories
{
    public class ServiceConfigurationFactory : IServiceConfigurationFactory
    {
        public ServiceConfiguration MapConfiguration(ServiceConfigurationDto configurationDto)
        {
            var result = new ServiceConfiguration()
            {
                AdImpersonationConfig = new AdImpersonationConfig()
                {
                    ActiveDirectoryUserDisableLoadProfile = configurationDto.AdImpersonationConfiguration.ActiveDirectoryUserDisableLoadProfile,
                    ActiveDirectoryUserDomain = configurationDto.AdImpersonationConfiguration.ActiveDirectoryUserDomain,
                    ActiveDirectoryUserPassword = configurationDto.AdImpersonationConfiguration.ActiveDirectoryUserPassword,
                    ActiveDirectoryUserUserName = configurationDto.AdImpersonationConfiguration.ActiveDirectoryUserUserName
                },
                MimConfiguration = new FimConfiguration()
                {
                    FimProviderExecuteOperationsEndpoint = configurationDto.MimConfiguration.MimProviderExecuteOperationsEndpoint,
                    FimProviderFimFrontendBaseUrl = configurationDto.MimConfiguration.MimProviderMimFrontendBaseUrl,
                    FimProviderFindOperationsEndpoint = configurationDto.MimConfiguration.MimProviderFindOperationsEndpoint,
                    FimProviderPermissionOperationsEndpoint = configurationDto.MimConfiguration.MimProviderPermissionOperationsEndpoint,
                    FimProviderRequestOperationsEndpoint = configurationDto.MimConfiguration.MimProviderRequestOperationsEndpoint,
                    SignatureDciName = configurationDto.MimConfiguration.SignatureDciName,
                    SignatureEkuOid = configurationDto.MimConfiguration.SignatureEkuOid,
                    SignatureTemplateOid = configurationDto.MimConfiguration.SignatureTemplateOid,
                    RejectInvalidSigningCertificate = configurationDto.MimConfiguration.RejectInvalidSigningCertificate
                },
                LoggingConfiguration = new LoggingConfiguration()
                {
                    LoggingSqlAddress = configurationDto.LoggingConfiguration.LoggingSqlAddress
                }
            };
            return result;
        }

        public ServiceConfigurationDto MapConfigurationDto(ServiceConfiguration configuration)
        {
            var result = new ServiceConfigurationDto()
            {
                AdImpersonationConfiguration = new AdImpersonationConfigurationDto()
                {
                    ActiveDirectoryUserDisableLoadProfile = configuration.AdImpersonationConfig.ActiveDirectoryUserDisableLoadProfile,
                    ActiveDirectoryUserDomain = configuration.AdImpersonationConfig.ActiveDirectoryUserDomain,
                    ActiveDirectoryUserPassword = configuration.AdImpersonationConfig.ActiveDirectoryUserPassword,
                    ActiveDirectoryUserUserName = configuration.AdImpersonationConfig.ActiveDirectoryUserUserName
                },
                MimConfiguration = new MimConfigurationDto()
                {
                    MimProviderExecuteOperationsEndpoint = configuration.MimConfiguration.FimProviderExecuteOperationsEndpoint,
                    MimProviderMimFrontendBaseUrl = configuration.MimConfiguration.FimProviderFimFrontendBaseUrl,
                    MimProviderFindOperationsEndpoint = configuration.MimConfiguration.FimProviderFindOperationsEndpoint,
                    MimProviderPermissionOperationsEndpoint = configuration.MimConfiguration.FimProviderPermissionOperationsEndpoint,
                    MimProviderRequestOperationsEndpoint = configuration.MimConfiguration.FimProviderRequestOperationsEndpoint,
                    SignatureDciName = configuration.MimConfiguration.SignatureDciName,
                    SignatureEkuOid = configuration.MimConfiguration.SignatureEkuOid,
                    SignatureTemplateOid = configuration.MimConfiguration.SignatureTemplateOid,
                    RejectInvalidSigningCertificate = configuration.MimConfiguration.RejectInvalidSigningCertificate
                },
                LoggingConfiguration = new LoggingConfigurationDto()
                {
                    LoggingSqlAddress = configuration.LoggingConfiguration.LoggingSqlAddress
                }
            };
            return result;
        }
    }
}

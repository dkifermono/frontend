﻿using m2trust.Frontend.Model;
using m2trust.Frontend.Web.RestModels;
using System.Collections.Generic;

namespace m2trust.Frontend.Web.Infrastructure.MapperFactories
{
    public interface IProfileTemplateConfigurationFactory
    {
        Common.Configuration.Models.Configuration MapConfiguration(ProfileTemplatesConfigurationDto configurationDto, IEnumerable<ProfileTemplateEntry> profileTemplateLookup, Common.Configuration.Models.Configuration configuration);

        ProfileTemplatesConfigurationDto MapConfigurationDto(Common.Configuration.Models.Configuration configuration, IEnumerable<ProfileTemplateEntry> profileTemplateLookup);
    }
}

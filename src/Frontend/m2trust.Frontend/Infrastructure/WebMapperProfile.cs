﻿using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Model;
using m2trust.Frontend.Web.RestModels;
using m2trust.Messaging.OpenFim.Entities;
using System.Linq;
using static m2trust.Frontend.Controllers.DataCollectionItemController;
using static m2trust.Frontend.Controllers.FinishRequestController;
using static m2trust.Frontend.Controllers.LoggingController;
using static m2trust.Frontend.Controllers.ProfileController;
using static m2trust.Frontend.Controllers.RequestController;
using static m2trust.Frontend.Controllers.UserController;
using static m2trust.Frontend.Web.Controllers.ActivityController;
using Profile = AutoMapper.Profile;
using UserSearchRequest = m2trust.Frontend.Model.UserSearchRequest;

namespace m2trust.Frontend.Web.Infrastructure
{
    public class WebMapperProfile : Profile
    {
        #region Constructors

        public WebMapperProfile()
        {
            CreateMap<Log, LogEntryDto>();
            CreateMap<SignedRequest, SignedRequestDto>()
                .AfterMap((src, dest) => { dest.TargetType = src.TargetForm; });
            CreateMap<UserEntry, UserEntryDto>();
            CreateMap<RequestEventHistory, RequestEventHistoryDto>();
            CreateMap<RequestDetails, RequestDetailsDto>();
            CreateMap<RequestDetails, RequestDetailsEntryDto>();
            CreateMap<ProfileStubEntry, ProfileItemStubDto>()
                .AfterMap((src, dest) =>
                {
                    dest.Status = src.ProfileStatus;
                });
            CreateMap<CertificateStubEntry, CertificateDto>();
            CreateMap<DataCollectionItemEntry, DataCollectionItemDto>();
            CreateMap<Validation, ValidationDto>();

            CreateMap<RequestActionResponse, RequestActionResponseDto>();
            CreateMap<PolicyEntry, ApprovePolicyDto>();
            CreateMap<Secret, SecretDto>();

            CreateMap<ProfileTemplateEntry, ProfileTemplateDto>()
                .AfterMap((src, dest) =>
                {
                    dest.ProfileTemplateId = src.Id;
                    dest.Name = src.Name;
                });

            CreateMap<ProfileTemplateDto, ProfileTemplateEntry>()
                .AfterMap((src, dest) =>
                {
                    dest.Name = src.Name;
                    dest.Id = src.ProfileTemplateId;
                });

            CreateMap<UserSearchRequestDto, UserSearchRequest>();

            CreateMap<ProfileEntry, ProfileItemDto>();
            

            CreateMap<DciDataDto, AuditRequestEntry>()
                .AfterMap((src, dest) =>
                {
                    dest.ProfileTemplateId = src.ProfileTemplateId;
                    dest.ProfileId = src.ProfileId;
                    dest.UserId = src.UserId;
                    dest.DataCollectionItems = src.Items;
                });

            CreateMap<ChallengeResponseRequestDto, EnterChallengeRequestEntry>();
            CreateMap<ActivityInfo, ActivityDto>();

            CreateMap<ExecuteEnrollRequests, ExecuteEnrollRequestsDto>();

            CreateMap<AuditRequestDto, AuditRequestEntry>()
                .AfterMap((src, dest) =>
                {
                    dest.ProfileTemplateId = src.ProfileTemplateId;
                    dest.ProfileId = src.ProfileId.Value;
                    dest.UserId = src.UserId;
                    dest.DataCollectionItems = src.DataCollections?.ToDictionary(a => a.Name, b => b.Value);
                });
            CreateMap<AuditRequestEntry, AuditRequestDto>()
                 .AfterMap((src, dest) =>
                 {
                     dest.ProfileTemplateId = src.ProfileTemplateId;
                     dest.ProfileId = src.ProfileId;
                     dest.UserId = src.UserId;
                     dest.Comment = src.Comment;
                     dest.DataCollections = src.DataCollectionItems?.Select(dict => new DataItemDto
                     {
                         Name = dict.Key,
                         Value = dict.Value
                     }).ToList();
                 });
            
            CreateMap<LogFilterParams, LoggingFilterParameters>()
                .ForMember(dest => dest.From, m => m.MapFrom(src => src.From))
                .ForMember(dest => dest.To, m => m.MapFrom(src => src.To))
                .ForMember(dest => dest.SearchTerm, m => m.MapFrom(src => src.SearchTerm))
                .ForMember(dest => dest.LogLevel, m => m.MapFrom(src => src.LogLevel))
                .ForMember(dest => dest.ShowAuditLogs, m => m.MapFrom(src => src.ShowAuditLogs));
            CreateMap<LogFilterParams, PagingParameters>()
                .ForMember(dest => dest.PageNumber, m => m.MapFrom(src => src.PageSize))
                .ForMember(dest => dest.PageSize, m => m.MapFrom(src => src.PageSize));
            CreateMap<Colors, ColorsDto>().ReverseMap();

            CreateMap<ApproveResult, ApproveResultDto>();

            CreateMap<OtpResult, OtpResultDto>();

            CreateMap<ProfileTemplate, ProfileTemplateDto>().ReverseMap();
        }

        #endregion Constructors
    }
}
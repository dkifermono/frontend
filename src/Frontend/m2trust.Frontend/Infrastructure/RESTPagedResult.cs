﻿using AutoMapper;
using PagedList.Core;
using System.Collections.Generic;
using System.Linq;

namespace m2trust.Frontend.Web.Infrastructure
{
    public class RESTPagedResult<T> where T : class
    {
        public RESTPagedResult()
        {
        }

        public RESTPagedResult(IPagedList<T> pagedResultSet)
        {
            this.Items = pagedResultSet.AsEnumerable();
            this.PageNumber = pagedResultSet.PageNumber;
            this.PageSize = pagedResultSet.PageSize;
            this.TotalCount = pagedResultSet.TotalItemCount;
        }

        public void Initialize<P>(IPagedList<P> pagedResultSet, IMapper mapper)
        {
            Initialize<P>(pagedResultSet, mapper, null);
        }

        public void Initialize<P>(IPagedList<P> pagedResultSet, IMapper mapper, IEnumerable<string> tableFields)
        {
            mapper.Map(pagedResultSet, this.Items);
            this.PageNumber = pagedResultSet.PageNumber;
            this.PageSize = pagedResultSet.PageSize;
            this.TotalCount = pagedResultSet.TotalItemCount;
            this.TableFields = tableFields;
        }

        public IEnumerable<T> Items { get; private set; } = Enumerable.Empty<T>();

        public int PageNumber { get; private set; } = 1;

        public int PageSize { get; private set; }

        public int TotalCount { get; private set; }

        public IEnumerable<string> TableFields { get; private set; }
    }
}
﻿using Autofac;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using m2trust.Frontend.Web.Configuration.AppConfiguration.Keys;
using m2trust.Frontend.Web.Infrastructure.MapperFactories;

namespace m2trust.Frontend.Web.Infrastructure
{
    public class WebDIModule : Module
    {
        #region Methods

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserKeyProvider>().As<IUserKeyProvider>();
            builder.RegisterType<SettingsProviderFactory>().As<ISettingsProviderFactory>();

            builder.RegisterType<SignatureConfiguration>().As<ISignatureConfiguration>();
            builder.RegisterType<ProfileConfiguration>().As<IProfileConfiguration>();
            builder.RegisterType<ProfileStateFilterConfiguration>().As<IProfileStateFilterConfiguration>();
            builder.RegisterType<ProfileStubEntryFilterConfiguration>().As<IProfileStubEntryFilterConfiguration>();
            builder.RegisterType<SearchFormConfiguration>().As<ISearchFormConfiguration>();
            builder.RegisterType<ReinstateDCIConfiguration>().As<IReinstateDCIConfiguration>();
            builder.RegisterType<PageTextConfiguration>().As<IPageTextConfiguration>();
            builder.RegisterType<FrontendConfigurationFactory>().As<IFrontendConfigurationFactory>();
            builder.RegisterType<ProfileTemplateConfigurationFactory>().As<IProfileTemplateConfigurationFactory>();
            builder.RegisterType<ServiceConfigurationFactory>().As<IServiceConfigurationFactory>();

            builder.RegisterType<FrontendConfigurationService>().As<IFrontendConfigurationService>();

        }

        #endregion Methods
    }
}
﻿using m2trust.Frontend.Common.Enums;
using Microsoft.AspNetCore.Mvc;
using System;

namespace m2trust.Frontend.Web.Infrastructure.Security
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class AuthorizeActivities : TypeFilterAttribute
    {
        #region Constructors

        public AuthorizeActivities(params Activity[] activities) : base(typeof(ActivityFilter))
        {
            Arguments = new[] { new ActivityRequirement(activities) };
        }

        #endregion Constructors
    }
}
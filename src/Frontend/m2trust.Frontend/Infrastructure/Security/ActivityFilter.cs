﻿using m2trust.Frontend.Service.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace m2trust.Frontend.Web.Infrastructure.Security
{
    public class ActivityFilter : Attribute, IAsyncAuthorizationFilter
    {
        #region Fields

        private readonly IApplicationUserContext applicationUserContext;
        private readonly ActivityRequirement requirement;
        private readonly IUserActivityChecker userActivityChecker;

        #endregion Fields

        #region Constructors

        public ActivityFilter(IUserActivityChecker userActivityChecker, IApplicationUserContext applicationUserContext, ActivityRequirement requirement)
        {
            this.applicationUserContext = applicationUserContext;
            this.userActivityChecker = userActivityChecker;
            this.requirement = requirement;
        }

        #endregion Constructors

        #region Methods

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var currentUser = await this.applicationUserContext.GetCurrentUser();
            var queryString = context.HttpContext.Request.QueryString.ToString();
            var activityType = HttpUtility.ParseQueryString(queryString).Get("ActivityType");

            if (requirement.Activities.Any(i => i.ToString().Equals(activityType, StringComparison.InvariantCultureIgnoreCase)))
            {
                var hasPermission = await this.userActivityChecker.IsUserAuthorizedForActivityAsync(currentUser, activityType);
                if (hasPermission)
                {
                    return;
                }
            }
            context.Result = new ForbidResult();
        }

        #endregion Methods
    }
}
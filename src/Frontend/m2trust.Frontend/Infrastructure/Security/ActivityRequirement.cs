﻿using m2trust.Frontend.Common.Enums;
using Microsoft.AspNetCore.Authorization;

namespace m2trust.Frontend.Web.Infrastructure.Security
{
    public class ActivityRequirement : IAuthorizationRequirement
    {
        #region Constructors

        public ActivityRequirement(Activity[] activities)
        {
            Activities = activities;
        }

        #endregion Constructors

        #region Properties

        public Activity[] Activities { get; set; }

        #endregion Properties
    }
}
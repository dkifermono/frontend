﻿using m2trust.Frontend.Model;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.Infrastructure
{
    public interface IApplicationUserContext
    {
        Task<HttpContextApplicationUser> GetCurrentUser();
    }
}

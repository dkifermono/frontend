﻿using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using System;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using m2trust.Frontend.Common;
using Microsoft.Extensions.Options;

namespace m2trust.Frontend.Web.Infrastructure
{
    public class ApplicationUserContext: IApplicationUserContext
    {
        private readonly IUserService userService;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IConfiguration configuration;
        public readonly IOptions<AdministrationConfig> administrationConfiguration;
        private readonly string[] adminGroups;

        public ApplicationUserContext(IUserService userService, IHttpContextAccessor httpContextAccessor, IConfiguration configuration, IOptions<AdministrationConfig> administrationConfiguration)
        {
            this.userService = userService;
            this.httpContextAccessor = httpContextAccessor;
            this.configuration = configuration;
            this.administrationConfiguration = administrationConfiguration;
            this.adminGroups = administrationConfiguration.Value.Groups;
        }

        public async Task<HttpContextApplicationUser> GetCurrentUser()
        {
            var windowsIdentity = this.httpContextAccessor.HttpContext.User.Identity as WindowsIdentity;


            var user = await userService.GetUserAsync(ResolveCurrentUserSid(windowsIdentity)).ConfigureAwait(false);
            if (windowsIdentity != null && user != null)
            {
                return new HttpContextApplicationUser
                {
                    UniqueIdentifier = user.Id,
                    UserName = windowsIdentity?.Name,
                    Name = user.Name,
                    IsAdmin = adminGroups.Any(g => this.httpContextAccessor.HttpContext.User.IsInRole(g))
                };
            }

            throw new Exception(
                $"User {windowsIdentity?.Name} is not valid because an underlying user could not be found in the user repository.");
        }

        private string ResolveCurrentUserSid(System.Security.Principal.WindowsIdentity identity)
        {
#if DEBUG
            var debugSid = configuration.GetSection("Orleans:DebugSid").Get<string>();
            return debugSid;
#else
            return identity.User.Value;
#endif
        }
    }
}

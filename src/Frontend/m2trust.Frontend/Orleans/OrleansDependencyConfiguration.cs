﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Autofac;
using m2trust.Frontend.Service.Stubs;
using m2trust.Messaging.ActiveDirectory.API;
using m2trust.Messaging.Configuration.API;
using m2trust.Messaging.Logging.API;
using m2trust.Messaging.OpenFim.API;
using m2trust.Messaging.Settings;
using m2trust.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Orleans;

namespace m2trust.Frontend.Orleans
{
    public class OrleansDependencyConfiguration
    {
        public static void Configure(IHostingEnvironment hostingEnvironment, IConfiguration configuration,
            ContainerBuilder builder)
        {
            

            if (true || hostingEnvironment.IsProduction())
            {
                var client = SiloConnector.ConnectSilo(configuration);
                builder.RegisterInstance<ILogProvider>(client.GetGrain<ILogProvider>(Guid.NewGuid()));
                builder.RegisterInstance<ILogger>(client.GetGrain<ILogger>(Guid.NewGuid()));
                builder.RegisterInstance<IGrainFactory>(client);
                builder.RegisterInstance<IAuditLogger>(client.GetGrain<IAuditLogger>(Guid.NewGuid()));

                var getImpersonableGrainId = new Func<HttpContextAccessor, string>((HttpContextAccessor ctxAccessor) =>
                {
                    var userGrainId = (ctxAccessor.HttpContext?.User?.Identity as WindowsIdentity)?.User?.Value ?? Guid.NewGuid().ToString();
                    return $"{userGrainId}.{ctxAccessor.HttpContext?.Connection?.Id}";
                });

                builder.Register(ctx =>
                {
                    var ctxAccesor = new HttpContextAccessor();
                    var userGrainId = getImpersonableGrainId(ctxAccesor);
                    return client.GetGrain<IMimAccessApi>(userGrainId);
                }).As<IMimAccessApi>();

                builder.Register(ctx =>
                {
                    var ctxAccesor = new HttpContextAccessor();
                    var userGrainId = getImpersonableGrainId(ctxAccesor);
                    return client.GetGrain<IServiceConfigurationStore>(userGrainId);
                }).As<IServiceConfigurationStore>();

                builder.Register(ctx =>
                {
                    var ctxAccesor = new HttpContextAccessor();
                    var userGrainId = getImpersonableGrainId(ctxAccesor);
                    return client.GetGrain<IConfigurationStore>(userGrainId);
                }).As<IConfigurationStore>();

                builder.Register(ctx => client.GetGrain<IActiveDirectoryApi>(Guid.NewGuid())).As<IActiveDirectoryApi>();


                builder.Register(ctx =>
                {
                    var ctxAccesor = new HttpContextAccessor();
                    var userGrainId = getImpersonableGrainId(ctxAccesor);
                    return client.GetGrain<ICertificateManagerApi>(userGrainId);
                }).As<ICertificateManagerApi>();
            }
            else
            {
                builder.RegisterType<DemoActiveDirectoryApi>().As<IActiveDirectoryApi>();
            }
        }
    }
}
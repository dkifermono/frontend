﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using m2trust.Messaging.Common.API;
using m2trust.Messaging.OpenFim.API;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NSspi;
using NSspi.Contexts;
using NSspi.Credentials;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Runtime;

namespace m2trust.Frontend.Orleans
{
    public class SiloConnector
    {
        public static IClusterClient ConnectSilo(IConfiguration configuration)
        {
            var targetIp = configuration.GetSection("Orleans:Target").Get<string>();
            var targetClusteringSql = configuration.GetSection("Orleans:SqlClustering").Get<string>();

            var gatewayPort = 40000;
            var clientBuilder = new ClientBuilder()
                //Configure ClusterOptions
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "m2trust";
                    options.ServiceId = "m2trust";
                })
#if !DEBUG
                .AddOutgoingGrainCallFilter(new ImpersonateGrainfilter(new HttpContextAccessor()))
#endif
                .ConfigureLogging(logging => logging.AddConsole());

            if (!String.IsNullOrEmpty(targetClusteringSql))
            {
                var connectionString = new SqlConnectionStringBuilder
                {
                    DataSource = targetClusteringSql,
                    IntegratedSecurity = true,
                    InitialCatalog = "SiloRunnerDb"
                }.ToString();
                clientBuilder
                    .UseAdoNetClustering(x =>
                    {
                        x.Invariant = "System.Data.SqlClient";
                        x.ConnectionString = connectionString;
                    });
            }
            else if (!String.IsNullOrEmpty(targetIp))
            {
                var ipAddress = IPAddress.Parse(targetIp);
                //Use StaticClustering in client side
                clientBuilder
                    .UseStaticClustering(options =>
                        options.Gateways =
                            new List<Uri>() { (new IPEndPoint(ipAddress, gatewayPort)).ToGatewayUri() });
            }
            else
            {
                throw new Exception(
                    "You must either configure Orleans.Target (IP) or Orleans.SqlClustering (Sql-Addresse)");
            }

            var client = clientBuilder.Build();
            client.Connect().GetAwaiter().GetResult();
            return client;
        }
    }

    public class ImpersonateGrainfilter : IOutgoingGrainCallFilter
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        public ImpersonateGrainfilter(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }
        public async Task Invoke(IOutgoingGrainCallContext context)
        {
            var grain = context.Grain;
            if (grain is IImpersonableGrain)
            {
                const string impersonating = "IMPERSONATING";
                if (RequestContext.Get(impersonating) == null)
                {
                    RequestContext.Set(impersonating, true);
                    var ig = grain as IImpersonableGrain;
                    await ImpersonationHelper.InitImpersonalbleGrain(ig, this.httpContextAccessor).ConfigureAwait(false);
                    RequestContext.Set(impersonating, null);
                }
            }

            await context.Invoke().ConfigureAwait(false);
        }
    }

    public static class ImpersonationHelper
    {
        static SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);

        public static async Task InitImpersonalbleGrain(IImpersonableGrain grain,
            IHttpContextAccessor httpContextAccessor)
        {
            await semaphoreSlim.WaitAsync();
            try
            {
                if (!await grain.IsImpersonationInitialized())
                {
                    Credential clientCred = null;
                    var wi = httpContextAccessor.HttpContext.User.Identity as WindowsIdentity;
                    WindowsIdentity.RunImpersonated(wi.AccessToken, () => clientCred = new ClientCurrentCredential(PackageNames.Negotiate));

                    var principalName = await grain.GetServerPrincipal().ConfigureAwait(false);
                    var client = new ClientContext(
                        clientCred,
                        principalName,
                        ContextAttrib.MutualAuth |
                        ContextAttrib.Delegate);
                    var clientStatus = client.Init(null, out var clientToken);

                    while (true)
                    {
                        var res = await grain.AcceptToken(clientToken).ConfigureAwait(false);
                        if (!res.ContinuationNeeded && clientStatus != SecurityStatus.ContinueNeeded)
                        {
                            break;
                        }

                        byte[] serverToken = res.ServerToken;
                        clientStatus = client.Init(serverToken, out clientToken);
                        if (!res.ContinuationNeeded && clientStatus != SecurityStatus.ContinueNeeded)
                        {
                            break;
                        }
                    }
                }
            }
            finally
            {
                semaphoreSlim.Release();
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using m2trust.Messaging.ActiveDirectory.API;
using m2trust.Messaging.ActiveDirectory.Entities;
using m2trust.Messaging.ActiveDirectory.Enumerations;

namespace m2trust.Frontend.Orleans
{
    internal class DemoActiveDirectoryApi : IActiveDirectoryApi
    {
        public Task<IEnumerable<Domain>> AllDomainsAsync()
        {
            throw new NotImplementedException();
        }

        public Task<Domain> CurrentLocalDomainAsync()
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsObjectInGroupsAsync(string uniqueIdentifier, IEnumerable<string> groups, IdentityType identityType)
        {
            throw new NotImplementedException();
        }

        public Task<User> LookupUserAsync(string ldapQuery, string[] propertiesToFetch)
        {
            throw new NotImplementedException();
        }

        public Task<ActiveDirectorySearchUsersResult> SearchUsersAsync(int offset, int count, string domain, string searchTerm, ICollection<string> fields,
            IEnumerable<string> propertiesToLoad, bool extendedSearch)
        {
            return Task.FromResult(new ActiveDirectorySearchUsersResult()
            {
                Users = new PagedList<User>(Enumerable.Range(0, 3).Select(x => new User
                {
                    Name = x.ToString(),
                    DirectoryPath = "/path1",
                    Id = Guid.NewGuid(),
                })),
                TotalCount = 1000,
            });
        }
    }
}
using Autofac;
using AutoMapper;
using GlobalExceptionHandler.WebApi;
using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Controllers;
using m2trust.Frontend.Orleans;
using m2trust.Frontend.Service;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Service.Infrastructure;
using m2trust.Frontend.Web.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration.Keys;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Web.Infrastructure.Security;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;
using m2trust.Messaging.Common;


namespace m2trust.Frontend
{
    public class Startup
    {
        public Startup(IHostingEnvironment env, IConfiguration config)
        {
            this.HostingEnvironment = env;
            this.Configuration = config;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseExceptionHandler("/error").WithConventions(x =>
            {
                x.ContentType = "application/json";
                x.ForException<Exception>().ReturnStatusCode(StatusCodes.Status500InternalServerError)
                 .UsingMessageFormatter((ex, context) => JsonConvert.SerializeObject(new ExceptionDto
                 {
                     Message = ex.Message,
                     ErrorId = ex.GetErrorId()
                 },
                 new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver() }));
            });

            app.Map("/error", x => x.Run(y => throw new Exception()));

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod());

            app.UseIdentityServer();

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "m2trust"); });

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.MapWhen(x => x.Request.Path.Value.Contains("/api/"), builder =>
            {
                builder.UseMvc(routes =>
                {
                    routes.MapSpaFallbackRoute(
                        name: "spa-fallback",
                        defaults: new { controller = "Default", action = "Error" });
                });
            });
            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    //spa.UseAngularCliServer(npmScript: "start");
                    spa.UseProxyToSpaDevelopmentServer("http://localhost:4200");
                }
            });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            try
            {
                OrleansDependencyConfiguration.Configure(HostingEnvironment, Configuration, builder);
                builder.RegisterModule(new ServiceDIModule());
                builder.RegisterModule(new WebDIModule());
                builder.RegisterType<ApplicationUserContext>().As<IApplicationUserContext>();
                builder.RegisterType<ActivityFilter>().As<IAsyncAuthorizationFilter>().SingleInstance();
                builder.RegisterType<ActivityConfiguration>().As<IActivityConfiguration>();
                builder.RegisterInstance<IAppKeyProvider>(new AppKeyProvider(Configuration.GetSection("appKey").Get<string>()));
                builder.RegisterInstance<IFileProvider>(new FileProvider(Configuration.GetSection("logoImagePath").Get<string>(), Configuration.GetSection("cssVariablesPath").Get<string>()));
            }
            catch (Exception ex)
            {
                Environment.FailFast(ex.Message);
                throw;
            }
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<IISOptions>(options => { options.AutomaticAuthentication = true; });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddCors();
            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddInMemoryPersistedGrants()
                .AddInMemoryIdentityResources(IdentityConfig.GetIdentityResources())
                .AddInMemoryApiResources(IdentityConfig.GetApiResources())
                .AddInMemoryClients(IdentityConfig.GetClients());

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o =>
            {
                o.Audience = "api";
                o.RequireHttpsMetadata = false;
            });

            Action<MvcOptions> tes = GetTes;

            services.AddMvc()
                .AddMvcOptions(tes)
                .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                });

            services.AddAutoMapper(
                typeof(ServiceMapperProfile),
                typeof(WebMapperProfile));
            services.AddMemoryCache();
            var mvcOption = new MvcOptions();
            services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1", new Info
                {
                    Title = "m2trust",
                    Version = "v1"
                });
                x.DescribeAllEnumsAsStrings();

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                x.IncludeXmlComments(xmlPath);


            });


            services.AddSingleton<IConfiguration>(Configuration);

            services.AddSpaStaticFiles(configuration => { configuration.RootPath = "ClientApp/dist"; });

            services.Configure<AdministrationConfig>(Configuration.GetSection("AdministrationConfig"));
        }

        private static void GetTes(MvcOptions MvcOptions)
        {
            MvcOptions.AllowCombiningAuthorizeFilters = false;
        }
    }
}
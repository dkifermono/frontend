using AutoMapper;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Web.Infrastructure.Security;
using Microsoft.AspNetCore.Mvc;
using PagedList.Core;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Controllers
{
    /// <summary>
    /// Logging controller.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Route("api/[controller]")]
    public class LoggingController : BaseController
    {
        #region Fields

        private readonly ILoggingService loggingService;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingController" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="loggingService">The logging service.</param>
        /// <param name="mapper">The mapper.</param>
        /// <exception cref="ArgumentNullException">logger or loggingService or mapper</exception>
        public LoggingController(ILogWriter logger, ILoggingService loggingService, IMapper mapper) : base(mapper, logger)
        {
            this.loggingService = loggingService ?? throw new ArgumentNullException(nameof(loggingService));
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Gets the log entries.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /Logging/GetLogEntries?From=2018-09-20T07%3A50%3A55.416Z&#38;LogLevel=&#38;PageNumber=1&#38;PageSize=100&#38;SearchTerm=&#38;ShowAuditLogs=&#38;To=2018-09-28T07%3A50%3A55.416Z&#38;activityType=Logging
        /// </remarks>
        /// <param name="logParams">The log parameters.</param>
        /// <param name="activityType">Possible values: Logging</param>
        /// <returns></returns>
        /// <response code="200">Returns the list of log entries.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In a case of an exception.</response>
        [HttpGet("[action]")]
        [SwaggerResponse(200, Type = typeof(RESTPagedResult<LogEntryDto>))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.Administration)]
        public async Task<IActionResult> GetLogEntries([FromQuery] LogFilterParams logParams, string activityType)
        {
            var filterParameters = mapper.Map<LoggingFilterParameters>(logParams);
            var pagingParameters = mapper.Map<PagingParameters>(logParams);

            var logs = await loggingService.GetEntriesAsync(filterParameters, pagingParameters);
            var logEnries = this.mapper.Map<IEnumerable<LogEntryDto>>(logs);
            IPagedList<LogEntryDto> result = new StaticPagedList<LogEntryDto>(logEnries, pagingParameters.PageNumber, pagingParameters.PageSize, logs.TotalItemCount);
            var collection = new RESTPagedResult<LogEntryDto>(result);
            collection.Initialize(result, mapper);

            return Ok(collection);
        }

        #endregion Methods

        #region Classes

        public class LogEntryDto
        {
            #region Properties

            /// <summary>
            /// Gets or sets the log date.
            /// </summary>
            /// <value>The log date.</value>
            public DateTime LogDate { get; set; }

            /// <summary>
            /// Gets or sets the log level.
            /// </summary>
            /// <value>The log level.</value>
            public string LogLevel { get; set; }

            /// <summary>
            /// Gets or sets the message.
            /// </summary>
            /// <value>The message.</value>
            public string Message { get; set; }

            /// <summary>
            /// Gets or sets the system.
            /// </summary>
            /// <value>The system.</value>
            public string System { get; set; }

            /// <summary>
            /// Gets or sets the user.
            /// </summary>
            /// <value>The user.</value>
            public string User { get; set; }

            #endregion Properties
        }

        public class LogFilterParams
        {
            #region Constructors

            public LogFilterParams()
            {
                PageNumber = 1;
                PageSize = 10;
            }

            #endregion Constructors

            #region Properties

            public DateTime? From { get; set; }
            public LogLevelType? LogLevel { get; set; }
            public int PageNumber { get; set; }
            public int PageSize { get; set; }
            public string SearchTerm { get; set; }
            public bool? ShowAuditLogs { get; set; }
            public DateTime? To { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}
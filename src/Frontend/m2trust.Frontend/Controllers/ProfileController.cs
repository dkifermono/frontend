﻿using AutoMapper;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Web.Infrastructure.Security;
using m2trust.Frontend.Web.RestModels;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace m2trust.Frontend.Controllers
{
    [Route("api/[controller]")]
    public class ProfileController : BaseController
    {
        #region Fields

        private readonly IApplicationUserContext applicationUserContext;
        private readonly IProfileConfiguration profileConfiguration;
        private readonly IProfileService profileService;

        #endregion Fields

        #region Constructors

        public ProfileController(
            IProfileService profileService,
            IMapper mapper,
            IApplicationUserContext applicationUserContext,
            ILogWriter logger,
            IProfileConfiguration profileConfiguration) : base(mapper, logger)
        {
            this.profileService = profileService ?? throw new ArgumentNullException(nameof(profileService));
            this.applicationUserContext = applicationUserContext;
            this.profileConfiguration = profileConfiguration;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Gets the profiles.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /Profile/GetProfiles?activityType=RetireTemporarySmartcard?ActivityType=RetireTemporarySmartcard&#38;UserId=01203f03-d612-47fe-bf28-aa354df6a812
        ///     
        /// </remarks>
        /// <param name="request">The profile request.</param>
        /// <param name="activityType">Possible values: RetireTemporarySmartcard</param>
        /// <returns></returns>
        /// <response code="200">Returns the profile dto with list of the profile items.</response>
        /// <response code="400">If UserId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("[action]")]
        [SwaggerResponse(200, Type = typeof(ProfileDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.RetireTemporarySmartcard)]
        public async Task<IActionResult> GetProfiles(ProfileRequestDto request, string activityType)
        {
            if (request.UserId == Guid.Empty)
                return BadRequest();

            var profiles = await profileService.GetProfilesAsync(request.UserId);
            var result = new ProfileDto
            {
                Profiles = mapper.Map<List<ProfileItemDto>>(profiles),
                EligibleProfilesStates = new[] { ProfileState.Active }
            };
            return Ok(result);
        }

        /// <summary>
        /// Gets the profile stub items.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /Profile/GetProfileStubs?SectionType=Revoke&#38;UserId=42740d23-5a17-4c31-9360-b09291456829&#38;activityType=RecoverCertificate
        /// 
        /// </remarks>
        /// <param name="request">The profile stub request.</param>
        /// <param name="activityType">Possible values: DisableSmartcard, RecoverCertificate, ReinstateSmartcard, RevokeCertificate, RewriteSmartcard, SuspendSmartcard, UnblockSmartcard, RenewCertificate</param>
        /// <returns></returns>
        /// <response code="200">Returns the profile stub dto with list of the profile stub items.</response>
        /// <response code="400">If UserId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("[action]")]
        [SwaggerResponse(200, Type = typeof(ProfileStubDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.DisableSmartcard, Activity.RecoverCertificate, Activity.ReinstateSmartcard, Activity.RevokeCertificate,
         Activity.RewriteSmartcard, Activity.SuspendSmartcard, Activity.UnblockSmartcard, Activity.RenewCertificate)]
        public async Task<IActionResult> GetProfileStubs(ProfileStubRequestDto request, string activityType)
        {
            if (request.UserId == Guid.Empty)
                return BadRequest();
            
            var profileStubs = await profileService.GetProfileStubsAsync(request.UserId, activityType);
            var result = new ProfileStubDto
            {
                SelectDefaultProfile = await profileConfiguration.SelectDefaultProfile,
                Profiles = mapper.Map<List<ProfileItemStubDto>>(profileStubs)
            };

            if (request.SectionType == SectionType.Reinstate)
            {
                result.EligibleProfilesStates = new[] { ProfileState.Suspended };
            }
            else
            {
                result.EligibleProfilesStates = new[] { ProfileState.Active };
            }
            return Ok(result);
        }

        /// <summary>
        /// Gets the profile templates.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /Profile/GetProfileTemplates?UserId=20feb097-1388-445f-a191-d054da5e2870&#38;activityType=Enroll
        /// 
        /// </remarks>
        /// <param name="request">The profile request.</param>
        /// <param name="activityType">Possible values: Enroll</param>
        /// <returns></returns>
        /// <response code="200">Returns the the list of profile templates.</response>
        /// <response code="400">If request UserId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("[action]")]
        [SwaggerResponse(200, Type = typeof(List<ProfileTemplateDto>))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.Enroll)]
        public async Task<IActionResult> GetProfileTemplates(ProfileRequestDto request, string activityType)
        {
            if (request.UserId == Guid.Empty)
            {
                return BadRequest();
            }

            var avalilableProfileTemplates = await profileService.GetProfileTemplatesAsync(request.UserId);

            if (avalilableProfileTemplates != null)
            {
                var result = mapper.Map<List<ProfileTemplateDto>>(avalilableProfileTemplates);
                return Ok(result);
            }

            return Ok(new List<ProfileTemplateDto>());
        }

        #endregion Methods

        #region Classes

        public class CertificateDto 
        {
            #region Properties

            public string CertificateTemplate { get; set; }
            public string CommonName { get; set; }
            public DateTime? ExpirationDate { get; set; }
            public CertificateState Status { get; set; }

            #endregion Properties
        }

        public class ExecuteEnrollRequestsDto 
        {
            #region Properties
            
            public bool HasOtpRequests { get; set; }
            public IEnumerable<RequestDetailsEntryDto> NonOtpProfiles { get; set; }

            #endregion Properties
        }

        public class ProfileDto 
        {
            #region Properties

            public IEnumerable<ProfileState> EligibleProfilesStates { get; set; }
            public List<ProfileItemDto> Profiles { get; set; }
            public Guid UserId { get; set; }

            public String UserName { get; set; }

            #endregion Properties
        }

        public class ProfileItemDto 
        {
            #region Properties

            public DateTime EnrollCompleted { get; set; }
            public DateTime EnrollSubmitted { get; set; }
            public Guid Id { get; set; }

            public bool IsSmartCard { get; set; }

            public DateTime? Issued { get; set; }
            public ProfileTemplateDto ProfileTemplate { get; set; }
            public RequestState RequestStatus { get; set; }
            public ProfileState Status { get; set; }
            public Guid UserId { get; set; }

            #endregion Properties
        }

        public class ProfileItemStubDto 
        {
            #region Properties

            public ProfileState CalculatedProfileStatus { get; set; }
            public IEnumerable<CertificateDto> Certificates { get; set; }
            public Guid Id { get; set; }
            public bool IsSmartCard { get; set; }
            public DateTime? IssuanceDate { get; set; }
            public string Name { get; set; }
            public Guid? SmartcardId { get; set; }
            public Guid? SoftProfileId { get; set; }
            public ProfileState Status { get; set; }

            #endregion Properties
        }

        public class ProfileRequestDto 
        {
            #region Properties            
            
            public Guid UserId { get; set; }

            #endregion Properties
        }

        public class ProfileStubDto 
        {
            #region Properties

            public IEnumerable<ProfileState> EligibleProfilesStates { get; set; }
            public List<ProfileItemStubDto> Profiles { get; set; }
            public bool SelectDefaultProfile { get; set; }

            #endregion Properties
        }

        public class ProfileStubRequestDto 
        {
            #region Properties
            
            public SectionType SectionType { get; set; }
            
            public Guid UserId { get; set; }

            #endregion Properties
        }

        public class RequestDetailsEntryDto 
        {
            #region Properties
            public DateTime DateCompleted { get; set; }
            
            public DateTime DateSubmitted { get; set; }
            
            public Guid ProfileId { get; set; }
            
            public ProfileTemplateDto ProfileTemplate { get; set; }
            
            public Guid UserId { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}
﻿using AutoMapper;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Common.Parameters.Sorting;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Web.Infrastructure.Security;
using Microsoft.AspNetCore.Mvc;
using PagedList.Core;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Web.RestModels;

namespace m2trust.Frontend.Controllers
{
    /// <summary>
    /// Request controller.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Route("api/[controller]")]
    public class RequestController : BaseController
    {
        #region Fields

        private readonly ISearchFormConfiguration configuration;
        private readonly IRequestService Service;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestController" /> class.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="configuration">The configuration.</param>
        public RequestController(IRequestService service, IMapper mapper, ILogWriter logger, ISearchFormConfiguration configuration) : base(mapper, logger)
        {
            Service = service;
            this.configuration = configuration;
        }

        #endregion Constructors

        #region Methods


        /// <summary>
        /// Abandons the requests.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Request/Abandon?activityType=ManageRequests
        ///     [
        ///         "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45"
        ///     ]
        /// </remarks>
        /// <param name="requestIds">The request identifiers.</param>
        /// <param name="activityType">Possible values: ManageRequests</param>
        /// <returns></returns>
        /// <response code="200">Returns the request action response.</response>
        /// <response code="400">If RequestIds is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(RequestActionResponseDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.ManageRequests)]
        public async Task<IActionResult> Abandon([FromBody] Guid[] requestIds, string activityType)
        {
            if (requestIds == null || requestIds.Length == 0 || Array.Exists(requestIds, id => id == Guid.Empty))
            {
                return BadRequest();
            }
            var result = mapper.Map<RequestActionResponseDto>(await Service.AbandonAsync(requestIds));
            return Ok(result);
        }

        /// <summary>
        /// Approves the requests.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Request/Approve?activityType=ManageRequests
        ///     [
        ///         "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45"
        ///     ]
        /// </remarks>
        /// <param name="requestIds">The request identifiers.</param>
        /// <param name="activityType">Possible values: ManageRequests</param>
        /// <returns></returns>
        /// <response code="200">Returns the approve result.</response>
        /// <response code="400">If RequestIds is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(ApproveResultDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.ManageRequests)]
        public async Task<IActionResult> Approve([FromBody] Guid[] requestIds, string activityType)
        {
            if (requestIds == null || requestIds.Length == 0 || Array.Exists(requestIds, id => id == Guid.Empty))
            {
                return BadRequest();
            }
            var result = mapper.Map<ApproveResultDto>(await Service.ApproveAsync(requestIds));

            return Ok(result);
        }

        /// <summary>
        /// Cancels the requests.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Request/Cancel?activityType=ManageRequests
        ///     [
        ///         "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45"
        ///     ]
        /// </remarks>
        /// <param name="requestIds">The request identifiers.</param>
        /// <param name="activityType">Possible values: ManageRequests</param>
        /// <returns></returns>
        /// <response code="200">Returns the request action response.</response>
        /// <response code="400">If RequestIds is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(RequestActionResponseDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.ManageRequests)]
        public async Task<IActionResult> Cancel([FromBody] Guid[] requestIds, string activityType)
        {
            if (requestIds == null || requestIds.Length == 0 || Array.Exists(requestIds, id => id == Guid.Empty))
            {
                return BadRequest();
            }
            var result = mapper.Map<RequestActionResponseDto>(await Service.CancelAsync(requestIds));
            return Ok(result);
        }

        /// <summary>
        /// Denies the requests.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Request/Deny?activityType=ManageRequests
        ///     {
        ///         "comment":"test", 
        ///         "requestIds":["b4cc4ff0-e10f-4828-b27d-deb62b1d6c45"]
        ///     }
        ///     
        /// </remarks>
        /// <param name="denyDto">The deny request with comment and request identifiers.</param>
        /// <param name="activityType">Possible values: ManageRequests</param>
        /// <returns></returns>
        /// <response code="200">Returns the request action response.</response>
        /// <response code="400">If RequestIds is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(RequestActionResponseDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.ManageRequests)]
        public async Task<IActionResult> Deny([FromBody] DenyDto denyDto, string activityType)
        {
            if (denyDto.RequestIds == null || denyDto.RequestIds.Length == 0 || Array.Exists(denyDto.RequestIds, id => id == Guid.Empty))
            {
                return BadRequest();
            }
            var result = mapper.Map<RequestActionResponseDto>(await Service.DenyAsync(denyDto.RequestIds, denyDto.Comment));
            return Ok(result);
        }

        /// <summary>
        /// Distribute secrets for the requests.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Request/DistributeSecrets?activityType=ManageRequests
        ///     [
        ///         "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45"
        ///     ]
        /// </remarks>
        /// <param name="requestIds">The request identifiers.</param>
        /// <param name="activityType">Possible values: ManageRequests</param>
        /// <returns></returns>
        /// <response code="200">Returns the list of secrets.</response>
        /// <response code="400">If RequestIds is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(IEnumerable<SecretDto>))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.ManageRequests)]
        public async Task<IActionResult> DistributeSecrets([FromBody] Guid[] requestIds, string activityType)
        {
            if (requestIds == null || requestIds.Length == 0 || Array.Exists(requestIds, id => id == Guid.Empty))
            {
                return BadRequest();
            }
            var result = mapper.Map<IEnumerable<SecretDto>>(await Service.DistributeSecretsAsync(requestIds));
            return Ok(result);
        }

        /// <summary>
        /// Gets the request details.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /Request/GetRequest?ActivityType=ManageRequests&#38;requestId=01203f03-d612-47fe-bf28-aa354df6a812
        /// </remarks>
        /// <param name="requestId">The request identifier.</param>
        /// <param name="activityType">Possible values: ManageRequests</param>
        /// <returns></returns>
        /// <response code="200">Returns the request details.</response>
        /// <response code="400">If request RequestId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="404">If no item was found.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("[action]")]
        [SwaggerResponse(200, Type = typeof(RequestDetailsDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.ManageRequests)]
        public async Task<IActionResult> GetRequest(Guid requestId, string activityType)
        {
            if (requestId == Guid.Empty)
            {
                return BadRequest();
            }

            var result = await Service.GetRequestAsync(requestId);

            if (result != null)
            {
                return Ok(mapper.Map<RequestDetailsDto>(result));
            }

            return NotFound();
        }

        /// <summary>
        /// Gets the list of the requests.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /Request/GetRequestList?Ascending=false&#38;OrderBy=Submitted&#38;OriginatorUserName=&#38;Page=1&#38;RequestForm=&#38;RequestState=Pending&#38;SubmittedFrom=&#38;SubmittedTo=&#38;TargetUserName=&#38;activityType=ManageRequests
        /// </remarks>
        /// <param name="searchRequest">The search request.</param>
        /// <param name="activityType">Possible values: ManageRequests</param>
        /// <returns></returns>
        /// <response code="200">Returns the list of the requests.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("[action]")]
        [SwaggerResponse(200, Type = typeof(RESTPagedResult<SignedRequestDto>))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.ManageRequests)]
        public async Task<IActionResult> GetRequestList(SearchRequestDto searchRequest, string activityType)
        {
            IPagedList<SignedRequestDto> result = null;

            var filterOptions = new RequestFilterParameters()
            {
                RequestState = searchRequest.RequestState,
                RequestForm = searchRequest.RequestForm,
                OriginatorUserName = searchRequest.OriginatorUserName,
                TargetUserName = searchRequest.TargetUserName,
                SubmittedFrom = searchRequest.SubmittedFrom,
                SumbittedTo = searchRequest.SubmittedTo
            };

            var sortingOptions = new SortingParameters()
            {
                OrderBy = searchRequest.OrderBy,
                Ascending = searchRequest.Ascending
            };

            var pageSize = await configuration.SearchUsersPageSize;

            var pagingOptions = new PagingParameters(searchRequest.Page, pageSize);

            var list = await Service.FindRequestsAsync(filterOptions, pagingOptions, sortingOptions);

            var requests = mapper.Map<IEnumerable<SignedRequestDto>>(list);
            result = new StaticPagedList<SignedRequestDto>(requests, pagingOptions.PageNumber, pagingOptions.PageSize, list.TotalItemCount);
            var collection = new RESTPagedResult<SignedRequestDto>(result);

            return Ok(collection);
        }

        /// <summary>
        /// Validates the request.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /Request/ValidateActions?requestId=b4cc4ff0-e10f-4828-b27d-deb62b1d6c45&#38;activityType=ManageRequests
        /// </remarks>
        /// <param name="requestId">The request id.</param>
        /// <param name="activityType">Possible values: ManageRequests</param>
        /// <returns></returns>
        /// <response code="200">Returns the validation object.</response>
        /// <response code="400">If request RequestId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("[action]")]
        [SwaggerResponse(200, Type = typeof(ValidationDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.ManageRequests)]
        public async Task<IActionResult> ValidateActions(Guid requestId, string activityType)
        {
            if (requestId == Guid.Empty)
            {
                return BadRequest();
            }

            var result = mapper.Map<ValidationDto>(await Service.ValidateActionsAsync(requestId));

            return Ok(result);
        }

        #endregion Methods

        #region Classes

        public class ApprovePolicyDto 
        {
            #region Properties
            public bool DisplaySecretsOnScreen { get; set; }
            
            public bool EmailSecrets { get; set; }
            
            public int NumberOfApprovers { get; set; }

            #endregion Properties
        }

        public class DenyDto 
        {
            #region Properties

            public string Comment { get; set; }
            public Guid[] RequestIds { get; set; }

            #endregion Properties
        }

        public class RequestActionResponseDto 
        {
            #region Properties
            public bool HasSecrets { get; set; }
            
            public bool IsSuccessful { get; set; }

            #endregion Properties
        }

        public class ApproveResultDto 
        {
            public RequestActionResponseDto Result { get; set; }

            public IEnumerable<SecretDto> Secrets { get; set; }
        }

        public class RequestDetailsDto 
        {
            #region Properties
            
            public string Comment { get; set; }
            
            public IDictionary<string, string> DataCollectionItems { get; set; }
            
            public DateTime DateCompleted { get; set; }
            
            public DateTime DateSubmitted { get; set; }
            
            public Guid OriginatorUserId { get; set; }
            
            public string OriginatorUserName { get; set; }
            
            public int Priority { get; set; }
            
            public Guid ProfileId { get; set; }
            
            public ProfileTemplateDto ProfileTemplate { get; set; }
            
            public Guid ProfileTemplateId { get; set; }
            
            public RequestEventHistoryDto RequestEventHistory { get; set; }
            
            public RequestForm RequestForm { get; set; }
            
            public Guid RequestId { get; set; }
            
            public RequestState RequestState { get; set; }
            
            public TargetForm TargetForm { get; set; }
            
            public bool TargetFormSet { get; set; }
            
            public String TargetUserName { get; set; }
            
            public Guid UserId { get; set; }

            #endregion Properties
        }

        public class RequestEventHistoryDto 
        {
            #region Properties
            
            public string Action { get; set; }
            
            public string EventDetails { get; set; }
            
            public DateTime Time { get; set; }
            
            public string User { get; set; }

            #endregion Properties
        }

        public class SearchRequestDto
        {
            #region Properties
            
            public bool Ascending { get; set; } = true;
            
            public string OrderBy { get; set; }
            
            public string OriginatorUserName { get; set; }
            
            public int Page { get; set; }
            
            public RequestForm? RequestForm { get; set; }
            
            public RequestState? RequestState { get; set; }
            
            public DateTime? SubmittedFrom { get; set; }
            
            public DateTime? SubmittedTo { get; set; }
            
            public string TargetUserName { get; set; }

            #endregion Properties
        }

        public class SecretDto 
        {
            #region Properties
            public ApprovePolicyDto Distribution { get; set; }
            
            public RequestDetailsDto RequestDetails { get; set; }
            
            public IEnumerable<string> Secrets { get; set; }

            #endregion Properties
        }

        public class SignedRequestDto
        {
            #region Properties
            
            public DateTime DateSubmitted { get; set; }
            
            public String OriginatorUserName { get; set; }
            
            public RequestForm RequestForm { get; set; }
            
            public Guid RequestId { get; set; }
            
            public RequestState RequestState { get; set; }
            
            public SignatureState SignatureStatus { get; set; }
            
            public TargetForm TargetType { get; set; }
            
            public String TargetUserName { get; set; }

            #endregion Properties
        }

        public class ValidationDto
        {
            #region Properties
            
            public bool Abandon { get; set; }
            
            public bool Approve { get; set; }
            
            public bool Cancel { get; set; }
            
            public bool Deny { get; set; }
            
            public bool DistributeSecrets { get; set; }
            
            public Guid RequestId { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}
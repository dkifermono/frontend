﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Controllers;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Web.RestModels;

namespace m2trust.Frontend.Web.Controllers
{
    public class BaseFinishController : BaseController
    {
        protected readonly IDataCollectionItemService dataCollectionItemService;
        protected readonly IProfileService profileService;
        protected readonly ISignatureConfiguration configuration;
        protected readonly IApplicationUserContext applicationUserContext;

        public BaseFinishController(
            IMapper mapper, 
            ILogWriter logger,
            IDataCollectionItemService dataCollectionItemService,
            IProfileService profileService,
            ISignatureConfiguration configuration,
            IApplicationUserContext applicationUserContext) : base(mapper, logger)
        {
            this.dataCollectionItemService = dataCollectionItemService;
            this.profileService = profileService;
            this.configuration = configuration;
            this.applicationUserContext = applicationUserContext;
        }

        protected async Task ValidateDataCollectionItemsAsync(AuditRequestEntry request, SectionType actionType)
        {
            var errors = new List<Common.ValidationError>();
            var items = await dataCollectionItemService.GetDataCollectionItemsAsync(actionType, request.ProfileTemplateId);

            if (!dataCollectionItemService.ValidateDataCollectionItems(items, request.DataCollectionItems, errors))
            {
                throw new Common.ValidationException(errors);
            }
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            var currentUser = await applicationUserContext.GetCurrentUser();
            if (!await profileService.ValidateProfileTemplateRequestAsync(request))
            {
                throw new Common.ValidationException("Signature is missing.");
            }
        }

        protected List<DataItemDto> AddSignatureToItems(List<DataItemDto> items, string signatureKey, string signature)
        {
            foreach (DataItemDto item in items)
            {
                if (item.Name.Equals(signatureKey, StringComparison.InvariantCultureIgnoreCase))
                {
                    item.Value = signature;
                }
            }

            return items;
        }
    }
}

﻿using AutoMapper;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Web.Infrastructure.Security;
using Microsoft.AspNetCore.Mvc;
using PagedList.Core;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Controllers
{
    [Route("api/[controller]")]
    public class UserController : BaseController
    {
        #region Fields

        private const string objectclass = "objectclass";
        private readonly ISearchFormConfiguration configuration;
        private readonly bool extendedSearch;
        private readonly IUserService userService;

        #endregion Fields

        #region Constructors

        public UserController(IUserService userService, IMapper mapper, ILogWriter logger, ISearchFormConfiguration configuration) : base(mapper, logger)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            extendedSearch = false;
            this.configuration = configuration;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Gets the user.
        /// </summary>
        ///<remarks>
        /// Sample request: 
        /// 
        ///     GET /User/GetUser?ActivityType=Enroll
        /// </remarks>
        /// <param name="model">The search user request.</param>
        /// <param name="activityType">Possible values: Enroll, UnblockSmartcard</param>
        /// <response code="200">Returns the user entry.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="404">If no item was found.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("[action]")]
        [SwaggerResponse(200, Type = typeof(UserEntryDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.Enroll, Activity.UnblockSmartcard)]
        public async Task<IActionResult> GetUser(SearchUserRequestDto model, string activityType)
        {
            if (model.UserId == Guid.Empty)
            {
                return BadRequest();
            }

            var user = await userService.GetUserAsync(model.UserId);

            if (user != null)
            {
                var result = mapper.Map<UserEntryDto>(user);
                return Ok(result);
            }

            return NotFound();
        }

        /// <summary>
        /// Gets the user filters.
        /// </summary>
        ///<remarks>
        /// Sample request: 
        /// 
        ///     GET /User/GetUserFilters
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Returns the user search fields.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("[action]")]
        [SwaggerResponse(200, Type = typeof(SearchFieldsDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        public async Task<IActionResult> GetUserFilters()
        {
            var domains = await userService.GetDomainsAsync();
            var searchFields = await configuration.UserSearchProperties;
            if (!extendedSearch)
                searchFields = searchFields.Where(i => !i.ToLowerInvariant().Equals(objectclass)).ToArray();
            var result = new SearchFieldsDto
            {
                Domains = domains,
                SearchFields = searchFields.ToArray()
            };
            return Ok(result);
        }

        /// <summary>
        /// Gets the user list.
        /// </summary>
        ///<remarks>
        /// Sample request: 
        /// 
        ///     GET /User/GetUsers?ActivityType=Enroll
        /// </remarks>
        /// <param name="model">The model.</param>
        /// <param name="additionalDisplayProperties">The additional display properties.</param>
        /// <param name="additionalSearchProperties">The additional search properties.</param>
        /// <param name="activityType">Possible values: Enroll, DisableSmartcard, RecoverCertificate, ReinstateSmartcard, RetireTemporarySmartcard, RevokeCertificate, RewriteSmartcard, SuspendSmartcard, UnblockSmartcard, RenewCertificate</param>
        /// <returns></returns>
        /// <response code="200">Returns the list of the users.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("[action]")]
        [SwaggerResponse(200, Type = typeof(RESTPagedResult<UserEntryDto>))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.Enroll, Activity.DisableSmartcard, Activity.RecoverCertificate, Activity.ReinstateSmartcard, Activity.RetireTemporarySmartcard, Activity.RevokeCertificate,
         Activity.RewriteSmartcard, Activity.SuspendSmartcard, Activity.UnblockSmartcard, Activity.RenewCertificate)]
        public async Task<IActionResult> GetUsers(UserSearchRequestDto model, string[] additionalDisplayProperties, string[] additionalSearchProperties, string activityType)
        {
            IPagedList<UserEntryDto> result = null;
            var displayFields = (await configuration.UserDisplayProperties).Concat(additionalDisplayProperties).ToArray();
            var searchFields = (await configuration.UserSearchProperties).Concat(additionalSearchProperties).Union(displayFields).ToArray();
            var pageSize = await configuration.SearchUsersPageSize;
            var pagingParameters = new PagingParameters(model.Page, pageSize);
            var request = this.mapper.Map<UserSearchRequest>(model);
            request.Fields = string.IsNullOrEmpty(model.SearchField) ? searchFields : new[] { model.SearchField };
            var users = await userService.GetUsersAsync(pagingParameters, request, displayFields.ToArray(), extendedSearch);
            var userEntries = this.mapper.Map<IEnumerable<UserEntryDto>>(users);
            result = new StaticPagedList<UserEntryDto>(userEntries, pagingParameters.PageNumber, pagingParameters.PageSize, users.TotalItemCount);
            var collection = new RESTPagedResult<UserEntryDto>(result);
            if (!extendedSearch)
                displayFields = displayFields.Where(i => !i.ToLowerInvariant().Equals(objectclass)).ToArray();
            collection.Initialize(result, mapper, displayFields);

            return Ok(collection);
        }

        #endregion Methods

        #region Classes

        public class SearchFieldsDto 
        {
            #region Properties

            public IEnumerable<DomainEntry> Domains { get; set; }
            public ICollection<string> SearchFields { get; set; }

            #endregion Properties
        }

        public class SearchUserRequestDto
        {
            #region Properties
            
            public Guid UserId { get; set; }

            #endregion Properties
        }

        public class UserEntryDto 
        {
            #region Fields

            private string _directoryPath = String.Empty;

            #endregion Fields

            #region Properties

            public string DirectoryPath
            {
                get
                {
                    if (!String.IsNullOrEmpty(_directoryPath))
                    {
                        var startIndex = _directoryPath.LastIndexOf('/') != -1 ? (_directoryPath.LastIndexOf('/') + 1) : 0;
                        return _directoryPath.Substring(startIndex, _directoryPath.Length - startIndex);
                    }
                    return null;
                }
                set
                {
                    _directoryPath = value;
                }
            }

            public Guid Id { get; set; }
            public string Name { get; set; }
            public string ObjectClass { get; set; }
            public IDictionary<string, object> Properties { get; set; }
            public byte[] Sid { get; set; }

            #endregion Properties
        }

        public class UserSearchRequestDto
        {
            #region Properties
            
            public string Domain { get; set; }
            
            public int Page { get; set; } = 1;
            
            public string SearchField { get; set; }
            
            public string SearchTerm { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}
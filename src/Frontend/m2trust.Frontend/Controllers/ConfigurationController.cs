﻿using AutoMapper;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Controllers;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Web.Infrastructure.MapperFactories;
using m2trust.Frontend.Web.Infrastructure.Security;
using m2trust.Frontend.Web.RestModels;
using m2trust.Frontend.Web.RestModels.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.Controllers
{
    /// <summary>
    /// UI configuration controller.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Controllers.BaseController" />
    [Route("api/Configuration")]
    public class ConfigurationController : BaseController
    {
        #region Fields

        private readonly IUIConfigurationService uIConfigurationService;
        private readonly IFrontendConfigurationService frontendConfigurationService;
        private readonly IApplicationUserContext applicationUserContext;
        private readonly IProfileService profileService;
        private readonly IProfileTemplateConfigurationFactory profileTemplateConfigurationFactory;
        private readonly IServiceConfigurationFactory serviceConfigurationFactory;
        private readonly IFrontendConfigurationFactory frontendConfigurationFactory;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationController" /> class.
        /// </summary>
        /// <param name="mapper">The mapper.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="uIConfigurationService">The service.</param>
        /// <param name="frontendConfigurationService"></param>
        /// <param name="applicationUserContext"></param>
        /// <param name="profileService"></param>
        /// <param name="profileTemplateConfigurationFactory"></param>
        /// <param name="serviceConfigurationFactory"></param>
        /// <param name="frontendConfigurationFactory"></param>
        public ConfigurationController(IMapper mapper, ILogWriter logger,
            IUIConfigurationService uIConfigurationService,
            IFrontendConfigurationService frontendConfigurationService,
            IApplicationUserContext applicationUserContext,
            IProfileService profileService,
            IProfileTemplateConfigurationFactory profileTemplateConfigurationFactory,
            IServiceConfigurationFactory serviceConfigurationFactory,
            IFrontendConfigurationFactory frontendConfigurationFactory) : base(mapper, logger)
        {
            this.uIConfigurationService = uIConfigurationService;
            this.frontendConfigurationService = frontendConfigurationService;
            this.applicationUserContext = applicationUserContext;
            this.profileService = profileService;
            this.profileTemplateConfigurationFactory = profileTemplateConfigurationFactory;
            this.serviceConfigurationFactory = serviceConfigurationFactory;
            this.frontendConfigurationFactory = frontendConfigurationFactory;
        }

        #endregion Constructors

        #region Methods

        #region ServiceConfiguration
        /// <summary>
        /// Gets the service configuration for frontend.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /Configuration/GetServiceConfiguration
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Returns the base configuration.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("GetServiceConfiguration")]
        [SwaggerResponse(200, Type = typeof(ServiceConfigurationDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Common.Enums.Activity.Administration)]
        public async Task<IActionResult> GetServiceConfiguration(string activityType)
        {
            var serviceConfiguration = await frontendConfigurationService.GetServiceConfigurationAsync();

            if (serviceConfiguration == null)
            {
                return NotFound();
            }

            var result = this.serviceConfigurationFactory.MapConfigurationDto(serviceConfiguration);
            return Ok(result);
        }

        /// <summary>
        /// Saves new service configuration.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Configuration/PostServiceConfiguration
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("PostServiceConfiguration")]
        [SwaggerResponse(200)]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Common.Enums.Activity.Administration)]
        public async Task<IActionResult> PostServiceConfiguration([FromBody] ServiceConfigurationDto configurationDto, string activityType)
        {
            var config = this.serviceConfigurationFactory.MapConfiguration(configurationDto);
            await frontendConfigurationService.SaveServiceConfigurationAsync(config);
            return Ok();
        }
        #endregion ServiceConfiguration

        #region ProfileTemplateConfiguration

        /// <summary>
        /// Gets the profile template configuration.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /Configuration/GetProfileTemplatesConfiguration
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Returns the profile template configuration.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("GetProfileTemplatesConfiguration")]
        [SwaggerResponse(200, Type = typeof(ProfileTemplatesConfigurationDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Common.Enums.Activity.Administration)]
        public async Task<IActionResult> GetProfileTemplatesConfiguration(string activityType)
        {
            var configuration = await frontendConfigurationService.GetFrontendConfigurationAsync();
            var profileTemplateLookup = await profileService.GetProfileTemplatesAsync();

            if (configuration == null)
            {
                return NotFound();
            }
            var result = profileTemplateConfigurationFactory.MapConfigurationDto(configuration, profileTemplateLookup);
            return Ok(result);
        }

        /// <summary>
        /// Saves new profile template configuration.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Configuration/PostProfileTemplatesConfiguration
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("PostProfileTemplatesConfiguration")]
        [SwaggerResponse(200)]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Common.Enums.Activity.Administration)]
        public async Task<IActionResult> PostProfileTemplatesConfiguration([FromBody] ProfileTemplatesConfigurationDto configurationDto, string activityType)
        {
            var currentUser = await this.applicationUserContext.GetCurrentUser();
            if (configurationDto == null)
            {
                return BadRequest();
            }
            var profileTemplateLookup = await profileService.GetProfileTemplatesAsync();
            var configuration = await this.frontendConfigurationService.GetFrontendConfigurationAsync();
            var config = profileTemplateConfigurationFactory.MapConfiguration(configurationDto, profileTemplateLookup, configuration);
            await frontendConfigurationService.SaveFrontendConfigurationAsync(config, currentUser.UniqueIdentifier);
            return Ok();
        }

        #endregion ProfileTemplateConfiguration

        #region ActivitiesConfiguration

        /// <summary>
        /// Gets the activities configuration.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /Configuration/GetActivitiesConfiguration
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Returns the activities configuration.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("GetActivitiesConfiguration")]
        [SwaggerResponse(200, Type = typeof(ActivitiesConfigurationDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        public async Task<IActionResult> GetActivitiesConfiguration()
        {
            var config = await this.frontendConfigurationService.GetFrontendConfigurationAsync();

            var result = new ActivitiesConfigurationDto(config);

            return Ok(result);
        }

        /// <summary>
        /// Saves the new activities configuration.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Configuration/PostActivitiesConfiguration
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("PostActivitiesConfiguration")]
        [SwaggerResponse(200)]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Common.Enums.Activity.Administration)]
        public async Task<IActionResult> PostActivitiesConfiguration([FromBody] ActivitiesConfigurationDto configurationDto, string activityType)
        {
            var currentUser = await this.applicationUserContext.GetCurrentUser();
            if (configurationDto == null)
            {
                return BadRequest();
            }
            var config = await this.frontendConfigurationService.GetFrontendConfigurationAsync();
            config = frontendConfigurationFactory.MapConfiguration(configurationDto, config);
            await frontendConfigurationService.SaveFrontendConfigurationAsync(config, currentUser.UniqueIdentifier);
            return Ok();
        }

        #endregion ActivitiesConfiguration

        #region UserPageConfiguration

        /// <summary>
        /// Gets the user page configuration.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /Configuration/GetUserPageConfiguration
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Returns the page text configuration.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("GetUserPageConfiguration")]
        [SwaggerResponse(200, Type = typeof(UserPageConfigurationDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        public async Task<IActionResult> GetUserPageConfiguration()
        {
            var config = await this.frontendConfigurationService.GetFrontendConfigurationAsync();

            var result = new UserPageConfigurationDto(config);

            return Ok(result);
        }

        /// <summary>
        /// Saves new the user page configuration.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Configuration/PostUserPageConfiguration
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("PostUserPageConfiguration")]
        [SwaggerResponse(200)]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Common.Enums.Activity.Administration)]
        public async Task<IActionResult> PostUserPageConfiguration([FromBody] UserPageConfigurationDto configurationDto, string activityType)
        {
            var currentUser = await this.applicationUserContext.GetCurrentUser();
            if (configurationDto == null)
            {
                return BadRequest();
            }
            var config = await this.frontendConfigurationService.GetFrontendConfigurationAsync();
            config = frontendConfigurationFactory.MapConfiguration(configurationDto, config);
            await frontendConfigurationService.SaveFrontendConfigurationAsync(config, currentUser.UniqueIdentifier);
            return Ok();
        }

        #endregion UserPageConfiguration

        #region TextConfiguration

        /// <summary>
        /// Gets the text configuration.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /Configuration/GetTextConfiguration
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Returns the page text configuration.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("GetTextConfiguration")]
        [SwaggerResponse(200, Type = typeof(TextConfigurationDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        public async Task<IActionResult> GetTextConfiguration()
        {
            var config = await this.frontendConfigurationService.GetFrontendConfigurationAsync();

            var result = new TextConfigurationDto(config);

            return Ok(result);
        }

        /// <summary>
        /// Saves new text configuration.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Configuration/PostTextConfiguration
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("PostTextConfiguration")]
        [SwaggerResponse(200)]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Common.Enums.Activity.Administration)]
        public async Task<IActionResult> PostTextConfiguration([FromBody] TextConfigurationDto configurationDto, string activityType)
        {
            var currentUser = await this.applicationUserContext.GetCurrentUser();
            if (configurationDto == null)
            {
                return BadRequest();
            }
            var config = await this.frontendConfigurationService.GetFrontendConfigurationAsync();
            config = frontendConfigurationFactory.MapConfiguration(configurationDto, config);
            await frontendConfigurationService.SaveFrontendConfigurationAsync(config, currentUser.UniqueIdentifier);
            return Ok();
        }

        #endregion TextConfiguration

        #region UISettings
        /// <summary>
        /// Gets the colors.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /Configuration/GetColors?ActivityType=UiSettings
        /// </remarks>
        /// <param name="activityType">Possible values: UiSettings</param>
        /// <returns></returns>
        /// <response code="200">Returns the colors.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="404">If no item was found.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("GetColors")]
        [SwaggerResponse(200, Type = typeof(ColorsDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Common.Enums.Activity.Administration)]
        public async Task<IActionResult> GetColors(string activityType)
        {
            var colors = await uIConfigurationService.GetColorsAsync();
            if (colors == null)
            {
                return NotFound();
            }
            var result = mapper.Map<ColorsDto>(colors);

            return Ok(result);
        }

        [HttpGet("GetLogoImage")]
        [SwaggerResponse(200, Type = typeof(string))]
        public async Task<IActionResult> GetLogoImage()
        {
            var logo = await uIConfigurationService.GetLogoAsync();

            return Ok(logo);
        }

        [HttpGet("GetColorVariables")]
        public async Task<IActionResult> GetColorVariables()
        {
            var colors = await uIConfigurationService.GetColorVariablesAsync();

            return new ContentResult()
            {
                ContentType = "text/css",
                StatusCode = (int)HttpStatusCode.OK,
                Content = colors
            };
        }


        [HttpPost("PostUISettings")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [AuthorizeActivities(Common.Enums.Activity.Administration)]
        public async Task<IActionResult> PostUISettings([FromForm]UIConfigurationDto settings, string activityType)
        {
            var result = new UIConfigurationResultDto();
            if (settings == null)
            {
                return BadRequest();
            }
            if (settings.Colors != null)
            {
                result.ColorsResult = await uIConfigurationService.WriteColorsAsync(mapper.Map<Colors>(settings.Colors));
            }
            if (settings.Image != null)
            {
                var contentType = settings.Image.ContentType.ToLower();

                if (contentType != "image/png" &&
                    contentType != "image/jpeg" &&
                    contentType != "image/jpg" &&
                    contentType != "image/pjpeg" &&
                    contentType != "image/x-png")
                {
                    return BadRequest();
                }

                var fileName = Path.GetExtension(settings.Image.FileName).ToLower();
                if (fileName != ".png" &&
                    fileName != ".jpg" &&
                    fileName != ".jpeg")
                {
                    return BadRequest();
                }

                var logoBase64 = await UploadImageAsync(settings.Image);
                result.ImageResult = await uIConfigurationService.WriteLogoAsync(logoBase64);
            }
            return Ok(result);
        }
        private async Task<string> UploadImageAsync(IFormFile file)
        {
            if (file != null && file.Length > 0)
            {
                using (var stream = new MemoryStream())
                {
                    await file.CopyToAsync(stream);
                    var fileBytes = stream.ToArray();
                    return Convert.ToBase64String(fileBytes);
                }
            }
            return String.Empty;
        }
        #endregion UISettings

        /// <summary>
        /// Gets the product version.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /Configuration/GetProductVersion
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Returns the version.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("GetProductVersion")]
        [SwaggerResponse(200, Type = typeof(string))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        public async Task<IActionResult> GetProductVersion()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string productVersion = FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion;

            return Ok(productVersion);
        }


        #endregion Methods

    }
}
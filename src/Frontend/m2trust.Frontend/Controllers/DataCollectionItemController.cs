﻿using AutoMapper;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Web.Infrastructure.Security;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Controllers
{
    [Route("api/[controller]")]
    public class DataCollectionItemController : BaseController
    {
        #region Fields

        private readonly ISignatureConfiguration configuration;
        private readonly IDataCollectionItemService dataCollectionItemService;
        private readonly IProfileService profileService;
        private readonly ISigningService signingService;

        #endregion Fields

        #region Constructors

        public DataCollectionItemController(
            IMapper mapper,
            ILogWriter logger,
            IDataCollectionItemService dataCollectionItemService,
            IProfileService profileService,
            ISigningService signingService,
            ISignatureConfiguration configuration) : base(mapper, logger)
        {
            this.dataCollectionItemService = dataCollectionItemService;
            this.profileService = profileService;
            this.signingService = signingService;
            this.configuration = configuration;
        }

        #endregion Constructors

        #region Methods


        /// <summary>
        /// Gets the data collection items.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /DataCollectionItem/GetDataCollectionItems?ActivityType=Enroll&#38;ProfileTemplateId=01203f03-d612-47fe-bf28-aa354df6a812&#38;SectionType=Enroll
        /// </remarks>
        /// 
        /// <param name="param">The choose template.</param>
        /// <returns>
        /// Returns the data collection result with data collection items.
        /// </returns>
        /// <response code="200">Returns the data collection result with data collection items.</response>
        /// <response code="400">If request ProfileTemplateId or ProfileId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="404">If no item was found.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpGet("[action]")]
        [SwaggerResponse(200, Type = typeof(DataCollectionDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.Enroll, Activity.DisableSmartcard, Activity.RecoverCertificate, Activity.ReinstateSmartcard, Activity.RetireTemporarySmartcard, Activity.RevokeCertificate,
         Activity.RewriteSmartcard, Activity.SuspendSmartcard, Activity.UnblockSmartcard, Activity.RenewCertificate)]
        public async Task<IActionResult> GetDataCollectionItems(ChooseTemplateDto param)
        {
            const string errorMessage = "You must either provide ProfileTemplateId or ProfileId";
            if (!param.ProfileTemplateId.HasValue || param.ProfileTemplateId.Value == Guid.Empty)
            {
                if (!param.ProfileId.HasValue || param.ProfileId == Guid.Empty)
                    return BadRequest(errorMessage);

                var profile = await profileService.GetProfileAsync(param.ProfileId.Value);

                if (profile == null && profile.ProfileTemplate == null)
                    return BadRequest(errorMessage);
                param.ProfileTemplateId = profile.ProfileTemplate.Id;
            }

            var items = await dataCollectionItemService.GetDataCollectionItemsAsync(param.SectionType, param.ProfileTemplateId.Value);

            if (items != null && items.Count > 0)
            {
                var signatureDciKey = await configuration.SignatureDataCollectionItemName;
                var mappedItems = mapper.Map<ICollection<DataCollectionItemDto>>(items).ToList();
               
                var result = new DataCollectionDto
                {
                    DataCollectionItems = mappedItems,
                    HasElegibleDcis = mappedItems.Any(i => !string.Equals(i.Name, signatureDciKey, StringComparison.InvariantCultureIgnoreCase)),
                    HasSignature = items.Any(c => c.Name.Equals(signatureDciKey, StringComparison.InvariantCultureIgnoreCase)),
                    SignatureDciKey = signatureDciKey
                };

                return Ok(result);
            }

            var emptyResult = new DataCollectionDto
            {
                DataCollectionItems = new Collection<DataCollectionItemDto>()
            };

            return Ok(emptyResult);
        }

        #endregion Methods

        #region Classes

        public class ChooseTemplateDto
        {
            #region Properties            
            /// <summary>
            /// Possible values: Enroll, DisableSmartcard, RecoverCertificate, ReinstateSmartcard, RetireTemporarySmartcard, RevokeCertificate, RewriteSmartcard, SuspendSmartcard, UnblockSmartcard, RenewCertificate
            /// </summary>
            public string ActivityType { get; set; }
            public Guid? ProfileId { get; set; }
            public Guid? ProfileTemplateId { get; set; }
            public SectionType SectionType { get; set; }

            #endregion Properties
        }

        public class DataCollectionDto
        {
            #region Constructors

            public DataCollectionDto()
            {
                DataCollectionItems = new List<DataCollectionItemDto>();
            }

            #endregion Constructors

            #region Properties

            public ICollection<DataCollectionItemDto> DataCollectionItems { get; set; }
            public bool HasElegibleDcis { get; set; }
            public bool HasSignature { get; set; }
            public string SignatureDciKey { get; set; }

            #endregion Properties
        }

        public class DataCollectionItemDto
        {
            #region Properties

            public DataTypeDci DataType { get; set; }
            public string Description { get; set; }
            public string Name { get; set; }
            public bool Required { get; set; }
            public string ValidationData { get; set; }
            public ValidationTypeDci ValidationType { get; set; }
            public string Value { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}
﻿using AutoMapper;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using System;

namespace m2trust.Frontend.Controllers
{
    [Produces("application/json")]
    public class BaseController : Controller
    {
        #region Fields

        protected readonly ILogWriter logger;
        protected readonly IMapper mapper;

        #endregion Fields

        #region Constructors

        public BaseController(IMapper mapper, ILogWriter logger)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        #endregion Constructors

        #region Methods

        protected ExceptionDto CreateExceptionDto(string message)
        {
            var exception = new ExceptionDto
            {
                Message = message
            };

            return exception;
        }


        #endregion Methods
    }

    public class ExceptionDto 
    {
        #region Properties

        public string Message { get; set; }

        public Guid? ErrorId { get; set; } = null;

        #endregion Properties
    }
}
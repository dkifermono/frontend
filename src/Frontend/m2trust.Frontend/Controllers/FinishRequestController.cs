﻿using AutoMapper;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Web.Infrastructure.Security;
using m2trust.Frontend.Web.RestModels;
using m2trust.Messaging.Configuration.API;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using m2trust.Frontend.Web.Controllers;

namespace m2trust.Frontend.Controllers
{
    [Route("api/[controller]")]
    public class FinishRequestController : BaseFinishController
    {
        #region Fields

        private readonly IActivityConfiguration activityConfiguration;
        private readonly ISigningService signingService;
        private readonly ISmartcardService smartcardService;
        private readonly IServiceConfigurationStore serviceConfigurationStore;

        #endregion Fields

        #region Constructors

        public FinishRequestController(IMapper mapper,
            IProfileService profileService,
            ISigningService signingService,
            ISmartcardService smartcardService,
            IDataCollectionItemService dataCollectionItemService,
            ILogWriter logger,
            IApplicationUserContext applicationUserContext,
            ISignatureConfiguration configuration,
            IActivityConfiguration activityConfiguration,
            IServiceConfigurationStore serviceConfigurationStore)
            : base(mapper,
            logger,
            dataCollectionItemService,
            profileService,
            configuration,
            applicationUserContext)
        {
            this.signingService = signingService;
            this.smartcardService = smartcardService;
            this.activityConfiguration = activityConfiguration;
            this.serviceConfigurationStore = serviceConfigurationStore;
        }

        #endregion Constructors

        #region Methods


        /// <summary>
        /// Audits the data collection items. 
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     POST /FinishRequest/Audit?activityType=Enroll 
        ///     {
        ///         "items":
        ///         {
        ///             "Optional String Item":null,
        ///             "Optional Date Item":null,
        ///             "Optional Integer Item":null
        ///         },
        ///         "profileTemplateId":"01203f03-d612-47fe-bf28-aa354df6a812",
        ///         "sectionType":"Enroll",
        ///         "userId":"02ae5258-e333-4d99-9f00-9e27c9331d31"
        ///     }
        /// </remarks>
        /// <param name="dciData">The data collection items.</param>
        /// <param name="activityType">Possible values: Enroll, DisableSmartcard, RecoverCertificate, ReinstateSmartcard, RetireTemporarySmartcard, RevokeCertificate, RewriteSmartcard, SuspendSmartcard, UnblockSmartcard, RenewCertificate</param>
        /// <returns>Data collection item data.</returns>
        /// <response code="200">Returns data collection item data.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In a case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(DciDataDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.Enroll, Activity.DisableSmartcard, Activity.RecoverCertificate, Activity.ReinstateSmartcard, Activity.RetireTemporarySmartcard, Activity.RevokeCertificate,
         Activity.RewriteSmartcard, Activity.SuspendSmartcard, Activity.UnblockSmartcard, Activity.RenewCertificate)]
        public async Task<IActionResult> Audit([FromBody]DciDataDto dciData, string activityType)
        {
            await InitializeSignatureParams(dciData);

            var showComments = await activityConfiguration.GetShowCommentsAsync(activityType);

            dciData.ShowComments = showComments.GetValueOrDefault();

            return Ok(dciData);
        }

        /// <summary>
        /// Checks if action has specify delay enabled.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        ///     
        ///     POST /FinishRequest/AllowSpecifyDelay?activityType=ReinstateSmartcard
        ///     { 
        ///         "profileId": "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45", 
        ///         "userId":"6663af4b-1f66-4a63-833a-9a30e2c1715c", 
        ///         "comment": "this is a test" 
        ///     }
        ///     
        /// </remarks>
        /// <param name="auditRequest">The audit request.</param>
        /// <param name="activityType">Possible values: ReinstateSmartcard, SuspendSmartcard, DisableSmartcard, RevokeCertificate</param>
        /// <returns></returns>
        /// <response code="200">Returns if Profile template has allow specify delay on specified policy.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In a case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(bool))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.ReinstateSmartcard, Activity.SuspendSmartcard, Activity.DisableSmartcard, Activity.RevokeCertificate)]
        public async Task<IActionResult> AllowSpecifyDelay([FromBody] AuditRequestDto auditRequest, string activityType)
        {
            var enableDelay = await profileService.AllowSpecifyDelayAsync(auditRequest.ProfileId.Value, activityType);
            return Ok(enableDelay);
        }

        /// <summary>
        /// Disables the smartcard.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     POST /FinishRequest/DisableSmartcard?activityType=DisableSmartcard
        ///     { 
        ///         "profileId": "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45", 
        ///         "userId":"6663af4b-1f66-4a63-833a-9a30e2c1715c", 
        ///         "comment": "this is a test" 
        ///     }
        ///     
        /// </remarks>
        /// <param name="requestDisable">The audit request.</param>
        /// <param name="activityType">Possible values: DisableSmartcard</param>
        /// <returns></returns>
        /// <response code="200">Returns disable result.</response>
        /// <response code="400">If request UserId or ProfileId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In a case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(DisableResultDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.DisableSmartcard)]    
        public async Task<IActionResult> DisableSmartcard([FromBody]AuditRequestDto requestDisable, string activityType)
        {
            if (requestDisable == null || requestDisable.UserId == Guid.Empty || requestDisable.ProfileId == Guid.Empty)
            {
                return BadRequest();
            }
            
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            var result = new DisableResultDto();
            if(requestDisable.DataCollections != null)
            {
                AddSignatureToItems(requestDisable.DataCollections, signatureDciKey, requestDisable.Signature);
            }           
            var mappedEntry = mapper.Map<AuditRequestEntry>(requestDisable);
            var profile = await profileService.GetSmartcardProfileAsync(requestDisable.ProfileId.Value);
            mappedEntry.ProfileTemplateId = profile.ProfileTemplate.Id;

            await ValidateDataCollectionItemsAsync(mappedEntry, SectionType.Revoke);
            var response = await smartcardService.DisableSmartcardAsync(mappedEntry);
            result.OtpResult = mapper.Map<OtpResultDto>(response);
            result.OtpResult.HasSignature = mappedEntry.DataCollectionItems != null &&
                                   mappedEntry.DataCollectionItems.ContainsKey(signatureDciKey) &&
                                   mappedEntry.DataCollectionItems[signatureDciKey].Length > 0;
            result.Continue = (await activityConfiguration.GetContinueAsync(activityType)).GetValueOrDefault();
            return Ok(result);
        }


        /// <summary>
        /// Enrolls the request.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     POST /FinishRequest/EnrollRequest?activityType=Enroll
        ///     { 
        ///         "profileTemplateId": "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45", 
        ///         "userId":"6663af4b-1f66-4a63-833a-9a30e2c1715c", 
        ///         "comment": "this is a test" 
        ///     }
        /// </remarks>
        /// <param name="requestEnroll">The audit request.</param>
        /// <param name="activityType">Possible values: Enroll</param>
        /// <returns></returns>
        /// <response code="200">Returns request success result with HasSignature.</response>
        /// <response code="400">If request UserId or ProfileTemplateId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In a case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(RequestSuccessDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.Enroll)]
        public async Task<IActionResult> EnrollRequest([FromBody]AuditRequestDto requestEnroll, string activityType)
        {
            if (requestEnroll == null || requestEnroll.UserId == Guid.Empty || requestEnroll.ProfileTemplateId == Guid.Empty)
            {
                return BadRequest();
            }
            
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            var result = new RequestSuccessDto();
            
            if (requestEnroll.DataCollections != null)
            {
                AddSignatureToItems(requestEnroll.DataCollections, signatureDciKey, requestEnroll.Signature);
            }                 
            var auditRequest = mapper.Map<AuditRequestEntry>(requestEnroll);
            await ValidateDataCollectionItemsAsync(auditRequest, SectionType.Enroll);
            await profileService.EnrollProfileTemplateAsync(auditRequest);

            result.HasSignature = auditRequest.DataCollectionItems != null &&
                                auditRequest.DataCollectionItems.ContainsKey(signatureDciKey) &&
                                auditRequest.DataCollectionItems[signatureDciKey] != null;
            return Ok(result);
        }


        /// <summary>
        /// Enters the challenge.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     POST /FinishRequest/EnterChallenge?activityType=UnblockSmartcard
        ///     { 
        ///         "profileId": "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45", 
        ///         "userId":"6663af4b-1f66-4a63-833a-9a30e2c1715c", 
        ///         "challenge": "AABCDEFF5432A67D" 
        ///     }
        /// </remarks>
        /// <param name="challengeResponseRequest">The challenge response request.</param>
        /// <param name="activityType">Possible values: UnblockSmartcard</param>
        /// <returns></returns>
        /// <response code="200">Returns challenge response string.</response>
        /// <response code="400">If request UserId or ProfileId or Challenge is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In a case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(string))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.UnblockSmartcard)]
        public async Task<IActionResult> EnterChallenge([FromBody]ChallengeResponseRequestDto challengeResponseRequest, string activityType)
        {
            if (challengeResponseRequest == null || challengeResponseRequest.UserId == Guid.Empty ||
                challengeResponseRequest.ProfileId == Guid.Empty || challengeResponseRequest.Challenge == null)
            {
                return BadRequest();
            }

            var mappedRequest = mapper.Map<EnterChallengeRequestEntry>(challengeResponseRequest);

            var response = await smartcardService.OfflineUnblockChallengeAsync(mappedRequest);
            if (response != null)
            {
                return Ok(response);
            }

            return StatusCode((int)HttpStatusCode.InternalServerError);
        }

        /// <summary>
        /// Initiates the offline unblock.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     POST /FinishRequest/InitiateOfflineUnblock?activityType=UnblockSmartcard
        ///     { 
        ///         "profileId": "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45", 
        ///         "userId":"6663af4b-1f66-4a63-833a-9a30e2c1715c", 
        ///         "comment": "this is a test" 
        ///     }
        /// </remarks>
        /// <param name="request">The audit request.</param>
        /// <param name="activityType">Possible values: UnblockSmartcard</param>
        /// <returns></returns>
        /// <response code="200">Returns challenge response request.</response>
        /// <response code="400">If request UserId or ProfileId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In a case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(EnterChallengeRequestDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.UnblockSmartcard)]
        public async Task<IActionResult> InitiateOfflineUnblock([FromBody]AuditRequestDto request, string activityType)
        {
            if (request == null || request.UserId == Guid.Empty || request.ProfileId == Guid.Empty)
            {
                return BadRequest();
            }
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            if(request.DataCollections != null)
            {
                AddSignatureToItems(request.DataCollections, signatureDciKey, request.Signature);
            }           
            var mappedEntry = mapper.Map<AuditRequestEntry>(request);
            var smartcardProfile = await profileService.GetSmartcardProfileAsync(request.ProfileId.Value);
            mappedEntry.ProfileTemplateId = smartcardProfile.ProfileTemplate.Id;

            await ValidateDataCollectionItemsAsync(mappedEntry, SectionType.OfflineUnblock);
            var response = await smartcardService.InitiateOfflineUnblockAsync(mappedEntry);
            var challengeRequest = new EnterChallengeRequestDto
            {
                ProfileId = mappedEntry.ProfileId,
                UserId = mappedEntry.UserId,
                ProfileName = smartcardProfile.ProfileTemplate.Name,
                RequestGuid = response.RequestId
            };

            return Ok(challengeRequest);
        }

        /// <summary>
        /// Recovers the profile.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     POST /FinishRequest/ProfileRecover?activityType=RecoverCertificate
        ///     { 
        ///         "profileTemplateId": "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45", 
        ///         "userId":"6663af4b-1f66-4a63-833a-9a30e2c1715c", 
        ///         "comment": "this is a test" 
        ///     }
        /// </remarks>
        /// <param name="requestRecover">The audit request.</param>
        /// <param name="activityType">Possible values: RecoverCertificate</param>
        /// <returns></returns>
        /// <response code="200">Returns one-time password result entry.</response>
        /// <response code="400">If request UserId or ProfileId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In a case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(OtpResultDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.RecoverCertificate)]
        public async Task<IActionResult> ProfileRecover([FromBody]AuditRequestDto requestRecover, string activityType)
        {
            if (requestRecover == null || requestRecover.ProfileId == Guid.Empty || requestRecover.UserId == Guid.Empty)
            {
                return BadRequest();
            }
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            var result = new OtpResultDto();
            if(requestRecover != null)
            {
                AddSignatureToItems(requestRecover.DataCollections, signatureDciKey, requestRecover.Signature);
            }           
            var mappedEntry = mapper.Map<AuditRequestEntry>(requestRecover);
            var profile = await profileService.GetProfileAsync(requestRecover.ProfileId.Value);
            if (profile?.ProfileTemplate != null)
            {
                mappedEntry.ProfileTemplateId = profile.ProfileTemplate.Id;
            }
            else
            {
                return BadRequest("Error while fetching profile template");
            }

            await ValidateDataCollectionItemsAsync(mappedEntry, SectionType.Recover);
            result = mapper.Map<OtpResultDto>(await profileService.ProfileRecoverAsync(mappedEntry));
            result.HasSignature = mappedEntry.DataCollectionItems != null &&
                               mappedEntry.DataCollectionItems.ContainsKey(signatureDciKey) &&
                               mappedEntry.DataCollectionItems[signatureDciKey].Length > 0;
            return Ok(result);
        }

        /// <summary>
        /// Revokes the profile.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     POST /FinishRequest/ProfileRevoke?activityType=RevokeCertificate
        ///     { 
        ///         "profileTemplateId": "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45", 
        ///         "userId":"6663af4b-1f66-4a63-833a-9a30e2c1715c", 
        ///         "comment": "this is a test" 
        ///     }
        /// </remarks>
        /// <param name="requestRevoke">The audit request.</param>
        /// <param name="activityType">Possible values: RevokeCertificate</param>
        /// <returns></returns>
        /// <response code="200">Returns otp result with OneTimePassword.</response>
        /// <response code="400">If request UserId or ProfileId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In a case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(OtpResultDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.RevokeCertificate)]
        public async Task<IActionResult> ProfileRevoke([FromBody] AuditRequestDto requestRevoke, string activityType)
        {
            if (requestRevoke == null || requestRevoke.ProfileId == Guid.Empty || requestRevoke.UserId == Guid.Empty)
            {
                return BadRequest();
            }
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            var result = new OtpResultDto();
            if(requestRevoke.DataCollections != null)
            {
                AddSignatureToItems(requestRevoke.DataCollections, signatureDciKey, requestRevoke.Signature);
            }         
            var mappedEntry = mapper.Map<AuditRequestEntry>(requestRevoke);
            var profile = await profileService.GetProfileAsync(requestRevoke.ProfileId.Value);
            if (profile?.ProfileTemplate != null)
            {
                mappedEntry.ProfileTemplateId = profile.ProfileTemplate.Id;
            }
            else
            {
                return BadRequest("Error while fetching profile template");
            }

            await ValidateDataCollectionItemsAsync(mappedEntry, SectionType.Revoke);
            result = mapper.Map<OtpResultDto>(await profileService.ProfileRevokeAsync(mappedEntry));
            result.HasSignature = mappedEntry.DataCollectionItems != null &&
                               mappedEntry.DataCollectionItems.ContainsKey(signatureDciKey) &&
                               mappedEntry.DataCollectionItems[signatureDciKey].Length > 0;
            return Ok(result);
        }

        /// <summary>
        /// Renew a selected profile.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /FinishRequest/RenewProfile?activityType=RenewCertificate
        ///     { 
        ///         "profileId": "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45", 
        ///         "userId":"6663af4b-1f66-4a63-833a-9a30e2c1715c", 
        ///         "comment": "this is a test" 
        ///     }
        /// </remarks>
        /// <param name="requestRenew">The renew request.</param>
        /// <param name="activityType">Possible values: RenewCertificate</param>
        /// <returns>A renew result.</returns>
        /// <response code="200">Returns one-time password result</response>
        /// <response code="400">If request UserId or ProfileId is null</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In a case of an exception</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(OtpResultDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.RenewCertificate)]
        public async Task<IActionResult> RenewProfile([FromBody] AuditRequestDto requestRenew, string activityType)
        {
            if (requestRenew == null || requestRenew.ProfileId == Guid.Empty || requestRenew.UserId == Guid.Empty)
            {
                return BadRequest();
            }
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            var result = new OtpResultDto();
            if(requestRenew.DataCollections != null)
            {
                AddSignatureToItems(requestRenew.DataCollections, signatureDciKey, requestRenew.Signature);
            }            
            var mappedEntry = mapper.Map<AuditRequestEntry>(requestRenew);
            var profile = await profileService.GetProfileAsync(requestRenew.ProfileId.Value);
            if (profile != null && profile.ProfileTemplate != null)
            {
                mappedEntry.ProfileTemplateId = profile.ProfileTemplate.Id;
            }
            else
            {
                return BadRequest("Error while fetching profile template");
            }

            await ValidateDataCollectionItemsAsync(mappedEntry, SectionType.Renew);
            result = mapper.Map<OtpResultDto>(await profileService.ProfileRenewAsync(mappedEntry));
            
            result.HasSignature = mappedEntry.DataCollectionItems != null &&
                               mappedEntry.DataCollectionItems.ContainsKey(signatureDciKey) &&
                               mappedEntry.DataCollectionItems[signatureDciKey].Length > 0;

            
            return Ok(result);
        }

        private async Task InitializeSignatureParams(DciDataDto dciData)
        {
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            var serviceFimConfiguration = await serviceConfigurationStore.GetFimConfiguration();

            if (dciData.Items == null)
                dciData.Items = new Dictionary<string, string>();

            var hasSignature = dciData.HasSignature ||
                dciData.Items.Any(
                    c =>
                    c.Key.Equals(signatureDciKey, StringComparison.InvariantCultureIgnoreCase));

            if (dciData.Items == null && hasSignature)
            {
                dciData.Items = new Dictionary<string, string> { { signatureDciKey, String.Empty } };
            }


            if (ModelState.IsValid && hasSignature)
            {
                var currentUser = await applicationUserContext.GetCurrentUser();              
                dciData.OriginatorId = currentUser.UniqueIdentifier;
                dciData.UsesSignature = true;
                dciData.SignatureTemplateOid = serviceFimConfiguration.SignatureTemplateOid;
                dciData.SignatureEkuOid = serviceFimConfiguration.SignatureEkuOid;
            }
        }

        #endregion Methods

        #region Classes
        
        

        public class ChallengeResponseRequestDto : EnterChallengeRequestDto
        {
            #region Properties

            [Required]
            [RegularExpression(@"[0-9a-fA-F]{16}")]
            public string Challenge { get; set; }

            #endregion Properties
        }



        public class DciDataDto
        {
            #region Properties
            public bool HasSignature { get; set; }
            public IDictionary<string, string> Items { get; set; }
            public Guid ProfileId { get; set; }
            public Guid ProfileTemplateId { get; set; }
            public SectionType SectionType { get; set; }
            public bool ShowComments { get; set; }
            public Guid UserId { get; set; }
            public bool UsesSignature { get; set; }
            public string SignatureTemplateOid { get; set; }
            public string SignatureEkuOid { get; set; }

            public Guid OriginatorId { get; set; }

            #endregion Properties
        }

        public class EnterChallengeRequestDto 
        {
            #region Properties

            public Guid ProfileId { get; set; }
            public string ProfileName { get; set; }
            public Guid RequestGuid { get; set; }
            public Guid UserId { get; set; }

            #endregion Properties
        }

        public class RequestSuccessDto 
        {
            #region Properties

            public bool HasSignature { get; set; }

            #endregion Properties
        }


        public class DisableResultDto
        {
            #region Properties 

            public OtpResultDto OtpResult { get; set; }
            public bool Continue { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}
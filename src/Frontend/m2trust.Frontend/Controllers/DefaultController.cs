﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.Controllers
{
    public class DefaultController : Controller
    {

        public IActionResult Error()
        {
            throw new Exception("Can't access the server please check request URL");
        }
    }
}

﻿using AutoMapper;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Controllers;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Web.Infrastructure.Security;
using m2trust.Frontend.Web.RestModels;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.Controllers
{
    [Route("api/[controller]")]
    public class SmartcardController : BaseFinishController
    {
        #region Fields
        private readonly ISigningService signingService;
        private readonly ISmartcardService smartcardService;
        private readonly IActivityConfiguration activityConfiguration;

        #endregion Fields

        #region Constructors

        public SmartcardController(IMapper mapper,
            ISigningService signingService,
            ISmartcardService smartcardService,
            IProfileService profileService,
            IApplicationUserContext applicationUserContext,
            ILogWriter logger,
            ISignatureConfiguration configuration,
            IDataCollectionItemService dataCollectionItemService,
            IActivityConfiguration activityConfiguration) 
            : base(mapper, 
                logger, 
                dataCollectionItemService, 
                profileService, 
                configuration, 
                applicationUserContext)
        {
            this.signingService = signingService;
            this.smartcardService = smartcardService;
            this.activityConfiguration = activityConfiguration;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Reinstates the smartcard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Smartcard/Reinstate?activityType=ReinstateSmartcard
        ///     { 
        ///         "profileId": "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45", 
        ///         "userId":"6663af4b-1f66-4a63-833a-9a30e2c1715c", 
        ///         "comment": "this is a test" 
        ///     }
        /// </remarks>
        /// <param name="model">The one-time password result.</param>
        /// <param name="activityType">Possible values: ReinstateSmartcard, SuspendSmartcard</param>
        /// <returns></returns>
        /// <response code="200">Returns one-time password result.</response>
        /// <response code="400">If request UserId or ProfileId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(OtpResultDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.ReinstateSmartcard, Activity.SuspendSmartcard)]
        public async Task<IActionResult> Reinstate([FromBody]AuditRequestDto model, string activityType)
        {
            if (model == null || model.UserId == Guid.Empty || model.ProfileId == Guid.Empty)
            {
                return BadRequest();
            }
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            var result = new OtpResultDto();
            if(model.DataCollections != null)
            {
                AddSignatureToItems(model.DataCollections, signatureDciKey, model.Signature);
            }            
            var mappedEntry = mapper.Map<AuditRequestEntry>(model);
            var profile = await profileService.GetSmartcardProfileAsync(model.ProfileId.Value);
            mappedEntry.ProfileTemplateId = profile.ProfileTemplate.Id;
            await ValidateDataCollectionItemsAsync(mappedEntry, SectionType.Reinstate);
            var response = await smartcardService.ReinstateSmartcardAsync(mappedEntry);
            result = mapper.Map<OtpResultDto>(response);
            result.HasSignature = mappedEntry.DataCollectionItems != null &&
                                   mappedEntry.DataCollectionItems.ContainsKey(signatureDciKey) &&
                                   mappedEntry.DataCollectionItems[signatureDciKey].Length > 0;
            

            return Ok(result);
        }

        /// <summary>
        /// Retires the and reinstates smartcard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Smartcard/RetireAndReinstate?activityType=RetireTemporarySmartcard
        ///     { 
        ///         "profileId": "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45", 
        ///         "userId":"6663af4b-1f66-4a63-833a-9a30e2c1715c", 
        ///         "comment": "this is a test" 
        ///     }
        /// </remarks>
        /// <param name="model">The audit request entry.</param>
        /// <param name="activityType">Possible values: RetireTemporarySmartcard</param>
        /// <returns></returns>
        /// <response code="200">Returns the retire reinstate result.</response>
        /// <response code="400">If request UserId or ProfileId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(RetireReinstateEntryDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.RetireTemporarySmartcard)]
        public async Task<IActionResult> RetireAndReinstate([FromBody] AuditRequestDto model, string activityType)
        {
            if (model == null || model.UserId == Guid.Empty || model.ProfileId == Guid.Empty)
            {
                return BadRequest();
            }
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            var smartcardProfile = await profileService.GetSmartcardProfileAsync(model.ProfileId.Value);
            if (smartcardProfile == null)
            {
                throw new Exception("Error while fetching profile template");
            }
            var result = new RetireReinstateEntryDto()
            {
                permanentCardId = (await smartcardService.IsSmartcardPermanentAsync(smartcardProfile))? smartcardProfile.PermanentSmartcardId : null,
                AutomaticReinstateEnabled = smartcardService.AutomaticReinstateEnabled
            };
            if (model.DataCollections != null)
            {
                AddSignatureToItems(model.DataCollections, signatureDciKey, model.Signature);
            }
            var mappedEntry = mapper.Map<AuditRequestEntry>(model);
            mappedEntry.ProfileTemplateId = smartcardProfile.ProfileTemplate.Id;
            await ValidateDataCollectionItemsAsync(mappedEntry, SectionType.Retire);
            var retireResponse = await smartcardService.RetireSmartcardAsync(mappedEntry);

            // Automatic Reinstate should be enabled and smartcard should have permanentCardId to Reinstate smartcard
            if (smartcardService.AutomaticReinstateEnabled && result.permanentCardId != null)
            {
                var suspendReinstateRequest = await smartcardService.GetSuspendReinstateRequestAsync(smartcardProfile);
                await smartcardService.ReinstateSmartcardAsync(suspendReinstateRequest).ConfigureAwait(false);
            }

            result.OtpResult = mapper.Map<OtpResultDto>(retireResponse);
            result.OtpResult.HasSignature = mappedEntry.DataCollectionItems != null &&
                                   mappedEntry.DataCollectionItems.ContainsKey(signatureDciKey) &&
                                   mappedEntry.DataCollectionItems[signatureDciKey].Length > 0;
            return Ok(result);
        }

        /// <summary>
        /// Retires the smartcard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Smartcard/Retire?activityType=RetireTemporarySmartcard
        ///     { 
        ///         "profileId": "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45", 
        ///         "userId":"6663af4b-1f66-4a63-833a-9a30e2c1715c", 
        ///         "comment": "this is a test" 
        ///     }
        /// </remarks>
        /// <param name="model">The audit request entry.</param>
        /// <param name="activityType">Possible values: RewriteSmartcard</param>
        /// <returns></returns>
        /// <response code="200">Returns the retire result.</response>
        /// <response code="400">If request UserId or ProfileId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(RetireResultDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.RewriteSmartcard)]
        public async Task<IActionResult> Retire([FromBody] AuditRequestDto model, string activityType)
        {
            if (model == null || model.UserId == Guid.Empty || model.ProfileId == Guid.Empty)
            {
                return BadRequest();
            }
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            var result = new RetireResultDto();
            if (model.DataCollections != null)
            {
                AddSignatureToItems(model.DataCollections, signatureDciKey, model.Signature);
            }
            var mappedEntry = mapper.Map<AuditRequestEntry>(model);
            var profile = await profileService.GetSmartcardProfileAsync(model.ProfileId.Value);
            mappedEntry.ProfileTemplateId = profile.ProfileTemplate.Id;
            await ValidateDataCollectionItemsAsync(mappedEntry, SectionType.Retire);
            var response = await smartcardService.RetireSmartcardAsync(mappedEntry);
            result.OtpResult = mapper.Map<OtpResultDto>(response);
            result.OtpResult.HasSignature = mappedEntry.DataCollectionItems != null &&
                                       mappedEntry.DataCollectionItems.ContainsKey(signatureDciKey) &&
                                       mappedEntry.DataCollectionItems[signatureDciKey].Length > 0;
            result.Continue = (await activityConfiguration.GetContinueAsync(activityType)).GetValueOrDefault();
            return Ok(result);
        }

        /// <summary>
        /// Suspends the smartcard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Smartcard/Suspend?activityType=SuspendSmartcard
        ///     { 
        ///         "profileId": "b4cc4ff0-e10f-4828-b27d-deb62b1d6c45", 
        ///         "userId":"6663af4b-1f66-4a63-833a-9a30e2c1715c", 
        ///         "comment": "this is a test" 
        ///     }
        /// </remarks>
        /// <param name="model">The suspend reinstate dto.</param>
        /// <param name="activityType">Possible values: SuspendSmartcard</param>
        /// <returns></returns>
        /// <response code="200">Returns the one-time password result.</response>
        /// <response code="400">If request UserId or ProfileId is null.</response>
        /// <response code="403">If user does not have permission to perform this action.</response>
        /// <response code="500">In case of an exception.</response>        
        [HttpPost("[action]")]
        [SwaggerResponse(200, Type = typeof(OtpResultDto))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        [AuthorizeActivities(Activity.SuspendSmartcard)]
        public async Task<IActionResult> Suspend([FromBody]AuditRequestDto model, string activityType)
        {
            if (model == null || model.UserId == Guid.Empty || model.ProfileId == Guid.Empty)
            {
                return BadRequest();
            }
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            var result = new OtpResultDto();
            if (model.DataCollections != null)
            {
                AddSignatureToItems(model.DataCollections, signatureDciKey, model.Signature);
            }
            var mappedEntry = mapper.Map<AuditRequestEntry>(model);
            var profile = await profileService.GetSmartcardProfileAsync(model.ProfileId.Value);
            mappedEntry.ProfileTemplateId = profile.ProfileTemplate.Id;
            await ValidateDataCollectionItemsAsync(mappedEntry, SectionType.Suspend);
            var response = await smartcardService.SuspendSmartcardAsync(mappedEntry);
            result = mapper.Map<OtpResultDto>(response);
            result.HasSignature = mappedEntry.DataCollectionItems != null &&
                                   mappedEntry.DataCollectionItems.ContainsKey(signatureDciKey) &&
                                   mappedEntry.DataCollectionItems[signatureDciKey].Length > 0;
            return Ok(result);
        }

        #endregion Methods

        #region Classes


        public class RetireReinstateEntryDto 
        {
            #region Properties

            public bool AutomaticReinstateEnabled { get; set; }
            public Guid? permanentCardId { get; set; }
            public OtpResultDto OtpResult { get; set; }

            #endregion Properties
        }



        public class RetireResultDto
        {
            #region Properties

            public OtpResultDto OtpResult { get; set; }
            public bool Continue { get; set; }
            public string Signature { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}
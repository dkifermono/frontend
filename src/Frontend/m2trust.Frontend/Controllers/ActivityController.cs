﻿using AutoMapper;
using m2trust.Frontend.Controllers;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.Controllers
{
    [Route("api/[controller]")]
    public class ActivityController : BaseController
    {
        #region Fields

        private readonly IActivityService activityService;
        private readonly IApplicationUserContext applicationUserContext;

        #endregion Fields

        #region Constructors

        public ActivityController(
            IActivityService activityService,
            IApplicationUserContext applicationUserContext,
            IMapper mapper,
            ILogWriter logger) : base(mapper, logger)
        {
            this.activityService = activityService ?? throw new ArgumentNullException(nameof(activityService));
            this.applicationUserContext = applicationUserContext ?? throw new ArgumentNullException(nameof(applicationUserContext));
        }

        #endregion Constructors

        #region Methods


        /// <summary>
        /// Gets the activities.
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// 
        ///     GET /Activity/GetActivities
        /// </remarks>
        /// 
        /// <returns>A list of activities that the user can perform.</returns>
        /// <response code="200">Returns the list of activities.</response>
        /// <response code="500">In a case of an exception</response>
        [HttpGet("[action]")]
        [SwaggerResponse(200, Type = typeof(List<ActivityDto>))]
        [SwaggerResponse(500, Type = typeof(ExceptionDto))]
        public async Task<IActionResult> GetActivities()
        {
            var currentUser = await this.applicationUserContext.GetCurrentUser();
            var activities = await this.activityService.GetActivitiesAsync(currentUser);
            return Ok(mapper.Map<IEnumerable<ActivityDto>>(activities));
        }

        #endregion Methods

        #region Classes

        public class ActivityDto
        {
            #region Properties

            public string ActivityName { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}
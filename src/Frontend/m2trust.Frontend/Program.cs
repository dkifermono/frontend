using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.HttpSys;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace m2trust.Frontend
{
    public class Program
    {
        #region Methods

        public static void Main(string[] args)
        {
            var config = WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureAppConfiguration((hostingContext, cfg) =>
                {
                    cfg.SetBasePath(Directory.GetCurrentDirectory());
                    cfg.AddJsonFile("appsettings.user.json", optional: true, reloadOnChange: true);
                })
                .ConfigureServices(services => services.AddAutofac());
//#if DEBUG
//            config.UseHttpSys(options =>
//            {
//                options.Authentication.Schemes = AuthenticationSchemes.Negotiate;
//                options.Authentication.AllowAnonymous = true;
//            })
//            .UseUrls("http://localhost:5000/", "https://localhost:5001/")
//            .UseEnvironment("Development");
//#else
            config.UseIISIntegration();
//#endif
            config.Build().Run();
        }

        #endregion Methods
    }
}
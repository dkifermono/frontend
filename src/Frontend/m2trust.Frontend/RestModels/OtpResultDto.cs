﻿using m2trust.Frontend.Web.Infrastructure;
using System;

namespace m2trust.Frontend.Web.RestModels
{
    public class OtpResultDto 
    {
        #region Properties

        public Guid RequestId { get; set; }
        public string OneTimePassword { get; set; }
        public bool HasSignature { get; set; }

        #endregion Properties
    }
}

﻿using m2trust.Frontend.Web.Infrastructure;

namespace m2trust.Frontend.Web.RestModels
{
    public class DataItemDto 
    {
        #region Properties

        public string Name { get; set; }
        public string Value { get; set; }

        #endregion Properties
    }
}

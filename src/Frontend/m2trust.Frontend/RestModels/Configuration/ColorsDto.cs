﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.RestModels
{
    public class ColorsDto
    {
        public string MenuBackgroundColor { get; set; }
        public string MenuHoverFontColor { get; set; }
        public string InnerMenuBackgroundColor { get; set; }
        public string MenuFontColor { get; set; }
        public string PageBackgroundColor { get; set; }
        public string PageTitleBackgroundColor { get; set; }
        public string PageTitleFontColor { get; set; }
        public string SelectedMenuItemBackgroundColor { get; set; }
        public string SelectedMenuItemFontColor { get; set; }
        public string SelectedTableRowBackgroundColor { get; set; }
        public string SelectedTableRowFontColor { get; set; }
        public string TableRowFontColor { get; set; }

    }
}

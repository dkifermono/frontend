﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.RestModels
{
    public class UIConfigurationDto
    {
        public ColorsDto Colors { get; set; }
        public IFormFile Image { get; set; }
    }
}

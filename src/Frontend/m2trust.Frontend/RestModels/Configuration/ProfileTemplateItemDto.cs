﻿using m2trust.Frontend.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.RestModels
{
    public class ProfileTemplateItemDto
    {

        public ProfileTemplateItemDto()
        {

        }

        public ProfileTemplateItemDto(ProfileTemplate profileTemplate)
        {
            this.ProfileTemplateId = profileTemplate.ProfileTemplateId;
            this.ReinstateDCIs = profileTemplate.ReinstateDCIs.Select(p => new DCIDto(p)).ToList();
        }

        public Guid ProfileTemplateId { get; set; }

        public List<DCIDto> ReinstateDCIs { get; set; }
    }
}

﻿using m2trust.Frontend.Common.Configuration.Models;
using System.Collections.Generic;

namespace m2trust.Frontend.Web.RestModels
{
    public class BaseActivityDto
    {
        public BaseActivityDto(){ }

        public BaseActivityDto(BaseActivity baseActivity)
        {
            this.AuthorizedGroups = baseActivity.AuthorizedGroups;
            this.Name = baseActivity.Name;
            this.ShowComments = baseActivity.ShowComments;
            this.Continue = baseActivity.Continue;
        }

        public IEnumerable<string> AuthorizedGroups { get; set; }
        public string Name { get; set; }
        public bool? ShowComments { get; set; }
        public bool? Continue { get; set; }
    }
}

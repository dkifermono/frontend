﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.RestModels.Configuration
{
    public class MimConfigurationDto
    {
        public string SignatureTemplateOid { get; set; }

        public string SignatureEkuOid { get; set; }
        public string SignatureDciName { get; set; }

        public string MimProviderMimFrontendBaseUrl { get; set; }

        public string MimProviderRequestOperationsEndpoint { get; set; }

        public string MimProviderFindOperationsEndpoint { get; set; }

        public string MimProviderPermissionOperationsEndpoint { get; set; }

        public string MimProviderExecuteOperationsEndpoint { get; set; }

        public bool RejectInvalidSigningCertificate { get; set; }
    }
}

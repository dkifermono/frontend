﻿using m2trust.Frontend.Common.Enums;
using System.Collections.Generic;

namespace m2trust.Frontend.Web.RestModels
{
    public class UserPageConfigurationDto
    {

        public UserPageConfigurationDto() { }

        public UserPageConfigurationDto(Common.Configuration.Models.Configuration config)
        {
            this.UserDisplayProperties = config.UserDisplayProperties;
            this.UserSearchProperties = config.UserSearchProperties;
            this.SearchUsersPageSize = config.SearchUsersPageSize;
        }


        public IEnumerable<string> UserSearchProperties { get; set; }

        public IEnumerable<string> UserDisplayProperties { get; set; }

        public UserDisplayProperties UserDisplayPropertiesEnum { get; set; }

        public UserSearchProperties UserSearchPropertiesEnum { get; set; }

        public int SearchUsersPageSize { get; set; }
    }
}

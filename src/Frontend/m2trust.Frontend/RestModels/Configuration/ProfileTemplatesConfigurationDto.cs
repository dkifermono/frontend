﻿using System.Collections.Generic;
using System.Linq;

namespace m2trust.Frontend.Web.RestModels
{
    public class ProfileTemplatesConfigurationDto
    {
        public ProfileTemplatesConfigurationDto() { }

        public ProfileTemplatesConfigurationDto(Common.Configuration.Models.Configuration config)
        {
            this.DCIProfileTemplates = config.DCIProfileTemplates.Select(dci => new ProfileTemplateItemDto(dci)).ToList();
            this.SelectDefaultProfile = config.SelectDefaultProfile;
            this.ShowDisabledProfiles = config.ShowDisabledProfiles;
        }

        public IEnumerable<ProfileTemplateDto> ProfileTemplateLookup { get; set; }

        public IEnumerable<ProfileTemplateDto> BlacklistedProfileTemplates { get; set; }

        public IEnumerable<ProfileTemplateDto> ExcludedProfileTemplates { get; set; }

        public List<ProfileTemplateItemDto> DCIProfileTemplates { get; set; }

        public bool SelectDefaultProfile { get; set; }

        public bool ShowDisabledProfiles { get; set; }

    }
}

﻿namespace m2trust.Frontend.Web.RestModels.Configuration
{
    public class UIConfigurationResultDto
    {
        public bool ColorsResult { get; set; }
        public bool ImageResult { get; set; }
    }
}

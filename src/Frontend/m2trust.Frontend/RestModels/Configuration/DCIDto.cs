﻿using m2trust.Frontend.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.RestModels
{
    public class DCIDto
    {
        public DCIDto()
        {

        }

        public DCIDto(DCI dci)
        {
            this.Name = dci.Name;
            this.Value = dci.Value;
        }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}

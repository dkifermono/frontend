﻿namespace m2trust.Frontend.Web.RestModels
{
    public class TextItemDto 
    {
        public string En { get; set; }
        public string De { get; set; }
    }
}

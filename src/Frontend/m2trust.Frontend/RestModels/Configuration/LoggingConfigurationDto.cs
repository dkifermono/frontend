﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.RestModels.Configuration
{
    public class LoggingConfigurationDto
    {
        public string LoggingSqlAddress { get; set; }
    }
}

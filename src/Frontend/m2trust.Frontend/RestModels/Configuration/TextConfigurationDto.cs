﻿using m2trust.Frontend.Common.Configuration.Models;

namespace m2trust.Frontend.Web.RestModels
{
    public class TextConfigurationDto
    {
        public TextConfigurationDto() { }

        public TextConfigurationDto(Common.Configuration.Models.Configuration config)
        {
            this.WelcomeText = this.MapPageTextItem(config.WelcomeText);
            this.FooterText = this.MapPageTextItem(config.FooterText);
            this.HeaderText = this.MapPageTextItem(config.HeaderText);
            this.ContactAddressText = this.MapPageTextItem(config.ContactAddressText);
            this.HelpDeskInstruction = this.MapPageTextItem(config.HelpDeskInstruction);
        }

        public TextItemDto WelcomeText { get; set; }
        public TextItemDto FooterText { get; set; }
        public TextItemDto HeaderText { get; set; }
        public TextItemDto ContactAddressText { get; set; }
        public TextItemDto HelpDeskInstruction { get; set; }

        private TextItemDto MapPageTextItem(PageTextItem item)
        {
            return new TextItemDto() { En = item.En, De = item.De };
        }
    }
}

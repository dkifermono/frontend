﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.RestModels.Configuration
{
    public class ServiceConfigurationDto
    {
        public MimConfigurationDto MimConfiguration { get; set; }

        public AdImpersonationConfigurationDto AdImpersonationConfiguration { get; set; }

        public LoggingConfigurationDto LoggingConfiguration { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.RestModels.Configuration
{
    public class AdImpersonationConfigurationDto
    {
        public string ActiveDirectoryUserUserName { get; set; }

        public string ActiveDirectoryUserDomain { get; set; }

        public string ActiveDirectoryUserPassword { get; set; }

        public bool ActiveDirectoryUserDisableLoadProfile { get; set; }
    }
}

﻿using m2trust.Frontend.Common.Configuration.Models;

namespace m2trust.Frontend.Web.RestModels
{
    public class ActivitiesConfigurationDto
    {
        public ActivitiesConfigurationDto() { }

        public ActivitiesConfigurationDto(Common.Configuration.Models.Configuration config)
        {
            this.DisableSmartcard = new BaseActivityDto(config.Activities.DisableSmartcard);
            this.Enroll = new BaseActivityDto(config.Activities.Enroll);
            this.ManageRequests = new BaseActivityDto(config.Activities.ManageRequests);
            this.RecoverCertificate = new BaseActivityDto(config.Activities.RecoverCertificate);
            this.ReinstateSmartcard = new BaseActivityDto(config.Activities.ReinstateSmartcard);
            this.RenewCertificate = new BaseActivityDto(config.Activities.RenewCertificate);
            this.RetireTemporarySmartcard = new BaseActivityDto(config.Activities.RetireTemporarySmartcard);
            this.RevokeCertificate = new BaseActivityDto(config.Activities.RevokeCertificate);
            this.RewriteSmartcard = new BaseActivityDto(config.Activities.RewriteSmartcard);
            this.SuspendSmartcard = new BaseActivityDto(config.Activities.SuspendSmartcard);
            this.UnblockSmartcard = new BaseActivityDto(config.Activities.UnblockSmartcard);
        }

        public BaseActivityDto DisableSmartcard { get; set; }

        public BaseActivityDto Enroll { get; set; }

        public BaseActivityDto ManageRequests { get; set; }

        public BaseActivityDto RecoverCertificate { get; set; }

        public BaseActivityDto ReinstateSmartcard { get; set; }

        public BaseActivityDto RenewCertificate { get; set; }

        public BaseActivityDto RetireTemporarySmartcard { get; set; }

        public BaseActivityDto RevokeCertificate { get; set; }

        public BaseActivityDto RewriteSmartcard { get; set; }

        public BaseActivityDto SuspendSmartcard { get; set; }

        public BaseActivityDto UnblockSmartcard { get; set; }

    }
}

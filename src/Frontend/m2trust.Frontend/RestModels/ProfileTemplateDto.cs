﻿using m2trust.Frontend.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Web.RestModels
{
    public class ProfileTemplateDto 
    {
        #region Properties

        public string Name { get; set; }
        public Guid ProfileTemplateId { get; set; }

        #endregion Properties
    }
}

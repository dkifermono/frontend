﻿using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Web.Infrastructure;
using System;
using System.Collections.Generic;

namespace m2trust.Frontend.Web.RestModels
{
    public class AuditRequestDto 
    {
        #region Properties

        public string Comment { get; set; }
        public List<DataItemDto> DataCollections { get; set; }
        public Guid? ProfileId { get; set; }
        public Guid ProfileTemplateId { get; set; }
        public Guid UserId { get; set; }
        public string Signature { get; set; }
        public Guid OriginatorId { get; set; }
        public int? EffectiveRevocationInHours { get; set; }
        public SuspendReinstateReasonType? Reason { get; set; }

        #endregion Properties
    }
}

﻿using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Model;
using PagedList.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    public interface IUserService
    {
        #region Methods

        /// <summary>
        /// Asynchronously get current domain
        /// </summary>
        /// <returns></returns>
        Task<DomainEntry> GetCurrentDomainAsync();

        /// <summary>
        /// Asynchronously get all domains
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<DomainEntry>> GetDomainsAsync();

        /// <summary>
        /// Asynchronously get single user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<UserEntry> GetUserAsync(Guid userId);

        /// <summary>
        /// Gets the user asynchronous.
        /// </summary>
        /// <param name="securityIdentifier">The security identifier.</param>
        /// <returns></returns>
        Task<UserEntry> GetUserAsync(string securityIdentifier);

        /// <summary>
        /// Asynchronously get all users
        /// </summary>
        /// <param name="paging"></param>
        /// <param name="request"></param>
        /// <param name="userDisplayProperties"></param>
        /// <param name="extendedSearch"></param>
        /// <returns></returns>
        Task<IPagedList<UserEntry>> GetUsersAsync(PagingParameters paging, UserSearchRequest request, string[] userDisplayProperties, bool extendedSearch);

        #endregion Methods
    }
}
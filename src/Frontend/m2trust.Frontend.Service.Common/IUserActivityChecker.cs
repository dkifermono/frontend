﻿using System;
using System.Threading.Tasks;
using m2trust.Frontend.Model;

namespace m2trust.Frontend.Service.Common
{
    public interface IUserActivityChecker
    {
        /// <summary>
        /// Asynchronously checks if the user is authorized for an activity.
        /// </summary>
        /// <param name="user">The user</param>
        /// <param name="activityName">The name of the activity to check.</param>
        /// <returns></returns>
        Task<bool> IsUserAuthorizedForActivityAsync(HttpContextApplicationUser user, string activityName);

    }
}

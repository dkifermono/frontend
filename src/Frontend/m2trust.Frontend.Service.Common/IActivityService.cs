﻿using m2trust.Frontend.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    public interface IActivityService
    {
        /// <summary>
        /// Asynchronously gets allowed activities for a user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        Task<IEnumerable<ActivityInfo>> GetActivitiesAsync(HttpContextApplicationUser userId);
    }
}

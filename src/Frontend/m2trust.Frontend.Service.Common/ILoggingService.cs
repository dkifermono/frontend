﻿using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Model;
using PagedList.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    public interface ILoggingService
    {
        /// <summary>
        /// Asynchronously finds log entries.
        /// </summary>
        /// <param name="filterOptions">Filtering options.</param>
        /// <param name="paging">Paging options.</param>
        /// <returns></returns>
        Task<IPagedList<Log>> GetEntriesAsync(LoggingFilterParameters filterOptions, PagingParameters paging);
    }
}
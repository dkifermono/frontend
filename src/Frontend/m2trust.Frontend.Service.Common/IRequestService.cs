﻿using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Common.Parameters.Sorting;
using m2trust.Frontend.Model;
using PagedList.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    /// <summary>
    /// Request service contract.
    /// </summary>
    public interface IRequestService
    {
        /// <summary>
        /// Asynchronously finds the requests.
        /// </summary>
        /// <param name="filterOptions">The filter options.</param>
        /// <param name="pagingOptions">The paging options.</param>
        /// <param name="sortingOptions">The sorting options.</param>
        /// <returns></returns>
        Task<IPagedList<SignedRequest>> FindRequestsAsync(RequestFilterParameters filterOptions, PagingParameters pagingOptions, SortingParameters sortingOptions);

        /// <summary>
        /// Asynchronously gets the request.
        /// </summary>
        /// <param name="requestId">The request identifier.</param>
        /// <returns></returns>
        Task<RequestDetails> GetRequestAsync(Guid requestId);

        /// <summary>
        /// Asynchronously approves requests.
        /// </summary>
        /// <param name="requestIds">The request ids.</param>
        /// <returns></returns>
        Task<ApproveResult> ApproveAsync(IEnumerable<Guid> requestIds);

        /// <summary>
        /// Asynchronously denies requests.
        /// </summary>
        /// <param name="requestIds">The request ids.</param>
        /// <returns></returns>
        Task<RequestActionResponse> DenyAsync(IEnumerable<Guid> requestIds, string comment);

        /// <summary>
        /// Asynchronously abandons requests.
        /// </summary>
        /// <param name="requestIds">The request ids.</param>
        /// <returns></returns>
        Task<RequestActionResponse> AbandonAsync(IEnumerable<Guid> requestIds);

        /// <summary>
        /// Asynchronously distributes the secrets.
        /// </summary>
        /// <param name="requestIds">The request ids.</param>
        /// <returns></returns>
        Task<IEnumerable<Secret>> DistributeSecretsAsync(IEnumerable<Guid> requestIds);

        /// <summary>
        /// Asynchronously cancels.
        /// </summary>
        /// <param name="requestIds">The request ids.</param>
        /// <returns></returns>
        Task<RequestActionResponse> CancelAsync(IEnumerable<Guid> requestIds);

        /// <summary>
        /// Asynchronously validates the actions.
        /// </summary>
        /// <param name="requestIds">The request ids.</param>
        /// <returns></returns>
        Task<Validation> ValidateActionsAsync(Guid requestId);
    }
}
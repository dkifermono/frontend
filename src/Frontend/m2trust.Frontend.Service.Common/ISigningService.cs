﻿using m2trust.Frontend.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    public interface ISigningService
    { 
        Task CheckSigningCertificateAsync(AuditRequestEntry request);
    }
}

﻿using m2trust.Frontend.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    /// <summary>
    /// Request action validator contract.
    /// </summary>
    public interface IRequestActionValidator
    {
        /// <summary>
        /// Asynchronously validates the approve action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<bool> ValidateApproveAsync(RequestDetails request);

        /// <summary>
        /// Asynchronously validates the deny action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<bool> ValidateDenyAsync(RequestDetails request);

        /// <summary>
        /// Asynchronously validates the distribute secrets action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<bool> ValidateDistributeSecretsAsync(RequestDetails request);

        /// <summary>
        /// Asynchronously validates the cancel action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<bool> ValidateCancelAsync(RequestDetails request);

        /// <summary>
        /// Asynchronously validates the abandon action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<bool> ValidateAbandonAsync(RequestDetails request);
    }
}
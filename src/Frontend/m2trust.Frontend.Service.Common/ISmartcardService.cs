﻿using m2trust.Frontend.Model;
using System;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    public interface ISmartcardService
    {
        #region Properties

        bool AutomaticReinstateEnabled { get; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Asynchronously disables smartcard
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<OtpResult> DisableSmartcardAsync(AuditRequestEntry request);

        /// <summary>
        /// Gets the permanent smartcard Id asynchronous
        /// </summary>
        /// <param name="smartcardId"></param>
        /// <returns></returns>
        Task<Guid?> GetPermanentSmartcardIdAsync(Guid smartcardId);

        Task<bool> IsSmartcardPermanentAsync(SmartcardProfileEntry smartcard);

        /// <summary>
        /// Asynchronously unblocks smartcard
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<RequestDetails> InitiateOfflineUnblockAsync(AuditRequestEntry request);

        /// <summary>
        /// Asynchronously unblock challenge
        /// </summary>
        /// <param name="enterChallengeRequestEntry"></param>
        /// <returns></returns>
        Task<string> OfflineUnblockChallengeAsync(EnterChallengeRequestEntry enterChallengeRequestEntry);

        /// <summary>
        /// Asynchronously Reinstate Smartcard
        /// </summary>
        /// <param name="requestEntry"></param>
        /// <returns></returns>
        Task<OtpResult> ReinstateSmartcardAsync(AuditRequestEntry requestEntry);


        /// <summary>
        /// Retires the smartcard asynchronous
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<OtpResult> RetireSmartcardAsync(AuditRequestEntry request);

        /// <summary>
        /// Asynchronously Suspend Smartcard
        /// </summary>
        /// <param name="requestEntry"></param>
        /// <returns></returns>
        Task<OtpResult> SuspendSmartcardAsync(AuditRequestEntry requestEntry);

        Task<AuditRequestEntry> GetSuspendReinstateRequestAsync(SmartcardProfileEntry smartcard);

        #endregion Methods
    }
}
﻿using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    /// <summary>
    /// File provider contract.
    /// </summary>
    public interface IFileProvider
    {
        #region Methods

        /// <summary>
        /// Asynchronously reads the file.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <returns></returns>
        Task<string> ReadFileAsync();

        /// <summary>
        /// Asynchronously writes to the file.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <param name="content">The file content.</param>
        /// <returns></returns>
        Task WriteFileAsync(string content);

        /// <summary>
        /// Asynchronously gets the logo path.
        /// </summary>
        /// <returns></returns>
        Task<string> GetLogoPathAsync();

        #endregion Methods
    }
}
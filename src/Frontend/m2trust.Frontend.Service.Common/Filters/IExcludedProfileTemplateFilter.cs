﻿using m2trust.Frontend.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Service.Common
{
    public interface IExcludedProfileTemplateFilter : IBaseFilter<ProfileTemplateEntry>
    {
    }
}

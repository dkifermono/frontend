﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common.Filters
{
    public interface IFilterFactory
    {
        Task<List<IProfileStubEntryFilter>> CreateStubsFilterAsync(string activityType);

        Task<List<IProfileFilter>> CreateTemporarySmartcardFilterAsync();

        Task<IExcludedProfileTemplateFilter> CreateProfileTemplateFilterAsync();
    }
}

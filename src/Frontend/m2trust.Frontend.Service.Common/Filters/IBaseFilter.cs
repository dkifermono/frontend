﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    public interface IBaseFilter<T>
    {
        /// <summary>
        /// Filters the specified items asynchronous.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        Task<ICollection<T>> FilterAsync(ICollection<T> items);
    }
}

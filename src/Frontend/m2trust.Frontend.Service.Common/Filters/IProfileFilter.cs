﻿using m2trust.Frontend.Model;

namespace m2trust.Frontend.Service.Common
{
    public interface IProfileFilter : IBaseFilter<ProfileEntry>
    {
    }
}
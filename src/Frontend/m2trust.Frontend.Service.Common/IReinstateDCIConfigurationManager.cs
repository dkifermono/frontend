﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    public interface IReinstateDCIConfigurationManager
    {
        #region Methods

        Task<IDictionary<string, string>> GetDCIs(Guid profileTemplateId);

        #endregion Methods
    }
}
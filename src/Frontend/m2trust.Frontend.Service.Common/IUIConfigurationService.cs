﻿using m2trust.Frontend.Model;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    /// <summary>
    /// UI configuration service contract
    /// </summary>
    public interface IUIConfigurationService
    {
        #region Methods
        
        Task<Colors> GetColorsAsync();

        Task<string> GetColorVariablesAsync();

        Task<bool> WriteColorsAsync(Colors colors);

        Task<string> GetLogoAsync();

        Task<bool> WriteLogoAsync(string logo);

        #endregion Methods
    }
}
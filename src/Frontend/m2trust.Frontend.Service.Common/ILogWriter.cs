﻿using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    /// <summary>
    /// Log writer contract.
    /// </summary>
    public interface ILogWriter
    {
        /// <summary>
        /// Asynchronously writes a debug message.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <returns></returns>
        Task WriteDebugAsync(string message);

        /// <summary>
        /// Asynchronously writes an error message.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <returns></returns>
        Task WriteErrorAsync(string message);

        /// <summary>
        /// Asynchronously writes a info message.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <returns></returns>
        Task WriteInfoAsync(string message);

        /// <summary>
        /// Asynchronously writes a trace message.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <returns></returns>
        Task WriteTraceAsync(string message);

        /// <summary>
        /// Asynchronously writes a warn message.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <returns></returns>
        Task WriteWarnAsync(string message);

        /// <summary>
        /// Asynchronously writes the audit info.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        Task WriteAuditInfoAsync(string message);
    }
}
﻿using m2trust.Frontend.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using m2trust.Messaging.OpenFim.Entities;

namespace m2trust.Frontend.Service.Common
{
    public interface IProfileService
    {
        #region Methods

        /// <summary>
        /// Gets the Allow Specify Delay suspend asynchronously
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="activityType"></param>
        /// <returns></returns>
        Task<bool?> AllowSpecifyDelayAsync(Guid profileId, string activityType);


        /// <summary>
        /// Enroll Profile Template asynchronus
        /// </summary>
        /// <param name="requestProfile"></param>
        /// <returns></returns>
        Task<AuditRequestEntry> EnrollProfileTemplateAsync(AuditRequestEntry requestProfile);

        /// <summary>
        /// Gets the profile asynchronous.
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <returns></returns>
        Task<ProfileEntry> GetProfileAsync(Guid profileId);

        /// <summary>
        /// Gets the profile asynchronous
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ICollection<ProfileEntry>> GetProfilesAsync(Guid userId);

        /// <summary>
        /// Gets the profile stubs asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="activityType"></param>
        /// <returns></returns>
        Task<ICollection<ProfileStubEntry>> GetProfileStubsAsync(Guid userId, string activityType);
        

        /// <summary>
        /// Gets the profile template asynchronous.
        /// </summary>
        /// <param name="profileTemplateId">The profile template identifier.</param>
        /// <param name="profileId">The profile identifier.</param>
        /// <returns></returns>
        Task<ProfileTemplateEntry> GetProfileTemplateAsync(Guid? profileTemplateId, Guid? profileId);

        /// <summary>
        /// Asynchronously Get Profile Templates
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<ProfileTemplateEntry>> GetProfileTemplatesAsync(Guid userId);

        /// <summary>
        /// Gets the smartcard profile asynchronous.
        /// </summary>
        /// <param name="profileId">The smartcard profile identifier.</param>
        /// <returns></returns>
        Task<SmartcardProfileEntry> GetSmartcardProfileAsync(Guid smartcardId);

        /// <summary>
        /// Gets the soft profile asynchronous.
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <returns></returns>
        Task<ProfileEntry> GetSoftProfileAsync(Guid profileId);

        /// <summary>
        /// Profiles the recover asynchronous.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<OtpResult> ProfileRecoverAsync(AuditRequestEntry request);

        /// <summary>
        /// Profiles the revoke asynchronous.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<OtpResult> ProfileRevokeAsync(AuditRequestEntry request);

        /// <summary>
        /// Validates the profile template request asynchronous.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<bool> ValidateProfileTemplateRequestAsync(AuditRequestEntry request);

        /// <summary>
        /// Profiles the renew asynchronous.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<OtpResult> ProfileRenewAsync(AuditRequestEntry request);

        Task<ICollection<ProfileTemplateEntry>> GetProfileTemplatesAsync();

        #endregion Methods
    }
}
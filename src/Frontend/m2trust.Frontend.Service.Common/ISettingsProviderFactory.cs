﻿using System.Threading.Tasks;
using m2trust.Settings;

namespace m2trust.Frontend.Service.Common
{
    /// <summary>
    /// Settings provider factory contract.
    /// </summary>
    public interface ISettingsProviderFactory
    {
        #region Methods

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns></returns>
        Task<ISettingsProvider> CreateAsync();

        #endregion Methods
    }
}
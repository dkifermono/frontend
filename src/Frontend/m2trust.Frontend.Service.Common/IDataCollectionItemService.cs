﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    public interface IDataCollectionItemService
    {
        /// <summary>
        /// Gets all data collection items from the profile template
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="profileTemplateId">The profile template identifier.</param>
        /// <returns></returns>
        Task<ICollection<DataCollectionItemEntry>> GetDataCollectionItemsAsync(SectionType type, Guid profileTemplateId);

        /// <summary>
        /// Validates the data collection items.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <param name="values">The values.</param>
        /// <param name="errors">The errors.</param>
        /// <returns></returns>
        bool ValidateDataCollectionItems(ICollection<DataCollectionItemEntry> items, IDictionary<string, string> values, List<ValidationError> errors);

        /// <summary>
        /// Validates the data collection item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="value">The value.</param>
        /// <param name="errors">The errors.</param>
        /// <returns></returns>
        bool ValidateDataCollectionItem(DataCollectionItemEntry item, string value, List<ValidationError> errors);
    }
}

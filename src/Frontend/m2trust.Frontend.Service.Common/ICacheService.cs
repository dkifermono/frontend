﻿using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Common
{
    public interface ICacheService
    {
        /// <summary>
        /// Asynchronously adds or replaces an item.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        Task AddOrReplaceAsync<T>(string key, T item);

        /// <summary>
        /// Asynchronously gets the item.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        Task<T> GetAsync<T>(string key);

        /// <summary>
        /// Asynchronously Removes the item
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task RemoveKeyAsync(string key);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class CertificateTemplateEntry
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the ca configuration.
        /// </summary>
        /// <value>
        /// The ca configuration.
        /// </value>
        public string CaConfig { get; set; }
    }
}

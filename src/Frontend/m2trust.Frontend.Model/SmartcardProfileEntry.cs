﻿using m2trust.Frontend.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class SmartcardProfileEntry: ProfileEntry
    {
        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        /// <value>
        /// The serial number.
        /// </value>
        public string SerialNumber { get; set; }
        /// <summary>
        /// Gets or sets the atr.
        /// </summary>
        /// <value>
        /// The atr.
        /// </value>
        public string Atr { get; set; }
        /// <summary>
        /// Gets or sets the middleware.
        /// </summary>
        /// <value>
        /// The middleware.
        /// </value>
        public string Middleware { get; set; }
        /// <summary>
        /// Gets or sets the type of the target.
        /// </summary>
        /// <value>
        /// The type of the target.
        /// </value>
        public TargetForm TargetType { get; set; }
        /// <summary>
        /// Gets or sets the permanent smartcard identifier.
        /// </summary>
        /// <value>
        /// The permanent smartcard identifier.
        /// </value>
        public Guid? PermanentSmartcardId { get; set; }
    }
}

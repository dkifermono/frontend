﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class SignatureParameters
    {
        /// <summary>
        /// Gets or sets EkuOid
        /// </summary>
        public string EkuOid { get; set; }

        /// <summary>
        /// Gets or sets TemplateOid
        /// </summary>
        public string TemplateOid { get; set; }

        /// <summary>
        /// Gets or sets UseActivexInIe
        /// </summary>
        public bool UseActivexInIe { get; set; }

        /// <summary>
        /// Gets or sets SignatureDataCollectionItemName
        /// </summary>
        public string SignatureDataCollectionItemName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class EnterChallengeRequestEntry
    {
        /// <summary>
        /// Gets or sets User Id
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets Profile Id
        /// </summary>
        public Guid ProfileId { get; set; }

        /// <summary>
        /// Gets or sets Challenge
        /// </summary>
        public string Challenge { get; set; }

        /// <summary>
        /// Gets or sets RequestGuid
        /// </summary>
        public Guid RequestGuid { get; set; }
    }
}

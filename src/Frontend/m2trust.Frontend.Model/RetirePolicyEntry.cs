﻿namespace m2trust.Frontend.Model
{
    public class RetirePolicyEntry : PolicyEntry
    {
        public bool AllowSpecifyDelay { get; set; }
        public bool AllowSpecifyReason { get; set; }
        public bool BlockAdminPin { get; set; }
        public bool BlockUserPin { get; set; }
        public bool EraseCard { get; set; }
        public bool ResetAdminPin { get; set; }
    }

}

﻿using m2trust.Frontend.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    /// <summary>
    /// Request details model.
    /// </summary>
    public class RequestDetails
    {
        /// <summary>
        /// Gets or sets the profile template.
        /// </summary>
        /// <value>
        /// The profile template.
        /// </value>
        public ProfileTemplateEntry ProfileTemplate { get; set; }

        /// <summary>
        /// Gets or sets the profile template identifier.
        /// </summary>
        /// <value>
        /// The profile template identifier.
        /// </value>
        public Guid ProfileTemplateId { get; set; }

        /// <summary>
        /// Gets or sets the profile identifier.
        /// </summary>
        /// <value>
        /// The profile identifier.
        /// </value>
        public Guid ProfileId { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the request identifier.
        /// </summary>
        /// <value>
        /// The request identifier.
        /// </value>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Gets or sets the submitted date.
        /// </summary>
        /// <value>
        /// The date submitted date.
        /// </value>
        public DateTime DateSubmitted { get; set; }

        /// <summary>
        /// Gets or sets the date completed.
        /// </summary>
        /// <value>
        /// The date completed.
        /// </value>
        public DateTime DateCompleted { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>
        /// The priority.
        /// </value>
        public int Priority { get; set; }

        /// <summary>
        /// Gets or sets the state of the request.
        /// </summary>
        /// <value>
        /// The state of the request.
        /// </value>
        public RequestState RequestState { get; set; }

        /// <summary>
        /// Gets or sets the name of the originator user.
        /// </summary>
        /// <value>
        /// The name of the originator user.
        /// </value>
        public string OriginatorUserName { get; set; }

        /// <summary>
        /// Gets or sets the originator user identifier.
        /// </summary>
        /// <value>
        /// The originator user identifier.
        /// </value>
        public Guid OriginatorUserId { get; set; }

        /// <summary>
        /// Gets or sets the target form.
        /// </summary>
        /// <value>
        /// The target form.
        /// </value>
        public TargetForm TargetForm { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [target form set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [target form set]; otherwise, <c>false</c>.
        /// </value>
        public bool TargetFormSet { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        /// <value>
        /// The comment.
        /// </value>
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the data collection items.
        /// </summary>
        /// <value>
        /// The data collection items.
        /// </value>
        public IDictionary<string, string> DataCollectionItems { get; set; }

        /// <summary>
        /// Gets or sets the request event history.
        /// </summary>
        /// <value>
        /// The request event history.
        /// </value>
        public RequestEventHistory RequestEventHistory { get; set; }

        /// <summary>
        /// Gets or sets the type of the request.
        /// </summary>
        /// <value>
        /// The type of the request.
        /// </value>
        public RequestForm RequestForm { get; set; }

        /// <summary>
        /// Gets or sets the name of the target user.
        /// </summary>
        /// <value>
        /// The name of the target user.
        /// </value>
        public String TargetUserName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    /// <summary>
    /// Approve policy model.
    /// </summary>
    public class PolicyEntry
    {
        /// <summary>
        /// Gets or sets the number of approvers.
        /// </summary>
        /// <value>
        /// The number of approvers.
        /// </value>
        public int NumberOfApprovers { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [display secrets on screen].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [display secrets on screen]; otherwise, <c>false</c>.
        /// </value>
        public bool DisplaySecretsOnScreen { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to [email secrets].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [email secrets]; otherwise, <c>false</c>.
        /// </value>
        public bool EmailSecrets { get; set; }
    }
}
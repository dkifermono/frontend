﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class RenewResultEntry
    {
        public Guid? RequestId { get; set; }
        public string OneTimePassword { get; set; }
    }
}
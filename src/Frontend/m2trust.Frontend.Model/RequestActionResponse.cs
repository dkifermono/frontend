﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class RequestActionResponse
    {
        public bool IsSuccessful { get; set; }

        public bool HasSecrets { get; set; }
    }
}
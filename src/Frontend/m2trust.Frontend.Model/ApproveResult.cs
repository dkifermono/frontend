﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class ApproveResult
    {
        public RequestActionResponse Result { get; set; }

        public IEnumerable<Secret> Secrets { get; set; }
    }
}

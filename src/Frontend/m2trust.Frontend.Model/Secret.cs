﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    /// <summary>
    /// Secret model.
    /// </summary>
    public class Secret
    {
        /// <summary>
        /// Gets or sets the secrets.
        /// </summary>
        /// <value>
        /// The secrets.
        /// </value>
        public IEnumerable<string> Secrets { get; set; }

        /// <summary>
        /// Gets or sets the request details.
        /// </summary>
        /// <value>
        /// The request details.
        /// </value>
        public RequestDetails RequestDetails { get; set; }

        /// <summary>
        /// Gets or sets the approve policy.
        /// </summary>
        /// <value>
        /// The approve policy.
        /// </value>
        public PolicyEntry Distribution { get; set; }
    }
}
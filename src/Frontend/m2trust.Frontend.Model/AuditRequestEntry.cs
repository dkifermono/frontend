﻿using m2trust.Frontend.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class AuditRequestEntry
    {
        /// <summary>
        /// Gets or sets User Id
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets Profile Template Id
        /// </summary>
        public Guid ProfileTemplateId { get; set; }

        /// <summary>
        /// Gets or sets Comment
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets Priority
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Gets or sets Data Collection Items
        /// </summary>
        public IDictionary<string, string> DataCollectionItems { get; set; }

        /// <summary>
        /// Gets or sets Profile Id
        /// </summary>
        public Guid ProfileId { get; set; }

        /// <summary>
        /// Gets or sets Originator Id
        /// </summary>
        public Guid OriginatorId { get; set; }

        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public Guid Id { get; set; }

        public SuspendReinstateReasonType? Reason { get; set; }

        public int? EffectiveRevocationInHours { get; set; }
    }
}

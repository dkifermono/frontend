﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class ReinstatePolicyEntry : PolicyEntry
    {
        /// <summary>
        /// Gets or sets a value indicating whether [allow specify delay].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow specify delay]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowSpecifyDelay { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [allow specify reason].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow specify reason]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowSpecifyReason { get; set; }
    }
}

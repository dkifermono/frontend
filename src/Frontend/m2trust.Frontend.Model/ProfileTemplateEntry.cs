﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class ProfileTemplateEntry
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the enroll policy.
        /// </summary>
        /// <value>
        /// The enroll policy.
        /// </value>
        public PolicyEntry EnrollPolicy { get; set; }
        /// <summary>
        /// Gets or sets the offline unblock policy.
        /// </summary>
        /// <value>
        /// The offline unblock policy.
        /// </value>
        public PolicyEntry OfflineUnblockPolicy { get; set; }
        /// <summary>
        /// Gets or sets the recover policy.
        /// </summary>
        /// <value>
        /// The recover policy.
        /// </value>
        public PolicyEntry RecoverPolicy { get; set; }
        /// <summary>
        /// Gets or sets the temporary cards policy.
        /// </summary>
        /// <value>
        /// The temporary cards policy.
        /// </value>
        public TemporaryCardsPolicyEntry TemporaryCardsPolicy { get; set; }
        /// <summary>
        /// Gets or sets the revoke policy.
        /// </summary>
        /// <value>
        /// The revoke policy.
        /// </value>
        public RevokePolicyEntry RevokePolicy { get; set; }
        /// <summary>
        /// Gets or sets the retire policy.
        /// </summary>
        /// <value>
        /// The retire policy.
        /// </value>
        public RetirePolicyEntry RetirePolicy { get; set; }
        /// <summary>
        /// Gets or sets the renew policy.
        /// </summary>
        /// <value>
        /// The renew policy.
        /// </value>
        public PolicyEntry RenewPolicy { get; set; }
        /// <summary>
        /// Gets or sets the reinstate policy.
        /// </summary>
        /// <value>
        /// The reinstate policy.
        /// </value>
        public ReinstatePolicyEntry ReinstatePolicy { get; set; }
        /// <summary>
        /// Gets or sets the certificate templates.
        /// </summary>
        /// <value>
        /// The certificate templates.
        /// </value>
        public IEnumerable<CertificateTemplateEntry> CertificateTemplates { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [server generates archived keys].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [server generates archived keys]; otherwise, <c>false</c>.
        /// </value>
        public bool ServerGeneratesArchivedKeys { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [allow smartcard reuse].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow smartcard reuse]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowSmartcardReuse { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [initialize new smartcard].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [initialize new smartcard]; otherwise, <c>false</c>.
        /// </value>
        public bool InitializeNewSmartcard { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [diversify admin key].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [diversify admin key]; otherwise, <c>false</c>.
        /// </value>
        public bool DiversifyAdminKey { get; set; }
    }
}

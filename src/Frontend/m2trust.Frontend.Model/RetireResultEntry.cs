﻿namespace m2trust.Frontend.Model
{
    public class RetireResultEntry : AuditRequestEntry
    {
        #region Properties

        public string OneTimePassword { get; set; }

        #endregion Properties
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class Validation
    {
        public Guid RequestId { get; set; }

        public bool Approve { get; set; }

        public bool Deny { get; set; }

        public bool DistributeSecrets { get; set; }

        public bool Cancel { get; set; }

        public bool Abandon { get; set; }
    }
}
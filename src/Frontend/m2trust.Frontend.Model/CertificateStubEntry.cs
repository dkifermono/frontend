﻿using m2trust.Frontend.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class CertificateStubEntry
    {
        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>
        /// The expiration date.
        /// </value>
        public DateTime? ExpirationDate { get; set; }
        /// <summary>
        /// Gets or sets the name of the common.
        /// </summary>
        /// <value>
        /// The name of the common.
        /// </value>
        public string CommonName { get; set; }
        /// <summary>
        /// Gets or sets the certificate template.
        /// </summary>
        /// <value>
        /// The certificate template.
        /// </value>
        public string CertificateTemplate { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public CertificateState Status { get; set; }
    }
}

﻿using m2trust.Frontend.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class ProfileEntry
    {
        /// <summary>
        /// Gets or sets the enroll submitted.
        /// </summary>
        /// <value>
        /// The enroll submitted.
        /// </value>
        public DateTime EnrollSubmitted { get; set; }
        /// <summary>
        /// Gets or sets the enroll completed.
        /// </summary>
        /// <value>
        /// The enroll completed.
        /// </value>
        public DateTime EnrollCompleted { get; set; }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is smartcard.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is smartcard; otherwise, <c>false</c>.
        /// </value>
        public bool IsSmartcard { get; set; }
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public Guid UserId { get; set; }
        /// <summary>
        /// Gets or sets the profile template.
        /// </summary>
        /// <value>
        /// The profile template.
        /// </value>
        public ProfileTemplateEntry ProfileTemplate { get; set; }
        /// <summary>
        /// Gets or sets the request status.
        /// </summary>
        /// <value>
        /// The request status.
        /// </value>
        public RequestState RequestStatus { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public ProfileState Status { get; set; }
        /// <summary>
        /// Gets or sets the issued.
        /// </summary>
        /// <value>
        /// The issued.
        /// </value>
        public DateTime? Issued { get; set; }
    }
}

﻿namespace m2trust.Frontend.Model
{
    /// <summary>
    /// Colors model.
    /// </summary>
    public class Colors
    {
        #region Properties

        /// <summary>
        /// Gets or sets the color of the menu background.
        /// </summary>
        /// <value>The color of the menu background.</value>
        public string MenuBackgroundColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the menu font on hover.
        /// </summary>
        /// <value>The color of the menu font.</value>
        public string MenuHoverFontColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the inner menu background.
        /// </summary>
        /// <value>The color of the inner menu background.</value>
        public string InnerMenuBackgroundColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the menu font.
        /// </summary>
        /// <value>The color of the menu font.</value>
        public string MenuFontColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the page background.
        /// </summary>
        /// <value>The color of the page background.</value>
        public string PageBackgroundColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the page title background.
        /// </summary>
        /// <value>The color of the page title background.</value>
        public string PageTitleBackgroundColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the page title font.
        /// </summary>
        /// <value>The color of the page title font.</value>
        public string PageTitleFontColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the selected menu item background.
        /// </summary>
        /// <value>The color of the selected menu item background.</value>
        public string SelectedMenuItemBackgroundColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the selected menu item font.
        /// </summary>
        /// <value>The color of the selected menu item font.</value>
        public string SelectedMenuItemFontColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the selected table row background.
        /// </summary>
        /// <value>The color of the selected table row background.</value>
        public string SelectedTableRowBackgroundColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the selected table row font.
        /// </summary>
        /// <value>The color of the selected table row font.</value>
        public string SelectedTableRowFontColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the table row font.
        /// </summary>
        /// <value>The color of the table row font.</value>
        public string TableRowFontColor { get; set; }

        #endregion Properties
    }
}
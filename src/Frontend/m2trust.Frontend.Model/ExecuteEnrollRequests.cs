﻿using System.Collections.Generic;

namespace m2trust.Frontend.Model
{
    /// <summary>
    /// Execute enroll requests model.
    /// </summary>
    public class ExecuteEnrollRequests
    {
        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this instance has otp requests.
        /// </summary>
        /// <value><c>true</c> if this instance has otp requests; otherwise, <c>false</c>.</value>
        public bool HasOtpRequests { get; set; }

        /// <summary>
        /// Gets or sets the non otp requests.
        /// </summary>
        /// <value>The non otp requests.</value>
        public IEnumerable<RequestDetails> NonOtpProfiles { get; set; }

        #endregion Properties
    }
}
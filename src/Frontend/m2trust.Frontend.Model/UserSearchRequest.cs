﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class UserSearchRequest
    {
        /// <summary>
        /// Gets or sets search term
        /// </summary>
        public string SearchTerm { get; set; }

        /// <summary>
        /// Gets or sets domain
        /// </summary>
        public string Domain { get; set; }

        public string SearchField { get; set; }
        /// <summary>
        /// Gets or sets fields
        /// </summary>
        public ICollection<string> Fields { get; set; }
    }
}

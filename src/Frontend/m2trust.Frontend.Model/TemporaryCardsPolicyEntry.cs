﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class TemporaryCardsPolicyEntry: PolicyEntry
    {
        /// <summary>
        /// Gets or sets a value indicating whether [immediately suspend linked card].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [immediately suspend linked card]; otherwise, <c>false</c>.
        /// </value>
        public bool ImmediatelySuspendLinkedCard { get; set; }
    }
}

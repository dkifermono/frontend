﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class HttpContextApplicationUser
    {
        public string UserName { get; set; }

        public string Name { get; set; }

        public Guid UniqueIdentifier { get; set; }

        public bool IsAdmin { get; set; }

        public override bool Equals(object obj)
        {
            var objUser = obj as HttpContextApplicationUser;
            if (objUser != null)
            {
                return UniqueIdentifier.Equals(objUser.UniqueIdentifier);
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return string.Concat("HttpContextApplicationUser.", UniqueIdentifier).GetHashCode();
        }
    }
}

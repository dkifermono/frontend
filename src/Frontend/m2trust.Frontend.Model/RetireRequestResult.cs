﻿namespace m2trust.Frontend.Model
{
    public class RetireRequestResult : AuditRequestEntry
    {
        #region Properties

        /// <summary>
        /// Gets or sets One time password
        /// </summary>
        public string OneTimePassword { get; set; }

        #endregion Properties
    }
}
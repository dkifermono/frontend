﻿using m2trust.Frontend.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class DataCollectionItemEntry
    {
        /// <summary>
        /// Gets or sets name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets Required
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// Gets or sets DataType
        /// </summary>
        public DataTypeDci DataType { get; set; }

        /// <summary>
        /// Gets or sets ValidationType
        /// </summary>
        public ValidationTypeDci ValidationType { get; set; }

        /// <summary>
        /// Gets or sets ValidationData
        /// </summary>
        public string ValidationData { get; set; }
    }
}

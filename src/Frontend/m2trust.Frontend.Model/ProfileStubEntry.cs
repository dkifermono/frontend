﻿using m2trust.Frontend.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
    public class ProfileStubEntry
    {
        /// <summary>
        /// Gets or sets the soft profile identifier.
        /// </summary>
        /// <value>
        /// The soft profile identifier.
        /// </value>
        public Guid? SoftProfileId { get; set; }
        /// <summary>
        /// Gets or sets the smartcard identifier.
        /// </summary>
        /// <value>
        /// The smartcard identifier.
        /// </value>
        public Guid? SmartcardId { get; set; }
        /// <summary>
        /// Gets or sets the issuance date.
        /// </summary>
        /// <value>
        /// The issuance date.
        /// </value>
        public DateTime? IssuanceDate { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the profile status.
        /// </summary>
        /// <value>
        /// The profile status.
        /// </value>
        public ProfileState ProfileStatus { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is smart card.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is smart card; otherwise, <c>false</c>.
        /// </value>
        public bool IsSmartCard { get; set; }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the certificates.
        /// </summary>
        /// <value>
        /// The certificates.
        /// </value>
        public ICollection<CertificateStubEntry> Certificates { get; set; }
        /// <summary>
        /// Gets or sets the calculated profile status.
        /// </summary>
        /// <value>
        /// The calculated profile status.
        /// </value>
        public ProfileState CalculatedProfileStatus { get; set; }
    }
}

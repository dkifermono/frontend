﻿using m2trust.Frontend.Common.Enums;
using System;
using System.Collections.Generic;

namespace m2trust.Frontend.Model
{
    /// <summary>
    /// Signed Request model.
    /// </summary>
    public class SignedRequest : RequestDetails
    {
        public SignatureState SignatureStatus { get; set; }
    }
}
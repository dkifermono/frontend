﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model
{
     public class UserEntry
    {
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// gets or sets DirectoryPath
        /// </summary>
        public string DirectoryPath { get; set; }

        /// <summary>
        /// gets or sets Properties
        /// </summary>
        public IDictionary<string, object> Properties { get; set; }

        /// <summary>
        /// gets or sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// gets or sets ObjectClass
        /// </summary>
        public string ObjectClass { get; set; }

        /// <summary>
        /// gets or sets Sid
        /// </summary>
        public byte[] Sid { get; set; }
    }
}

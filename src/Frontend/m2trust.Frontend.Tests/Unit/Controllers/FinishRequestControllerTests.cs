﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Moq;
using NUnit.Framework;
using FluentAssertions;
using m2trust.Frontend.Service.Common;
using System.Threading.Tasks;
using static m2trust.Frontend.Controllers.FinishRequestController;
using m2trust.Frontend.Controllers;
using Microsoft.AspNetCore.Mvc;
using m2trust.Frontend.Model;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Common.Configuration;
using m2trust.Messaging.Configuration.API;
using m2trust.Frontend.Web.RestModels;
using m2trust.Messaging.OpenFim.Entities;

namespace m2trust.Frontend.Tests.Unit.Controllers
{
    [TestFixture]
    public class FinishRequestControllerTests
    {
        private FinishRequestControllerFixture fixture;

        public FinishRequestControllerTests()
        {
            this.fixture = new FinishRequestControllerFixture();
        }

        [Test]
        public async Task EnrollRequest_UserIdIsEmpty_Test()
        {
            var activityType = "Enroll";
            var request = new AuditRequestDto()
            {
                UserId = Guid.Empty,
                ProfileTemplateId = Guid.NewGuid()
            };

            var result = await fixture.target.EnrollRequest(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task EnrollRequest_ProfileTemplateIdIsEmpty_Test()
        {
            var activityType = "Enroll";
            var request = new AuditRequestDto()
            {
                UserId = Guid.NewGuid(),
                ProfileTemplateId = Guid.Empty
            };

            var result = await fixture.target.EnrollRequest(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task EnrollRequest_requestExist_Test()
        {
            var activityType = "Enroll";
            var request = new AuditRequestDto()
            {
                UserId = Guid.NewGuid(),
                ProfileTemplateId = Guid.NewGuid()
            };

            var auditRequest = new AuditRequestEntry()
            {
                UserId = request.UserId,
                ProfileTemplateId = request.ProfileTemplateId
            };

            var id = Guid.NewGuid();

            fixture.mapper.Setup(m => m.Map<AuditRequestEntry>(It.IsAny<AuditRequestDto>())).Returns(auditRequest);
            fixture.dataCollectionItemService.Setup(m => m.ValidateDataCollectionItems(null, null, new List<Common.ValidationError>())).Returns(true);
            fixture.applicationUserContext.Setup(m => m.GetCurrentUser()).ReturnsAsync(new HttpContextApplicationUser() { UniqueIdentifier = id });
            fixture.profileService.Setup(m => m.ValidateProfileTemplateRequestAsync(auditRequest)).ReturnsAsync(true);
            fixture.profileService.Setup(m => m.EnrollProfileTemplateAsync(auditRequest)).ReturnsAsync(auditRequest);


            var result = await fixture.target.EnrollRequest(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task ProfileRecover_ProfileIdIsEmpty_Test()
        {
            var activityType = "RecoverCertificate";
            var request = new AuditRequestDto()
            {
                ProfileId = Guid.Empty
            };

            var result = await fixture.target.ProfileRecover(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test] 
        public async Task ProfileRevoke_ProfileIdIsEmpty_Test()
        {
            var activityType = "RevokeCertificate";
            var request = new AuditRequestDto()
            {
                ProfileId = Guid.Empty
            };

            var result = await fixture.target.ProfileRevoke(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task ProfileRevoke_RequestIsNull_Test()
        {
            var activityType = "RevokeCertificate";
            AuditRequestDto request = null;

            var result = await fixture.target.ProfileRevoke(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task ProfileRevoke_RequestExists_Test()
        {
            var activityType = "RevokeCertificate";
            var request = new AuditRequestDto()
            {
                ProfileTemplateId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid()

            };
            var auditRequest = new AuditRequestEntry()
            {
                UserId = request.UserId,
                ProfileTemplateId = request.ProfileTemplateId
            };
            var profileEntry = new ProfileEntry() {
                Id = request.ProfileId.Value,
                ProfileTemplate = new ProfileTemplateEntry()
            };
            var revokeResult = new OtpResult()
            {
                OneTimePassword = "bla"
            };
            var revokeResultDto = new OtpResultDto()
            {
                OneTimePassword = revokeResult.OneTimePassword
            };

            var id = Guid.NewGuid();
            fixture.mapper.Setup(m => m.Map<AuditRequestEntry>(It.IsAny<AuditRequestDto>())).Returns(auditRequest);
            fixture.profileService.Setup(m => m.GetProfileAsync(request.ProfileId.Value)).ReturnsAsync(profileEntry);
            fixture.dataCollectionItemService.Setup(m => m.ValidateDataCollectionItems(null, null, new List<Common.ValidationError>())).Returns(true);
            fixture.applicationUserContext.Setup(m => m.GetCurrentUser()).ReturnsAsync(new HttpContextApplicationUser() { UniqueIdentifier = id });
            fixture.profileService.Setup(m => m.ValidateProfileTemplateRequestAsync(auditRequest)).ReturnsAsync(true);
            fixture.profileService.Setup(m => m.ProfileRevokeAsync(auditRequest)).ReturnsAsync(revokeResult);
            fixture.mapper.Setup(m => m.Map<OtpResultDto>(revokeResult)).Returns(revokeResultDto);
            var result = await fixture.target.ProfileRevoke(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task DisableSmartcard_requestIsNull_Test()
        {
            var activityType = "DisableSmartcard";
            AuditRequestDto request = null;

            var result = await fixture.target.DisableSmartcard(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task DisableSmartcard_userIdIsNull_Test()
        {
            var activityType = "DisableSmartcard";
            var request = new AuditRequestDto
            {
                ProfileTemplateId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid()
            };

            var result = await fixture.target.DisableSmartcard(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }
        
        [Test]
        public async Task DisableSmartcard_profileIdIsGuidEmpty_Test()
        {
            var activityType = "DisableSmartcard";
            var request = new AuditRequestDto
            {
                UserId = Guid.NewGuid(),
                ProfileTemplateId = Guid.NewGuid(),
                ProfileId = Guid.Empty
            };

            var result = await fixture.target.DisableSmartcard(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task DisableSmartcard_requestIsValid_Test()
        {
            var activityType = "DisableSmartcard";

            var request = new AuditRequestDto
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
                ProfileTemplateId = Guid.NewGuid()
            };

            var auditRequest = new AuditRequestEntry
            {
                UserId = request.UserId,
                ProfileId = request.ProfileId.Value,
                ProfileTemplateId = request.ProfileTemplateId
            };

            var profile = new SmartcardProfileEntry
            {
                ProfileTemplate = new ProfileTemplateEntry
                {
                    Id = auditRequest.ProfileTemplateId
                }
            };

            var otpResult = new OtpResult
            {
                RequestId = Guid.NewGuid(),
                OneTimePassword = "stringing"
            };

            var otpResultDto = new OtpResultDto()
            {
                RequestId = otpResult.RequestId,
                OneTimePassword = otpResult.OneTimePassword
            };

            var disableResult = new DisableResultDto()
            {
                OtpResult = otpResultDto,
                Continue = true
            };
            

            var id = Guid.NewGuid();
            fixture.mapper.Setup(m => m.Map<AuditRequestEntry>(request)).Returns(auditRequest);
            fixture.profileService.Setup(m => m.GetSmartcardProfileAsync(auditRequest.ProfileId)).ReturnsAsync(profile);
            fixture.dataCollectionItemService.Setup(m => m.ValidateDataCollectionItems(null, null, new List<Common.ValidationError>())).Returns(true);
            fixture.applicationUserContext.Setup(m => m.GetCurrentUser()).ReturnsAsync(new HttpContextApplicationUser() { UniqueIdentifier = id });
            fixture.profileService.Setup(m => m.ValidateProfileTemplateRequestAsync(auditRequest)).ReturnsAsync(true);
            fixture.smartcardService.Setup(m => m.DisableSmartcardAsync(auditRequest)).ReturnsAsync(otpResult);
            fixture.mapper.Setup(m => m.Map<OtpResultDto>(otpResult)).Returns(otpResultDto);
            
            var result = await fixture.target.DisableSmartcard(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task ProfileRenew_ProfileIdIsEmpty_Test()
        {
            var activityType = "RenewCertificate";

            var request = new AuditRequestDto()
            {
                ProfileId = Guid.Empty
            };

            var result = await fixture.target.RenewProfile(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task ProfileRenew_RequestIsNull_Test()
        {
            var activityType = "RenewCertificate";
            AuditRequestDto request = null;

            var result = await fixture.target.RenewProfile(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task ProfileRenew_RequestExists_Test()
        {
            var activityType = "RenewCertificate";
            var request = new AuditRequestDto()
            {
                ProfileTemplateId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid()
            };
            var requestEntry = new AuditRequestEntry()
            {
                UserId = request.UserId,
                ProfileId = request.ProfileId.Value
            };
            var profileEntry = new ProfileEntry()
            {
                ProfileTemplate = new ProfileTemplateEntry() { Id = request.ProfileTemplateId }
            };
            var otpResult = new OtpResult()
            {
                OneTimePassword = "otp",
                RequestId = Guid.NewGuid()
            };
            var otpResultEntry = new OtpResultDto()
            {
                OneTimePassword = otpResult.OneTimePassword,
                RequestId = otpResult.RequestId
            };

            var id = Guid.NewGuid();
            fixture.mapper.Setup(m => m.Map<AuditRequestEntry>(request)).Returns(requestEntry);
            fixture.profileService.Setup(m => m.GetProfileAsync(request.ProfileId.Value)).ReturnsAsync(profileEntry);
            fixture.dataCollectionItemService.Setup(m => m.ValidateDataCollectionItems(null, null, new List<Common.ValidationError>())).Returns(true);
            fixture.applicationUserContext.Setup(m => m.GetCurrentUser()).ReturnsAsync(new HttpContextApplicationUser() { UniqueIdentifier = id });
            fixture.profileService.Setup(m => m.ValidateProfileTemplateRequestAsync(requestEntry)).ReturnsAsync(true);
            fixture.profileService.Setup(m => m.ProfileRenewAsync(requestEntry)).ReturnsAsync(otpResult);
            fixture.mapper.Setup(m => m.Map<OtpResultDto>(otpResult)).Returns(otpResultEntry);

            var result = await fixture.target.RenewProfile(request, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        public class FinishRequestControllerFixture
        {
            public Mock<IProfileService> profileService;
            public Mock<IMapper> mapper;
            public Mock<ISigningService> signingService;
            public Mock<ISmartcardService> smartcardService;
            public Mock<IDataCollectionItemService> dataCollectionItemService;
            public Mock<ILogWriter> logger;
            public Mock<IApplicationUserContext> applicationUserContext;
            public Mock<ISignatureConfiguration> configuration;
            public Mock<IActivityConfiguration> activityConfiguration;
            public Mock<IServiceConfigurationStore> serviceConfigurationStore;
            public FinishRequestController target;

            public FinishRequestControllerFixture()
            {
                this.profileService = new Mock<IProfileService>();
                this.mapper = new Mock<IMapper>();
                this.dataCollectionItemService = new Mock<IDataCollectionItemService>();
                this.logger = new Mock<ILogWriter>();
                this.signingService = new Mock<ISigningService>();
                this.applicationUserContext = new Mock<IApplicationUserContext>();
                this.smartcardService = new Mock<ISmartcardService>();
                this.configuration = new Mock<ISignatureConfiguration>();
                this.activityConfiguration = new Mock<IActivityConfiguration>();
                this.serviceConfigurationStore = new Mock<IServiceConfigurationStore>();

                target = new FinishRequestController(mapper.Object, profileService.Object, signingService.Object, smartcardService.Object, dataCollectionItemService.Object, logger.Object, applicationUserContext.Object, configuration.Object, activityConfiguration.Object, serviceConfigurationStore.Object);
            }
        }
    }
}

﻿using AutoMapper;
using FluentAssertions;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Controllers;
using m2trust.Frontend.Web.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static m2trust.Frontend.Web.Controllers.ActivityController;

namespace m2trust.Frontend.Tests.Unit.Controllers
{
    [TestFixture]
    public class ActivityControllerTests
    {
        private readonly TestFixture testFixture = new TestFixture();

        [Test]
        public async Task GetActivities_ShouldReturnCollectionOfActivities()
        {
            var result = await this.testFixture.target.GetActivities();

            result.Should().BeOfType(typeof(OkObjectResult));
            var okObjectResult = result as OkObjectResult;
            var realResult = okObjectResult.Value as IEnumerable<ActivityDto>;
            realResult.Should().BeSameAs(this.testFixture.activityDtos);
        }

        private class TestFixture
        {
            public readonly Mock<IActivityService> activityServiceStub;
            public readonly Mock<IApplicationUserContext> appUserContextStub;
            public readonly IEnumerable<ActivityInfo> activityInfos;
            public readonly IEnumerable<ActivityDto> activityDtos;
            public readonly Mock<ILogWriter> logWriterStub;
            public readonly Mock<IMapper> mapperStub;
            public readonly ActivityController target;

            public TestFixture()
            {
                this.activityInfos = new[]
                {
                    new ActivityInfo(){ ActivityName = "EnrollActivity" },
                    new ActivityInfo(){ ActivityName = "ReinstateActivity" }
                };
                this.activityDtos = this.activityInfos.Select(ai => new ActivityDto() { ActivityName = ai.ActivityName });

                this.activityServiceStub = new Mock<IActivityService>();
                this.appUserContextStub = new Mock<IApplicationUserContext>();
                this.logWriterStub = new Mock<ILogWriter>();
                this.mapperStub = new Mock<IMapper>();
                this.activityServiceStub.Setup(m => m.GetActivitiesAsync(It.IsAny<HttpContextApplicationUser>())).ReturnsAsync(this.activityInfos);
                this.appUserContextStub.Setup(m => m.GetCurrentUser()).ReturnsAsync(new HttpContextApplicationUser() { Name = "Test", UniqueIdentifier = Guid.NewGuid(), UserName = "test" });
                this.mapperStub.Setup(m => m.Map<IEnumerable<ActivityDto>>(this.activityInfos)).Returns(this.activityDtos);
                this.target = new ActivityController(activityServiceStub.Object, appUserContextStub.Object, mapperStub.Object, logWriterStub.Object);
            }
        }
    }
}

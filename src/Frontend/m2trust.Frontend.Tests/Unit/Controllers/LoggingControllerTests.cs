using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Controllers;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Logging.API;
using m2trust.Messaging.Logging.Entities;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using PagedList.Core;
using static m2trust.Frontend.Controllers.LoggingController;

namespace m2trust.Frontend.Tests.Unit.Controllers
{

    [TestFixture]
    public class LoggingControllerTests
    {
        private LoggingControllerFixture fixture;

        public LoggingControllerTests()
        {
            this.fixture = new LoggingControllerFixture();
        }

        [Test]
        public async Task GetLogEntiresAsync_pagingParamsExists_Test()
        {
            var param = new LogFilterParams
            {
                PageSize = 10,
                PageNumber = 1
            };
            var activityType = "Logging";
            fixture.logService.Setup(m => m.GetEntriesAsync(It.IsAny<LoggingFilterParameters>(), It.IsAny<PagingParameters>())).ReturnsAsync(new StaticPagedList<Log>(new List<Log>(), param.PageNumber, param.PageSize, 0));
            fixture.mapper.Setup(m => m.Map<LogEntryDto>(fixture.logService.Object)).Returns(It.IsAny<LogEntryDto>());
            fixture.mapper.Setup(m => m.Map<PagingParameters>(It.IsAny<LogFilterParams>())).Returns(new PagingParameters(param.PageNumber, param.PageSize));
            var result = await fixture.target.GetLogEntries(param, activityType);
            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        public class LoggingControllerFixture
        {
            public Mock<ILoggingService> logService;
            public Mock<IMapper> mapper;
            public Mock<ILogWriter> logger;
            public LoggingController target;

            public LoggingControllerFixture()
            {
                this.logService = new Mock<ILoggingService>();
                this.mapper = new Mock<IMapper>();
                this.logger = new Mock<ILogWriter>();
                target = new LoggingController(logger.Object, logService.Object, mapper.Object);
            }
        }
    }   
}
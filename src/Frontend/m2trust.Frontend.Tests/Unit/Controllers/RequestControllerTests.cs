﻿using AutoMapper;
using FluentAssertions;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Controllers;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using static m2trust.Frontend.Controllers.RequestController;

namespace m2trust.Frontend.Tests.Unit.Controllers
{
    [TestFixture]
    public class RequestControllerTests
    {
        private RequestControllerFixture fixture;

        public RequestControllerTests()
        {
            this.fixture = new RequestControllerFixture();
        }

        [Test]
        public async Task GetRequestDetails_requestIdEmpty_Test()
        {
            var activityType = "ManageRequests";
            Guid requestId = Guid.Empty;
            
            var result = await fixture.target.GetRequest(requestId, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task GetRequestDetails_requestIdExist_Test()
        {
            var activityType = "ManageRequests";
            Guid requestId = Guid.NewGuid();

            fixture.requestService.Setup(m => m.GetRequestAsync(requestId)).ReturnsAsync(new RequestDetails());
            fixture.mapper.Setup(m => m.Map<RequestDetailsDto>(fixture.requestService.Object)).Returns(It.IsAny<RequestDetailsDto>());
            
            var result = await fixture.target.GetRequest(requestId, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task GetRequestDetails_ServiceReturnsNull_Test()
        {
            var activityType = "ManageRequests";
            Guid requestId = Guid.NewGuid();

            fixture.requestService.Setup(m => m.GetRequestAsync(requestId)).Returns(Task.FromResult<RequestDetails>(null));
            
            var result = await fixture.target.GetRequest(requestId, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(NotFoundResult));
        }

        [Test]
        public async Task ValidateActions_requestIdsEmpty_Test()
        {
            var activityType = "ManageRequests";
            Guid requestId = Guid.Empty;
            
            var result = await fixture.target.ValidateActions(requestId, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task ValidateActions_requestIdsExist_Test()
        {
            var activityType = "ManageRequests";
            Guid requestId = Guid.NewGuid();

            fixture.requestService.Setup(m => m.ValidateActionsAsync(requestId)).ReturnsAsync(new Validation());
            fixture.mapper.Setup(m => m.Map<ValidationDto>(fixture.requestService.Object)).Returns(It.IsAny<ValidationDto>());
            
            var result = await fixture.target.ValidateActions(requestId, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task Approve_requestIdsNull_Test()
        {
            var activityType = "ManageRequests";
            Guid[] requestIds = null;
            
            var result = await fixture.target.Approve(requestIds, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task Approve_requestIdsEmpty_Test()
        {
            var activityType = "ManageRequests";
            Guid[] requestIds = { Guid.Empty };
            
            var result = await fixture.target.Approve(requestIds, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task Approve_requestIdsExist_Test()
        {
            var activityType = "ManageRequests";
            Guid[] requestIds = { Guid.NewGuid() };

            var approveResult = new ApproveResult()
            {
                Result = new RequestActionResponse()
                {
                    HasSecrets = true,
                    IsSuccessful = true
                },
                Secrets = new List<Secret>()
                {

                }
            };

            var approveResultDto = new ApproveResultDto()
            {
                Result = new RequestActionResponseDto()
                {
                    HasSecrets = true,
                    IsSuccessful = true
                },
                Secrets = new List<SecretDto>()
                {

                }
            };

            fixture.requestService.Setup(m => m.ApproveAsync(requestIds)).ReturnsAsync(approveResult);
            fixture.mapper.Setup(m => m.Map<ApproveResultDto>(fixture.requestService.Object)).Returns(approveResultDto);
            
            var result = await fixture.target.Approve(requestIds, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task Deny_requestIdsExist_Test()
        {
            var activityType = "ManageRequests";
            var denyDto = new DenyDto()
            {
                RequestIds = new Guid[] { Guid.NewGuid() },
                Comment = ""
            };
            fixture.requestService.Setup(m => m.DenyAsync(denyDto.RequestIds, denyDto.Comment)).ReturnsAsync(new RequestActionResponse());
            fixture.mapper.Setup(m => m.Map<RequestActionResponseDto>(fixture.requestService.Object)).Returns(It.IsAny<RequestActionResponseDto>());
            
            var result = await fixture.target.Deny(denyDto, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task Abandon_requestIdsExist_Test()
        {
            var activityType = "ManageRequests";
            Guid[] requestIds = { Guid.NewGuid() };
            fixture.requestService.Setup(m => m.AbandonAsync(requestIds)).ReturnsAsync(new RequestActionResponse());
            fixture.mapper.Setup(m => m.Map<RequestActionResponseDto>(fixture.requestService.Object)).Returns(It.IsAny<RequestActionResponseDto>());

            var result = await fixture.target.Abandon(requestIds, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task DistributeSecrets_requestIdsExist_Test()
        {
            var activityType = "ManageRequests";
            Guid[] requestIds = { Guid.NewGuid() };

            fixture.requestService.Setup(m => m.DistributeSecretsAsync(requestIds)).Returns(Task.FromResult<IEnumerable<Secret>>(new List<Secret> { new Secret() }));
            fixture.mapper.Setup(m => m.Map<SecretDto>(fixture.requestService.Object)).Returns(It.IsAny<SecretDto>());
            
            var result = await fixture.target.DistributeSecrets(requestIds, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task Cancel_requestIdsExist_Test()
        {
            var activityType = "ManageRequests";
            Guid[] requestIds = { Guid.NewGuid() };
            fixture.requestService.Setup(m => m.CancelAsync(requestIds)).ReturnsAsync(new RequestActionResponse());
            fixture.mapper.Setup(m => m.Map<RequestActionResponseDto>(fixture.requestService.Object)).Returns(It.IsAny<RequestActionResponseDto>());
            

            var result = await fixture.target.Cancel(requestIds, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        public class RequestControllerFixture
        {
            public Mock<IRequestService> requestService;
            public Mock<IMapper> mapper;
            public Mock<ILogWriter> logger;
            public Mock<ISearchFormConfiguration> configuration;
            public RequestController target;

            public RequestControllerFixture()
            {
                this.requestService = new Mock<IRequestService>();
                this.mapper = new Mock<IMapper>();
                this.logger = new Mock<ILogWriter>();
                this.configuration = new Mock<ISearchFormConfiguration>();

                target = new RequestController(requestService.Object, mapper.Object, logger.Object, configuration.Object);
            }
        }
    }
}
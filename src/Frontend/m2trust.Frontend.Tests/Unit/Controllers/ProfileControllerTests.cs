﻿using AutoMapper;
using FluentAssertions;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Controllers;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Web.RestModels;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static m2trust.Frontend.Controllers.ProfileController;

namespace m2trust.Frontend.Tests.Unit.Controllers
{
    [TestFixture]
    public class ProfileControllerTests
    {
        private ProfileControllerFixture fixture;

        public ProfileControllerTests()
        {
            this.fixture = new ProfileControllerFixture();
        }

        [Test]
        public async Task GetProfileStubsAsync_userIdEmpty_Test()
        {
            var request = new ProfileStubRequestDto
            {
                UserId = Guid.Empty,
                SectionType = SectionType.Enroll
            };
           

            var controller = new ProfileController(fixture.ProfileService.Object, fixture.mapper.Object, fixture.applicationUserContext.Object, fixture.logger.Object, fixture.profileConfiguration.Object);

            var result = await controller.GetProfileStubs(request, "DisableSmartcard");
            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task GetProfileStubsAsync_userIdExist_Test()
        {
            var request = new ProfileStubRequestDto
            {
                UserId = Guid.NewGuid(),
                SectionType = SectionType.Enroll
            };
            var controller = new ProfileController(fixture.ProfileService.Object, fixture.mapper.Object, fixture.applicationUserContext.Object, fixture.logger.Object, fixture.profileConfiguration.Object);
            var result = await controller.GetProfileStubs(request, "Enroll");
            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
            var contentResult = result as OkObjectResult;
            contentResult.Should().NotBeNull();
            contentResult.Value.Should().BeOfType(typeof(ProfileStubDto));
        }

        [Test]
        public async Task GetProfileTemplatesAsync_userIdEmpty_Test()
        {
            var request = new ProfileRequestDto
            {
                UserId = Guid.Empty
            };          
            var controller = new ProfileController(fixture.ProfileService.Object, fixture.mapper.Object, fixture.applicationUserContext.Object, fixture.logger.Object, fixture.profileConfiguration.Object);
            var result = await controller.GetProfileTemplates(request, "DisableSmartcard");
            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task GetProfileTemplatesAsync_userIdExist_Test()
        {
            var request = new ProfileRequestDto
            {
                UserId = Guid.NewGuid()
            };

            fixture.ProfileService.Setup(m => m.GetProfileTemplatesAsync(request.UserId)).ReturnsAsync(new List<ProfileTemplateEntry>());
            fixture.mapper.Setup(m => m.Map<List<ProfileTemplateDto>>(fixture.ProfileService.Object)).Returns(It.IsAny<List<ProfileTemplateDto>>());

            var controller = new ProfileController(fixture.ProfileService.Object, fixture.mapper.Object, fixture.applicationUserContext.Object, fixture.logger.Object, fixture.profileConfiguration.Object);
            var result = await controller.GetProfileTemplates(request, "DisableSmartcard");
            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task GetProfileTemplatesAsync_userIdExistAndServiceReturnsNull_Test()
        {
            var request = new ProfileRequestDto
            {
                UserId = Guid.NewGuid()
            };

            fixture.ProfileService.Setup(m => m.GetProfileTemplatesAsync(request.UserId)).Returns(Task.FromResult<List<ProfileTemplateEntry>>(null));

            var controller = new ProfileController(fixture.ProfileService.Object, fixture.mapper.Object, fixture.applicationUserContext.Object, fixture.logger.Object, fixture.profileConfiguration.Object);
            var result = await controller.GetProfileTemplates(request, "DisableSmartcard");
            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }



        public class ProfileControllerFixture
        {
            public Mock<IProfileService> ProfileService;            
            public Mock<IMapper> mapper;
            public Mock<IApplicationUserContext> applicationUserContext;
            public Mock<ILogWriter> logger;
            public Mock<IProfileConfiguration> profileConfiguration;

            public ProfileControllerFixture()
            {
                this.ProfileService = new Mock<IProfileService>();
                this.mapper = new Mock<IMapper>();
                this.applicationUserContext = new Mock<IApplicationUserContext>();
                this.logger = new Mock<ILogWriter>();
                this.profileConfiguration = new Mock<IProfileConfiguration>();
            }
        }
    }
}
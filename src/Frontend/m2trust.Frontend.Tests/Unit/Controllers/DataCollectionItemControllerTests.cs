﻿using AutoMapper;
using FluentAssertions;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Controllers;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static m2trust.Frontend.Controllers.DataCollectionItemController;

namespace m2trust.Frontend.Tests.Unit.Controllers
{
    [TestFixture]
    public class DataCollectionItemControllerTests
    {
        private DataCollectionItemFixture fixture;

        public DataCollectionItemControllerTests()
        {
            this.fixture = new DataCollectionItemFixture();
        }

        [Test]
        public async Task GetEnrollDataCollectionItemsAsync_profileTemplateIdEmpty_Test()
        {
            var req = new ChooseTemplateDto
            {
                ProfileTemplateId = Guid.Empty,
                SectionType = Common.Enums.SectionType.Enroll
            };

            var result = await fixture.target.GetDataCollectionItems(req);
            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestObjectResult));
        }


        [Test]
        public async Task GetEnrollDataCollectionItemsAsync_profileTemplateIdExistAndServiceReturnsNull_Test()
        {
            var req = new ChooseTemplateDto
            {
                ProfileTemplateId = Guid.NewGuid(),
                SectionType = Common.Enums.SectionType.Enroll
            };
            Guid profileTemplateId = Guid.NewGuid();
            var actionType = Common.Enums.SectionType.Enroll;
            fixture.DataCollectionItemService.Setup(m => m.GetDataCollectionItemsAsync(actionType, profileTemplateId)).Returns(Task.FromResult<ICollection<DataCollectionItemEntry>>(null));
            var result = await fixture.target.GetDataCollectionItems(req);
            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task GetEnrollDataCollectionItemsAsync_ServiceReturnsNonSignutureItem_Test()
        {
            var req = new ChooseTemplateDto
            {
                ProfileTemplateId = Guid.NewGuid(),
                SectionType = Common.Enums.SectionType.Enroll
            };
            var returnResult = new DataCollectionItemEntry
            {
                DataType = Common.Enums.DataTypeDci.String,
                Description = "TestElement",
                Name = "NotSignature",
                Required = false
            };
            var mappedResult = new DataCollectionItemDto
            {
                DataType = returnResult.DataType,
                Description = returnResult.Description,
                Name = returnResult.Name,
                Required = returnResult.Required
            };

            fixture.DataCollectionItemService.Setup(m => m.GetDataCollectionItemsAsync(req.SectionType, req.ProfileTemplateId.GetValueOrDefault()))
                .Returns(Task.FromResult<ICollection<DataCollectionItemEntry>>(new List<DataCollectionItemEntry> { returnResult }));
            fixture.mapper.Setup(m => m.Map<ICollection<DataCollectionItemDto>>(It.IsAny<ICollection<DataCollectionItemEntry>>())).Returns(new List<DataCollectionItemDto> { mappedResult });
            var result = await fixture.target.GetDataCollectionItems(req);
            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
            var contentResult = result as OkObjectResult;
            contentResult.Should().NotBeNull();
            contentResult.Value.Should().BeOfType(typeof(DataCollectionDto));
            var dataCollection = contentResult.Value as DataCollectionDto;
            dataCollection.Should().NotBeNull();
            dataCollection.HasElegibleDcis.Should().BeTrue();
        }

        [Test]
        public async Task GetEnrollDataCollectionItemsAsync_ServiceReturnsSignutureItem_Test()
        {
            var req = new ChooseTemplateDto
            {
                ProfileTemplateId = Guid.NewGuid(),
                SectionType = Common.Enums.SectionType.Enroll
            };
            var returnResult = new DataCollectionItemEntry
            {
                DataType = Common.Enums.DataTypeDci.String,
                Description = "TestElement",
                Name = "Signature",
                Required = false
            };
            var mappedResult = new DataCollectionItemDto
            {
                DataType = returnResult.DataType,
                Description = returnResult.Description,
                Name = returnResult.Name,
                Required = returnResult.Required
            };

            fixture.DataCollectionItemService.Setup(m => m.GetDataCollectionItemsAsync(req.SectionType, req.ProfileTemplateId.GetValueOrDefault()))
                .Returns(Task.FromResult<ICollection<DataCollectionItemEntry>>(new List<DataCollectionItemEntry> { returnResult }));
            fixture.mapper.Setup(m => m.Map<ICollection<DataCollectionItemDto>>(It.IsAny<ICollection<DataCollectionItemEntry>>())).Returns(new List<DataCollectionItemDto> { mappedResult });
            fixture.configuration.Setup(c => c.SignatureDataCollectionItemName).ReturnsAsync("Signature");
            var result = await fixture.target.GetDataCollectionItems(req);
            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
            var contentResult = result as OkObjectResult;
            contentResult.Should().NotBeNull();
            contentResult.Value.Should().BeOfType(typeof(DataCollectionDto));
            var dataCollection = contentResult.Value as DataCollectionDto;
            dataCollection.Should().NotBeNull();
            dataCollection.HasElegibleDcis.Should().BeFalse();
        }

        public class DataCollectionItemFixture
        {
            public Mock<IDataCollectionItemService> DataCollectionItemService;
            public Mock<IProfileService> ProfileService;
            public Mock<IMapper> mapper;
            public Mock<ILogWriter> logger;
            public Mock<ISigningService> signingService;
            public Mock<ISignatureConfiguration> configuration;
            public DataCollectionItemController target;

            public DataCollectionItemFixture()
            {
                this.DataCollectionItemService = new Mock<IDataCollectionItemService>();
                this.ProfileService = new Mock<IProfileService>();
                this.mapper = new Mock<IMapper>();
                this.signingService = new Mock<ISigningService>();
                this.logger = new Mock<ILogWriter>();
                this.configuration = new Mock<ISignatureConfiguration>();

                target = new DataCollectionItemController(mapper.Object, logger.Object, DataCollectionItemService.Object, ProfileService.Object, signingService.Object, configuration.Object);
            }
        }
    }
}

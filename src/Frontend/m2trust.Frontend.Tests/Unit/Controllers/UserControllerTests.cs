﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using AutoMapper;
using FluentAssertions;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Controllers;
using Microsoft.AspNetCore.Mvc;
using m2trust.Frontend.Model;
using static m2trust.Frontend.Controllers.UserController;
using m2trust.Frontend.Common.Configuration;

namespace m2trust.Frontend.Tests.Unit.Controllers
{
    [TestFixture]
    public class UserControllerTests
    {
        private UserControllerFixture fixture;

        public UserControllerTests()
        {
            this.fixture = new UserControllerFixture();
        }

        [Test]
        public async Task GetUserAsync_userIdEmpty_Test()
        {
            var model = new SearchUserRequestDto
            {
                UserId = Guid.Empty
            };

            var result = await fixture.target.GetUser(model, "Enroll");

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task GetUserAsync_userIdExist_Test()
        {
            var model = new SearchUserRequestDto
            {
                UserId = Guid.NewGuid()
            };
            
            fixture.UserService.Setup(m => m.GetUserAsync(model.UserId)).ReturnsAsync(new UserEntry());
            fixture.mapper.Setup(m => m.Map<UserEntryDto>(fixture.UserService.Object)).Returns(It.IsAny<UserEntryDto>());

            var result = await fixture.target.GetUser(model, "Enroll");

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task GetUserAsync_userIdExistAndServiceReturnsNull_Test()
        {
            var model = new SearchUserRequestDto
            {
                UserId = Guid.NewGuid()
            };

            fixture.UserService.Setup(m => m.GetUserAsync(model.UserId)).Returns(Task.FromResult<UserEntry>(null));

            var result = await fixture.target.GetUser(model, "Enroll");

            result.Should().NotBeNull().And.BeOfType(typeof(NotFoundResult));
        }
       
        public class UserControllerFixture
        {
            public Mock<IUserService> UserService;
            public Mock<IMapper> mapper;
            public Mock<ILogWriter> logger;
            public Mock<ISearchFormConfiguration> configuration;
            public UserController target;

            public UserControllerFixture()
            {
                this.UserService = new Mock<IUserService>();
                this.mapper = new Mock<IMapper>();
                this.logger = new Mock<ILogWriter>();
                this.configuration = new Mock<ISearchFormConfiguration>();

                target = new UserController(UserService.Object, mapper.Object, logger.Object, configuration.Object);
            }
        }
    }
}

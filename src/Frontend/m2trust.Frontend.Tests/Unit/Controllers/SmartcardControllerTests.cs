﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using AutoMapper;
using NUnit.Framework;
using FluentAssertions;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Service.Common;
using System.Threading.Tasks;
using static m2trust.Frontend.Web.Controllers.SmartcardController;
using m2trust.Frontend.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using m2trust.Frontend.Model;
using m2trust.Frontend.Common.Configuration;
using m2trust.Messaging.OpenFim.Entities;
using m2trust.Frontend.Web.RestModels;

namespace m2trust.Frontend.Tests.Unit.Controllers
{
    [TestFixture]
    public class SmartcardControllerTests
    {
        private SmartCardControllerFixture fixture;

        public SmartcardControllerTests()
        {
            this.fixture = new SmartCardControllerFixture();
        }

        [Test]
        public async Task Reinstate_modelIsNull_Test()
        {
            var activityType = "ReinstateSmartcard";
            AuditRequestDto model = null;

            var result = await fixture.target.Reinstate(model, activityType);
            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task Reinstate_userIdIsEmpty_Test()
        {
            var activityType = "ReinstateSmartcard";
            var model = new AuditRequestDto
            {
                UserId = Guid.Empty,
                ProfileId = Guid.NewGuid()
            };

            var result = await fixture.target.Reinstate(model, activityType);
            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task Reinstate_profileIdIsEmpty_Test()
        {
            var activityType = "ReinstateSmartcard";
            var model = new AuditRequestDto
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.Empty
            };

            var result = await fixture.target.Reinstate(model, activityType);
            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task Reinstate_modelExists_Test()
        {
            var activityType = "ReinstateSmartcard";
            var model = new AuditRequestDto
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid()
            };

            var requestEntry = new AuditRequestEntry
            {
                UserId = model.UserId,
                ProfileId = model.ProfileId.Value
            };

            var auditRequest = new AuditRequestEntry
            {
                UserId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                ProfileId = Guid.NewGuid()
            };

            var auditRequestDto = new AuditRequestDto
            {
                UserId = auditRequest.UserId,
                ProfileId = auditRequest.ProfileId
            };
            var profileEntry = new SmartcardProfileEntry()
            {
                ProfileTemplate = new ProfileTemplateEntry(),
                Id = Guid.NewGuid()
            };
            var otpResult = new OtpResult()
            {
                OneTimePassword = "one-time-password",
                RequestId = Guid.NewGuid()
            };
            var otpResultDto = new OtpResultDto()
            {
                OneTimePassword = "one-time-password",
                RequestId = Guid.NewGuid()
            };
            var id = Guid.NewGuid();
            fixture.configuration.Setup(c => c.SignatureDataCollectionItemName).ReturnsAsync("Signature");
            fixture.mapper.Setup(m => m.Map<AuditRequestEntry>(model)).Returns(requestEntry);
            fixture.smartcardService.Setup(m => m.ReinstateSmartcardAsync(requestEntry)).ReturnsAsync(otpResult);
            fixture.profileService.Setup(m => m.GetSmartcardProfileAsync(model.ProfileId.Value)).ReturnsAsync(profileEntry);
            fixture.dataCollectionItemService.Setup(m => m.ValidateDataCollectionItems(null, null, new List<Common.ValidationError>())).Returns(true);
            fixture.applicationUserContext.Setup(m => m.GetCurrentUser()).ReturnsAsync(new HttpContextApplicationUser() { UniqueIdentifier = id });
            fixture.profileService.Setup(m => m.ValidateProfileTemplateRequestAsync(requestEntry)).ReturnsAsync(true);
            fixture.mapper.Setup(m => m.Map<OtpResultDto>(otpResult)).Returns(otpResultDto);

            var result = await fixture.target.Reinstate(model, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));

        }

        [Test]
        public async Task Retire_modelIsNull_Test()
        {
            var activityType = "RewriteSmartcard";
            AuditRequestDto model = null;

            var result = await fixture.target.Retire(model, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task Retire_modelExists_Test()
        {
            var activityType = "RewriteSmartcard";
            var model = new AuditRequestDto
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid()
            };

            var auditRequest = new AuditRequestEntry
            {
                UserId = model.UserId,
                ProfileId = model.ProfileId.Value
            };
            
            

            var profileEntry = new SmartcardProfileEntry()
            {
                ProfileTemplate = new ProfileTemplateEntry(),
                Id = Guid.NewGuid()
            };

            var otpResult = new OtpResult()
            {
                OneTimePassword = "one-time-password",
                RequestId = Guid.NewGuid()
            };
            var otpResultDto = new OtpResultDto()
            {
                OneTimePassword = otpResult.OneTimePassword,
                RequestId = otpResult.RequestId
            };
            var retireResultDto = new RetireResultDto()
            {
                OtpResult = otpResultDto,
                Continue = true
            };
            var id = Guid.NewGuid();
            fixture.mapper.Setup(m => m.Map<AuditRequestEntry>(model)).Returns(auditRequest);
            fixture.profileService.Setup(m => m.GetSmartcardProfileAsync(model.ProfileId.Value)).ReturnsAsync(profileEntry);
            fixture.smartcardService.Setup(m => m.RetireSmartcardAsync(auditRequest)).ReturnsAsync(otpResult);
            fixture.dataCollectionItemService.Setup(m => m.ValidateDataCollectionItems(null, null, new List<Common.ValidationError>())).Returns(true);
            fixture.applicationUserContext.Setup(m => m.GetCurrentUser()).ReturnsAsync(new HttpContextApplicationUser() { UniqueIdentifier = id });
            fixture.profileService.Setup(m => m.ValidateProfileTemplateRequestAsync(auditRequest)).ReturnsAsync(true);
            fixture.mapper.Setup(m => m.Map<OtpResultDto>(otpResult)).Returns(otpResultDto);

            var result = await fixture.target.Retire(model, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task Suspend_modelIsNull_Test()
        {
            var activityType = "SuspendSmartcard";
            AuditRequestDto model = null;

            var result = await fixture.target.Suspend(model, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(BadRequestResult));
        }

        [Test]
        public async Task Suspend_modelExists_Test()
        {
            var activityType = "SuspendSmartcard";
            var model = new AuditRequestDto
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
                EffectiveRevocationInHours = 3
            };

            var requestEntry = new AuditRequestEntry
            {
                UserId = model.UserId,
                ProfileId = model.ProfileId.Value,
                EffectiveRevocationInHours = model.EffectiveRevocationInHours
            };

            var response = new AuditRequestEntry
            {
                Comment = "audit",
                ProfileId = Guid.NewGuid()
            };

            var auditRequest = new AuditRequestDto
            {
                ProfileId = response.ProfileId,
                Comment = response.Comment
            };

            var profileEntry = new SmartcardProfileEntry()
            {
                ProfileTemplate = new ProfileTemplateEntry(),
                Id = Guid.NewGuid()
            };

            var otpResult = new OtpResult()
            {
                OneTimePassword = "one-time-password",
                RequestId = Guid.NewGuid()
            };
            var otpResultDto = new OtpResultDto()
            {
                OneTimePassword = otpResult.OneTimePassword,
                RequestId = otpResult.RequestId
            };
            var id = Guid.NewGuid();

            fixture.mapper.Setup(m => m.Map<AuditRequestEntry>(model)).Returns(requestEntry);
            fixture.profileService.Setup(m => m.GetSmartcardProfileAsync(model.ProfileId.Value)).ReturnsAsync(profileEntry);
            fixture.smartcardService.Setup(m => m.SuspendSmartcardAsync(requestEntry)).ReturnsAsync(otpResult);
            fixture.dataCollectionItemService.Setup(m => m.ValidateDataCollectionItems(null, null, new List<Common.ValidationError>())).Returns(true);
            fixture.applicationUserContext.Setup(m => m.GetCurrentUser()).ReturnsAsync(new HttpContextApplicationUser() { UniqueIdentifier = id });
            fixture.profileService.Setup(m => m.ValidateProfileTemplateRequestAsync(requestEntry)).ReturnsAsync(true);
            fixture.mapper.Setup(m => m.Map<OtpResultDto>(otpResult)).Returns(otpResultDto);
            
            var result = await fixture.target.Suspend(model, activityType);

            result.Should().NotBeNull().And.BeOfType(typeof(OkObjectResult));
        }
    }

    public class SmartCardControllerFixture
    {
        public Mock<IApplicationUserContext> applicationUserContext;
        public Mock<IProfileService> profileService;
        public Mock<ISigningService> signingService;
        public Mock<ISmartcardService> smartcardService;
        public Mock<IMapper> mapper;
        public Mock<ILogWriter> logger;
        public Mock<IDataCollectionItemService> dataCollectionItemService;
        public Mock<ISignatureConfiguration> configuration;
        public Mock<IActivityConfiguration> activityConfiguration;
        public SmartcardController target;

        public SmartCardControllerFixture()
        {
            this.applicationUserContext = new Mock<IApplicationUserContext>();
            this.profileService = new Mock<IProfileService>();
            this.signingService = new Mock<ISigningService>();
            this.smartcardService = new Mock<ISmartcardService>();
            this.mapper = new Mock<IMapper>();
            this.logger = new Mock<ILogWriter>();
            this.configuration = new Mock<ISignatureConfiguration>();
            this.dataCollectionItemService = new Mock<IDataCollectionItemService>();
            this.activityConfiguration = new Mock<IActivityConfiguration>();
            target = new SmartcardController(mapper.Object, signingService.Object, smartcardService.Object, profileService.Object, applicationUserContext.Object, logger.Object, configuration.Object, dataCollectionItemService.Object, activityConfiguration.Object);
        }
    }
}

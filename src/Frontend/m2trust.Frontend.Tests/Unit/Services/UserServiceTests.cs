﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Moq;
using NUnit.Framework;
using FluentAssertions;
using m2trust.Messaging.ActiveDirectory.API;
using System.Threading.Tasks;
using m2trust.Messaging.ActiveDirectory.Entities;
using m2trust.Frontend.Service;
using m2trust.Frontend.Model;
using m2trust.Frontend.Common.Configuration;

namespace m2trust.Frontend.Tests.Unit.Services
{
    [TestFixture]
    public class UserServiceTests
    {        
        private UserServiceFixture fixture;

        public UserServiceTests()
        {
            this.fixture = new UserServiceFixture();
        }

        [Test]
        public async Task GetUserAsync_userIsNull_Test()
        {
            var userId = Guid.NewGuid();
            var defaultPropertiesToLoad = new[] { "objectguid", "objectSid", "displayname", "cn" };
            
            fixture.activeDirectoryApi.Setup(m => m.LookupUserAsync(userId.ToString(), defaultPropertiesToLoad)).Returns(Task.FromResult<User>(null));

            var result = await fixture.target.GetUserAsync(userId);

            result.Should().BeNull();
        }

        [Test]
        public async Task GetUserAsync_userExist_Test()
        {
            var userId = Guid.Parse("951aa504-7984-42b6-b7a8-74a815ad31eb");
            var defaultPropertiesToLoad = new[] { "objectguid", "objectSid", "displayname", "cn" };            
            var ldap = "LDAP://<GUID={0}>";

            var userID = String.Format(ldap, userId);

            var user = new User()
            {
                Id = userId,
                Name = "mir"
            };

            var userEntry = new UserEntry()
            {
                Id = user.Id,
                Name = user.Name
            };

            fixture.activeDirectoryApi.Setup(m => m.LookupUserAsync(userID, defaultPropertiesToLoad)).ReturnsAsync(user);
            fixture.mapper.Setup(m => m.Map<UserEntry>(It.IsAny<User>())).Returns(userEntry);
            var result = await fixture.target.GetUserAsync(userId);

            result.Should().NotBeNull().And.BeOfType(typeof(UserEntry));
        }

        

        public class UserServiceFixture
        {
            public Mock<IMapper> mapper;
            public Mock<IActiveDirectoryApi> activeDirectoryApi;
            public UserService target;

            public UserServiceFixture()
            {
                this.mapper = new Mock<IMapper>();
                this.activeDirectoryApi = new Mock<IActiveDirectoryApi>();

                target = new UserService(activeDirectoryApi.Object, mapper.Object);
            }
        }
    }
}

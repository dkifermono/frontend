﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Moq;
using NUnit.Framework;
using FluentAssertions;
using m2trust.Messaging.OpenFim.API;
using System.Threading.Tasks;
using m2trust.Frontend.Service;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.OpenFim.Entities;
using m2trust.Frontend.Model;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Web.Infrastructure;
using m2trust.Frontend.Service.Filters;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Service.Common.Filters;

namespace m2trust.Frontend.Tests.Unit.Services
{
    [TestFixture]
    public class ProfileServiceTests
    {
        private ProfileServiceFixture fixture;

        public ProfileServiceTests()
        {
            this.fixture = new ProfileServiceFixture();
        }
        

        [Test]
        public async Task GetSmartcardProfileAsync_responseExists_Test()
        {
            var profileId = Guid.NewGuid();

            var smartcardProfile = new SmartcardProfile
            {
                Id = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                Status = Messaging.OpenFim.Enumerations.ProfileStatus.Active,
                SerialNumber = "serial",
                ProfileTemplate = new ProfileTemplate()
                {
                    Id = Guid.NewGuid()
                }
            };

            var smartcardProfileEntry = new SmartcardProfileEntry
            {
                Id = smartcardProfile.Id,
                UserId = smartcardProfile.UserId,
                Status = Common.Enums.ProfileState.Active,
                SerialNumber = smartcardProfile.SerialNumber,
                ProfileTemplate = new ProfileTemplateEntry()
                {
                    Id = Guid.NewGuid()
                }
            };

            fixture.mimAccessApi.Setup(m => m.FindSmartcardProfileByIdAsync(profileId)).ReturnsAsync(smartcardProfile);
            fixture.mapper.Setup(m => m.Map<SmartcardProfileEntry>(smartcardProfile)).Returns(smartcardProfileEntry);
            var result = await fixture.target.GetSmartcardProfileAsync(profileId);

            result.Should().NotBeNull().And.BeOfType(typeof(SmartcardProfileEntry));
        }

        [Test]
        public async Task GetSmartcardProfileAsync_responseIsNull_Test()
        {
            var profileId = Guid.NewGuid();
            SmartcardProfile smartcardProfile = null;

            fixture.mimAccessApi.Setup(m => m.FindSmartcardProfileByIdAsync(profileId)).ReturnsAsync(smartcardProfile);
            var result = await fixture.target.GetSmartcardProfileAsync(profileId);

            result.Should().BeNull();
        }

        [Test]
        public async Task GetSoftProfileAsync_responseExists_Test()
        {
            var profileId = Guid.NewGuid();

            var profile = new Messaging.OpenFim.Entities.Profile
            {
                Id = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                EnrollSubmitted = DateTime.UtcNow,
                EnrollCompleted = DateTime.UtcNow,
                Status = Messaging.OpenFim.Enumerations.ProfileStatus.Active
            };

            var profileEntry = new ProfileEntry
            {
                Id = profile.Id,
                UserId = profile.UserId,
                EnrollSubmitted = profile.EnrollSubmitted,
                EnrollCompleted = profile.EnrollCompleted,
                Status = ProfileState.Active
            };

            fixture.mimAccessApi.Setup(m => m.FindSoftProfileByIdAsync(profileId)).ReturnsAsync(profile);
            fixture.mapper.Setup(m => m.Map<ProfileEntry>(profile)).Returns(profileEntry);
            var result = await fixture.target.GetSoftProfileAsync(profileId);

            result.Should().NotBeNull().And.BeOfType(typeof(ProfileEntry));
        }

        [Test]
        public async Task GetSoftProfileAsync_responseIsNull_Test()
        {
            var profileId = Guid.NewGuid();
            Messaging.OpenFim.Entities.Profile profileNull = null;

            fixture.mimAccessApi.Setup(m => m.FindSoftProfileByIdAsync(profileId)).ReturnsAsync(profileNull);
            var result = await fixture.target.GetSoftProfileAsync(profileId);

            result.Should().BeNull();
        }

        [Test]
        public async Task EnrollProfileTemplateAsync_requestProfileIsNull_Test()
        {
            AuditRequestEntry requestProfile = null;
            
            var result = await fixture.target.EnrollProfileTemplateAsync(requestProfile);

            result.Should().BeNull();
        }

        [Test]
        public async Task EnrollProfileTemplateAsync_responseIsNull_Test()
        {
            var requestProfile = new AuditRequestEntry
            {
                Id = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
            };

            var profile = new ProfileTemplateRequest
            {
                Id = requestProfile.Id,
                UserId = requestProfile.UserId,
                ProfileId = requestProfile.ProfileId
            };

            ProfileTemplateRequest emptyRequest = null;

            fixture.mapper.Setup(m => m.Map<ProfileTemplateRequest>(requestProfile)).Returns(profile);
            fixture.mimAccessApi.Setup(m => m.ProfileTemplateEnrollAsync(profile)).ReturnsAsync(emptyRequest);

            var result = await fixture.target.EnrollProfileTemplateAsync(requestProfile);

            result.Should().BeNull();
        }

        [Test]
        public async Task EnrollProfileTemplateAsync_responseExists_Test()
        {
            var requestProfile = new AuditRequestEntry
            {
                Id = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
            };

            var profile = new ProfileTemplateRequest
            {
                Id = requestProfile.Id,
                UserId = requestProfile.UserId,
                ProfileId = requestProfile.ProfileId
            };

            fixture.mapper.Setup(m => m.Map<ProfileTemplateRequest>(requestProfile)).Returns(profile);
            fixture.mimAccessApi.Setup(m => m.ProfileTemplateEnrollAsync(profile)).ReturnsAsync(profile);
            fixture.mapper.Setup(m => m.Map<AuditRequestEntry>(profile)).Returns(requestProfile);

            var result = await fixture.target.EnrollProfileTemplateAsync(requestProfile);
            result.Should().NotBeNull().And.BeOfType(typeof(AuditRequestEntry));
        }
        

        [Test]
        public async Task ProfileRenew_responseIsNull()
        {
            var request = new AuditRequestEntry
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid()
            };

            RenewProfileResult emptyResult = null;

            fixture.mimAccessApi.Setup(m => m.RenewProfileAsync(It.IsAny<Guid>(), It.IsAny<Dictionary<string, string>>(), It.IsAny<string>(), It.IsAny<Guid>())).ReturnsAsync(emptyResult);
            var result = await fixture.target.ProfileRenewAsync(request);
            result.Should().BeNull();
        }

        [Test]
        public async Task ProfileRenew_responseExists()
        {
            var request = new AuditRequestEntry
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid()
            };

            var renewResult = new RenewProfileResult
            {
                RequestId = Guid.NewGuid(),
                OneTimePassword = "otp"
            };

            var expected = new OtpResult
            {
                RequestId = (Guid)renewResult.RequestId,
                OneTimePassword = renewResult.OneTimePassword
            };

            fixture.mimAccessApi.Setup(m => m.RenewProfileAsync(It.IsAny<Guid>(), It.IsAny<Dictionary<string, string>>(), It.IsAny<string>(), It.IsAny<Guid>())).ReturnsAsync(renewResult);
            fixture.mapper.Setup(m => m.Map<OtpResult>(renewResult)).Returns(expected);
            var result = await fixture.target.ProfileRenewAsync(request);
            result.Should().NotBeNull().And.BeOfType(typeof(OtpResult));
        }

        public class ProfileServiceFixture
        {
            public Mock<IMimAccessApi> mimAccessApi;
            public Mock<IMapper> mapper;
            public Mock<ISigningService> signingService;
            public Mock<IApplicationUserContext> applicationUserContext;
            public Mock<ISignatureConfiguration> configuration;
            public Mock<IFilterFactory> filterFactory;
            public ProfileService target;
            
            public ProfileServiceFixture()
            {
                this.mimAccessApi = new Mock<IMimAccessApi>();
                this.mapper = new Mock<IMapper>();
                this.signingService = new Mock<ISigningService>();
                this.applicationUserContext = new Mock<IApplicationUserContext>();
                this.configuration = new Mock<ISignatureConfiguration>();
                this.filterFactory = new Mock<IFilterFactory>();

                target = new ProfileService(mimAccessApi.Object, mapper.Object, signingService.Object, configuration.Object, filterFactory.Object);
            }
        }
    }
}

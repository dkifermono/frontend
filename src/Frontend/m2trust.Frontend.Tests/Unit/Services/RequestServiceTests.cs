﻿using AutoMapper;
using FluentAssertions;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Common.Parameters.Sorting;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.OpenFim.API;
using m2trust.Messaging.OpenFim.Entities;
using Moq;
using NUnit.Framework;
using PagedList.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Tests.Unit.Services
{
    [TestFixture]
    public class RequestServiceTests
    {
        private readonly RequestServiceFixture fixture;

        public RequestServiceTests()
        {
            this.fixture = new RequestServiceFixture();
        }

        [Test]
        public async Task GetRequestDetails_requestIsNull_Test()
        {
            var requestId = Guid.NewGuid();

            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(requestId)).Returns(Task.FromResult<Request>(null));
            var result = await fixture.target.GetRequestAsync(requestId);

            result.Should().BeNull();
        }

        [Test]
        public async Task Abandon_ValidationIsSuccessful_Test()
        {
            var id = Guid.NewGuid();

            var requestIds = new List<Guid>()
            {
                { id }
            };
            var request= new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = request.Id
            };
            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateAbandonAsync(requestDetails)).ReturnsAsync(true);
            
            var result = await fixture.target.AbandonAsync(requestIds);
            result.Should().BeOfType(typeof(RequestActionResponse));
            result.IsSuccessful.Should().BeTrue();
        }

        [Test]
        public async Task Abandon_ValidationIsNotSuccessful_Test()
        {
            var id = Guid.NewGuid();

            var requestIds = new List<Guid>()
            {
                { id }
            };
            var request = new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = request.Id
            };
            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateAbandonAsync(requestDetails)).ReturnsAsync(false);

            var result = await fixture.target.AbandonAsync(requestIds);
            result.Should().BeOfType(typeof(RequestActionResponse));
            result.IsSuccessful.Should().BeFalse();
        }

        [Test]
        public async Task Approve_ValidationIsSuccessful_Test()
        {
            var id = Guid.NewGuid();

            var requestIds = new List<Guid>()
            {
                { id }
            };
            var request = new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = request.Id
            };
            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateApproveAsync(requestDetails)).ReturnsAsync(true);

            var result = await fixture.target.ApproveAsync(requestIds);
            result.Should().BeOfType(typeof(ApproveResult));
            result.Result.IsSuccessful.Should().BeTrue();
        }

        [Test]
        public async Task Approve_ValidationIsNotSuccessful_Test()
        {
            var id = Guid.NewGuid();
            var requestIds = new List<Guid>()
            {
                { id }
            };
            var request = new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = request.Id
            };
            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateApproveAsync(requestDetails)).ReturnsAsync(false);

            var result = await fixture.target.ApproveAsync(requestIds);
            result.Should().BeOfType(typeof(ApproveResult));
            result.Result.IsSuccessful.Should().BeFalse();
        }

        [Test]
        public async Task Approve_SecretsIsNull_Test()
        {
            var id = Guid.NewGuid();
            var requestIds = new List<Guid>()
            {
                { id }
            };
            var request = new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = request.Id
            };
            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateApproveAsync(requestDetails)).ReturnsAsync(true);
            fixture.mimAccessApi.Setup(m => m.ApproveRequestAsync(id)).ReturnsAsync((RequestSecret)null);

            var result = await fixture.target.ApproveAsync(requestIds);
            result.Should().BeOfType(typeof(ApproveResult));
            result.Result.IsSuccessful.Should().BeTrue();
            result.Result.HasSecrets.Should().BeFalse();
        }

        [Test]
        public async Task Approve_SecretExist_Test()
        {
            var id = Guid.NewGuid();
            var requestIds = new List<Guid>()
            {
                { id }
            };
            var request = new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = request.Id
            };
            var secret = new RequestSecret()
            {
                Request = request,
                Distribution = new Policy()
                {
                    EmailSecrets = true
                }
            };
            var mapSecret = new Secret()
            {
                RequestDetails = requestDetails,
                Distribution = new PolicyEntry()
                {
                    EmailSecrets = true
                }
            };
            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateApproveAsync(requestDetails)).ReturnsAsync(true);
            fixture.mimAccessApi.Setup(m => m.ApproveRequestAsync(id)).ReturnsAsync(secret);
            fixture.mapper.Setup(m => m.Map<List<Secret>>(new RequestSecret[] { secret })).Returns(new List<Secret> { mapSecret });

            var result = await fixture.target.ApproveAsync(requestIds);
            result.Should().BeOfType(typeof(ApproveResult));
            result.Result.IsSuccessful.Should().BeTrue();
            result.Result.HasSecrets.Should().BeTrue();
        }


        [Test]
        public async Task Cancel_ValidationIsSuccessful_Test()
        {
            var id = Guid.NewGuid();
            var requestIds = new List<Guid>()
            {
                { id }
            };
            var request = new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = request.Id
            };
            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateCancelAsync(requestDetails)).ReturnsAsync(true);

            var result = await fixture.target.CancelAsync(requestIds);
            result.Should().BeOfType(typeof(RequestActionResponse));
            result.IsSuccessful.Should().BeTrue();
        }

        [Test]
        public async Task Cancel_ValidationIsNotSuccessful_Test()
        {
            var id = Guid.NewGuid();
            var requestIds = new List<Guid>()
            {
                { id }
            };
            var request = new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = request.Id
            };
            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateCancelAsync(requestDetails)).ReturnsAsync(false);

            var result = await fixture.target.CancelAsync(requestIds);
            result.Should().BeOfType(typeof(RequestActionResponse));
            result.IsSuccessful.Should().BeFalse();
        }

        [Test]
        public async Task Deny_ValidationIsSuccessful_Test()
        {
            var id = Guid.NewGuid();
            var requestIds = new List<Guid>()
            {
                { id }
            };
            var request = new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = request.Id
            };
            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateDenyAsync(requestDetails)).ReturnsAsync(true);

            var result = await fixture.target.DenyAsync(requestIds, null);
            result.Should().BeOfType(typeof(RequestActionResponse));
            result.IsSuccessful.Should().BeTrue();
        }

        [Test]
        public async Task Deny_ValidationIsNotSuccessful_Test()
        {
            var id = Guid.NewGuid();
            var requestIds = new List<Guid>()
            {
                { id }
            };
            var request = new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = request.Id
            };
            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateDenyAsync(requestDetails)).ReturnsAsync(false);

            var result = await fixture.target.DenyAsync(requestIds, null);
            result.Should().BeOfType(typeof(RequestActionResponse));
            result.IsSuccessful.Should().BeFalse();
        }

        [Test]
        public async Task DistributeSecrets_SecretsExist_Test()
        {
            var id = Guid.NewGuid();
            var requestIds = new List<Guid>()
            {
                { id }
            };
            var request = new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = request.Id
            };
            var secret = new RequestSecret()
            {
                Request = request,
                Distribution = new Policy()
                {
                    EmailSecrets = true
                }
            };
            var mapSecret = new Secret()
            {
                RequestDetails = requestDetails,
                Distribution = new PolicyEntry()
                {
                    EmailSecrets = true
                }
            };
            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateDistributeSecretsAsync(requestDetails)).ReturnsAsync(true);
            fixture.mimAccessApi.Setup(a => a.DistributeSecretsRequestAsync(id)).ReturnsAsync(secret);
            fixture.mapper.Setup(m => m.Map<List<Secret>>(new RequestSecret[] { secret })).Returns(new List<Secret> { mapSecret });
            var result = await fixture.target.DistributeSecretsAsync(requestIds);
            result.Should().BeOfType(typeof(List<Secret>));
        }

        [Test]
        public async Task DistributeSecrets_SecretsIsNull_Test()
        {
            var id = Guid.NewGuid();
            var requestIds = new List<Guid>()
            {
                { id }
            };
            var request = new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = request.Id
            };
            var secret = new RequestSecret()
            {
                Request = request,
                Distribution = new Policy()
                {
                    EmailSecrets = true
                }
            };
            var mapSecret = new Secret()
            {
                RequestDetails = requestDetails,
                Distribution = new PolicyEntry()
                {
                    EmailSecrets = true
                }
            };
            fixture.mimAccessApi.Setup(m => m.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateDistributeSecretsAsync(requestDetails)).ReturnsAsync(true);
            fixture.mimAccessApi.Setup(a => a.DistributeSecretsRequestAsync(id)).ReturnsAsync((RequestSecret)null);
            fixture.mapper.Setup(m => m.Map<List<Secret>>(new RequestSecret[] { secret })).Returns(new List<Secret> { mapSecret });
            var result = await fixture.target.DistributeSecretsAsync(requestIds);
            result.Should().BeNullOrEmpty();
        }

        [Test]
        public async Task FindRequest_RequestsIsNull_Test()
        {
            var filter = new RequestFilterParameters();
            var paging = new PagingParameters(1, 1);
            var sorting = new SortingParameters();
            var searchRequest = new RequestSearchRequest()
            {
                OriginatorUserName = filter.OriginatorUserName,
                RequestStatus = Messaging.OpenFim.Enumerations.ReqStatus.Approved,
                RequestType = Messaging.OpenFim.Enumerations.RequestType.Disable,
                TargetUserName = "targetUserName"
            };
            fixture.mapper.Setup(m => m.Map<RequestSearchRequest>(filter)).Returns(searchRequest);
            fixture.mimAccessApi.Setup(a => a.ActionRequestAsync(searchRequest, 1, 10)).ReturnsAsync((Messaging.OpenFim.Entities.PagedList<Request>)null);
            var result = await fixture.target.FindRequestsAsync(filter, paging, sorting);

            result.Should().BeOfType(typeof(StaticPagedList<SignedRequest>));
        }
        

        [Test]
        public async Task Validate_RequestExist_Test()
        {
            var id = Guid.NewGuid();
            var request = new Request()
            {
                Id = id
            };
            var requestDetails = new RequestDetails()
            {
                RequestId = id
            };
            fixture.mimAccessApi.Setup(a => a.ActionRequestByIdAsync(id)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);
            fixture.validator.Setup(v => v.ValidateDenyAsync(requestDetails)).ReturnsAsync(true);

            var result = await fixture.target.ValidateActionsAsync(id);
            result.Should().BeOfType(typeof(Validation));
            result.RequestId.Should().Be(id);
            result.Approve.Should().BeFalse();
            result.Deny.Should().BeTrue();
        }

        [Test]
        public async Task Validate_RequestIsNull_Test()
        {
            var id = Guid.NewGuid();
            Request request = null;

            fixture.mimAccessApi.Setup(a => a.ActionRequestByIdAsync(id)).ReturnsAsync(request);

            var result = await fixture.target.ValidateActionsAsync(id);
            result.Should().BeOfType(typeof(Validation));
            result.RequestId.Should().Be(id);
            result.Approve.Should().BeFalse();
            result.Deny.Should().BeFalse();
            result.DistributeSecrets.Should().BeFalse();
            result.Cancel.Should().BeFalse();
            result.Abandon.Should().BeFalse();
        }


        
        private class RequestServiceFixture
        {
            public readonly RequestService target;
            public readonly Mock<IMapper> mapper;
            public readonly Mock<IMimAccessApi> mimAccessApi;
            public readonly Mock<ICertificateManagerApi> certificateManagerApi;
            public readonly Mock<IRequestActionValidator> validator;
            public readonly Mock<ISignatureConfiguration> configuration;

            public RequestServiceFixture()
            {
                this.mapper = new Mock<IMapper>();
                this.mimAccessApi = new Mock<IMimAccessApi>();
                this.certificateManagerApi = new Mock<ICertificateManagerApi>();
                this.validator = new Mock<IRequestActionValidator>();
                this.configuration = new Mock<ISignatureConfiguration>();

                target = new RequestService(mimAccessApi.Object, certificateManagerApi.Object, validator.Object, mapper.Object, configuration.Object);
            }


        }
    }
}
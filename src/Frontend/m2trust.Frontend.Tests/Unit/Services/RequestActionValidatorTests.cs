﻿using AutoMapper;
using FluentAssertions;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.OpenFim.API;
using m2trust.Messaging.OpenFim.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Tests.Unit.Services
{
    public class RequestActionValidatorTests
    {
        private readonly TestFixture testFixture;

        public RequestActionValidatorTests()
        {
            testFixture = new TestFixture();
        }

        [Test]
        public async Task ValidateAbandon_RequestNotValid_Test()
        {
            var action = RequestAction.Abandon;
            var actionPermission = ActionPermission.Abandon; 
            var request = new RequestDetails()
            {
                RequestId = Guid.NewGuid(),
                TargetUserName = "TargetTest",
                OriginatorUserName = "OriginatorTest"
            };

            testFixture.mapper.Setup(m => m.Map<ActionPermission>(action)).Returns(actionPermission);
            testFixture.mimAccessApi.Setup(a => a.ActionPermissionGrantAsync(request.RequestId, actionPermission)).ReturnsAsync(false);

            var result = await testFixture.target.ValidateAbandonAsync(request);
            result.Should().BeFalse();
        }

        [Test]
        public async Task ValidateAbandon_RequestValid_Test()
        {
            var action = RequestAction.Abandon;
            var actionPermission = ActionPermission.Abandon;
            var request = new RequestDetails()
            {
                RequestId = Guid.NewGuid(),
                TargetUserName = "TargetTest",
                OriginatorUserName = "OriginatorTest"
            };

            testFixture.mapper.Setup(m => m.Map<ActionPermission>(action)).Returns(actionPermission);
            testFixture.mimAccessApi.Setup(a => a.ActionPermissionGrantAsync(request.RequestId, actionPermission)).ReturnsAsync(true);

            var result = await testFixture.target.ValidateAbandonAsync(request);
            result.Should().BeTrue();
        }

        [Test]
        public async Task ValidateApprove_RequestValid_Test()
        {
            var action = RequestAction.Approve;
            var actionPermission = ActionPermission.Approve;
            var request = new RequestDetails()
            {
                RequestId = Guid.NewGuid(),
                TargetUserName = "TargetTest",
                OriginatorUserName = "OriginatorTest",
                RequestState = RequestState.Pending
            };

            testFixture.mapper.Setup(m => m.Map<ActionPermission>(action)).Returns(actionPermission);
            testFixture.mimAccessApi.Setup(a => a.ActionPermissionGrantAsync(request.RequestId, actionPermission)).ReturnsAsync(true);

            var result = await testFixture.target.ValidateApproveAsync(request);
            result.Should().BeTrue();
        }

        [Test]
        public async Task ValidateApprove_RequestStateNotPending_Test()
        {
            var action = RequestAction.Approve;
            var actionPermission = ActionPermission.Approve;
            var request = new RequestDetails()
            {
                RequestId = Guid.NewGuid(),
                TargetUserName = "TargetTest",
                OriginatorUserName = "OriginatorTest",
                RequestState = RequestState.Canceled
            };

            testFixture.mapper.Setup(m => m.Map<ActionPermission>(action)).Returns(actionPermission);
            testFixture.mimAccessApi.Setup(a => a.ActionPermissionGrantAsync(request.RequestId, actionPermission)).ReturnsAsync(true);

            var result = await testFixture.target.ValidateApproveAsync(request);
            result.Should().BeFalse();
        }

        [Test]
        public async Task ValidateCancel_RequestValid_Test()
        {
            var action = RequestAction.Cancel;
            var actionPermission = ActionPermission.Cancel;
            var request = new RequestDetails()
            {
                RequestId = Guid.NewGuid(),
                TargetUserName = "TargetTest",
                OriginatorUserName = "OriginatorTest",
                RequestState = RequestState.Pending
            };

            testFixture.mapper.Setup(m => m.Map<ActionPermission>(action)).Returns(actionPermission);
            testFixture.mimAccessApi.Setup(a => a.ActionPermissionGrantAsync(request.RequestId, actionPermission)).ReturnsAsync(true);

            var result = await testFixture.target.ValidateCancelAsync(request);
            result.Should().BeTrue();
        }

        [Test]
        public async Task ValidateCancel_RequestStateNotPending_Test()
        {
            var action = RequestAction.Cancel;
            var actionPermission = ActionPermission.Cancel;
            var request = new RequestDetails()
            {
                RequestId = Guid.NewGuid(),
                TargetUserName = "TargetTest",
                OriginatorUserName = "OriginatorTest",
                RequestState = RequestState.Canceled
            };

            testFixture.mapper.Setup(m => m.Map<ActionPermission>(action)).Returns(actionPermission);
            testFixture.mimAccessApi.Setup(a => a.ActionPermissionGrantAsync(request.RequestId, actionPermission)).ReturnsAsync(true);

            var result = await testFixture.target.ValidateCancelAsync(request);
            result.Should().BeFalse();
        }

        [Test]
        public async Task ValidateDeny_RequestValid_Test()
        {
            var action = RequestAction.Deny;
            var actionPermission = ActionPermission.Deny;
            var request = new RequestDetails()
            {
                RequestId = Guid.NewGuid(),
                TargetUserName = "TargetTest",
                OriginatorUserName = "OriginatorTest",
                RequestState = RequestState.Pending
            };

            testFixture.mapper.Setup(m => m.Map<ActionPermission>(action)).Returns(actionPermission);
            testFixture.mimAccessApi.Setup(a => a.ActionPermissionGrantAsync(request.RequestId, actionPermission)).ReturnsAsync(true);

            var result = await testFixture.target.ValidateDenyAsync(request);
            result.Should().BeTrue();
        }
        
        [Test]
        public async Task ValidateDistributeSecrets_RequestValid_Test()
        {
            var action = RequestAction.DistributeSecrets;
            var actionPermission = ActionPermission.DistributeSecrets;
            var request = new RequestDetails()
            {
                RequestId = Guid.NewGuid(),
                TargetUserName = "TargetTest",
                OriginatorUserName = "OriginatorTest",
                RequestForm = RequestForm.Retire
            };

            testFixture.mapper.Setup(m => m.Map<ActionPermission>(action)).Returns(actionPermission);
            testFixture.mimAccessApi.Setup(a => a.ActionPermissionGrantAsync(request.RequestId, actionPermission)).ReturnsAsync(true);

            var result = await testFixture.target.ValidateDistributeSecretsAsync(request);
            result.Should().BeFalse();
        }

        private class TestFixture
        {
            public readonly RequestActionValidator target;
            public readonly Mock<IMapper> mapper;
            public readonly Mock<IMimAccessApi> mimAccessApi;
            public readonly Mock<ILogWriter> logger;

            public TestFixture()
            {
                this.mapper = new Mock<IMapper>();
                this.mimAccessApi = new Mock<IMimAccessApi>();
                this.logger = new Mock<ILogWriter>();

                target = new RequestActionValidator(mimAccessApi.Object, logger.Object, mapper.Object);
            }


        }
    }
}

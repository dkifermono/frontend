﻿using m2trust.Frontend.Service;
using m2trust.Frontend.Service.Common;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using m2trust.Frontend.Model;
using static m2trust.Frontend.Service.CachedUserActivityChecker;

namespace m2trust.Frontend.Tests.Unit.Services
{
    [TestFixture]
    public class CachedUserActivityCheckerTests
    {
        private Guid authorizedUserId = Guid.NewGuid();
        private const string AuthorizedActivity = "Activity1";
        private const string UnAuthorizedActivity = "Activity2";

        [Test]
        public async Task IsUserAuthorizedForActivityAsync_ShouldNotCallServiceIfCacheEntryExists()
        {
            var userActivityCheckerStub = new Mock<IUserActivityChecker>();

            var cacheService = new Mock<ICacheService>();
            cacheService.Setup(m => m.GetAsync<UserActivityCacheEntry>(It.IsAny<string>())).ReturnsAsync(new UserActivityCacheEntry() { IsAuthorized = true });

            var sut = new CachedUserActivityChecker(userActivityCheckerStub.Object, cacheService.Object); ;

            var result = await sut.IsUserAuthorizedForActivityAsync(new HttpContextApplicationUser(), AuthorizedActivity);
            userActivityCheckerStub.Verify(m => m.IsUserAuthorizedForActivityAsync(It.IsAny<HttpContextApplicationUser>(), It.IsAny<string>()), Times.Never);

        }

        [Test]
        public async Task IsUserAuthorizedForActivityAsync_ShouldCallServiceIfCacheEntryDoesNotExist()
        {
            var userActivityCheckerStub = new Mock<IUserActivityChecker>();

            var cacheService = new Mock<ICacheService>();
            cacheService.Setup(m => m.GetAsync<UserActivityCacheEntry>(It.IsAny<string>())).ReturnsAsync((UserActivityCacheEntry)null);

            var sut = new CachedUserActivityChecker(userActivityCheckerStub.Object, cacheService.Object); ;

            var result = await sut.IsUserAuthorizedForActivityAsync(new HttpContextApplicationUser(), AuthorizedActivity);

            userActivityCheckerStub.Verify(m => m.IsUserAuthorizedForActivityAsync(It.IsAny<HttpContextApplicationUser>(), It.IsAny<string>()), Times.Once);
        }
    }
}

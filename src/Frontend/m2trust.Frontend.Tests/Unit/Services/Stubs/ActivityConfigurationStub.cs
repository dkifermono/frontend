﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Tests.Unit.Services.Stubs
{
    internal class ActivityConfigurationStub : IActivityConfiguration
    {
        #region Constructors

        public ActivityConfigurationStub()
        {
            this.RegisteredActivities = new[] { Permissions.Enroll,
                                        Permissions.ReinstateSmartcard,
                                        Permissions.ManageRequests };
        }

        #endregion Constructors

        #region Properties

        public IEnumerable<string> RegisteredActivities { get; }

        #endregion Properties

        #region Methods

        public Task<string[]> GetActivitiesAsync()
        {
            var activities = new[] { Permissions.Enroll,
                                        Permissions.ReinstateSmartcard,
                                        Permissions.ManageRequests };
            return Task.FromResult(activities);
        }

        public Task<IEnumerable<string>> GetAuthorizedADGroups(string activityName)
        {
            switch (activityName)
            {
                case Permissions.Enroll:
                    return Task.FromResult(new[] { "Group1", "Group2" } as IEnumerable<string>);

                case Permissions.ReinstateSmartcard:
                    return Task.FromResult(new[] { "Group2" } as IEnumerable<string>);

                case Permissions.ManageRequests:
                    return Task.FromResult(new[] { "Group3" } as IEnumerable<string>);

                default: return Task.FromResult(Enumerable.Empty<string>());
            }
        }

        public Task<bool?> GetContinueAsync(string activityName)
        {
            bool? result = true;
            return Task.FromResult(result);
        }

        public Task<bool?> GetShowCommentsAsync(string activityName)
        {
            bool? result = true;
            return Task.FromResult(result);
        }

        public async Task<bool> IsActivityPermissionRegistered(string activityName)
        {
            var activities = await GetActivitiesAsync();
            return activities.Contains(activityName);
        }

        #endregion Methods
    }
}
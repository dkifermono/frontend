﻿using FluentAssertions;
using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Service;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Tests.Unit.Services.Stubs;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using m2trust.Frontend.Model;

namespace m2trust.Frontend.Tests.Unit.Services
{
    [TestFixture]
    public class ActivityServiceTests
    {
        private readonly TestFixture fixture;

        public ActivityServiceTests()
        {
            this.fixture = new TestFixture();
        }

        [Test]
        public async Task GetActivitiesAsync_ShouldReturnCollectionOfActivitiesTheUserIsAuthorizedFor()
        {
            var result = await this.fixture.Target.GetActivitiesAsync(new HttpContextApplicationUser());

            var activityNames = result.Select(a => a.ActivityName);

            activityNames.Should().Match(r => r.Contains(Permissions.Enroll) && r.Contains(Permissions.ReinstateSmartcard) && !r.Contains(Permissions.ManageRequests));

        }

        private class TestFixture
        {
            public IActivityConfiguration configuration;
            public UserActivityCheckerStub UserActivityChecker;
            public ActivityService Target;

            public TestFixture()
            {
                this.configuration = new ActivityConfigurationStub();
                this.UserActivityChecker = new UserActivityCheckerStub();
                this.Target = new ActivityService(this.configuration, this.UserActivityChecker);
            }
        }

        #region Stubs

        private class UserActivityCheckerStub : IUserActivityChecker
        {
            private IEnumerable<string> authorizedActivities = new[] { Permissions.Enroll, Permissions.ReinstateSmartcard };

            public Task<bool> IsUserAuthorizedForActivityAsync(HttpContextApplicationUser user, string activityName)
            {
                return Task.FromResult(authorizedActivities.Contains(activityName, StringComparer.InvariantCulture));
            }
        }

        #endregion
    }
}

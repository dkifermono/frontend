﻿using FluentAssertions;
using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Service;
using m2trust.Frontend.Tests.Unit.Services.Stubs;
using m2trust.Messaging.ActiveDirectory.API;
using m2trust.Messaging.ActiveDirectory.Entities;
using m2trust.Messaging.ActiveDirectory.Enumerations;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using m2trust.Frontend.Model;

namespace m2trust.Frontend.Tests.Unit.Services
{
    [TestFixture]
    public class AdApiUserActivityCheckerTests
    {
        private readonly TestFixture testFixture;

        public AdApiUserActivityCheckerTests()
        {
            this.testFixture = new TestFixture();
        }

        [Test]
        public async Task IsUserAuthorizedForActivityAsync_ShouldReturnTrueIfUserIsInOneOfRequiredGroups()
        {
            var result = await this.testFixture.Target.IsUserAuthorizedForActivityAsync(new HttpContextApplicationUser(), Permissions.Enroll);
            result.Should().BeTrue();
        }

        [Test]
        public async Task IsUserAuthorizedForActivityAsync_ShouldReturnFalseIfUserIsNotInAnyOfRequiredGroups()
        {
            var result = await this.testFixture.Target.IsUserAuthorizedForActivityAsync(new HttpContextApplicationUser(), Permissions.ManageRequests);
            result.Should().BeFalse();
        }

        [Test]
        public async Task IsUserAuthorizedForActivityAsync_ShouldReturnFalseIfActivityIsNotRegistered()
        {
            var result = await this.testFixture.Target.IsUserAuthorizedForActivityAsync(new HttpContextApplicationUser(), Permissions.RenewCertificates);
            result.Should().BeFalse();
        }

        private class TestFixture
        {
            public AdApiUserActivityChecker Target;
            public IActiveDirectoryApi ActiveDirectoryApiStub;
            public IActivityConfiguration ActivityConfigurationStub;

            public TestFixture()
            {
                this.ActiveDirectoryApiStub = new ActiveDirectoryApiStub();
                this.ActivityConfigurationStub = new ActivityConfigurationStub();
                this.Target = new AdApiUserActivityChecker(ActivityConfigurationStub, ActiveDirectoryApiStub);
            }
        }

        #region Stubs

        private class ActiveDirectoryApiStub : IActiveDirectoryApi
        {
            public Task<IEnumerable<Domain>> AllDomainsAsync()
            {
                throw new NotImplementedException();
            }

            public Task<Domain> CurrentLocalDomainAsync()
            {
                throw new NotImplementedException();
            }

            public Task<bool> IsObjectInGroupsAsync(string uniqueIdentifier, IEnumerable<string> groups, IdentityType identityType)
            {
                return Task.FromResult(groups.Contains("Group1") || groups.Contains("Group2"));
            }

            public Task<User> LookupUserAsync(string ldapQuery, string[] propertiesToFetch)
            {
                throw new NotImplementedException();
            }

            public Task<ActiveDirectorySearchUsersResult> SearchUsersAsync(int offset, int count, string domain, string searchTerm, ICollection<string> fields, IEnumerable<string> propertiesToLoad, bool extendedSearch)
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}

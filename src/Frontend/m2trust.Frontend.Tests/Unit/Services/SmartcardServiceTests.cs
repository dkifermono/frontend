﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using NUnit.Framework;
using FluentAssertions;
using m2trust.Messaging.OpenFim.API;
using AutoMapper;
using System.Threading.Tasks;
using m2trust.Frontend.Model;
using m2trust.Messaging.OpenFim.Entities;
using m2trust.Frontend.Service;
using m2trust.Frontend.Service.Common;

namespace m2trust.Frontend.Tests.Unit.Services
{
    [TestFixture]
    public class SmartcardServiceTests
    {
        private SmartcardServiceFixture fixture;

        public SmartcardServiceTests()
        {
            this.fixture = new SmartcardServiceFixture();
        }

        [Test]
        public async Task DisableSmartcardAsync_responseIsNull_Test()
        {
            var auditRequestEntry = new AuditRequestEntry
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
                ProfileTemplateId = Guid.NewGuid(),
                DataCollectionItems = new Dictionary<string, string>()
                {
                    {"one", "two" },
                    {"three", "four" },
                    {"five", "six" }
                },
                Comment = "comment"
            };

            DisableSmartcardResult disableSmartcard = null;

            fixture.mimAccessApi.Setup(m => m.DisableSmartcardAsync(auditRequestEntry.ProfileId, auditRequestEntry.DataCollectionItems, auditRequestEntry.Comment, auditRequestEntry.EffectiveRevocationInHours)).ReturnsAsync(disableSmartcard);

            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.DisableSmartcardAsync(auditRequestEntry);

            result.Should().BeNull();
        }

        [Test]
        public async Task DisableSmartcardAsync_responseExists_Test()
        {
            var auditRequestEntry = new AuditRequestEntry
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
                ProfileTemplateId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                DataCollectionItems = new Dictionary<string, string>()
                {
                    {"one", "two" },
                    {"three", "four" },
                    {"five", "six" }
                },
                Comment = "comment"
            };

            var disableSmartcard = new DisableSmartcardResult
            {
                RequestId = Guid.NewGuid(),
                OneTimePassword = "password"
            };

            var otpResult = new OtpResult
            {
                RequestId = (Guid)disableSmartcard.RequestId,
                OneTimePassword = disableSmartcard.OneTimePassword
            };

            fixture.mimAccessApi.Setup(m => m.DisableSmartcardAsync(auditRequestEntry.ProfileId, auditRequestEntry.DataCollectionItems, auditRequestEntry.Comment, auditRequestEntry.EffectiveRevocationInHours)).ReturnsAsync(disableSmartcard);
            fixture.mapper.Setup(m => m.Map<OtpResult>(disableSmartcard)).Returns(otpResult);

            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.DisableSmartcardAsync(auditRequestEntry);

            result.Should().NotBeNull().And.BeOfType(typeof(OtpResult));
        }

        [Test]
        public async Task RetireSmartcardAsync_requestIsNull_Test()
        {
            AuditRequestEntry request = null;
            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.RetireSmartcardAsync(request);

            result.Should().BeNull();
        }

        [Test]
        public async Task RetireSmartCardAsync_responseIsNull_Test()
        {
            var request = new AuditRequestEntry
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
                DataCollectionItems = new Dictionary<string, string>()
                {
                    {"one", "two" },
                    {"three", "four" },
                    {"five", "six" }
                }
            };
            var profileRequest = new ProfileRequest
            {
                DataCollectionItems = request.DataCollectionItems,
                ProfileId = request.ProfileId
            };
            RetireResult response = null;

            fixture.mapper.Setup(m => m.Map<ProfileRequest>(request)).Returns(profileRequest);
            fixture.mimAccessApi.Setup(m => m.RetireSmartcardAsync(profileRequest.ProfileId, profileRequest.DataCollectionItems, profileRequest.Comment, (byte)profileRequest.Priority)).ReturnsAsync(response);
            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.RetireSmartcardAsync(request);

            result.Should().BeNull();
        }

        [Test]
        public async Task RetireSmartCardAsync_ResponseExist_Test()
        {
            var request = new AuditRequestEntry
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
                DataCollectionItems = new Dictionary<string, string>()
                {
                    {"one", "two" },
                    {"three", "four" },
                    {"five", "six" }
                }
            };
            var profileRequest = new ProfileRequest
            {
                DataCollectionItems = request.DataCollectionItems,
                ProfileId = request.ProfileId
            };
            var response = new RetireResult
            {
                OneTimePassword = "somepassword",
                RequestId = Guid.NewGuid(),
            };
            var otpResult = new OtpResult
            {
                OneTimePassword = response.OneTimePassword,
                RequestId = response.RequestId ?? Guid.Empty,
            };

            fixture.mapper.Setup(m => m.Map<ProfileRequest>(request)).Returns(profileRequest);
            fixture.mimAccessApi.Setup(m => m.RetireSmartcardAsync(profileRequest.ProfileId, profileRequest.DataCollectionItems, profileRequest.Comment, (byte)profileRequest.Priority)).ReturnsAsync(response);
            fixture.mapper.Setup(m => m.Map<OtpResult>(response)).Returns(otpResult);
            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.RetireSmartcardAsync(request);

            result.Should().NotBeNull().And.BeOfType(typeof(OtpResult));
        }

        [Test]
        public async Task InitiateOfflineUnblockAsync_responseIsNull_Test()
        {
            var auditRequestEntry = new AuditRequestEntry
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
                ProfileTemplateId = Guid.NewGuid(),
                DataCollectionItems = new Dictionary<string, string>()
                {
                    {"one", "two" },
                    {"three", "four" },
                    {"five", "six" }
                },
                Comment = "comment"
            };
            

            Request emptyRequest = null;
            
            fixture.mimAccessApi.Setup(m => m.InitiateOfflineUnblockAsync(auditRequestEntry.DataCollectionItems, auditRequestEntry.ProfileId)).ReturnsAsync(emptyRequest);

            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.InitiateOfflineUnblockAsync(auditRequestEntry);

            result.Should().BeNull();
        }

        [Test]
        public async Task InitiateOfflineUnblockAsync_responseExists_Test()
        {
            var auditRequestEntry = new AuditRequestEntry
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
                ProfileTemplateId = Guid.NewGuid(),
                DataCollectionItems = new Dictionary<string, string>()
                {
                    {"one", "two" },
                    {"three", "four" },
                    {"five", "six" }
                },
                Comment = "comment"
            };

            var request = new Request()
            {
                UserId = auditRequestEntry.UserId,
                ProfileId = auditRequestEntry.ProfileId,
                ProfileTemplateId = auditRequestEntry.ProfileTemplateId,
                DataCollectionItems = auditRequestEntry.DataCollectionItems,
                Comment = auditRequestEntry.Comment
            };

            var requestDetails = new RequestDetails
            {
                UserId = request.UserId,
                ProfileId = request.ProfileId,
                ProfileTemplateId = request.ProfileTemplateId,
                DataCollectionItems = request.DataCollectionItems,
                Comment = request.Comment
            };

            fixture.mimAccessApi.Setup(m => m.InitiateOfflineUnblockAsync(auditRequestEntry.DataCollectionItems, auditRequestEntry.ProfileId)).ReturnsAsync(request);
            fixture.mapper.Setup(m => m.Map<RequestDetails>(request)).Returns(requestDetails);

            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.InitiateOfflineUnblockAsync(auditRequestEntry);

            result.Should().NotBeNull().And.BeOfType(typeof(RequestDetails));
        }

        [Test]
        public async Task OfflineUnblockChallengeAsync_responseIsNull_Test()
        {
            var requestEntry = new EnterChallengeRequestEntry
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
                Challenge = "challenge",
                RequestGuid = Guid.NewGuid()
            };

            String response = null;

            fixture.mimAccessApi.Setup(m => m.OfflineUnblockChallengeAsync(requestEntry.ProfileId, requestEntry.RequestGuid, requestEntry.Challenge)).ReturnsAsync(response);
            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.OfflineUnblockChallengeAsync(requestEntry);

            result.Should().BeNull();
        }

        [Test]
        public async Task OfflineUnblockChallengeAsync_responseExists_Test()
        {
            var requestEntry = new EnterChallengeRequestEntry
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
                Challenge = "challenge",
                RequestGuid = Guid.NewGuid()
            };

            var response = "response";

            fixture.mimAccessApi.Setup(m => m.OfflineUnblockChallengeAsync(requestEntry.ProfileId, requestEntry.RequestGuid, requestEntry.Challenge)).ReturnsAsync(response);
            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.OfflineUnblockChallengeAsync(requestEntry);

            result.Should().NotBeNull().And.BeOfType(typeof(string));
        }

        [Test]
        public async Task ReinstateSmartcardAsync_requestEntryIsNull_Test()
        {
            AuditRequestEntry requestEntry = null;

            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.ReinstateSmartcardAsync(requestEntry);

            result.Should().BeNull();
        }

        [Test]
        public async Task ReinstateSmartcardAsync_requestEntryIsNotNull_Test()
        {
            var requestEntry = new AuditRequestEntry
            {
                Reason = Common.Enums.SuspendReinstateReasonType.CertificateReinstate,
                EffectiveRevocationInHours = 23,
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid()
            };

            var profile = new ProfileRequestResult
            {
                BaseRequest = new ProfileRequest
                {
                    ProfileId = Guid.NewGuid(),
                    Id = Guid.NewGuid(),
                    Comment = "stringComment"
                }
            };

            var auditRequest = new AuditRequestEntry
            {
                Id = profile.BaseRequest.Id,
                ProfileId = profile.BaseRequest.ProfileId,
                Comment = profile.BaseRequest.Comment
            };

            var reinstateResult = new ReinstateResult()
            {
                OneTimePassword = "one-time-password",
                RequestId = auditRequest.Id
            };

            var otpResult = new OtpResult()
            {
                OneTimePassword = "one-time-password",
                RequestId = (Guid)reinstateResult.RequestId
            };
            
            fixture.mimAccessApi.Setup(m => m.ReinstateSmartcardAsync(requestEntry.ProfileId, requestEntry.DataCollectionItems, requestEntry.Comment, requestEntry.EffectiveRevocationInHours)).ReturnsAsync(reinstateResult);
            fixture.mapper.Setup(m => m.Map<OtpResult>(reinstateResult)).Returns(otpResult);

            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.ReinstateSmartcardAsync(requestEntry);

            result.Should().NotBeNull().And.BeOfType(typeof(OtpResult));
        }

        [Test]
        public async Task SuspendSmartcardAsync_requestEntryIsNull_Test()
        {
            AuditRequestEntry requestEntry = null;

            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.SuspendSmartcardAsync(requestEntry);

            result.Should().BeNull();
        }

        [Test]
        public async Task SuspendSmartcardAsync_requestEntryIsNotNull_Test()
        {
            var requestEntry = new AuditRequestEntry
            {
                Reason = Common.Enums.SuspendReinstateReasonType.CertificateReinstate,
                EffectiveRevocationInHours = 23,
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid()
            };

            var profile = new ProfileRequestResult
            {
                BaseRequest = new ProfileRequest
                {
                    ProfileId = Guid.NewGuid(),
                    Id = Guid.NewGuid(),
                    Comment = "stringComment"
                }
            };

            var auditRequest = new AuditRequestEntry
            {
                Id = profile.BaseRequest.Id,
                ProfileId = profile.BaseRequest.ProfileId,
                Comment = profile.BaseRequest.Comment
            };

            var suspendResult = new SuspendResult()
            {
                RequestId = auditRequest.Id,
                OneTimePassword = "one-time-password"
            };

            var otpResult = new OtpResult()
            {
                RequestId = (Guid)suspendResult.RequestId,
                OneTimePassword = suspendResult.OneTimePassword
            };
            fixture.mimAccessApi.Setup(m => m.SuspendSmartcardAsync(requestEntry.ProfileId, requestEntry.DataCollectionItems, requestEntry.Comment, requestEntry.EffectiveRevocationInHours)).ReturnsAsync(suspendResult);
            fixture.mapper.Setup(m => m.Map<OtpResult>(suspendResult)).Returns(otpResult);

            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.SuspendSmartcardAsync(requestEntry);

            result.Should().NotBeNull().And.BeOfType(typeof(OtpResult));
        }

        [Test]
        public async Task RetireSmartcardAsync_requestEntryIsNull_Test()
        {
            AuditRequestEntry requestEntry = null;

            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);

            var result = await service.RetireSmartcardAsync(requestEntry);

            result.Should().BeNull();
        }

        [Test]
        public async Task RetireSmartcardAsync_requestEntryIsNotNull_Test()
        {
            var requestEntry = new AuditRequestEntry
            {
                UserId = Guid.NewGuid(),
                ProfileId = Guid.NewGuid()
            };

            var profileRequest = new ProfileRequest
            {
                ProfileId = requestEntry.ProfileId
            };

            var retireResult = new RetireResult
            {
                RequestId = Guid.NewGuid(),
                OneTimePassword = "password"
            };

            var otpResult = new OtpResult
            {
                OneTimePassword = retireResult.OneTimePassword,
                RequestId = retireResult.RequestId ?? Guid.Empty,
            };

            fixture.mapper.Setup(m => m.Map<ProfileRequest>(requestEntry)).Returns(profileRequest);
            fixture.mimAccessApi.Setup(m => m.RetireSmartcardAsync(profileRequest.ProfileId, profileRequest.DataCollectionItems, profileRequest.Comment, (byte)profileRequest.Priority)).ReturnsAsync(retireResult);
            fixture.mapper.Setup(m => m.Map<OtpResult>(retireResult)).Returns(otpResult);

            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);

            var result = await service.RetireSmartcardAsync(requestEntry);

            result.Should().NotBeNull().And.BeOfType(typeof(OtpResult));
        }

        [Test]
        public async Task GetPermanentSmartcardIdAsync_smartCardIsNull_Test()
        {
            Guid profileId = Guid.NewGuid();
            SmartcardProfileEntry profileEntry = null;

            fixture.profileService.Setup(m => m.GetSmartcardProfileAsync(profileId)).ReturnsAsync(profileEntry);
            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.GetPermanentSmartcardIdAsync(profileId);

            result.Should().BeNull();
        }

        [Test]
        public async Task GetPermanentSmartcardIdAsync_smartcardExistAndSmartcardIsNotProperValue_Test()
        {
            Guid profileId = Guid.NewGuid();
            var profileEntry = new SmartcardProfileEntry
            {
                Id = Guid.NewGuid(),
                ProfileTemplate = new ProfileTemplateEntry
                {
                    TemporaryCardsPolicy = new TemporaryCardsPolicyEntry
                    {
                        ImmediatelySuspendLinkedCard = true
                    }
                }
            };

            fixture.profileService.Setup(m => m.GetSmartcardProfileAsync(profileId)).ReturnsAsync(profileEntry);
            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.GetPermanentSmartcardIdAsync(profileId);

            result.Should().BeNull();
        }

        [Test]
        public async Task GetPermanentSmartcardIdAsync_smartcardExistAndSmartcardIsProperValue_Test()
        {
            Guid profileId = Guid.NewGuid();
            var profileEntry = new SmartcardProfileEntry
            {
                Id = Guid.NewGuid(),
                TargetType = Common.Enums.TargetForm.TemporarySmartcard,
                PermanentSmartcardId = Guid.NewGuid(),
                ProfileTemplate = new ProfileTemplateEntry
                {
                    TemporaryCardsPolicy = new TemporaryCardsPolicyEntry
                    {
                        ImmediatelySuspendLinkedCard = true
                    }
                }
            };

            fixture.profileService.Setup(m => m.GetSmartcardProfileAsync(profileId)).ReturnsAsync(profileEntry);
            var service = new SmartcardService(fixture.mimAccessApi.Object, fixture.mapper.Object, fixture.profileService.Object, fixture.manager.Object);
            var result = await service.GetPermanentSmartcardIdAsync(profileId);

            result.Should().NotBeNull().And.Be(profileEntry.PermanentSmartcardId);
        }
    }

    public class SmartcardServiceFixture
    {
        public Mock<IMimAccessApi> mimAccessApi;
        public Mock<IMapper> mapper;
        public Mock<IProfileService> profileService;
        public Mock<IReinstateDCIConfigurationManager> manager;

        public SmartcardServiceFixture()
        {
            this.mimAccessApi = new Mock<IMimAccessApi>();
            this.mapper = new Mock<IMapper>();
            this.profileService = new Mock<IProfileService>();
            this.manager = new Mock<IReinstateDCIConfigurationManager>();
        }
    }
}

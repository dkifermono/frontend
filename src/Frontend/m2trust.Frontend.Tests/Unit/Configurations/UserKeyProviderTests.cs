﻿using FluentAssertions;
using m2trust.Frontend.Model;
using m2trust.Frontend.Web.Configuration.AppConfiguration.Keys;
using m2trust.Frontend.Web.Infrastructure;
using Microsoft.AspNetCore.Http;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Tests.Unit.Configurations
{
    public class UserKeyProviderTests
    {
        private readonly UserKeyProviderFixture testFixture; 

        public UserKeyProviderTests()
        {
            testFixture = new UserKeyProviderFixture();
        }

        [Test]
        public async Task GetKey_UserExsit_Test()
        {
            var user = new HttpContextApplicationUser()
            {
                Name = "test",
                UniqueIdentifier = Guid.NewGuid(),
                UserName = "test"
            };

            testFixture.applicationUserContext.Setup(m => m.GetCurrentUser()).ReturnsAsync(user);

            var result = await testFixture.target.GetKeyAsync();
            result.Should().NotBeNull().And.NotBeNullOrWhiteSpace();
        }
        
        [Test]
        public async Task GetKey_UserIsNull_Test()
        {
            HttpContextApplicationUser user = null;
            testFixture.applicationUserContext.Setup(m => m.GetCurrentUser()).ReturnsAsync(user);
            var result = await testFixture.target.GetKeyAsync();
            result.Should().BeNull();
        }

        private class UserKeyProviderFixture
        {
            public readonly Mock<IApplicationUserContext> applicationUserContext;
            public readonly UserKeyProvider target;
            
            public UserKeyProviderFixture()
            {
                applicationUserContext = new Mock<IApplicationUserContext>();
                target = new UserKeyProvider(applicationUserContext.Object);
            }
           
        }
    }
}

﻿using FluentAssertions;
using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Models;
using m2trust.Frontend.Web.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using m2trust.Settings;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;

namespace m2trust.Frontend.Tests.Unit.Configurations
{
    public class ReinstateDCIConfigurationTests
    {
        private readonly TestFixture testFixture;

        public ReinstateDCIConfigurationTests()
        {
            testFixture = new TestFixture();
        }

        [Test]
        public async Task GetDCIProfileTemplates_SettingExist_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.DCIProfileTemplates = new List<ProfileTemplate> { new ProfileTemplate() };

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var target = new ReinstateDCIConfiguration(testFixture.settingsProviderFactory.Object, testFixture.serviceConfigurationStore.Object);

            var result = await target.DCIProfileTemplates;
            result.Should().NotBeEmpty();
        }

        [Test]
        public async Task GetDCIProfileTemplates_SettingIsNull_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.DCIProfileTemplates = new List<ProfileTemplate> { };

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var result = await testFixture.target.DCIProfileTemplates;
            result.Should().BeEmpty();
        }

        private class TestFixture
        {
            public readonly Mock<ISettingsProviderFactory> settingsProviderFactory;
            public readonly Mock<ISettingsProvider> settingsProvider;
            public readonly Mock<IApplicationSettings> applicationSettings;
            public readonly Mock<IServiceConfigurationStore> serviceConfigurationStore;
            public readonly ReinstateDCIConfiguration target;

            public TestFixture()
            {
                settingsProviderFactory = new Mock<ISettingsProviderFactory>();
                settingsProvider = new Mock<ISettingsProvider>();
                applicationSettings = new Mock<IApplicationSettings>();
                serviceConfigurationStore = new Mock<IServiceConfigurationStore>();

                target = new ReinstateDCIConfiguration(settingsProviderFactory.Object, serviceConfigurationStore.Object);
            }
        }
    }
}

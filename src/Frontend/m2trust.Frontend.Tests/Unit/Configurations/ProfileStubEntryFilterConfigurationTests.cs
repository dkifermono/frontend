﻿using FluentAssertions;
using m2trust.Frontend.Common;
using m2trust.Frontend.Web.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using m2trust.Settings;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;

namespace m2trust.Frontend.Tests.Unit.Configurations
{
    public class ProfileStubEntryFilterConfigurationTests
    {
        private readonly TestFixture testFixture;

        public ProfileStubEntryFilterConfigurationTests()
        {
            testFixture = new TestFixture();
        }

        [Test]
        public async Task GetBlacklistedProfileTemplates_SettingExist_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.BlacklistedProfileTemplates = new List<string> { "test" };

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var target = new ProfileStubEntryFilterConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.BlacklistedProfileTemplates;
            result.Should().NotBeEmpty();   
        }

        [Test]
        public async Task GetBlacklistedProfileTemplates_SettingIsEmpty_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.BlacklistedProfileTemplates = new List<string> { };

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);
            var target = new ProfileStubEntryFilterConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.BlacklistedProfileTemplates;
            result.Should().BeEmpty();
        }

        [Test]
        public async Task GetExcludedProfileTemplates_SettingExist_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.ExcludedProfileTemplates = new List<string> { "test" };

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var target = new ProfileStubEntryFilterConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.ExcludedProfileTemplates;
            result.Should().NotBeEmpty();
        }

        [Test]
        public async Task GetExcludedProfileTemplates_SettingIsEmpty_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.ExcludedProfileTemplates = new List<string> { };

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);
            var target = new ProfileStubEntryFilterConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.ExcludedProfileTemplates;
            result.Should().BeEmpty();
        }



        [Test]
        public async Task GetSettingFromBaseClass_ProviderIsNull_Test()
        {
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync((ISettingsProvider)null);
            var target = new ProfileStubEntryFilterConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.GetSettingAsync<string>(SettingKeys.ExcludedProfileTemplatesKey, null);
            result.Should().BeNull();
        }

        private class TestFixture
        {
            public readonly Mock<ISettingsProviderFactory> settingsProviderFactory;
            public readonly Mock<ISettingsProvider> settingsProvider;
            public readonly Mock<IApplicationSettings> applicationSettings;
            public readonly Mock<IServiceConfigurationStore> serviceConfigurationStore;

            public TestFixture()
            {
                settingsProviderFactory = new Mock<ISettingsProviderFactory>();
                settingsProvider = new Mock<ISettingsProvider>();
                applicationSettings = new Mock<IApplicationSettings>();
                serviceConfigurationStore = new Mock<IServiceConfigurationStore>();
            }
        }
    }
}

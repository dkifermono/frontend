﻿using FluentAssertions;
using m2trust.Frontend.Common;
using m2trust.Frontend.Web.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using m2trust.Settings;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;

namespace m2trust.Frontend.Tests.Unit.Configurations
{
    public class ProfileStateFilterConfigurationTests
    {
        private readonly TestFixture testFixture;

        public ProfileStateFilterConfigurationTests()
        {
            testFixture = new TestFixture();
        }

        [Test]
        public async Task GetShowDisabledProfiles_SettingIsTrue_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.ShowDisabledProfiles = true;

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var target = new ProfileStateFilterConfiguration(testFixture.settingsProviderFactory.Object, testFixture.serviceConfigurationStore.Object);

            var result = await target.ShowDisabledProfiles;
            result.Should().BeTrue();
        }

        [Test]
        public async Task GetShowDisabledProfiles_SettingDoesntExist_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.ShowDisabledProfiles = false;

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);
            var target = new ProfileStateFilterConfiguration(testFixture.settingsProviderFactory.Object, testFixture.serviceConfigurationStore.Object);

            var result = await target.ShowDisabledProfiles;
            result.Should().Be(default(bool));
        }

        private class TestFixture
        {
            public readonly Mock<ISettingsProviderFactory> settingsProviderFactory;
            public readonly Mock<ISettingsProvider> settingsProvider;
            public readonly Mock<IApplicationSettings> applicationSettings;
            public readonly Mock<IServiceConfigurationStore> serviceConfigurationStore;

            public TestFixture()
            {
                settingsProviderFactory = new Mock<ISettingsProviderFactory>();
                settingsProvider = new Mock<ISettingsProvider>();
                applicationSettings = new Mock<IApplicationSettings>();
                serviceConfigurationStore = new Mock<IServiceConfigurationStore>();
            }
        }
    }
}

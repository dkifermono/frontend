﻿using FluentAssertions;
using m2trust.Frontend.Common;
using m2trust.Frontend.Web.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using m2trust.Settings;
using Moq;
using Moq.Protected;
using NUnit.Framework;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;

namespace m2trust.Frontend.Tests.Unit.Configurations
{
    public class ProfileConfigurationTests
    {

        private readonly TestFixture testFixture;

        public ProfileConfigurationTests()
        {
            testFixture = new TestFixture();
        }


        [Test]
        public async Task GetSelectDefaultProfile_SettingIsTrue_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.SelectDefaultProfile = true;

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var target = new ProfileConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.SelectDefaultProfile;
            result.Should().BeTrue();
        }

        [Test]
        public async Task GetSelectDefaultProfile_SettingDoesntExist_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.SelectDefaultProfile = false;
            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);
            var target = new ProfileConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.SelectDefaultProfile;
            result.Should().Be(default(bool));
        }

        private class TestFixture
        {
            public readonly Mock<ISettingsProviderFactory> settingsProviderFactory;
            public readonly Mock<ISettingsProvider> settingsProvider;
            public readonly Mock<IApplicationSettings> applicationSettings;
            public readonly Mock<IServiceConfigurationStore> serviceConfigurationStore;

            public TestFixture()
            {
                settingsProviderFactory = new Mock<ISettingsProviderFactory>();
                settingsProvider = new Mock<ISettingsProvider>();
                applicationSettings = new Mock<IApplicationSettings>();
                serviceConfigurationStore = new Mock<IServiceConfigurationStore>();
            }
        }
    }
}

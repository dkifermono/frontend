﻿using FluentAssertions;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using m2trust.Frontend.Web.Configuration.AppConfiguration.Keys;
using m2trust.Messaging.Settings;
using m2trust.Settings.Service;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Threading.Tasks;
using m2trust.Messaging.Configuration.API;
using Orleans;

namespace m2trust.Frontend.Tests.Unit.Configurations
{
    [TestFixture]
    public class SettingsProviderFactoryTests
    {
        private readonly TestFixture testFixture;
        
        public SettingsProviderFactoryTests()
        {
            testFixture = new TestFixture();
        }
        
        [Test]
        public async Task Create_AppAndUserKeyExist_Test()
        {
            testFixture.appKeyProvider.Setup(p => p.GetKeyAsync()).ReturnsAsync("appKeyTest");
            testFixture.userKeyProvider.Setup(p => p.GetKeyAsync()).ReturnsAsync("userKeyTest");
            var target = new SettingsProviderFactory(testFixture.appKeyProvider.Object, testFixture.userKeyProvider.Object, testFixture.settingsStore.Object);
            var result = await target.CreateAsync();
            result.Should().BeOfType(typeof(ConfigurationProvider));
        }

        [Test]
        public void Create_AppKeyIsNull_Test()
        {
            testFixture.appKeyProvider.Setup(p => p.GetKeyAsync()).ReturnsAsync((string)null);
            var target = new SettingsProviderFactory(testFixture.appKeyProvider.Object, testFixture.userKeyProvider.Object, testFixture.settingsStore.Object);
            Func<Task> result = async () => await target.CreateAsync();
            result.Should().Throw<Exception>();
        }

        [Test]
        public void  Create_UserKeyIsNull_Test()
        {
            testFixture.userKeyProvider.Setup(p => p.GetKeyAsync()).ReturnsAsync((string)null);
            var target = new SettingsProviderFactory(testFixture.appKeyProvider.Object, testFixture.userKeyProvider.Object, testFixture.settingsStore.Object);
            Func<Task> result = async () => await target.CreateAsync();
            result.Should().Throw<Exception>();
        }
       
        private class TestFixture
        {
            public readonly Mock<IUserKeyProvider> userKeyProvider;
            public readonly Mock<IAppKeyProvider> appKeyProvider;
            public readonly Mock<IConfigurationStore> settingsStore;
            public readonly Mock<IGrainFactory> grainFactory;

            public TestFixture()
            {
                settingsStore = new Mock<IConfigurationStore>();
                userKeyProvider = new Mock<IUserKeyProvider>();
                appKeyProvider = new Mock<IAppKeyProvider>();
                grainFactory = new Mock<IGrainFactory>();

                grainFactory.Setup(x => x.GetGrain<IConfigurationStore>(It.IsAny<string>(), It.IsAny<string>()))
                    .Returns(this.settingsStore.Object);
            }
        }
    }
}

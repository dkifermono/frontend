﻿using FluentAssertions;
using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration.Models;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Web.Configuration;
using m2trust.Messaging.Configuration.API;
using m2trust.Settings;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;

namespace m2trust.Frontend.Tests.Unit.Configurations
{
    public class PageTextConfigurationTests
    {
        private readonly TestFixture testFixture;

        public PageTextConfigurationTests()
        {
            testFixture = new TestFixture();
        }

        [Test]
        public async Task GetWelcomeText_SettingExist_Test()
        {
            var item = new PageTextItem()
            {
                En = "en",
                De = "de"
            };

            var config = new Common.Configuration.Models.Configuration();
            config.WelcomeText = item;

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var result = await testFixture.target.WelcomeText;
            result.Should().NotBeNull().And.BeOfType(typeof(PageTextItem));
        }

        [Test]
        public async Task GetWelcomeText_SettingIsNotSet_Test()
        {
            var config = new Common.Configuration.Models.Configuration();

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var result = await testFixture.target.WelcomeText;
            result.Should().NotBeNull().And.BeSameAs(config.WelcomeText);
        }

        [Test]
        public async Task GetFooterText_SettingExist_Test()
        {
            var item = new PageTextItem()
            {
                En = "en",
                De = "de"
            };

            var config = new Common.Configuration.Models.Configuration();
            config.FooterText = item;

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var result = await testFixture.target.FooterText;
            result.Should().NotBeNull().And.BeSameAs(item);
        }

        [Test]
        public async Task GetFooterText_SettingIsNotSet_Test()
        {
            var config = new Common.Configuration.Models.Configuration();

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var result = await testFixture.target.FooterText;
            result.Should().NotBeNull().And.BeSameAs(config.FooterText);
        }

        [Test]
        public async Task GetHeaderText_SettingExist_Test()
        {
            var item = new PageTextItem()
            {
                En = "en",
                De = "de"
            };

            var config = new Common.Configuration.Models.Configuration();
            config.HeaderText= item;

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var result = await testFixture.target.HeaderText;
            result.Should().NotBeNull().And.BeSameAs(item);
        }

        [Test]
        public async Task GetHeaderText_SettingIsNull_Test()
        {
            var config = new Common.Configuration.Models.Configuration();

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var result = await testFixture.target.HeaderText;
            result.Should().NotBeNull().And.BeSameAs(config.HeaderText);
        }

        [Test]
        public async Task GetContactAddress_SettingExist_Test()
        {
            var item = new PageTextItem()
            {
                En = "en",
                De = "de"
            };

            var config = new Common.Configuration.Models.Configuration();
            config.ContactAddressText = item;

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var result = await testFixture.target.ContactAddress;
            result.Should().NotBeNull().And.BeSameAs(item);
        }

        [Test]
        public async Task GetContactAddress_SettingIsNull_Test()
        {
            var config = new Common.Configuration.Models.Configuration();

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var result = await testFixture.target.ContactAddress;
            result.Should().NotBeNull().And.BeSameAs(config.ContactAddressText);
        }

        [Test]
        public async Task GetHelpDeskInstruction_SettingExist_Test()
        {
            var item = new PageTextItem()
            {
                En = "en",
                De = "de"
            };

            var config = new Common.Configuration.Models.Configuration();
            config.HelpDeskInstruction = item;

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var result = await testFixture.target.HelpDeskInstruction;
            result.Should().NotBeNull().And.BeSameAs(item);
        }

        [Test]
        public async Task GetHelpDeskInstruction_SettingIsNull_Test()
        {
            var config = new Common.Configuration.Models.Configuration();

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var result = await testFixture.target.HelpDeskInstruction;
            result.Should().NotBeNull().And.BeSameAs(config.HelpDeskInstruction);
        }


        private class TestFixture
        {
            public readonly Mock<ISettingsProviderFactory> settingsProviderFactory;
            public readonly Mock<ISettingsProvider> settingsProvider;
            public readonly Mock<IApplicationSettings> applicationSettings;
            public readonly PageTextConfiguration target;

            public TestFixture()
            {
                settingsProviderFactory = new Mock<ISettingsProviderFactory>();
                settingsProvider = new Mock<ISettingsProvider>();
                applicationSettings = new Mock<IApplicationSettings>();
                var serviceConfigurationStore = new Mock<IServiceConfigurationStore>();

                target = new PageTextConfiguration(settingsProviderFactory.Object, serviceConfigurationStore.Object);
            }
        }
    }
}

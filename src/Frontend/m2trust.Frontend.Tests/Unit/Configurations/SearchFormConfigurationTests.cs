﻿using FluentAssertions;
using m2trust.Frontend.Common;
using m2trust.Frontend.Web.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using m2trust.Settings;
using Moq;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;

namespace m2trust.Frontend.Tests.Unit.Configurations
{
    public class SearchFormConfigurationTests
    {
        private readonly TestFixture testFixture; 

        public SearchFormConfigurationTests()
        {
            testFixture = new TestFixture();
        }

        [Test]
        public async Task GetSearchUsersPageSize_SettingExist_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.SearchUsersPageSize = 10;

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);
            var target = new SearchFormConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.SearchUsersPageSize;
            result.Should().Be(10);
        }

        [Test]
        public async Task GetSearchUsersPageSize_SettingDoesntExist_Test()
        {
            var config = new Common.Configuration.Models.Configuration();

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);
            var target = new SearchFormConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.SearchUsersPageSize;
            result.Should().Be(config.SearchUsersPageSize);
        }

        [Test]
        public async Task GetUserDisplayProperties_SettingExist_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.UserDisplayProperties = new List<string> { "test" };

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);
            var target = new SearchFormConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.UserDisplayProperties;
            result.Should().NotBeEmpty();
        }

        [Test]
        public async Task GetUserDisplayProperties_SettingIsNull_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.UserDisplayProperties = new List<string> {};

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var target = new SearchFormConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.UserDisplayProperties;
            result.Should().BeEmpty();
        }

        [Test]
        public async Task GetUserSearchProperties_SettingExist_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.UserSearchProperties = new List<string> { "test" };

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);
            var target = new SearchFormConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.UserSearchProperties;
            result.Should().NotBeEmpty();
        }

        [Test]
        public async Task GetUserSearchProperties_SettingIsNull_Test()
        {
            var config = new Common.Configuration.Models.Configuration();
            config.UserSearchProperties = new List<string> { };

            testFixture.applicationSettings.Setup(a => a.ReadSetting(SettingKeys.FrontendConfigKey, It.IsAny<Common.Configuration.Models.Configuration>())).Returns(config);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);

            var target = new SearchFormConfiguration(testFixture.settingsProviderFactory.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = await target.UserSearchProperties;
            result.Should().BeEmpty();
        }

        private class TestFixture
        {
            public readonly Mock<ISettingsProviderFactory> settingsProviderFactory;
            public readonly Mock<ISettingsProvider> settingsProvider;
            public readonly Mock<IApplicationSettings> applicationSettings;
            public readonly Mock<IServiceConfigurationStore> serviceConfigurationStore;

            public TestFixture()
            {
                settingsProviderFactory = new Mock<ISettingsProviderFactory>();
                settingsProvider = new Mock<ISettingsProvider>();
                applicationSettings = new Mock<IApplicationSettings>();
                serviceConfigurationStore = new Mock<IServiceConfigurationStore>();
            }
        }
    }
}

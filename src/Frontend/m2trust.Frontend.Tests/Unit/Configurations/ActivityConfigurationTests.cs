﻿using FluentAssertions;
using m2trust.Frontend.Common;
using m2trust.Frontend.Web.Configuration;
using m2trust.Frontend.Web.Configuration.AppConfiguration;
using m2trust.Settings;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using m2trust.Frontend.Common.Configuration.Models;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Configuration.API;
using Microsoft.Extensions.Options;

namespace m2trust.Frontend.Tests.Unit.Configurations
{
    [TestFixture]
    public class ActivityConfigurationTests
    {
        private readonly TestFixture testFixture;

        public ActivityConfigurationTests()
        {
            this.testFixture = new TestFixture();
        }

        [Test]
       public void GetActivitiesAsync_ActivitesConfigured_Test()
       {
                    
            testFixture.applicationSettings.Setup(a => a.ReadSetting<Activity>(SettingKeys.Activities, null)).Returns(this.testFixture.permission);
            testFixture.settingsProvider.SetupGet(p => p.ApplicationSettings).Returns(testFixture.applicationSettings.Object);
            testFixture.settingsProviderFactory.Setup(f => f.CreateAsync()).ReturnsAsync(testFixture.settingsProvider.Object);
            var configuration = new ActivityConfiguration(this.testFixture.settingsProviderFactory.Object, this.testFixture.administrationConfig.Object, this.testFixture.serviceConfigurationStore.Object);

            var result = configuration.GetActivitiesAsync();
            result.Should().NotBeNull();
            
       }

        private class TestFixture
        {
            public readonly Mock<ISettingsProviderFactory> settingsProviderFactory;
            public readonly Mock<ISettingsProvider> settingsProvider;
            public readonly Mock<IApplicationSettings> applicationSettings;
            public readonly Activity permission;
            public readonly Mock<IOptions<AdministrationConfig>> administrationConfig;
            public readonly Mock<IServiceConfigurationStore> serviceConfigurationStore;


            public TestFixture()
            {
                settingsProviderFactory = new Mock<ISettingsProviderFactory>();
                settingsProvider = new Mock<ISettingsProvider>();
                applicationSettings = new Mock<IApplicationSettings>();
                administrationConfig = new Mock<IOptions<AdministrationConfig>>();
                serviceConfigurationStore = new Mock<IServiceConfigurationStore>();

                this.permission = new Activity
                {
                    DisableSmartcard = new BaseActivity() { Name = "DisableSmartcard", AuthorizedGroups = new[] { "Group1" } },
                    Enroll = new BaseActivity() { Name = "Enroll", AuthorizedGroups = new[] { "Group1" } }
                };
            }
        }
    }
}

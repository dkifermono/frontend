﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Service.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service
{
    public class ReinstateDCIConfigurationManager : IReinstateDCIConfigurationManager
    {
        //TODO Check for Johanness answer
        //public ReinstateDCIConfigurationManager(string configData)
        //{
        //    ProfileTemplatesConfig = CreateProfileTemplatesConfig(configData);
        //}

        private readonly IReinstateDCIConfiguration configuration;

        #region Constructors

        public ReinstateDCIConfigurationManager(IReinstateDCIConfiguration configuration)
        {
            this.configuration = configuration;
        }

        #endregion Constructors

        //public ProfileTemplatesConfig ProfileTemplatesConfig { get; set; }
        //protected string ConfigData { get; set; }

        #region Methods

        public async Task<IDictionary<string, string>> GetDCIs(Guid profileTemplateId)
        {
            var profileTemplates = await configuration.DCIProfileTemplates;
            var profileTemplateConfig = profileTemplates.FirstOrDefault(t => t.ProfileTemplateId == profileTemplateId);
            if (profileTemplateConfig == null)
                throw new Exception(String.Format("Could not find configuration for template {0}.", profileTemplateId));

            var result = profileTemplateConfig.ReinstateDCIs.ToDictionary(dci => dci.Name, dci => dci.Value);
            return result;
        }

        #endregion Methods
    }
}
﻿using AutoMapper;
using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.OpenFim.API;
using m2trust.Messaging.OpenFim.Entities;
using m2trust.Messaging.OpenFim.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service
{
    public class SigningService : ISigningService
    {
        #region Fields

        private readonly ISignatureConfiguration configuration;
        private readonly ICertificateManagerApi certificateApi;
        private readonly ILogWriter logger;
        private readonly IMapper mapper;

        #endregion

        #region Contructors

        public SigningService(
            ICertificateManagerApi certificateApi,
            ILogWriter logger,
            IMapper mapper, 
            ISignatureConfiguration configuration)
        {            
            this.certificateApi = certificateApi;
            this.logger = logger;
            this.mapper = mapper;
            this.configuration = configuration;
        }

        #endregion

        #region Methods

        public async Task CheckSigningCertificateAsync(AuditRequestEntry request)
        {
            var dciName = await configuration.SignatureDataCollectionItemName;

            // The signature data collection item fields isn't specified in the policy
            if (request.DataCollectionItems == null ||
                !request.DataCollectionItems.ContainsKey(dciName))
            {
                return;
            }

            var hasSignature = request.DataCollectionItems != null &&
                               request.DataCollectionItems.ContainsKey(dciName) &&
                               request.DataCollectionItems[dciName].Length > 0;

            if (!hasSignature)
            {
                await logger.WriteErrorAsync("The signature does not exist or is not valid.");
                throw new Exception("SignatureNotFound");
            }

            var signatureString = request.DataCollectionItems[dciName];

            var signutureStatus = await certificateApi.ComputeSignatureStatusAsync(
                new ProfileTemplateRequest()
                {
                    UserId = request.UserId,
                    Comment = request.Comment,
                    DataCollectionItems = request.DataCollectionItems,
                    Id = request.Id,
                    OriginatorId =  request.OriginatorId,
                    Priority = request.Priority,
                    ProfileId = request.ProfileId,
                    ProfileTemplateId = request.ProfileTemplateId,
                },
                null
            ).ConfigureAwait(false);

            if (signutureStatus == SignatureStatus.ValidSignature) return;

            var statusName = typeof(SignatureStatus).GetEnumName(signutureStatus);
            await logger.WriteErrorAsync($"Invalid signature. Result: {statusName}");

            throw InvalidCertificateException(signutureStatus);
        }

        private Exception InvalidCertificateException(SignatureStatus status)
        {
            var exceptionMessage = string.Empty;

            switch (status)
            {
                case SignatureStatus.ExpiredCert:
                    exceptionMessage = "The signing certificate is expired and cannot be used.";
                    break;

                case SignatureStatus.InvalidCert:
                    exceptionMessage = "The signing certificate is invalid and cannot be used.";
                    break;

                case SignatureStatus.InvalidHash:
                    exceptionMessage = "The signature hash is invalid.";
                    break;

                case SignatureStatus.NoCert:
                    exceptionMessage = "No valid signing certificate found on your machine.";
                    break;

                case SignatureStatus.NoTrustedRoot:
                    exceptionMessage = "No trusted root certificates were provided.";
                    break;

                case SignatureStatus.RevokedCert:
                    exceptionMessage = "The signing certificate is revoked and cannot be used.";
                    break;

                case SignatureStatus.Unsigned:
                    exceptionMessage = "No valid signing certificate found on your machine.";
                    break;

                case SignatureStatus.UserNameMismatch:
                    exceptionMessage = "The user name does not match with the certificate owner.";
                    break;
            }
            throw new Exception(exceptionMessage);
        }
        #endregion
    }
}

﻿using AutoMapper;
using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Logging.API;
using m2trust.Messaging.Logging.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using m2trust.Messaging.Logging.Enumerations;
using PagedList.Core;

namespace m2trust.Frontend.Service
{
    /// <summary>
    /// Logging service.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Service.Common.ILoggingService" />
    public class LoggingService : ServiceBase, ILoggingService
    {
        #region Fields

        private readonly ILogProvider logProvider;

        private readonly IMapper mapper;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingService" /> class.
        /// </summary>
        /// <param name="logProvider">The log provider.</param>
        public LoggingService(ILogProvider logProvider, IMapper mapper)
        {
            this.logProvider = logProvider ?? throw new ArgumentNullException(nameof(logProvider));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Asynchronously finds log entries.
        /// </summary>
        /// <param name="filterOptions">Filtering options.</param>
        /// <param name="paging">Paging options.</param>
        /// <returns></returns>
        public async Task<IPagedList<Log>> GetEntriesAsync(LoggingFilterParameters filterOptions, PagingParameters paging)
        {
            int skip = paging.PageSize * (paging.PageNumber - 1);
            var filter = new LogFilter
            {
                MinimumDate = filterOptions.From,
                MaximumDate = filterOptions.To,
                Contains = filterOptions.SearchTerm,
                AuditLogs = filterOptions.ShowAuditLogs,
                MinimumLogLevel = (LogLevel?)filterOptions.LogLevel
            };

            var logs = await logProvider.GetPagedLogs(filter, skip, paging.PageSize).ConfigureAwait(false);
            var result = this.mapper.Map<IEnumerable<Log>>(logs);
            return new StaticPagedList<Log>(result, paging.PageNumber, paging.PageSize, logs.TotalCount);
        }

        #endregion Methods
    }
}
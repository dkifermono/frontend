﻿using ExCSS;
using m2trust.Frontend.Service.Common;
using System;
using System.Linq;
using System.Threading.Tasks;
using m2trust.Settings;
using System.Reflection;
using System.IO;

namespace m2trust.Frontend.Service
{
    /// <summary>
    /// UI configuration service.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Service.Common.IUIConfigurationService" />
    public class UIConfigurationService : IUIConfigurationService
    {
        private readonly ISettingsProviderFactory settingsProviderFactory;

        private readonly string backgroundColorPropertyName = "background-color";
        private readonly string fontColorPropertyName = "color";
        private readonly string pageBackgroundColorStyleRuleValue = "body";
        private readonly string pageTitleBackgroundColorStyleRuleValue = ".heading";
        private readonly string pageTitleFontColorStyleRuleValue = ".heading__title";
        private readonly string selectedTableRowColorStyleRuleValue = ".table__row.is-active";
        private readonly string tableRowFontColorStyleRuleValue = ".table__row";

        #region MenuColorsRules
        private readonly string menuBackgroundColorStyleRuleValue = ".layout--aside";
        private readonly string innerMenuBackgroundColorStyleRuleValue = ".nav--secondary__list";
        private readonly string menuFontColorStyleRuleValue = ".nav--primary__link";
        private readonly string menuFontColorSecondaryStyleRuleValue = ".nav--secondary__link";

        private readonly string selectedMenuItemColorStyleRuleValue = ".nav--primary__link.active";
        private readonly string selectedMenuItemColorStyleRuleValueBefore = ".nav--primary__link.active:before";

        private readonly string selectedMenuItemColorSecondaryStyleRuleValue = ".nav--secondary__link.is-active";
        private readonly string selectedMenuItemColorSecondaryStyleRuleValueBefore = ".nav--secondary__link.is-active:before";

        private readonly string menuHoverColorStyleRuleValue = ".nav--primary__link:focus,.nav--primary__link:hover";
        private readonly string menuHoverColorSecondaryStyleRuleValue = ".nav--secondary__link:focus,.nav--secondary__link:hover";
        #endregion MenuColorsRules


        private static readonly string CSS_SETTINGS_KEY = nameof(UIConfigurationService) + ".CSS_SETTINGS_KEY";
        private static readonly string COLOR_SETTINGS_KEY = nameof(UIConfigurationService) + ".COLOR_SETTINGS_KEY";
        private static readonly string LOGO_SETTINGS_KEY = nameof(UIConfigurationService) + ".LOGO_SETTINGS_KEY";

        private readonly string variablesResourceName = "m2trust.Frontend.Web.Resources.defaultVariables.css";
        private readonly string logoResourceName = "m2trust.Frontend.Web.Resources.defaultLogo.png";

        public UIConfigurationService(ISettingsProviderFactory settingsProviderFactory)
        {
            this.settingsProviderFactory = settingsProviderFactory;
        }

        /// <summary>
        /// Gets the colors.
        /// </summary>
        /// <returns></returns>
        public async Task<Model.Colors> GetColorsAsync()
        {
            var result = new Model.Colors();

            var css = await GetCss();


            var parser = new Parser();
            var stylesheet = parser.Parse(css);
            if (stylesheet == null)
            {
                throw new Exception("Couldn't parse stylesheet");
            }

            var pageBackgroundColorProperty = await GetPropertyAsync(stylesheet, pageBackgroundColorStyleRuleValue, backgroundColorPropertyName);
            result.PageBackgroundColor = (pageBackgroundColorProperty.Term as HtmlColor)?.ToString();

            var pageTitleBackgroundColorProperty = await GetPropertyAsync(stylesheet, pageTitleBackgroundColorStyleRuleValue, backgroundColorPropertyName);
            result.PageTitleBackgroundColor = (pageTitleBackgroundColorProperty.Term as HtmlColor)?.ToString();

            var pageTitleFontColorProperty = await GetPropertyAsync(stylesheet, pageTitleFontColorStyleRuleValue, fontColorPropertyName);
            result.PageTitleFontColor = (pageTitleFontColorProperty.Term as HtmlColor)?.ToString();

            var menuBackgroundProperty = await GetPropertyAsync(stylesheet, menuBackgroundColorStyleRuleValue, backgroundColorPropertyName);
            result.MenuBackgroundColor = (menuBackgroundProperty.Term as HtmlColor)?.ToString();

            var innerMenuBackgroundColorProperty = await GetPropertyAsync(stylesheet, innerMenuBackgroundColorStyleRuleValue, backgroundColorPropertyName);
            result.InnerMenuBackgroundColor = (innerMenuBackgroundColorProperty.Term as HtmlColor)?.ToString();

            var menuFontColorProperty = await GetPropertyAsync(stylesheet, menuFontColorStyleRuleValue, fontColorPropertyName);
            result.MenuFontColor = (menuFontColorProperty.Term as HtmlColor)?.ToString();

            var selectedMenuItemBackgroundColorProperty = await GetPropertyAsync(stylesheet, selectedMenuItemColorStyleRuleValue, backgroundColorPropertyName);
            result.SelectedMenuItemBackgroundColor = (selectedMenuItemBackgroundColorProperty.Term as HtmlColor)?.ToString();

            var selectedMenuItemFontColorProperty = await GetPropertyAsync(stylesheet, selectedMenuItemColorStyleRuleValue, fontColorPropertyName);
            result.SelectedMenuItemFontColor = (selectedMenuItemFontColorProperty.Term as HtmlColor)?.ToString();

            var hoverMenuFontColorProperty = await GetPropertyAsync(stylesheet, menuHoverColorStyleRuleValue, fontColorPropertyName);
            result.MenuHoverFontColor = (hoverMenuFontColorProperty.Term as HtmlColor)?.ToString();

            var tableRowFontColorProperty = await GetPropertyAsync(stylesheet, tableRowFontColorStyleRuleValue, fontColorPropertyName);
            result.TableRowFontColor = (tableRowFontColorProperty.Term as HtmlColor)?.ToString();

            var selectedTableRowBackgroundColorProperty = await GetPropertyAsync(stylesheet, selectedTableRowColorStyleRuleValue, backgroundColorPropertyName);
            result.SelectedTableRowBackgroundColor = (selectedTableRowBackgroundColorProperty.Term as HtmlColor)?.ToString();

            var selectedTableRowFontColorProperty = await GetPropertyAsync(stylesheet, selectedTableRowColorStyleRuleValue, fontColorPropertyName);
            result.SelectedTableRowFontColor = (selectedTableRowFontColorProperty.Term as HtmlColor)?.ToString();

            return result;
        }

        public Task<string> GetColorVariablesAsync()
        {
            return GetCss();
        }

        public async Task<string> GetLogoAsync()
        {
            string defaultLogoBase64;

            var assembly = Assembly.GetEntryAssembly();

            using (Stream stream = assembly.GetManifestResourceStream(logoResourceName))
            using (BinaryReader reader = new BinaryReader(stream))
            {
                long length = stream.Length;
                byte[] buffer = reader.ReadBytes((int)length);
                defaultLogoBase64 = Convert.ToBase64String(buffer);
            }
            
            var prov = await settingsProviderFactory.CreateAsync();

            string logo = prov
                .ApplicationSettings
                .ReadSetting(LOGO_SETTINGS_KEY, defaultLogoBase64);
            return logo;
        }


        /// <summary>
        /// Writes the colors asynchronous.
        /// </summary>
        /// <param name="colors">The colors.</param>
        /// <returns></returns>
        public async Task<bool> WriteColorsAsync(Model.Colors colors)
        {
            if (colors == null)
            {
                return false;
            }
            bool parseResult;
            string css = await GetCss();
            HtmlColor newColor = null;

            var parser = new Parser();
            var stylesheet = parser.Parse(css);
            Property pageBackgroundColorProperty;
            if (!String.IsNullOrWhiteSpace(colors.PageBackgroundColor))
            {
                pageBackgroundColorProperty = await GetPropertyAsync(stylesheet, pageBackgroundColorStyleRuleValue, backgroundColorPropertyName);
                parseResult = HtmlColor.TryFromHex(colors.PageBackgroundColor.Substring(1), out newColor);
                if (!parseResult)
                {
                    return false;
                }
                pageBackgroundColorProperty.Term = newColor;
            }

            if (!String.IsNullOrWhiteSpace(colors.PageTitleBackgroundColor))
            {
                var pageTitleBackgroundColorProperty = await GetPropertyAsync(stylesheet, pageTitleBackgroundColorStyleRuleValue, backgroundColorPropertyName);
                parseResult = HtmlColor.TryFromHex(colors.PageTitleBackgroundColor.Substring(1), out newColor);
                if (!parseResult)
                {
                    return false;
                }
                pageTitleBackgroundColorProperty.Term = newColor;
            }

            if (!String.IsNullOrWhiteSpace(colors.PageTitleFontColor))
            {
                var pageTitleFontColorProperty = await GetPropertyAsync(stylesheet, pageTitleFontColorStyleRuleValue, fontColorPropertyName);
                parseResult = HtmlColor.TryFromHex(colors.PageTitleFontColor.Substring(1), out newColor);
                if (!parseResult)
                {
                    return false;
                }
                pageTitleFontColorProperty.Term = newColor;
            }

            if (!String.IsNullOrWhiteSpace(colors.MenuBackgroundColor))
            {
                var menuBackgroundProperty = await GetPropertyAsync(stylesheet, menuBackgroundColorStyleRuleValue, backgroundColorPropertyName);
                parseResult = HtmlColor.TryFromHex(colors.MenuBackgroundColor.Substring(1), out newColor);
                if (!parseResult)
                {
                    return false;
                }
                menuBackgroundProperty.Term = newColor;
            }

            if (!String.IsNullOrWhiteSpace(colors.InnerMenuBackgroundColor))
            {
                var innerMenuBackgroundProperty = await GetPropertyAsync(stylesheet, innerMenuBackgroundColorStyleRuleValue, backgroundColorPropertyName);
                parseResult = HtmlColor.TryFromHex(colors.InnerMenuBackgroundColor.Substring(1), out newColor);
                if (!parseResult)
                {
                    return false;
                }
                innerMenuBackgroundProperty.Term = newColor;
            }

            if (!String.IsNullOrWhiteSpace(colors.MenuFontColor))
            {
                var menuFontColorProperty = await GetPropertyAsync(stylesheet, menuFontColorStyleRuleValue, fontColorPropertyName);
                parseResult = HtmlColor.TryFromHex(colors.MenuFontColor.Substring(1), out newColor);
                if (!parseResult)
                {
                    return false;
                }
                menuFontColorProperty.Term = newColor;
                var menuFontColorSecondaryProperty = await GetPropertyAsync(stylesheet, menuFontColorSecondaryStyleRuleValue, fontColorPropertyName);
                menuFontColorSecondaryProperty.Term = newColor;
            }

            if (!String.IsNullOrWhiteSpace(colors.MenuHoverFontColor))
            {
                var menuHoverFontColorProperty = await GetPropertyAsync(stylesheet, menuHoverColorStyleRuleValue, fontColorPropertyName);
                parseResult = HtmlColor.TryFromHex(colors.MenuHoverFontColor.Substring(1), out newColor);
                if (!parseResult)
                {
                    return false;
                }
                menuHoverFontColorProperty.Term = newColor;
                var menuHoverColorSecondaryProperty = await GetPropertyAsync(stylesheet, menuHoverColorSecondaryStyleRuleValue, fontColorPropertyName);
                menuHoverColorSecondaryProperty.Term = newColor;
            }

            if (!String.IsNullOrWhiteSpace(colors.SelectedMenuItemBackgroundColor))
            {
                var selectedMenuItemBackgroundColorProperty = await GetPropertyAsync(stylesheet, selectedMenuItemColorStyleRuleValue, backgroundColorPropertyName);
                parseResult = HtmlColor.TryFromHex(colors.SelectedMenuItemBackgroundColor.Substring(1), out newColor);
                if (!parseResult)
                {
                    return false;
                }
                selectedMenuItemBackgroundColorProperty.Term = newColor;
                var selectedMenuItemSecondaryBackgroundColorProperty = await GetPropertyAsync(stylesheet, selectedMenuItemColorSecondaryStyleRuleValue, backgroundColorPropertyName);
                selectedMenuItemSecondaryBackgroundColorProperty.Term = newColor;


            }

            if (!String.IsNullOrWhiteSpace(colors.SelectedMenuItemFontColor))
            {
                var selectedMenuItemFontColorProperty = await GetPropertyAsync(stylesheet, selectedMenuItemColorStyleRuleValue, fontColorPropertyName);
                parseResult = HtmlColor.TryFromHex(colors.SelectedMenuItemFontColor.Substring(1), out newColor);
                if (!parseResult)
                {
                    return false;
                }
                selectedMenuItemFontColorProperty.Term = newColor;
                var selectedMenuItemSecondaryFontColorProperty = await GetPropertyAsync(stylesheet, selectedMenuItemColorSecondaryStyleRuleValue, fontColorPropertyName);
                selectedMenuItemSecondaryFontColorProperty.Term = newColor;

                var selectedMenuItemFontColorBeforeProperty = await GetPropertyAsync(stylesheet, selectedMenuItemColorStyleRuleValueBefore, backgroundColorPropertyName);
                selectedMenuItemFontColorBeforeProperty.Term = newColor;
                var selectedMenuItemSecondaryFontColorBeforeProperty = await GetPropertyAsync(stylesheet, selectedMenuItemColorSecondaryStyleRuleValueBefore, backgroundColorPropertyName);
                selectedMenuItemSecondaryFontColorBeforeProperty.Term = newColor;
            }

            if (!String.IsNullOrWhiteSpace(colors.TableRowFontColor))
            {
                var tableRowFontColorProperty = await GetPropertyAsync(stylesheet, tableRowFontColorStyleRuleValue, fontColorPropertyName);
                parseResult = HtmlColor.TryFromHex(colors.TableRowFontColor.Substring(1), out newColor);
                if (!parseResult)
                {
                    return false;
                }
                tableRowFontColorProperty.Term = newColor;
            }

            if (!String.IsNullOrWhiteSpace(colors.SelectedTableRowBackgroundColor))
            {
                var selectedTableRowBackgroundColorProperty = await GetPropertyAsync(stylesheet, selectedTableRowColorStyleRuleValue, backgroundColorPropertyName);
                parseResult = HtmlColor.TryFromHex(colors.SelectedTableRowBackgroundColor.Substring(1), out newColor);
                if (!parseResult)
                {
                    return false;
                }
                selectedTableRowBackgroundColorProperty.Term = newColor;
            }

            if (!String.IsNullOrWhiteSpace(colors.SelectedTableRowFontColor))
            {
                var selectedTableRowFontColorProperty = await GetPropertyAsync(stylesheet, selectedTableRowColorStyleRuleValue, fontColorPropertyName);
                parseResult = HtmlColor.TryFromHex(colors.SelectedTableRowFontColor.Substring(1), out newColor);
                if (!parseResult)
                {
                    return false;
                }
                selectedTableRowFontColorProperty.Term = newColor;
            }

            await SaveCss(stylesheet.ToString());

            return true;
        }

        public async Task<bool> WriteLogoAsync(string logo)
        {
            var prov = await settingsProviderFactory.CreateAsync();

            var appSettings = prov
                .ApplicationSettings
                .WriteSetting(LOGO_SETTINGS_KEY, logo);

            await prov.SaveApplicationSettings(appSettings);
            return true;
        }

        private Task<Property> GetPropertyAsync(StyleSheet stylesheet, string styleRuleValue, string propertyName)
        {
            var styleRule = stylesheet.StyleRules.FirstOrDefault(r => r.Value == styleRuleValue);
            if (styleRule != null)
            {
                var property = styleRule.Declarations.Properties.FirstOrDefault(p => p.Name == propertyName);
                if (property != null)
                {
                    return Task.FromResult(property);
                }
                else
                {
                    throw new ArgumentNullException($"{styleRuleValue} : {propertyName}");
                }
            }
            else
            {
                throw new ArgumentNullException(styleRuleValue);
            }
        }

        private async Task<string> GetCss()
        {
            var defaultCss = await GetDefaultCss();
            var prov = await settingsProviderFactory.CreateAsync();
            var css = prov
                .ApplicationSettings
                .ReadSetting(CSS_SETTINGS_KEY, defaultCss);
            return css;
        }

        private async Task<string> GetDefaultCss()
        {
            string defaultCss;
            var assembly = Assembly.GetEntryAssembly();
            using (Stream stream = assembly.GetManifestResourceStream(variablesResourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                defaultCss = await reader.ReadToEndAsync();
            }

            return defaultCss;
        }

        private async Task SaveCss(string css)
        {
            var prov = await settingsProviderFactory.CreateAsync();
            var appSettings = prov
                .ApplicationSettings
                .WriteSetting(CSS_SETTINGS_KEY, css);
           await prov.SaveApplicationSettings(appSettings);
        }
        
    }
}

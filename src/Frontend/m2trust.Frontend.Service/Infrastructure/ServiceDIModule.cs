﻿using Autofac;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Service.Common.Filters;
using m2trust.Frontend.Service.Filters;

namespace m2trust.Frontend.Service.Infrastructure
{
    public class ServiceDIModule : Module
    {
        #region Methods

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<LogWriter>().As<ILogWriter>();
            builder.RegisterType<LoggingService>().As<ILoggingService>();
            builder.RegisterType<RequestService>().As<IRequestService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<ProfileService>().As<IProfileService>();
            builder.RegisterType<SoftProfileStubEntryFilter>().As<ISoftProfileStubEntryFilter>();
            builder.RegisterType<ProfileStubEntryProfileStateFilter>().As<IProfileStubEntryProfileStateFilter>();
            builder.RegisterType<BlacklistedProfileTemplateProfileStubEntryFilter>().As<IBlacklistedProfileTemplateProfileStubEntryFilter>();
            builder.RegisterType<ProfileStatusFilter>().As<IProfileStatusFilter>();
            builder.RegisterType<DataCollectionItemService>().As<IDataCollectionItemService>();
            builder.RegisterType<SigningService>().As<ISigningService>();
            builder.RegisterType<RequestActionValidator>().As<IRequestActionValidator>();
            builder.RegisterType<SmartcardService>().As<ISmartcardService>();
            builder.RegisterType<ActivityService>().As<IActivityService>();
            builder.RegisterType<InMemoryCacheService>().As<ICacheService>().SingleInstance();
            builder.RegisterType<AdApiUserActivityChecker>().Named<IUserActivityChecker>("adApiChecker");
            builder.RegisterDecorator<IUserActivityChecker>((c, inner) => new CachedUserActivityChecker(inner, c.Resolve<ICacheService>()), "adApiChecker");
            builder.RegisterType<UIConfigurationService>().As<IUIConfigurationService>();
            builder.RegisterType<ReinstateDCIConfigurationManager>().As<IReinstateDCIConfigurationManager>();
            builder.RegisterType<ExcludedProfileTemplateFilter>().As<IExcludedProfileTemplateFilter>();
            builder.RegisterType<SmartcardProfileStubFilter>().As<ISmartcardProfileStubFilter>();
            builder.RegisterType<SuspendedProfileStubFilter>().As<ISuspendedProfileStubFilter>();
            builder.RegisterType<TemporarySmartcardProfileFilter>().As<ITemporarySmartcardProfileFilter>();

            builder.RegisterType<FilterFactory>().As<IFilterFactory>().SingleInstance();

        }

        #endregion Methods
    }
}
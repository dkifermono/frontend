﻿using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Model;
using m2trust.Messaging.ActiveDirectory.Entities;
using m2trust.Messaging.Logging.Entities;
using m2trust.Messaging.Logging.Enumerations;
using m2trust.Messaging.OpenFim.Entities;
using m2trust.Messaging.OpenFim.Enumerations;
using System;
using System.Collections.Generic;

namespace m2trust.Frontend.Service.Infrastructure
{
    public class ServiceMapperProfile : AutoMapper.Profile
    {
        #region Constructors

        public ServiceMapperProfile()
        {
            CreateMap<LogEntry, Log>()
                .AfterMap((src, dest) =>
                {
                    if (src.MetaData != null)
                    {
                        dest.System = src.MetaData.System;
                        dest.User = src.MetaData.User;
                    }
                });

            CreateMap<RequestFilterParameters, RequestSearchRequest>()
                .AfterMap((src, dest) =>
                {
                    dest.RequestType = (RequestType?)src.RequestForm;
                    dest.RequestStatus = (ReqStatus?)src.RequestState;
                });

            CreateMap<EventHistory, RequestEventHistory>();

            CreateMap<ProfileTemplate, ProfileTemplateEntry>();

            CreateMap<Request, RequestDetails>()
                 .AfterMap((src, dest) =>
                 {
                     dest.RequestId = src.Id;
                     dest.DateSubmitted = src.Submitted;
                     dest.DateCompleted = src.Completed;
                     dest.RequestState = (RequestState)src.Status;
                     dest.OriginatorUserName = src.OriginatorDisplayName;
                     dest.TargetForm = (TargetForm)src.TargetType;
                     dest.RequestForm = (RequestForm)src.RequestType;
                     dest.TargetUserName = src.UserDisplayName;
                     dest.TargetFormSet = src.TargetTypeSet;
                     dest.Comment = src.Comment;
                     dest.OriginatorUserId = src.OriginatorUserId;
                     dest.Priority = src.Priority;
                     dest.UserId = src.UserId;
                     dest.ProfileId = src.ProfileId;
                     dest.ProfileTemplateId = src.ProfileTemplateId;
                     dest.DataCollectionItems = src.DataCollectionItems;
                 });

            CreateMap<Request, SignedRequest>()
                .AfterMap((src, dest) =>
                {
                    dest.RequestId = src.Id;
                    dest.DateSubmitted = src.Submitted;
                    dest.DateCompleted = src.Completed;
                    dest.RequestState = (RequestState)src.Status;
                    dest.OriginatorUserName = src.OriginatorDisplayName;
                    dest.TargetForm = (TargetForm)src.TargetType;
                    dest.RequestForm = (RequestForm)src.RequestType;
                    dest.TargetUserName = src.UserDisplayName;
                    dest.TargetFormSet = src.TargetTypeSet;
                });

            CreateMap<User, UserEntry>()
                .AfterMap((src, dest) =>
                {
                    dest.Id = src.Id;
                    dest.DirectoryPath = src.DirectoryPath;
                    dest.Properties = src.Properties;
                    dest.Name = src.Name;
                    dest.ObjectClass = src.ObjectClass;
                    dest.Sid = src.Sid;
                });

            CreateMap<KeyValuePair<Guid, string>, ProfileTemplateEntry>()
                .ForMember(dest => dest.Id, m => m.MapFrom(src => src.Key))
                .ForMember(dest => dest.Name, m => m.MapFrom(src => src.Value));

            CreateMap<Policy, PolicyEntry>();
            CreateMap<TemporaryCardsPolicy, TemporaryCardsPolicyEntry>();
            CreateMap<RevokePolicy, RevokePolicyEntry>();
            CreateMap<ReinstatePolicy, ReinstatePolicyEntry>();
            CreateMap<RetirePolicy, RetirePolicyEntry>();
            CreateMap<CertificateTemplate, CertificateTemplateEntry>();
            CreateMap<SmartcardProfile, SmartcardProfileEntry>();
            CreateMap<Profile, SmartcardProfileEntry>();

            CreateMap<RequestSecret, Secret>()
                .ForMember(d => d.RequestDetails, opt => opt.MapFrom(s => s.Request));

            CreateMap<AuditRequestEntry, ProfileTemplateRequest>()
                .AfterMap((src, dest) =>
                {
                    dest.UserId = src.UserId;
                    dest.ProfileTemplateId = src.ProfileTemplateId;
                    dest.Comment = src.Comment;
                    dest.Priority = src.Priority;
                    dest.DataCollectionItems = src.DataCollectionItems;
                    dest.OriginatorId = src.OriginatorId;
                }).ReverseMap();

            CreateMap<ProfileTemplateRequest, AuditRequestEntry>()
               .AfterMap((src, dest) =>
               {
                   dest.UserId = src.UserId;
                   dest.ProfileTemplateId = src.ProfileTemplateId;
                   dest.Comment = src.Comment;
                   dest.Priority = src.Priority;
                   dest.DataCollectionItems = src.DataCollectionItems;
                   dest.OriginatorId = src.OriginatorId;
               }).ReverseMap();

            CreateMap<AuditRequestEntry, ProfileRequest>();

            CreateMap<Request, AuditRequestEntry>();
            
            CreateMap<DataCollectionItem, DataCollectionItemEntry>()
                .AfterMap((src, dest) =>
                {
                    dest.Name = src.Name;
                    dest.Description = src.Description;
                    dest.Required = src.Required;
                    dest.DataType = (DataTypeDci)src.DataType;
                    dest.ValidationType = (ValidationTypeDci)src.ValidationType;
                    dest.ValidationData = src.ValidationData;
                });

            CreateMap<ProfileStub, ProfileStubEntry>();
            CreateMap<CertificateStub, CertificateStubEntry>();
            CreateMap<CertificateStatus, CertificateState>();
            CreateMap<Profile, ProfileEntry>();
            CreateMap<Domain, DomainEntry>();
            
            CreateMap<ProfileRequestResult, AuditRequestEntry>()
                .AfterMap((src, dest) =>
                {
                    dest.ProfileId = src.BaseRequest.ProfileId;
                    dest.Id = src.BaseRequest.Id;
                    dest.Comment = src.BaseRequest.Comment;
                    dest.DataCollectionItems = src.BaseRequest.DataCollectionItems;
                    dest.Priority = src.BaseRequest.Priority;
                    dest.OriginatorId = src.BaseRequest.OriginatorId;
                });
            
            CreateMap<LogLevel, LogLevelType>().ReverseMap();

            CreateMap<SuspendResult, OtpResult>();
            CreateMap<RevokeResult, OtpResult>();
            CreateMap<RenewProfileResult, OtpResult>();
            CreateMap<RetireResult, OtpResult>();
            CreateMap<ReinstateResult, OtpResult>();
            CreateMap<SuspendResult, OtpResult>();
            CreateMap<RecoverResult, OtpResult>();
            CreateMap<DisableSmartcardResult, OtpResult>();
        }

        #endregion Constructors
    }
}
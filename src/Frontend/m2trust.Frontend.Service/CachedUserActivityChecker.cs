﻿using m2trust.Frontend.Service.Common;
using System;
using System.Threading.Tasks;
using m2trust.Frontend.Model;

namespace m2trust.Frontend.Service
{
    public class CachedUserActivityChecker : IUserActivityChecker
    {
        private readonly IUserActivityChecker userActivityChecker;
        private readonly ICacheService cacheService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedUserActivityChecker"/> class.
        /// </summary>
        /// <param name="userActivityChecker">The user activity checker.</param>
        /// <param name="cacheService">The caching service.</param>
        public CachedUserActivityChecker(IUserActivityChecker userActivityChecker, ICacheService cacheService)
        {
            this.userActivityChecker = userActivityChecker ?? throw new ArgumentNullException(nameof(userActivityChecker));
            this.cacheService = cacheService ?? throw new ArgumentNullException(nameof(cacheService));
        }

        /// <summary>
        /// Asynchronously checks if the user is authorized for an activity.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="activityName">The name of the activity to check.</param>
        /// <returns></returns>
        public async Task<bool> IsUserAuthorizedForActivityAsync(HttpContextApplicationUser user, string activityName)
        {
            var key = GetKey(user.UniqueIdentifier, activityName);
            var cacheEntry = await this.cacheService.GetAsync<UserActivityCacheEntry>(key);

            return cacheEntry?.IsAuthorized ?? await FetchResultAsync();

            async Task<bool> FetchResultAsync()
            {
                var result = await this.userActivityChecker.IsUserAuthorizedForActivityAsync(user, activityName);
                await this.cacheService.AddOrReplaceAsync(key, new UserActivityCacheEntry() { IsAuthorized = result });
                return result;
        }
    }

        private string GetKey(Guid userId, string activityName)
        {
            return $"{userId}.{activityName}.IsAuthorized";
        }

        public class UserActivityCacheEntry
        {
            public bool IsAuthorized { get; set; }
        }

    }
}

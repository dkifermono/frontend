﻿using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service
{
    public class ActivityService : IActivityService
    {
        #region Fields

        private readonly IActivityConfiguration activityConfiguration;
        private readonly IUserActivityChecker userActivityChecker;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityService" /> class.
        /// </summary>
        /// <param name="activityConfiguration">The activity configuration</param>
        /// <param name="userActivityChecker">The Active Directory API</param>
        public ActivityService(IActivityConfiguration activityConfiguration, IUserActivityChecker userActivityChecker)
        {
            this.activityConfiguration = activityConfiguration ?? throw new ArgumentNullException(nameof(activityConfiguration));
            this.userActivityChecker = userActivityChecker ?? throw new ArgumentNullException(nameof(userActivityChecker));
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Asynchronously gets the activities the user is authorized for.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public async Task<IEnumerable<ActivityInfo>> GetActivitiesAsync(HttpContextApplicationUser user)
        {
            var registeredActivities = await this.activityConfiguration.GetActivitiesAsync();

            var userActivityAuthorizationCheckTasks = registeredActivities
                .Select(activity => this.userActivityChecker.IsUserAuthorizedForActivityAsync(user, activity));

            var allowedActivityIndexedTaskResults = (await Task.WhenAll(userActivityAuthorizationCheckTasks).ConfigureAwait(false))
                .Select((r, i) => new { Result = r, TaskIndex = i })
                .Where(r => r.Result);

            return allowedActivityIndexedTaskResults.Select(r => new ActivityInfo() { ActivityName = registeredActivities[r.TaskIndex] });
        }

        #endregion Methods
    }
}
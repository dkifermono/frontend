﻿using AutoMapper;
using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.OpenFim.API;
using m2trust.Messaging.OpenFim.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service
{
    /// <summary>
    /// Request action validator.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Service.Common.IRequestActionValidator" />
    public class RequestActionValidator : IRequestActionValidator
    {
        #region Fields

        private readonly ILogWriter logger;

        private readonly IMapper mapper;

        private readonly IMimAccessApi mimAccessApi;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestActionValidator" /> class.
        /// </summary>
        /// <param name="mimAccessApi">The mim access API.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="mapper">The mapper.</param>
        public RequestActionValidator(IMimAccessApi mimAccessApi, ILogWriter logger, IMapper mapper)
        {
            this.mimAccessApi = mimAccessApi;
            this.logger = logger;
            this.mapper = mapper;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Asynchronously validates the abandon action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public Task<bool> ValidateAbandonAsync(RequestDetails request)
        {
            return ValidateActionAndLogErrors(request, RequestAction.Abandon);
        }

        /// <summary>
        /// Asynchronously validates the approve action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public Task<bool> ValidateApproveAsync(RequestDetails request)
        {
            return ValidateActionAndLogErrors(request, RequestAction.Approve, RequestState.Pending);
        }

        /// <summary>
        /// Asynchronously validates the cancel action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public Task<bool> ValidateCancelAsync(RequestDetails request)
        {
            return ValidateActionAndLogErrors(request, RequestAction.Cancel, RequestState.Pending);
        }

        /// <summary>
        /// Asynchronously validates the deny action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public Task<bool> ValidateDenyAsync(RequestDetails request)
        {
            return ValidateActionAndLogErrors(request, RequestAction.Deny, RequestState.Pending);
        }

        /// <summary>
        /// Asynchronously validates the distribute secrets action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public Task<bool> ValidateDistributeSecretsAsync(RequestDetails request)
        {
            return ValidateActionAndLogErrors(request, RequestAction.DistributeSecrets, null, RequestForm.Retire);
        }

        private async Task<bool> ValidateActionAndLogErrors(RequestDetails request, RequestAction action, RequestState? inState = null, RequestForm? inForm = null)
        {
            var validationResult = new ValidationResult()
            {
                IsValid = true,
                ValidationErrors = new List<ValidationError>()
            };

            if (inState != null && request.RequestState != inState)
            {
                var error = new ValidationError()
                {
                    ErrorMessage = $"The request for {request.TargetUserName} from {request.OriginatorUserName} must be pending."
                };
                validationResult.ValidationErrors.Add(error);
                validationResult.IsValid = false;
            }

            if (inForm != null && request.RequestForm == inForm)
            {
                var error = new ValidationError()
                {
                    ErrorMessage = $"To distribute the secrets for the request for {request.TargetUserName} from {request.OriginatorUserName} the request type must not be {request.RequestForm}."
                };
                validationResult.ValidationErrors.Add(error);
                validationResult.IsValid = false;
            }

            if (validationResult.IsValid)
            {
                var isAuthorized = await mimAccessApi.ActionPermissionGrantAsync(request.RequestId, mapper.Map<ActionPermission>(action));
                if (!isAuthorized)
                {
                    var error = new ValidationError()
                    {
                        ErrorMessage = $"The current user has insufficient FIM  permissions to {action} the request for {request.TargetUserName} from {request.OriginatorUserName}."
                    };
                    validationResult.ValidationErrors.Add(error);
                    validationResult.IsValid = false;
                }
                
            }

            if (!validationResult.IsValid)
            {
                foreach (var error in validationResult.ValidationErrors)
                {
                    await logger.WriteAuditInfoAsync(error.ErrorMessage).ConfigureAwait(false);
                }
            }
            return validationResult.IsValid;
        }

        #endregion Methods

        #region Classes

        public class ValidationResult
        {
            #region Properties

            public bool IsValid { get; set; }

            public List<ValidationError> ValidationErrors { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}
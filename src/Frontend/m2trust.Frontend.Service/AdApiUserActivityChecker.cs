﻿using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.ActiveDirectory.API;
using m2trust.Messaging.ActiveDirectory.Enumerations;
using System;
using System.Threading.Tasks;
using m2trust.Frontend.Model;

namespace m2trust.Frontend.Service
{
    public class AdApiUserActivityChecker : IUserActivityChecker
    {
        #region Fields

        private readonly IActiveDirectoryApi activeDirectoryApi;
        private readonly IActivityConfiguration activityConfiguration;

        private readonly string[] adminGroups = new[] {"Administrators", "DomainAdmins"};

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AdApiUserActivityChecker" /> class.
        /// </summary>
        /// <param name="activityConfiguration">The activity configuration.</param>
        /// <param name="activeDirectoryApi">The Active Directory API.</param>
        public AdApiUserActivityChecker(IActivityConfiguration activityConfiguration, IActiveDirectoryApi activeDirectoryApi)
        {
            this.activityConfiguration = activityConfiguration ?? throw new ArgumentNullException(nameof(activityConfiguration));
            this.activeDirectoryApi = activeDirectoryApi ?? throw new ArgumentNullException(nameof(activeDirectoryApi));
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Asynchronously checks if the user is authorized for an activity.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="activityName">The name of the activity to check.</param>
        /// <returns></returns>
        public async Task<bool> IsUserAuthorizedForActivityAsync(HttpContextApplicationUser user, string activityName)
        {
            if (string.IsNullOrEmpty(activityName))
            {
                throw new ArgumentNullException(nameof(activityName));
            }
            if (await this.activityConfiguration.IsActivityPermissionRegistered(activityName))
            {
                if (String.Equals(activityName, Activity.Administration.ToString(),
                    StringComparison.InvariantCultureIgnoreCase))
                {
                    return user.IsAdmin;
                }
                return await this.activeDirectoryApi.IsObjectInGroupsAsync(user.UniqueIdentifier.ToString(), await this.activityConfiguration.GetAuthorizedADGroups(activityName), IdentityType.Guid);
            }
            return false;
        }

        #endregion Methods
    }
}
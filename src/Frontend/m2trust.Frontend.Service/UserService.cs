﻿using AutoMapper;
using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.ActiveDirectory.API;
using PagedList.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service
{
    public class UserService : ServiceBase, IUserService
    {
        #region Fields

        private readonly IActiveDirectoryApi activeDirectoryApi;
        private readonly string[] defaultPropertiesToLoad = { "objectguid", "objectSid", "displayname", "cn" };
        private readonly string ldapBySecurityIdentifier = "LDAP://<SID={0}>";
        private readonly string ldapByUserId = "LDAP://<GUID={0}>";
        private readonly IMapper mapper;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of <see cref="UserService" /> class
        /// </summary>
        /// <param name="activeDirectoryApi">The active directory API.</param>
        /// <param name="mapper">The mapper.</param>
        public UserService(IActiveDirectoryApi activeDirectoryApi, IMapper mapper)
        {
            this.activeDirectoryApi = activeDirectoryApi;
            this.mapper = mapper;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Asynchronously get current domain
        /// </summary>
        /// <returns></returns>
        public async Task<DomainEntry> GetCurrentDomainAsync()
        {
            var domain = await this.activeDirectoryApi.CurrentLocalDomainAsync();
            if (domain != null)
            {
                var result = mapper.Map<DomainEntry>(domain);
                return result;
            }
            return null;
        }

        /// <summary>
        /// Asynchronously get all domains
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<DomainEntry>> GetDomainsAsync()
        {
            var domains = await this.activeDirectoryApi.AllDomainsAsync();
            if (domains != null)
            {
                var result = mapper.Map<IEnumerable<DomainEntry>>(domains);
                return result;
            }
            return null;
        }

        /// <summary>
        /// Asynchronously get single user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserEntry> GetUserAsync(Guid userId)
        {
            var user = await activeDirectoryApi.LookupUserAsync(String.Format(ldapByUserId, userId), defaultPropertiesToLoad).ConfigureAwait(false);
            if (user != null)
            {
                var result = mapper.Map<UserEntry>(user);
                return result;
            }

            return null;
        }

        /// <summary>
        /// Gets the user asynchronous.
        /// </summary>
        /// <param name="securityIdentifier">The security identifier.</param>
        /// <returns></returns>
        public async Task<UserEntry> GetUserAsync(string securityIdentifier)
        {
            var user = await activeDirectoryApi.LookupUserAsync(String.Format(ldapBySecurityIdentifier, securityIdentifier), defaultPropertiesToLoad)
                .ConfigureAwait(false);

            if (user != null)
            {
                var result = mapper.Map<UserEntry>(user);
                return result;
            }

            return null;
        }

        /// <summary>
        /// Asynchronously get users
        /// </summary>
        /// <param name="paging"></param>
        /// <param name="request"></param>
        /// <param name="userDisplayProperties"></param>
        /// <param name="extendedSearch"></param>
        /// <returns></returns>
        public async Task<IPagedList<UserEntry>> GetUsersAsync(PagingParameters paging, UserSearchRequest request, string[] userDisplayProperties, bool extendedSearch)
        {
            var usersResult = await activeDirectoryApi
                .SearchUsersAsync(
                    paging.PageNumber - 1,
                    paging.PageSize,
                    request.Domain,
                    request.SearchTerm,
                    request.Fields,
                    userDisplayProperties,
                    extendedSearch
                ).ConfigureAwait(false);

            var result = mapper.Map<IEnumerable<UserEntry>>(usersResult.Users);

            if (result != null && result.Any())
            {
                foreach (var user in result)
                {
                    user.Properties = ApplyDisplayProperty(userDisplayProperties, user.Properties);
                }

                return new StaticPagedList<UserEntry>(result, paging.PageNumber, paging.PageSize, usersResult.TotalCount);
            }
            else
            {
                return EmptyPagedResult<UserEntry>(paging);
            }
        }

        protected IDictionary<string, object> ApplyDisplayProperty(string[] userDisplayProperties, IDictionary<string, object> properties)
        {
            IDictionary<string, object> result = new Dictionary<string, object>();

            foreach (var prop in properties)
            {
                if (userDisplayProperties.Any(
                    field => field.Equals(prop.Key, StringComparison.InvariantCultureIgnoreCase)))
                {
                    result.Add(prop);
                }
            }

            return result;
        }

        #endregion Methods
    }
}
﻿using AutoMapper;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Service.Common.Filters;
using m2trust.Messaging.OpenFim.API;
using m2trust.Messaging.OpenFim.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Profile = AutoMapper.Profile;

namespace m2trust.Frontend.Service
{
    public class ProfileService : ServiceBase, IProfileService
    {
        #region Fields
        
        private readonly ISignatureConfiguration configuration;
        private readonly int defaultPriority = 0;
        private readonly IMapper mapper;
        private readonly IMimAccessApi mimAccessApi;
        private readonly ISigningService signingService;
        private readonly IFilterFactory filterFactory;

        #endregion Fields

        #region Constructors

        public ProfileService(IMimAccessApi mimAccessApi,
            IMapper mapper, 
            ISigningService signingService, 
            ISignatureConfiguration configuration,
            IFilterFactory filterFactory)
        {
            this.mimAccessApi = mimAccessApi;
            this.mapper = mapper;
            this.signingService = signingService;
            this.configuration = configuration;

            this.filterFactory = filterFactory;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Gets the Allow Specify Delay asynchronously
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="activityType"></param>
        /// <returns></returns>
        public async Task<bool?> AllowSpecifyDelayAsync(Guid profileId, string activityType)
        {
            bool? allowSpecifyDelay = null;
            var profile = await GetProfileAsync(profileId);
            Enum.TryParse(activityType, out Activity activity);

            switch (activity)
            {
                case Activity.SuspendSmartcard:
                case Activity.ReinstateSmartcard:
                    allowSpecifyDelay = profile.ProfileTemplate.ReinstatePolicy.AllowSpecifyDelay;
                    break;
                case Activity.DisableSmartcard:
                case Activity.RevokeCertificate:
                    allowSpecifyDelay = profile.ProfileTemplate.RevokePolicy.AllowSpecifyDelay;
                    break;
            }

            return allowSpecifyDelay;
        }

        /// <summary>
        /// Enroll Profile Template asynchronus
        /// </summary>
        /// <param name="requestProfile"></param>
        /// <returns></returns>
        public async Task<AuditRequestEntry> EnrollProfileTemplateAsync(AuditRequestEntry requestProfile)
        {
            if (requestProfile != null)
            {
                var profileTemplate = mapper.Map<ProfileTemplateRequest>(requestProfile);

                var response = await mimAccessApi.ProfileTemplateEnrollAsync(profileTemplate);

                if (response != null)
                {
                    var result = mapper.Map<AuditRequestEntry>(response);
                    return result;
                }
            }

            return null;
        }


        /// <summary>
        /// Gets the profile asynchronous.
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <returns></returns>
        public async Task<ProfileEntry> GetProfileAsync(Guid profileId)
        {
            if (profileId != Guid.Empty)
            {
                ProfileEntry result = await GetSmartcardProfileAsync(profileId);
                if (result == null)
                {
                    result = await GetSoftProfileAsync(profileId);
                }
                return result;
            }
            return null;
        }


        /// <summary>
        /// Gets the profile asynchronous
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ICollection<ProfileEntry>> GetProfilesAsync(Guid userId)
        {
            var response = await mimAccessApi.FindProfilesByUserIdAsync(userId).ConfigureAwait(false);
            
            if (response != null && response.Length > 0)
            {
                var mappedEntries = mapper.Map<Messaging.OpenFim.Entities.Profile[], SmartcardProfileEntry[]>(response);
                ICollection<ProfileEntry> result = new List<ProfileEntry>();
                var filters = await filterFactory.CreateTemporarySmartcardFilterAsync();
                foreach (var filter in filters)
                {
                    result = await filter.FilterAsync(mappedEntries).ConfigureAwait(false);
                }
                return result;
            }
            return null;
        }

        /// <summary>
        /// Gets the profile stubs asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="activityType">The activity type</param>
        /// <returns></returns>
        public async Task<ICollection<ProfileStubEntry>> GetProfileStubsAsync(Guid userId, string activityType)
        {
            var response = await mimAccessApi.ProfileStubsAsync(userId).ConfigureAwait(false);
            if (response != null && response.Length > 0)
            {
                var result = mapper.Map<ProfileStub[], ICollection<ProfileStubEntry>>(response);
                var filters = await filterFactory.CreateStubsFilterAsync(activityType);
                foreach (var filter in filters)
                {
                    result = await filter.FilterAsync(result).ConfigureAwait(false);
                }
                
                return result;
            }
            return null;
        }

        /// <summary>
        /// Asynchronously Get Profile Template by profileTemplateId
        /// </summary>
        /// <param name="profileTemplateId"></param>
        /// <returns></returns>
        private async Task<ProfileTemplateEntry> GetProfileTemplateAsync(Guid profileTemplateId)
        {
            var template = await mimAccessApi.ProfileTemplateByIdAsync(profileTemplateId);
            if (template != null)
            {
                var templateFilter = await filterFactory.CreateProfileTemplateFilterAsync();
                var result = mapper.Map<ProfileTemplateEntry>(template);
                result = (await templateFilter.FilterAsync(new List<ProfileTemplateEntry>() { result } ).ConfigureAwait(false)).FirstOrDefault();
                return result;
            }
            return null;
        }

        /// <summary>
        /// Gets the profile template asynchronous.
        /// </summary>
        /// <param name="profileTemplateId">The profile template identifier.</param>
        /// <param name="profileId">The profile identifier.</param>
        /// <returns></returns>
        public async Task<ProfileTemplateEntry> GetProfileTemplateAsync(Guid? profileTemplateId, Guid? profileId)
        {
            ProfileTemplateEntry template;
            if (!profileTemplateId.HasValue || profileTemplateId.Value == Guid.Empty)
            {
                if (!profileId.HasValue || profileId == Guid.Empty)
                    return null;

                var profile = await GetProfileAsync(profileId.Value);

                template = profile.ProfileTemplate;
            }
            else
            {
                template = await GetProfileTemplateAsync(profileTemplateId.Value);
            }
            return template;
        }

        /// <summary>
        /// Asynchronously Get Profile Templates
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<ProfileTemplateEntry>> GetProfileTemplatesAsync(Guid userId)
        {
            var response = await mimAccessApi.ProfileTemplateAsync(userId).ConfigureAwait(false);

            if (response != null && response.Count > 0)
            {
                var templateFilter = await filterFactory.CreateProfileTemplateFilterAsync();
                var result = mapper.Map<List<ProfileTemplateEntry>>(response);

                result = (await templateFilter.FilterAsync(result).ConfigureAwait(false)).ToList();

                return result;
            }

            return null;
        }

        /// <summary>
        /// Gets the smartcard profile asynchronous.
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <returns></returns>
        public async Task<SmartcardProfileEntry> GetSmartcardProfileAsync(Guid smartcardId)
        {
            var response = await mimAccessApi.FindSmartcardProfileByIdAsync(smartcardId).ConfigureAwait(false);
            if (response != null && response.ProfileTemplate != null && response.ProfileTemplate.Id != null)
            {
                var result = mapper.Map<SmartcardProfileEntry>(response);
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the soft profile asynchronous.
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <returns></returns>
        public async Task<ProfileEntry> GetSoftProfileAsync(Guid profileId)
        {
            var response = await mimAccessApi.FindSoftProfileByIdAsync(profileId).ConfigureAwait(false);
            if (response != null)
            {
                var result = mapper.Map<ProfileEntry>(response);
                return result;
            }

            return null;
        }

        /// <summary>
        /// Profiles the recover asynchronous.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<OtpResult> ProfileRecoverAsync(AuditRequestEntry request)
        {
            var response = await mimAccessApi.ProfileRecoverAsync(request.ProfileId, request.DataCollectionItems, request.Comment, (byte)request.Priority).ConfigureAwait(false);
            return mapper.Map<OtpResult>(response);
        }

        public async Task<OtpResult> ProfileRenewAsync(AuditRequestEntry request)
        {
            var response = await mimAccessApi.RenewProfileAsync(request.ProfileId, request.DataCollectionItems, request.Comment, request.UserId).ConfigureAwait(false);
            return mapper.Map<OtpResult>(response);
        }

        /// <summary>
        /// Profiles the revoke asynchronous.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<OtpResult> ProfileRevokeAsync(AuditRequestEntry request)
        {
            var response = await mimAccessApi.RevokeAsync(request.ProfileId, request.DataCollectionItems, request.Comment, request.EffectiveRevocationInHours).ConfigureAwait(false);
            return mapper.Map<OtpResult>(response);
        }

        /// <summary>
        /// Validates the profile template request asynchronous.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<bool> ValidateProfileTemplateRequestAsync(AuditRequestEntry request)
        {
            var dciName = await configuration.SignatureDataCollectionItemName;

            // The profile template doesn't contain a signature fields, so we're OK to return
            if (request.DataCollectionItems != null && !request.DataCollectionItems.ContainsKey(dciName))
            {
                return true;
            }

            if (!await configuration.RejectInvalidSigningCertificate) return true;

            // Signature cannot be empty
            if (request.DataCollectionItems != null && string.IsNullOrEmpty(request.DataCollectionItems[dciName]))
            {
                return false;
            }

            // Check signing certificate
            await this.signingService.CheckSigningCertificateAsync(request).ConfigureAwait(false);

            return true;
        }

        public async Task<ICollection<ProfileTemplateEntry>> GetProfileTemplatesAsync()
        {
            var profileTemplates = await mimAccessApi.ProfileTemplatesAsync();
            var mappedResult = mapper.Map<List<ProfileTemplateEntry>>(profileTemplates);
            return mappedResult;
        }

        private ICollection<RequestDetails> GetNonOtpRequests(IEnumerable<RequestDetails> requests)
        {
            return requests.Where(r => IsNoOtpRequest(r)).ToList();
        }

        private bool HasOtpRequests(IEnumerable<RequestDetails> requests)
        {
            return requests.Any(r => !IsNoOtpRequest(r));
        }

        private bool IsNoOtpRequest(RequestDetails request)
        {
            return !(request.ProfileTemplate.EnrollPolicy.DisplaySecretsOnScreen
                     || request.ProfileTemplate.EnrollPolicy.EmailSecrets);
        }

        #endregion Methods
    }
}
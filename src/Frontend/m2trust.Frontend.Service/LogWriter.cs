﻿using m2trust.Frontend.Service.Common;
using m2trust.Messaging.Logging.API;
using m2trust.Messaging.Logging.Entities;
using System;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service
{
    /// <summary>
    /// Log writer.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Service.Common.ILogWriter" />
    public class LogWriter : ILogWriter
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LogWriter" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="auditLogger">The audit logger.</param>
        public LogWriter(ILogger logger, IAuditLogger auditLogger)
        {
            this.logger = logger;
            this.auditLogger = auditLogger;
        }

        #endregion Constructor

        #region Properties

        private readonly ILogger logger;

        private readonly IAuditLogger auditLogger;

        #endregion Properties

        #region Methods

        /// <summary>
        /// Asynchronously writes a debug message.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <returns></returns>
        public Task WriteDebugAsync(string message)
        {
            return this.logger.Debug(new LoggerMetaData()
            {
                System = Environment.MachineName,
                User = Environment.UserName
            }, message);
        }

        /// <summary>
        /// Asynchronously writes an error message.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <returns></returns>
        public Task WriteErrorAsync(string message)
        {
            return this.logger.Error(new LoggerMetaData()
            {
                System = Environment.MachineName,
                User = Environment.UserName
            }, message);
        }

        /// <summary>
        /// Asynchronously writes a info message.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <returns></returns>
        public Task WriteInfoAsync(string message)
        {
            return this.logger.Info(new LoggerMetaData()
            {
                System = Environment.MachineName,
                User = Environment.UserName
            }, message);
        }

        /// <summary>
        /// Asynchronously writes a trace message.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <returns></returns>
        public Task WriteTraceAsync(string message)
        {
            return this.logger.Trace(new LoggerMetaData()
            {
                System = Environment.MachineName,
                User = Environment.UserName
            }, message);
        }

        /// <summary>
        /// Asynchronously writes a warn message.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <returns></returns>
        public Task WriteWarnAsync(string message)
        {
            return this.logger.Warn(new LoggerMetaData()
            {
                System = Environment.MachineName,
                User = Environment.UserName
            }, message);
        }

        public Task WriteAuditInfoAsync(string message)
        {
            return auditLogger.Trace(new LoggerMetaData()
            {
                System = Environment.MachineName,
                User = Environment.UserName
            }, message);
        }

        #endregion Methods
    }
}
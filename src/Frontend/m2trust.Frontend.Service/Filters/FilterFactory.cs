﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Service.Common.Filters;

namespace m2trust.Frontend.Service.Filters
{
    public class FilterFactory : IFilterFactory
    {
        private readonly ISmartcardProfileStubFilter smartcardProfileStubFilter;
        private readonly IProfileStubEntryProfileStateFilter profileStubEntryProfileStateFilter;
        private readonly ISoftProfileStubEntryFilter softProfileStubEntryFilter;
        private readonly IBlacklistedProfileTemplateProfileStubEntryFilter blacklistedProfileTemplateFilter;
        private readonly ISuspendedProfileStubFilter suspendedProfileStubFilter;

        private readonly ITemporarySmartcardProfileFilter temporarySmartcardProfileFilter;
        private readonly IProfileStatusFilter profileStatusFilter;

        private readonly IExcludedProfileTemplateFilter excludedProfileTemplateFilter;

        public FilterFactory(ISmartcardProfileStubFilter smartcardProfileStubFilter, 
            IProfileStubEntryProfileStateFilter profileStubEntryProfileStateFilter,
            ISoftProfileStubEntryFilter softProfileStubEntryFilter,
            IBlacklistedProfileTemplateProfileStubEntryFilter blacklistedProfileTemplateFilter,
            ISuspendedProfileStubFilter suspendedProfileStubFilter,
            ITemporarySmartcardProfileFilter temporarySmartcardProfileFilter,
            IProfileStatusFilter profileStatusFilter,
            IExcludedProfileTemplateFilter excludedProfileTemplateFilter)
        {
            this.smartcardProfileStubFilter = smartcardProfileStubFilter;
            this.profileStubEntryProfileStateFilter = profileStubEntryProfileStateFilter;
            this.softProfileStubEntryFilter = softProfileStubEntryFilter;
            this.blacklistedProfileTemplateFilter = blacklistedProfileTemplateFilter;
            this.suspendedProfileStubFilter = suspendedProfileStubFilter;
            this.temporarySmartcardProfileFilter = temporarySmartcardProfileFilter;
            this.profileStatusFilter = profileStatusFilter;
            this.excludedProfileTemplateFilter = excludedProfileTemplateFilter;
        }

        public Task<List<IProfileStubEntryFilter>>  CreateStubsFilterAsync(string activityType)
        {
            Enum.TryParse(activityType, out Activity activity);
            var filters = new List<IProfileStubEntryFilter>();
            switch (activity)
            {
                case Activity.DisableSmartcard:
                case Activity.RewriteSmartcard:
                case Activity.SuspendSmartcard:
                case Activity.UnblockSmartcard:
                    filters = new List<IProfileStubEntryFilter> () { smartcardProfileStubFilter, profileStubEntryProfileStateFilter };
                    break;
                case Activity.RecoverCertificate:
                case Activity.RenewCertificate:
                case Activity.RevokeCertificate:
                    filters = new List<IProfileStubEntryFilter>() { profileStubEntryProfileStateFilter, softProfileStubEntryFilter, blacklistedProfileTemplateFilter };
                    break;
                case Activity.ReinstateSmartcard: 
                    filters = new List<IProfileStubEntryFilter>() {smartcardProfileStubFilter, profileStubEntryProfileStateFilter, suspendedProfileStubFilter};
                    break;
            }

            return Task.FromResult(filters);

        }

        public Task<List<IProfileFilter>> CreateTemporarySmartcardFilterAsync()
        {
            var filters = new List<IProfileFilter>() { temporarySmartcardProfileFilter, profileStatusFilter };
            return Task.FromResult(filters);
        }

        public Task<IExcludedProfileTemplateFilter> CreateProfileTemplateFilterAsync()
        {
            return Task.FromResult(excludedProfileTemplateFilter);
        }
    }
}

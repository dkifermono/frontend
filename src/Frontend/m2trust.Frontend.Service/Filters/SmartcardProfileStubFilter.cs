﻿using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Service.Common.Filters;
using System;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Filters
{
    public class SmartcardProfileStubFilter : BaseFilter<ProfileStubEntry>, ISmartcardProfileStubFilter
    {
        private readonly string _errorMessage = "SmartcardProfileStubFilter.Check - Omitting profile stub {0} because it is not a smartcard.";

        public SmartcardProfileStubFilter(ILogWriter logger) : base(logger)
        {
        }

        protected override async Task<bool> Check(ProfileStubEntry item)
        {
            if (!item.IsSmartCard)
            {
                await _logger.WriteInfoAsync(String.Format(_errorMessage, item.SmartcardId));
            }
            return item.IsSmartCard;
        }
    }
}

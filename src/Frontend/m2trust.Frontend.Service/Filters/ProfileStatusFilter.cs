﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using m2trust.Frontend.Service.Common.Filters;

namespace m2trust.Frontend.Service.Filters
{
    public class ProfileStatusFilter : BaseFilter<ProfileEntry>, IProfileStatusFilter
    {
        #region Fields

        private readonly IProfileStateFilterConfiguration configuration;

        private readonly List<ProfileState> _smartCardStatuses =
                        new List<ProfileState>(new[] { ProfileState.Disabled, ProfileState.Retired, ProfileState.Suspended, ProfileState.Assigned });

        private readonly List<ProfileState> _softProfileStatuses =
            new List<ProfileState>(new[] { ProfileState.Disabled, ProfileState.Retired, ProfileState.New });

        #endregion Fields

        #region Constructors

        public ProfileStatusFilter(ILogWriter logger, IProfileStateFilterConfiguration configuration) : base(logger)
        {
            this.configuration = configuration;
        }

        #endregion Constructors

        #region Methods

        protected override async Task<bool> Check(ProfileEntry item)
        {
            if (await configuration.ShowDisabledProfiles)
            {
                return true;
            }

            if (item.IsSmartcard && _smartCardStatuses.Any(c => c == item.Status))
            {
                return false;
            }

            if (!item.IsSmartcard && _softProfileStatuses.Any(c => c == item.Status))
            {
                return false;
            }
            return true;
        }

        #endregion Methods
    }
}
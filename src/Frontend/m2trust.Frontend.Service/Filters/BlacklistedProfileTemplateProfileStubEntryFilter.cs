﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Filters
{
    /// <summary>
    /// Check if contains any of the blacklisted profile templates.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Service.Filters.BaseFilter{m2trust.Frontend.Model.ProfileStubEntry}" />
    /// <seealso cref="m2trust.Frontend.Service.Common.IBlacklistedProfileTemplateProfileStubEntryFilter" />
    public class BlacklistedProfileTemplateProfileStubEntryFilter : BaseFilter<ProfileStubEntry>, IBlacklistedProfileTemplateProfileStubEntryFilter
    {
        private readonly IProfileStubEntryFilterConfiguration configuration;

        public BlacklistedProfileTemplateProfileStubEntryFilter(ILogWriter logger, IProfileStubEntryFilterConfiguration configuration): base(logger)
        {
            this.configuration = configuration;
        }

        protected override async Task<bool> Check(ProfileStubEntry item)
        {
            var blacklistedTemplates = await configuration.BlacklistedProfileTemplates;

            await _logger.WriteAuditInfoAsync($"BlacklistedProfileTemplateFilter.Check - Check template {item.Name}");

            var result = !blacklistedTemplates.Any(c => string.Equals(c, item.Name, StringComparison.InvariantCultureIgnoreCase));

            if (!result)
            {
                await _logger.WriteAuditInfoAsync($"BlacklistedProfileTemplateFilter.Check - Ommiting profile template {item.Name} because it is explicitly blacklisted.");
            }

            return result;
        }
    }
}

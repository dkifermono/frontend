﻿using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Filters
{
    public class ExcludedProfileTemplateFilter : BaseFilter<ProfileTemplateEntry>, IExcludedProfileTemplateFilter
    {
        private readonly IProfileStubEntryFilterConfiguration configuration;

        public ExcludedProfileTemplateFilter(ILogWriter logger, IProfileStubEntryFilterConfiguration configuration) : base(logger)
        {
            this.configuration = configuration;
        }

        protected override async Task<bool> Check(ProfileTemplateEntry item)
        {
            var excludedTemplates = await configuration.ExcludedProfileTemplates;

            await _logger.WriteAuditInfoAsync($"ExcludedProfileTemplateFilter.Check - Check template {item.Name}");

            var  result = !excludedTemplates.Any(c => string.Equals(c, item.Name, StringComparison.InvariantCultureIgnoreCase));

            if (!result)
            {
                await _logger.WriteAuditInfoAsync($"ExcludedProfileTemplateFilter.Check - Ommiting profile template {item.Name} because it is explicitly excluded.");
            }

            return result;
        }
    }
}

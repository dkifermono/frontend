﻿using m2trust.Frontend.Service.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Filters
{
    public abstract class BaseFilter<T> : IBaseFilter<T>
    {
        protected readonly ILogWriter _logger;
        public BaseFilter(ILogWriter logger)
        {
            _logger = logger;
        }

        public async Task<ICollection<T>> FilterAsync(ICollection<T> items)
        {
            var result = new List<T>();
            var tasks = items.ToDictionary(item => item, item => Check(item));

            await Task.WhenAll(tasks.Values);

            foreach (var key in tasks.Keys)
            {
                if (await tasks[key])
                {
                    result.Add(key);
                }
            }
            return result;
        }

        protected abstract Task<bool> Check(T item);
    }
}
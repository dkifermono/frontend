﻿using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Filters
{
    /// <summary>
    /// Check profile status.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Service.Filters.BaseFilter{m2trust.Frontend.Model.ProfileStubEntry}" />
    /// <seealso cref="m2trust.Frontend.Service.Common.IProfileStubEntryProfileStateFilter" />
    public class ProfileStubEntryProfileStateFilter : BaseFilter<ProfileStubEntry>, IProfileStubEntryProfileStateFilter
    {
        private IProfileStateFilterConfiguration configuration;
        private readonly List<ProfileState> _smartCardStatuses =
                new List<ProfileState>(new[] { ProfileState.Disabled, ProfileState.Retired, ProfileState.Suspended, ProfileState.Assigned});

        private readonly List<ProfileState> _softProfileStatuses =
            new List<ProfileState>(new[] { ProfileState.Disabled, ProfileState.Retired, ProfileState.New });

        public ProfileStubEntryProfileStateFilter(ILogWriter logger, IProfileStateFilterConfiguration configuration) : base(logger)
        {
            this.configuration = configuration;
        }

        protected override async Task<bool> Check(ProfileStubEntry item)
        {
            if (await configuration.ShowDisabledProfiles)
            {
                return true;
            }

            if (item.IsSmartCard && _smartCardStatuses.Any(c => c == item.ProfileStatus))
            {
                return false;
            }

            if (!item.IsSmartCard && _softProfileStatuses.Any(c => c == item.ProfileStatus))
            {
                return false;
            }

            return true;
        }
    }
}

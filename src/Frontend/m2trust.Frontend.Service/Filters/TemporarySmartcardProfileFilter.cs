﻿using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Service.Common.Filters;
using System;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Filters
{
    public class TemporarySmartcardProfileFilter : BaseFilter<ProfileEntry>, ITemporarySmartcardProfileFilter
    {
        private const string SmartcardErrorMessage = "SmartcardProfileFilter.Check - Omitting profile {0} because it is not a smartcard.";
        private const string TemporarySmartcardErrorMessage = "TemporarySmartcardProfileFilter.Check - Omitting profile {0} because it is not a TemporarySmartcard profile.";
        

        public TemporarySmartcardProfileFilter(ILogWriter logger) : base(logger)
        {
        }

        protected override async Task<bool> Check(ProfileEntry item)
        {
            var result = item.IsSmartcard == true;
            if (!result)
            {
                await _logger.WriteInfoAsync(String.Format(SmartcardErrorMessage, item.Id));
            }
            else
            {
                var smartcardItem = item as SmartcardProfileEntry;
                if (smartcardItem.TargetType == TargetForm.TemporarySmartcard)
                    result = true;
                else
                {
                    result = false;
                    await _logger.WriteInfoAsync(String.Format(TemporarySmartcardErrorMessage, item.Id));
                }
            }


            return result;
        }
    }
}

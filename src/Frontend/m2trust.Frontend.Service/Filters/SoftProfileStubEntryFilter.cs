﻿using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Filters
{
    /// <summary>
    /// Check if provided the provided profile stub entry is smartcard.
    /// Returns <c>false</c> if this instance is smart card; otherwise, <c>true</c>.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Service.Common.ISoftProfileStubEntryFilter" />
    /// <seealso cref="m2trust.Frontend.Service.Filters.BaseFilter{m2trust.Frontend.Model.ProfileStubEntry}" />
    public class SoftProfileStubEntryFilter : BaseFilter<ProfileStubEntry>, ISoftProfileStubEntryFilter
    {
        private readonly string _errorMessage = "SoftProfileStubFilter.Check - Omitting profile stub {0} because it is not a soft profile.";

        public SoftProfileStubEntryFilter(ILogWriter logger): base(logger)
        {
        }

        protected override async Task<bool> Check(ProfileStubEntry item)
        {
            if(item.IsSmartCard)
            {
                await _logger.WriteInfoAsync(String.Format(_errorMessage, item.SoftProfileId));
            }
            return !item.IsSmartCard;
        }
    }
}

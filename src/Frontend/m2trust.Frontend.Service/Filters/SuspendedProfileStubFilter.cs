﻿using System.Threading.Tasks;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Frontend.Service.Common.Filters;

namespace m2trust.Frontend.Service.Filters
{
    public class SuspendedProfileStubFilter : BaseFilter<ProfileStubEntry>, ISuspendedProfileStubFilter
    {
        private const string ErrorMessage = "SuspendedProfileStubFilter.Check - Omitting profile stub {0} because it is not suspended.";


        public SuspendedProfileStubFilter(ILogWriter logger) : base(logger)
        {

        }

        protected override async Task<bool> Check(ProfileStubEntry item)
        {
            var result = item.ProfileStatus == ProfileState.Suspended;
            if (!result)
            {
                await _logger.WriteInfoAsync(string.Format(ErrorMessage, item.Id));
            }

            return result;
        }
    }
}

﻿using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service.Filters
{
    public class ProfileEntryOrderByDateFilter : IProfileFilter
    {
        #region Methods

        public Task<ICollection<ProfileEntry>> FilterAsync(ICollection<ProfileEntry> items)
        {
            return Task.FromResult(items);
        }

        #endregion Methods
    }
}
﻿using AutoMapper;
using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.OpenFim.API;
using m2trust.Messaging.OpenFim.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service
{
    public class DataCollectionItemService: ServiceBase, IDataCollectionItemService
    {
        private readonly IMimAccessApi mimAccessApi;
        private readonly IMapper mapper;

        public DataCollectionItemService(IMimAccessApi mimAccessApi, IMapper mapper)
        {
            this.mimAccessApi = mimAccessApi;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets all data collection items from the profile template
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="profileTemplateId">The profile template identifier.</param>
        /// <returns></returns>
        public async Task<ICollection<DataCollectionItemEntry>> GetDataCollectionItemsAsync(SectionType type,  Guid profileTemplateId)
        {
            Enum.TryParse(type.ToString(), out ActionType actionType);
            var dataCollectionResponse = await mimAccessApi.DataCollectionItemsByProfileTemplateAsync(profileTemplateId, actionType);

            if(dataCollectionResponse != null && dataCollectionResponse.Length > 0)
            {
                var result = mapper.Map<ICollection<DataCollectionItemEntry>>(dataCollectionResponse);
                return result;
            }

            return null;
        }

        /// <summary>
        /// Validates the data collection items.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <param name="values">The values.</param>
        /// <param name="errors">The errors.</param>
        /// <returns></returns>
        public bool ValidateDataCollectionItems(ICollection<DataCollectionItemEntry> items, IDictionary<string, string> values, List<ValidationError> errors)
        {
            var result = true;
            if(items?.Any() == true)
            {
                foreach (var item in items)
                {
                    string value = null;
                    if (values != null && values.ContainsKey(item.Name))
                    {
                        value = values[item.Name];
                    }
                    result &= ValidateDataCollectionItem(item, value, errors) != false;
                }
            }
            return result;
        }

        /// <summary>
        /// Validates the data collection item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="value">The value.</param>
        /// <param name="errors">The errors.</param>
        /// <returns></returns>
        public bool ValidateDataCollectionItem(DataCollectionItemEntry item, string value, List<ValidationError> errors)
        {
            var result = true;
            if(item.Required)
            {
                if (string.IsNullOrEmpty(value))
                {
                    result = false;
                    errors.Add(new ValidationError($"The field '{item.Name}' is required."));
                }
            }

            if(result && !String.IsNullOrEmpty(value))
            {
                switch (item.ValidationType)
                {
                    case ValidationTypeDci.DataType:
                        if(item.DataType == DataTypeDci.Date)
                        {
                            if (DateTime.TryParse(value, out DateTime temp) == false)
                            {
                                result = false;
                                errors.Add(new ValidationError($"The field '{item.Name}' is not a date."));
                            }
                        }
                        else if (item.DataType == DataTypeDci.Number)
                        {
                            if (int.TryParse(value, out int temp) == false)
                            {
                                result = false;
                                errors.Add(new ValidationError($"The field '{item.Name}' is not a number."));
                            }
                        }
                        break;
                    case ValidationTypeDci.RegularExpression:
                        var regex = new Regex(item.ValidationData);
                        var matches = regex.Matches(value);

                        if (matches.Count == 0 || matches.Cast<Capture>().Any(c => c.Value == value) == false)
                        {
                            result = false;
                            errors.Add(new ValidationError($"The data in field '{item.Name}' is not in the expected format."));
                        }
                        break;
                }
            }
            return result;
        }
    }
}

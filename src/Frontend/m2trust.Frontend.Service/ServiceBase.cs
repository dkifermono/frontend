﻿using m2trust.Frontend.Common.Parameters;
using PagedList.Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service
{
    public abstract class ServiceBase
    {
        protected IPagedList<T> EmptyPagedResult<T>(PagingParameters pagingOptions)
        {
            return new StaticPagedList<T>(Enumerable.Empty<T>(), pagingOptions.PageNumber, pagingOptions.PageSize, 0);
        }
    }
}

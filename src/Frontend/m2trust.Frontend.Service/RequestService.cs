﻿using AutoMapper;
using m2trust.Frontend.Common;
using m2trust.Frontend.Common.Configuration;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Common.Parameters;
using m2trust.Frontend.Common.Parameters.Sorting;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.OpenFim.API;
using m2trust.Messaging.OpenFim.Entities;
using PagedList.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using m2trust.Messaging.OpenFim.Enumerations;

namespace m2trust.Frontend.Service
{
    /// <summary>
    /// Request service.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Service.Common.IRequestService" />
    public class RequestService : ServiceBase, IRequestService
    {
        #region Fields

        private const string asc = "asc";
        private const string desc = "desc";
        private readonly ICertificateManagerApi certificateManagerApi;
        private readonly IMapper mapper;
        private readonly IMimAccessApi mimAccessApi;
        private readonly IRequestActionValidator validator;
        private readonly ISignatureConfiguration configuration;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ApproveActivityService" /> class.
        /// </summary>
        /// <param name="mimAccess">The mim access api.</param>
        /// <param name="certificateManagerApi">The certificate manager api.</param>
        public RequestService(IMimAccessApi mimAccessApi, ICertificateManagerApi certificateManagerApi, IRequestActionValidator validator, IMapper mapper, ISignatureConfiguration configuration)
        {
            this.mimAccessApi = mimAccessApi;
            this.certificateManagerApi = certificateManagerApi;
            this.validator = validator;
            this.mapper = mapper;
            this.configuration = configuration;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Asynchronously abandons requests.
        /// </summary>
        /// <param name="requestIds">The request ids.</param>
        /// <returns></returns>
        public async Task<RequestActionResponse> AbandonAsync(IEnumerable<Guid> requestIds)
        {
            var response = await CreateRequestActionResponseAsync(requestIds, (requestDetails) => validator.ValidateAbandonAsync(requestDetails));
            if (response.IsSuccessful)
            {
                var tasks = requestIds.Select(r => mimAccessApi.AbandonRequestAsync(r, null));
                await Task.WhenAll(tasks);
            }
            return response;
        }

        /// <summary>
        /// Asynchronously approves requests.
        /// </summary>
        /// <param name="requestIds">The request ids.</param>
        /// <returns></returns>
        public async Task<ApproveResult> ApproveAsync(IEnumerable<Guid> requestIds)
        {
            var response = new ApproveResult();

            response.Result = await CreateRequestActionResponseAsync(requestIds, (requestDetails) => validator.ValidateApproveAsync(requestDetails));

            if (response.Result.IsSuccessful)
            {
                var secrets = new List<Secret>();
                var tasks = requestIds.Select(r => mimAccessApi.ApproveRequestAsync(r));
                var requestSecrets = await Task.WhenAll(tasks);
                secrets = mapper.Map<List<Secret>>(requestSecrets);
                if (secrets != null && !secrets.All(item => item == null) && secrets.Any(s=> s.Distribution != null) && secrets.Any(s => s.Distribution.EmailSecrets))
                {
                    response.Secrets = secrets;
                    response.Result.HasSecrets = true;
                }
            }
            return response;
        }

        /// <summary>
        /// Asynchronously cancels the requests.
        /// </summary>
        /// <param name="requestIds">The request ids.</param>
        /// <returns></returns>
        public async Task<RequestActionResponse> CancelAsync(IEnumerable<Guid> requestIds)
        {
            var response = await CreateRequestActionResponseAsync(requestIds, (requestDetails) => validator.ValidateCancelAsync(requestDetails));

            if (response.IsSuccessful)
            {
                var tasks = requestIds.Select(r => mimAccessApi.CancelAsync(r, null));
                await Task.WhenAll(tasks);
            }
            return response;
        }

        /// <summary>
        /// Asynchronously denies requests.
        /// </summary>
        /// <param name="requestIds">The request ids.</param>
        /// <returns></returns>
        public async Task<RequestActionResponse> DenyAsync(IEnumerable<Guid> requestIds, string comment)
        {
            var response = await CreateRequestActionResponseAsync(requestIds, (requestDetails) => validator.ValidateDenyAsync(requestDetails));
            if (response.IsSuccessful)
            {
                var tasks = requestIds.Select(r => mimAccessApi.DenyRequestAsync(r, comment));
                await Task.WhenAll(tasks);
            }
            return response;
        }

        /// <summary>
        /// Asynchronously distributes the secrets.
        /// </summary>
        /// <param name="requestIds">The request ids.</param>
        /// <returns></returns>
        public async Task<IEnumerable<Secret>> DistributeSecretsAsync(IEnumerable<Guid> requestIds)
        {
            var secrets = new List<Secret>();
            var validation = await ValidateRequestActionAsync(requestIds, (requestDetails) => validator.ValidateDistributeSecretsAsync(requestDetails));
            if (validation)
            {
                var tasks = requestIds.Select(r => mimAccessApi.DistributeSecretsRequestAsync(r));
                var requestSecrets = await Task.WhenAll(tasks);
                secrets = mapper.Map<List<Secret>>(requestSecrets);
                if(secrets != null)
                {
                    secrets = secrets.FindAll(s => s.Distribution.DisplaySecretsOnScreen == true);
                }
                
            }
            return secrets;
        }

        /// <summary>
        /// Asynchronously finds the requests.
        /// </summary>
        /// <param name="filterOptions">The filter options.</param>
        /// <param name="pagingOptions">The paging options.</param>
        /// <param name="sortingOptions">The sorting options.</param>
        /// <returns></returns>
        public async Task<IPagedList<SignedRequest>> FindRequestsAsync(RequestFilterParameters filterOptions, PagingParameters pagingOptions, SortingParameters sortingOptions)
        {
            var searchRequest = mapper.Map<RequestSearchRequest>(filterOptions);
            if (!String.IsNullOrWhiteSpace(sortingOptions.OrderBy))
            {
                searchRequest.SortByColumn = sortingOptions.OrderBy;
                searchRequest.SortByDirection = sortingOptions.Ascending ? asc : desc;
            }

            var requests = await mimAccessApi.ActionRequestAsync(searchRequest, (pagingOptions.PageNumber - 1) * pagingOptions.PageSize, pagingOptions.PageSize).ConfigureAwait(false);

            var result = await MapAndCalculateSignedRequests(requests, pagingOptions);

            return result;
        }

        /// <summary>
        /// Gets the request asynchronous.
        /// </summary>
        /// <param name="requestId">The request identifier.</param>
        /// <returns></returns>
        public async Task<RequestDetails> GetRequestAsync(Guid requestId)
        {
            Request request = null;
            try
            {
                request = await mimAccessApi.ActionRequestByIdAsync(requestId).ConfigureAwait(false);
            }
            catch(ArgumentOutOfRangeException ex)
            {
                request = null;
            }
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;

            if (request != null)
            {
                var requestDetails = mapper.Map<RequestDetails>(request);

                if (requestDetails.RequestState == RequestState.Denied)
                {
                    var history = await mimAccessApi.EventHistoryAsync(requestDetails.RequestId).ConfigureAwait(false);
                    requestDetails.RequestEventHistory = mapper.Map<RequestEventHistory>(history.FirstOrDefault(e => e.Action == "Deny Request"));
                }

                if (requestDetails.DataCollectionItems.ContainsKey(signatureDciKey))
                {
                    requestDetails.DataCollectionItems.Remove(signatureDciKey);
                }

                return requestDetails;
            }

            return null;
        }

        /// <summary>
        /// Asynchronously validates the actions.
        /// </summary>
        /// <returns></returns>
        public async Task<Validation> ValidateActionsAsync(Guid requestId)
        {
            var validation = new Validation()
            {
                RequestId = requestId,
                Approve = true,
                Deny = true,
                DistributeSecrets = true,
                Cancel = true,
                Abandon = true
            };

            var responseRequest = await mimAccessApi.ActionRequestByIdAsync(requestId).ConfigureAwait(false);

            if (responseRequest != null)
            {
                var request = mapper.Map<RequestDetails>(responseRequest);

                var requestValidationTasks = new Dictionary<string, Func<RequestDetails, Task<bool>>>
                {
                    { nameof(Validation.Approve), validator.ValidateApproveAsync },
                    { nameof(Validation.Deny), validator.ValidateDenyAsync },
                    { nameof(Validation.DistributeSecrets), validator.ValidateDistributeSecretsAsync },
                    { nameof(Validation.Cancel), validator.ValidateCancelAsync },
                    { nameof(Validation.Abandon), validator.ValidateAbandonAsync }
                };

                foreach (var item in requestValidationTasks)
                {
                    if (item.Key.Equals(nameof(Validation.Approve)))
                    {
                        validation.Approve = await item.Value(request);
                    }
                    else if (item.Key.Equals(nameof(Validation.Deny)))
                    {
                        validation.Deny = await item.Value(request);
                    }
                    else if (item.Key.Equals(nameof(Validation.DistributeSecrets)))
                    {
                        validation.DistributeSecrets = await item.Value(request);
                    }
                    else if (item.Key.Equals(nameof(Validation.Cancel)))
                    {
                        validation.Cancel = await item.Value(request);
                    }
                    else if (item.Key.Equals(nameof(Validation.Abandon)))
                    {
                        validation.Abandon = await item.Value(request);
                    }
                }
            }
            else
            {
                validation = new Validation()
                {
                    RequestId = requestId
                };
            }

            return validation;
        }

        private async Task<RequestActionResponse> CreateRequestActionResponseAsync(IEnumerable<Guid> requestIds, Func<RequestDetails, Task<bool>> validate)
        {
            var response = InitializeRequestActionResponse();
            response.IsSuccessful = await ValidateRequestActionAsync(requestIds, (requestDetails) => validate(requestDetails));
            return response;
        }

        private RequestActionResponse InitializeRequestActionResponse()
        {
            return new RequestActionResponse()
            {
                IsSuccessful = true,
                HasSecrets = false
            };
        }

        private async Task<IPagedList<SignedRequest>> MapAndCalculateSignedRequests(m2trust.Messaging.OpenFim.Entities.PagedList<Request> requests, PagingParameters pagingOptions)
        {
            var signatureDciKey = await configuration.SignatureDataCollectionItemName;
            IList<SignedRequest> signedRequests = null;
            IPagedList<SignedRequest> result = EmptyPagedResult<SignedRequest>(pagingOptions);
            if (requests != null && requests.Count > 0)
            {
                signedRequests = mapper.Map<IList<SignedRequest>>(requests);
                foreach (var request in signedRequests)
                {
                    if (request.DataCollectionItems.ContainsKey(signatureDciKey))
                    {
                        request.SignatureStatus = mapper.Map<SignatureState>(
                            await certificateManagerApi.ComputeSignatureStatusAsync(
                                requests.FirstOrDefault(r => r.Id == request.RequestId)));
                    }
                }
                
            }
            if (signedRequests != null)
            {
                result = new StaticPagedList<SignedRequest>(signedRequests, pagingOptions.PageNumber, pagingOptions.PageSize, requests.TotalCount);
            }
            return result;
        }

        private async Task<bool> ValidateRequestActionAsync(IEnumerable<Guid> requestIds, Func<RequestDetails, Task<bool>> validate)
        {
            var result = false;
            foreach (var id in requestIds)
            {
                var request = await mimAccessApi.ActionRequestByIdAsync(id).ConfigureAwait(false);
                if (request != null)
                {
                    var requestDetails = mapper.Map<RequestDetails>(request);
                    result = await validate(requestDetails);
                }
            }
            return result;
        }

        #endregion Methods
    }
}
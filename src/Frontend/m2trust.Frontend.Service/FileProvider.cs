﻿using m2trust.Frontend.Service.Common;
using System;
using System.IO;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service
{
    /// <summary>
    /// File provider.
    /// </summary>
    /// <seealso cref="m2trust.Frontend.Service.Common.IFileProvider" />
    public class FileProvider : IFileProvider
    {
        #region Fields

        private string logoPath;
        private string variablesPath;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FileProvider" /> class.
        /// </summary>
        /// <param name="logoPath">The logo path.</param>
        /// <param name="variablesPath">The variables path.</param>
        public FileProvider(string logoPath, string variablesPath)
        {
            this.logoPath = logoPath;
            this.variablesPath = variablesPath;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Asynchronously gets the logo path.
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetLogoPathAsync()
        {
            await FileExsistsAsync(logoPath);
            return logoPath;
        }

        /// <summary>
        /// Asynchronously reads the file.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <returns></returns>
        public async Task<string> ReadFileAsync()
        {
            await FileExsistsAsync(variablesPath);
            return await File.ReadAllTextAsync(variablesPath);
        }

        /// <summary>
        /// Asynchronously writes to the file.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <param name="content">The file content.</param>
        /// <returns></returns>
        public async Task WriteFileAsync(string content)
        {
            await FileExsistsAsync(variablesPath);
            await File.WriteAllTextAsync(variablesPath, content);
        }

        /// <summary>
        /// Asynchronously check if the file exsists.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <returns></returns>
        private Task<bool> FileExsistsAsync(string path)
        {
            var result = File.Exists(path);
            if (!result)
            {
                throw new Exception("Invalid file path or user does not have sufficient permissions to read file.");
            }
            return Task.FromResult(result);
        }

        #endregion Methods
    }
}
﻿using m2trust.Frontend.Service.Common;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace m2trust.Frontend.Service
{
    public class InMemoryCacheService : ICacheService
    {
        private readonly IMemoryCache cache;

        /// <summary>
        /// Initializes a new instance of the <see cref="InMemoryCacheService"/> class.
        /// </summary>
        /// <param name="cache">The cache. See also <seealso cref="IMemoryCache"/></param>
        public InMemoryCacheService(IMemoryCache cache)
        {
            this.cache = cache ?? throw new ArgumentNullException(nameof(cache));
        }

        /// <summary>
        /// Asynchronously adds or replaces an item.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public Task AddOrReplaceAsync<T>(string key, T item)
        {
            cache.Set(key, item);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Asynchronously gets the item.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public Task<T> GetAsync<T>(string key)
        {
            if(cache.TryGetValue(key, out object value))
            {
                return Task.FromResult((T)value);
            }
            return Task.FromResult(default(T));
        }

        /// <summary>
        /// Asynchronously Removes the item
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Task RemoveKeyAsync(string key)
        {
            cache.Remove(key);
            return Task.CompletedTask;
        }
    }
}

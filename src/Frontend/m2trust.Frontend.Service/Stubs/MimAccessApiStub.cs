﻿using m2trust.Messaging.OpenFim.API;
using m2trust.Messaging.OpenFim.Entities;
using m2trust.Messaging.OpenFim.Enumerations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using m2trust.Messaging.Common.Entities;
using SmartcardProfile = m2trust.Messaging.OpenFim.Entities.SmartcardProfile;

namespace m2trust.Frontend.Service.Stubs
{
    public class MimAccessApiStub : IMimAccessApi
    {
        #region Methods

        public Task AbandonRequestAsync(Guid requestId, string comment)
        {
            return Task.FromResult(true);
        }

        public Task<AuthenticationResult> AcceptToken(byte[] clientToken)
        {
            throw new NotImplementedException();
        }

        public Task<Request[]> ActionLookupRequestsAsync(ReqStatus[] requestStatuses, RequestType requestType, Guid[] userIds)
        {
            var r1 = new Request()
            {
                TargetType = TargetType.Smartcard,
                RequestType = RequestType.Enroll,
                UserDisplayName = "UserDisplayName",
                ProfileId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                Submitted = DateTime.UtcNow,
                Status = ReqStatus.Executing,
                ProfileTemplateId = Guid.NewGuid(),
                Priority = 0,
                OriginatorDisplayName = "OriginatorDisplayName",
                OriginatorUserId = Guid.NewGuid(),
                DataCollectionItems = null,
                Completed = DateTime.UtcNow,
                Comment = "Comment",
                Id = Guid.NewGuid(),
                TargetTypeSet = true,
                ProfileTemplate = new ProfileTemplate()
                {
                    Id = Guid.NewGuid(),
                    Name = "ProfileTemplateName1",
                    EnrollPolicy = new Policy()
                    {
                        NumberOfApprovers = 1,
                        DisplaySecretsOnScreen = false,
                        EmailSecrets = false
                    }
                },
                EventHistory = null
            };

            var r2 = new Request()
            {
                TargetType = TargetType.Smartcard,
                RequestType = RequestType.Enroll,
                UserDisplayName = "UserDisplayName2",
                ProfileId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                Submitted = DateTime.UtcNow,
                Status = ReqStatus.Executing,
                ProfileTemplateId = Guid.NewGuid(),
                Priority = 0,
                OriginatorDisplayName = "OriginatorDisplayName2",
                OriginatorUserId = Guid.NewGuid(),
                DataCollectionItems = null,
                Completed = DateTime.UtcNow,
                Comment = "Comment2",
                Id = Guid.NewGuid(),
                TargetTypeSet = true,
                ProfileTemplate = new ProfileTemplate()
                {
                    Id = Guid.NewGuid(),
                    Name = "ProfileTemplateName2",
                    EnrollPolicy = new Policy()
                    {
                        NumberOfApprovers = 1,
                        DisplaySecretsOnScreen = false,
                        EmailSecrets = false
                    }
                },
                EventHistory = null
            };

            var r3 = new Request()
            {
                TargetType = TargetType.Smartcard,
                RequestType = RequestType.Enroll,
                UserDisplayName = "UserDisplayName3",
                ProfileId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                Submitted = DateTime.UtcNow,
                Status = ReqStatus.Approved,
                ProfileTemplateId = Guid.NewGuid(),
                Priority = 0,
                OriginatorDisplayName = "OriginatorDisplayName3",
                OriginatorUserId = Guid.NewGuid(),
                DataCollectionItems = null,
                Completed = DateTime.UtcNow,
                Comment = "Comment2",
                Id = Guid.NewGuid(),
                TargetTypeSet = true,
                ProfileTemplate = new ProfileTemplate()
                {
                    Id = Guid.NewGuid(),
                    Name = "ProfileTemplateName3",
                    EnrollPolicy = new Policy()
                    {
                        NumberOfApprovers = 1,
                        DisplaySecretsOnScreen = true,
                        EmailSecrets = false
                    }
                },
                EventHistory = null
            };

            var response = new List<Request>() { r1, r2, r3 };

            return Task.FromResult(response.ToArray());
        }

        public Task<bool> ActionPermissionGrantAsync(Guid requestId, ActionPermission requestedPermission)
        {
            return Task.FromResult(true);
        }

        public Task<PagedList<Request>> ActionRequestAsync(RequestSearchRequest request, int offset, int count)
        {
            var list = new List<Request>
            {
                new Request()
                {
                    TargetType = TargetType.SoftProfile, 
                    RequestType = RequestType.Renew, 
                    UserDisplayName = "user-display-name",
                    ProfileId = Guid.NewGuid(),
                    UserId = Guid.NewGuid(), 
                    Submitted = DateTime.Now, 
                    Status = ReqStatus.Pending, 
                    ProfileTemplateId = Guid.NewGuid(), 
                    Comment = "comment",
                    Id = Guid.NewGuid()
                },
                new Request()
                {
                    TargetType = TargetType.SoftProfile,
                    RequestType = RequestType.Renew,
                    UserDisplayName = "user-display-name-2",
                    ProfileId = Guid.NewGuid(),
                    UserId = Guid.NewGuid(),
                    Submitted = DateTime.Now,
                    Status = ReqStatus.Pending,
                    ProfileTemplateId = Guid.NewGuid(),
                    Comment = "comment 2",
                    Id = Guid.NewGuid()
                }
            };


            var requestList = new PagedList<Request>(list);
            return Task.FromResult(requestList);
        }

        public Task<Request> ActionRequestByIdAsync(Guid requestId)
        {
            var r3 = new Request()
            {
                TargetType = TargetType.Smartcard,
                RequestType = RequestType.Retire,
                UserDisplayName = "UserDisplayName3",
                ProfileId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                Submitted = DateTime.UtcNow,
                Status = ReqStatus.Pending,
                ProfileTemplateId = Guid.NewGuid(),
                Priority = 0,
                OriginatorDisplayName = "OriginatorDisplayName3",
                OriginatorUserId = Guid.NewGuid(),
                DataCollectionItems = null,
                Completed = DateTime.UtcNow,
                Comment = "Comment2",
                Id = Guid.NewGuid(),
                TargetTypeSet = true,
                ProfileTemplate = new ProfileTemplate()
                {
                    Id = Guid.NewGuid(),
                    Name = "ProfileTemplateName3",
                    EnrollPolicy = new Policy()
                    {
                        NumberOfApprovers = 1,
                        DisplaySecretsOnScreen = true,
                        EmailSecrets = false
                    }
                },
                EventHistory = null
            };
            return Task.FromResult(r3);
        }

        public Task<RequestSecret> ApproveRequestAsync(Guid requestId)
        {
            var result = new RequestSecret()
            {
                Request = new Request()
                {
                    
                },
                Secrets = new List<string>()
                {
                    "secret1", "secret2"
                },
                Distribution = new Policy()
                {
                    DisplaySecretsOnScreen = true, 
                    EmailSecrets = true
                }
            };
            return Task.FromResult(result);
        }

        public Task CancelAsync(Guid requestId, string comment)
        {
            return Task.FromResult(true);
        }

        public Task<TempEnrollResult> CertificateTempEnrollAsync(Guid profileId, IDictionary<string, string> dataCollectionItems, string comment)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<Guid>> CheckProfileTemplatePermissionAsync(Guid targetUserId)
        {
            throw new NotImplementedException();
        }

        public Task<DataCollectionItem[]> DataCollectionItemsByProfileTemplateAsync(Guid templateId, ActionType actionType)
        {
            var items = new List<DataCollectionItem>
            {
                new DataCollectionItem
                {
                    Name = "String smth",
                    Description = "string name",
                    Required = false,
                    DataType = DciDataType.String,
                    ValidationType = DciValidationType.DataType,
                    ValidationData = "value"
                },
                 new DataCollectionItem
                {
                    Name = "Date smth",
                    Description = "date name fgdgd ",
                    Required = false,
                    DataType = DciDataType.Date,
                    ValidationType = DciValidationType.DataType,
                    ValidationData = "value1"
                },
                  new DataCollectionItem
                {
                    Name = "Number smth",
                    Description = "number name",
                    Required = false,
                    DataType = DciDataType.Number,
                    ValidationType = DciValidationType.DataType,
                    ValidationData = "value2"
                },
                new DataCollectionItem
                {
                    Name = "String smth r",
                    Description = "string name",
                    Required = true,
                    DataType = DciDataType.String,
                    ValidationType = DciValidationType.DataType,
                    ValidationData = "value"
                },
                new DataCollectionItem
                {
                    Name = "Date smth r",
                    Description = "date name",
                    Required = true,
                    DataType = DciDataType.Date,
                    ValidationType = DciValidationType.DataType,
                    ValidationData = "value1"
                },
                new DataCollectionItem
                {
                    Name = "Number smth r",
                    Description = "number name",
                    Required = true,
                    DataType = DciDataType.Number,
                    ValidationType = DciValidationType.DataType,
                    ValidationData = "value2"
                }
            };
            //items = new List<DataCollectionItem>(0);
            return Task.FromResult(items.ToArray());
        }

        public Task DenyRequestAsync(Guid requestId, string comment)
        {
            return Task.FromResult(true);
        }

        public Task<DisableSmartcardResult> DisableSmartcardAsync(Guid smartcardId, IDictionary<string, string> dataCollectionItems, string comment,
            int? effectiveRevocationInHours)
        {
            var disableSmartcard = new DisableSmartcardResult
            {
                RequestId = Guid.NewGuid(),
                OneTimePassword = "disablePass"
            };

            return Task.FromResult(disableSmartcard);
        }

        public Task<RetireResult> RetireSmartcardWithDelayAsync(Guid smartcardId, IDictionary<string, string> dataCollectionItems, string comment,
            int? effectiveRevocationInHours)
        {
            var result = new RetireResult
            {
                RequestId = Guid.NewGuid(),
                OneTimePassword = "passwordone"
            };
            return Task.FromResult(result);
        }

        public Task<DisableSmartcardResult> DisableSmartcarRequestdAsync(Guid requestId, IDictionary<string, string> dataCollectionItems, string comment,
            int? effectiveRevocationInHours)
        {
            throw new NotImplementedException();
        }

        public Task<RequestSecret> DistributeSecretsRequestAsync(Guid requestId)
        {
            var result = new RequestSecret()
            {
                Request = new Request()
                {
                    UserDisplayName = "user-display-name", 
                    OriginatorDisplayName = "originator", 
                    TargetType = TargetType.SoftProfile
                },
                Secrets = new List<string>()
                {
                    "secret1", "secret2"
                },
                Distribution = new Policy()
                {
                    DisplaySecretsOnScreen = true,
                    EmailSecrets = true
                }
            };
            return Task.FromResult(result);
        }

        public Task<EventHistory[]> EventHistoryAsync(Guid requestId)
        {
            throw new NotImplementedException();
        }

        public Task<ProfileStub[]> FindProfileAsync(Guid requestId)
        {
            throw new NotImplementedException();
        }

        public Task<Guid[]> FindProfileIdAsync(Guid requestId)
        {
            throw new NotImplementedException();
        }

        public Task<Profile[]> FindProfilesByUserIdAsync(Guid userId)
        {
            var profiles = new SmartcardProfile[]
            {
                new SmartcardProfile
                {
                    Id = Guid.Parse("f56b168b-a97e-4378-aafa-d183187634ed"),
                    ProfileTemplate = new ProfileTemplate
                    {
                        Name = "Fim Profile Template",
                        Id = Guid.Parse("3da1fe30-bde1-482d-91d1-80562a34dd7b"),
                        
                    },
                    Status = Messaging.OpenFim.Enumerations.ProfileStatus.Active,
                    TargetType =  TargetType.TemporarySmartcard,
                    IsSmartcard = true
                },
                 new SmartcardProfile
                {
                    Id = Guid.Parse("05b3c4a5-95d3-4608-8de5-8b3d26fcbaab"),
                    ProfileTemplate = new ProfileTemplate
                    {
                        Name = "Fim Dev 1",
                        Id = Guid.Parse("b2a0314f-a7db-4f75-9794-2b2e175ab233")
                    },
                    Status = Messaging.OpenFim.Enumerations.ProfileStatus.Retired,
                    TargetType =  TargetType.TemporarySmartcard,
                    IsSmartcard = true
                },
                  new SmartcardProfile
                {
                    Id = Guid.Parse("09e64183-31b7-449f-9040-7c4ecae58802"),
                    ProfileTemplate = new ProfileTemplate
                    {
                        Name = "Fim Dev 2",
                        Id = Guid.Parse("a72ba1d1-4ba5-4dda-8467-3445bf3e3e48")
                    },
                    Status = Messaging.OpenFim.Enumerations.ProfileStatus.Suspended,
                    TargetType =  TargetType.TemporarySmartcard,
                    IsSmartcard = true
                }
            };

            return Task.FromResult((Profile[])profiles);
        }

        public Task<SmartcardProfile> FindSmartcardProfileByIdAsync(Guid profileId)
        {
            var profile = new SmartcardProfile
            {
                Id = Guid.Parse("4224d959-c991-4978-9d5a-44755f157f3c"),
                TargetType = TargetType.TemporarySmartcard,
                PermanentSmartcardId = Guid.NewGuid(),
                ProfileTemplate = new ProfileTemplate
                {
                    Id = Guid.Parse("a3a517cc-4a18-40af-bda5-743899e74233"),
                    ReinstatePolicy = new ReinstatePolicy
                    {
                        AllowSpecifyDelay = true
                    },
                    RevokePolicy = new RevokePolicy
                    {
                        AllowSpecifyDelay = true
                    },
                    TemporaryCardsPolicy = new TemporaryCardsPolicy
                    {
                        ImmediatelySuspendLinkedCard = true
                    },
                    Name = "Profile Template name",
                    
                }
            };

            return Task.FromResult(profile);
        }

        public Task<SmartcardProfile[]> FindSmartcardProfilesByUserIdAsync(Guid userId)
        {
            throw new NotImplementedException();
        }

        public Task<Profile> FindSoftProfileByIdAsync(Guid profileId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<string>> GetRenewedCertificatesAsync(Guid requestId, IEnumerable<string> encodedCertificateRequests, IDictionary<string, string> dataCollectionItems, string comment)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetServerPrincipal()
        {
            throw new NotImplementedException();
        }

        public Task<Request> InitiateOfflineUnblockAsync(IDictionary<string, string> dataCollectionItems, Guid smartcardId)
        {
            var r3 = new Request()
            {
                TargetType = TargetType.Smartcard,
                RequestType = RequestType.Enroll,
                UserDisplayName = "UserDisplayName3",
                ProfileId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                Submitted = DateTime.UtcNow,
                Status = ReqStatus.Approved,
                ProfileTemplateId = Guid.NewGuid(),
                Priority = 0,
                OriginatorDisplayName = "OriginatorDisplayName3",
                OriginatorUserId = Guid.NewGuid(),
                DataCollectionItems = null,
                Completed = DateTime.UtcNow,
                Comment = "Comment2",
                Id = Guid.NewGuid(),
                TargetTypeSet = true,
                ProfileTemplate = new ProfileTemplate()
                {
                    Id = Guid.NewGuid(),
                    Name = "ProfileTemplateName3",
                    EnrollPolicy = new Policy()
                    {
                        NumberOfApprovers = 1,
                        DisplaySecretsOnScreen = true,
                        EmailSecrets = false
                    }
                },
                EventHistory = null
            };
            return Task.FromResult(r3);
        }

        public Task<bool> IsImpersonationInitialized()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<string>> MimRenewedCertificatesAsync(Guid requestId, CookieContainer cookieContainer, IEnumerable<string> encodedCertificateRequests, IDictionary<string, string> dataCollectionItems, string comment)
        {
            throw new NotImplementedException();
        }

        public Task<string> OfflineUnblockChallengeAsync(Guid smartcardId, Guid? requestUuid, string challenge)
        {
            var response = "message response";
            return Task.FromResult(response);
        }

        public Task<RecoverResult> ProfileRecoverAsync(Guid profileId, IDictionary<string, string> dataCollectionItems, string comment, byte priority)
        {
            var result = new RecoverResult()
            {
                OneTimePassword = "one-time-password"
            };
            return Task.FromResult(result);
        }

        //public Task<ProfileRequestResult> ProfileRecoverAsync(ProfileRequest profileRequest)
        //{
        //    var profile = new ProfileRequestResult
        //    {
        //        BaseRequest = new ProfileRequest
        //        {
        //            Id = Guid.NewGuid(),
        //            ProfileId = Guid.NewGuid(),
        //            Comment = "commentcom"
        //        }
        //    };
        //    return Task.FromResult(profile);
        //}

        public Task<ProfileStub[]> ProfileStubsAsync(Guid userId)
        {
            var profiles = new List<ProfileStub>
            {
                new ProfileStub
                {
                    Id = Guid.Parse("db3ba6dd-8898-4b6f-9c89-cba44fef4f0f"),
                    Name = "Fim Cm Profile Template",
                    ProfileStatus = ProfileStatus.Active,
                    IssuanceDate = DateTime.UtcNow,
                    IsSmartCard = true,
                    SmartcardId = Guid.Parse("baf83d38-16b1-42f0-a1f6-d7655f4ba569")
                },
                new ProfileStub
                {
                    Id = Guid.Parse("5f441546-f453-4ebf-933a-c02dc1c3c35d"),
                    Name = "Fim Cm Profile Template 2",
                    ProfileStatus = ProfileStatus.Active,
                    IssuanceDate = DateTime.UtcNow,
                    IsSmartCard = true,
                    SmartcardId = Guid.Parse("0a797fba-d45e-42c4-b1fa-6e6488a1df8b")
                },
                new ProfileStub
                {
                    Id = Guid.Parse("e53dd57e-c0ff-4613-b7c8-18ffd04b29e3"),
                    Name = "Fim Cm Profile Template 3",
                    ProfileStatus = ProfileStatus.Active,
                    IssuanceDate = DateTime.UtcNow,
                    IsSmartCard = true,
                    SmartcardId = Guid.Parse("7ec30dcf-2ad0-466b-999a-4212b111cc0f")
                },
                new ProfileStub
                {
                    Id = Guid.Parse("c41746a6-b1f9-48ac-b7f2-99d40ef03a01"),
                    Name = "Fim Cm Profile Template 4",
                    ProfileStatus = ProfileStatus.Active,
                    IssuanceDate = DateTime.UtcNow,
                    IsSmartCard = true,
                    SmartcardId = Guid.Parse("52387adc-eebe-4cff-8ae4-f44ab2fd6e1d")
                },
                new ProfileStub
                {
                    Id = Guid.Parse("da1dc9d5-d013-4847-86de-24aba283079a"),
                    Name = "Profile Template Smartcard Disabled",
                    ProfileStatus = ProfileStatus.Disabled,
                    IssuanceDate = new DateTime(2018, 7, 15),
                    IsSmartCard = true,
                    SmartcardId = Guid.Parse("312c2797-d12d-4ddf-bbbd-431590814da7")
                },
                new ProfileStub
                {
                    Id = Guid.Parse("4b90b745-4f90-4033-966e-00c75440bf6e"),
                    Name = "Profile Template Smartcard Disabled 2",
                    ProfileStatus = ProfileStatus.Disabled,
                    IssuanceDate = new DateTime(2018, 7, 15),
                    IsSmartCard = true,
                    SmartcardId = Guid.Parse("8704d2e1-743a-4b86-a381-4aa4399c3459")
                },
                 new ProfileStub
                {
                    Id = Guid.Parse("1065dc2b-1616-4ab7-80b8-e16d38eda77e"),
                    Name = "Fim Cm Profile Sample Template",
                    ProfileStatus = ProfileStatus.Retired,
                    IssuanceDate = DateTime.UtcNow,
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "FIm dev user",
                            CertificateTemplate = "test1",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("e3919d6e-4dd2-4738-af34-0cc78f84ff1e"),
                    Name = "Fim Cm Profile Sample Template",
                    ProfileStatus = ProfileStatus.Suspended,
                    IssuanceDate = DateTime.UtcNow,
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "FIm dev user",
                            CertificateTemplate = "test1",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("582fb692-0ecd-4242-bd03-93eccbe9e0df"),
                    Name = "Profile Template",
                    ProfileStatus = ProfileStatus.Active,
                    IssuanceDate = new DateTime(2018, 7, 15),
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "e fe",
                            CertificateTemplate = "df",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("712a058a-af40-445e-ba45-4e50163dc2f1"),
                    Name = "Profile Template",
                    ProfileStatus = ProfileStatus.Disabled,
                    IssuanceDate = new DateTime(2018, 7, 11),
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "e fe",
                            CertificateTemplate = "df",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("e521a987-7169-461c-aa36-9889f8acff07"),
                    Name = "Profile Template",
                    ProfileStatus = ProfileStatus.Active,
                    IssuanceDate = new DateTime(2018, 7, 13),
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "e fe",
                            CertificateTemplate = "df",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("9f6b3d8e-7ec5-4c34-a0ff-46b09c3fa833"),
                    Name = "Profile Template",
                    ProfileStatus = ProfileStatus.Disabled,
                    IssuanceDate = new DateTime(2018, 7, 9),
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "e fe",
                            CertificateTemplate = "df",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("59241f01-8a69-42e7-8671-3a6b597f6a08"),
                    Name = "Profile Template",
                    ProfileStatus = ProfileStatus.Active,
                    IssuanceDate = new DateTime(2018, 7, 22),
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "e fe",
                            CertificateTemplate = "df",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("3b215321-7afa-499f-b910-d45ece236324"),
                    Name = "Profile Template",
                    ProfileStatus = ProfileStatus.Disabled,
                    IssuanceDate = new DateTime(2018, 7, 17),
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "e fe",
                            CertificateTemplate = "df",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("9386f785-85f7-4c54-8421-338cbd08b3b2"),
                    Name = "Profile Template",
                    ProfileStatus = ProfileStatus.Disabled,
                    IssuanceDate = new DateTime(2018, 7, 14),
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "e fe",
                            CertificateTemplate = "df",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("26230412-4fd8-4339-b33e-8ad758c41be0"),
                    Name = "Profile Template",
                    ProfileStatus = ProfileStatus.Active,
                    IssuanceDate = new DateTime(2018, 7, 23),
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "e fe",
                            CertificateTemplate = "df",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("131493e2-ec74-4cee-8dbd-9367e31c36be"),
                    Name = "Profile Template",
                    ProfileStatus = ProfileStatus.Disabled,
                    IssuanceDate = new DateTime(2018, 8, 14),
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "e fe",
                            CertificateTemplate = "df",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("6a57ff76-8f54-4a65-8c2f-8ea254555908"),
                    Name = "Profile Template",
                    ProfileStatus = ProfileStatus.Disabled,
                    IssuanceDate = new DateTime(2018, 9, 1),
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "e fe",
                            CertificateTemplate = "df",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("ab6ec206-3beb-490b-8616-e92a9652cf5c"),
                    Name = "Profile Template",
                    ProfileStatus = ProfileStatus.Active,
                    IssuanceDate = new DateTime(2018, 9, 23),
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "e fe",
                            CertificateTemplate = "df",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                },
                  new ProfileStub
                {
                    Id = Guid.Parse("0392f5fa-b657-4300-a7d7-fde2787ad858"),
                    Name = "Profile Template",
                    ProfileStatus = ProfileStatus.Disabled,
                    IssuanceDate = new DateTime(2018, 9, 8),
                    Certificates = new List<CertificateStub>
                    {
                        new CertificateStub
                        {
                            CommonName = "e fe",
                            CertificateTemplate = "df",
                            Status = CertificateStatus.Valid,
                            ExpirationDate = DateTime.UtcNow
                        }
                    }
                }
            };

            return Task.FromResult(profiles.ToArray());
        }

        public Task<IDictionary<Guid, string>> ProfileTemplateAsync(Guid targetUserId)
        {
            var profileTemplates = new Dictionary<Guid, string>
            {
                { Guid.Parse("def774bc-6316-4058-a268-525a4add475a"), "Profile Template 1" },
                { Guid.Parse("253a7d0b-e1c9-468e-bb95-5ee6c04517db"), "Profile Template 2" },
                { Guid.Parse("2a5d0d30-1f12-4a91-8fc9-78dbf1b28bb1"), "Profile Template 3" },
                { Guid.Parse("a369209d-940d-4a3f-a728-c3bc6727cee9"), "Profile Template 4" }
            };
            return Task.FromResult((IDictionary<Guid, string>)profileTemplates);
        }

        public Task<ProfileTemplate> ProfileTemplateByIdAsync(Guid templateId)
        {
            var x = new ProfileTemplate()
            {
                Id = templateId,
                Name = "NAME:" + templateId.ToString()
            };

            return Task.FromResult(x);
        }

        public Task<ProfileTemplateRequest> ProfileTemplateEnrollAsync(ProfileTemplateRequest profileTemplateRequest)
        {
            var profile = new ProfileTemplateRequest
            {
                Id = Guid.NewGuid(),
                ProfileId = Guid.NewGuid(),
                Comment = "skoasd"
            };

            return Task.FromResult(profile);
        }

        public Task<ProfileTemplate[]> ProfileTemplatesAsync()
        {
            throw new NotImplementedException();
        }

        public Task<ReinstateResult> ReinstateAsync(Guid uuid, bool isSmartcard, IDictionary<string, string> dataCollectionItems, string comment, int? effectiveRevocationInHours)
        {
            throw new NotImplementedException();
        }

        public Task<ReinstateResult> ReinstateSmartcardAsync(Guid smartcardId, IDictionary<string, string> dataCollectionItems, string comment, int? effectiveRevocationInHours)
        {
            var reinstateResult = new ReinstateResult()
            {
                OneTimePassword = "one-time-password",
                RequestId = Guid.NewGuid()
            };
            return Task.FromResult(reinstateResult);
        }

        //public Task<ProfileRequestResult> ReinstateSmartcardAsync(SuspendReinstateRequest profileRequest)
        //{
        //    var profile = new ProfileRequestResult
        //    {
        //        BaseRequest = new ProfileRequest
        //        {
        //            ProfileId = Guid.NewGuid(),
        //            Id = Guid.NewGuid(),
        //            Comment = "stringComment"
        //        }
        //    };
        //    return Task.FromResult(profile);
        //}

        public Task<RenewProfileResult> RenewProfileAsync(Guid uuid, IDictionary<string, string> dataCollectionItems, string comment, Guid assignedUserUuid)
        {
            var result = new RenewProfileResult
            {
                RequestId = Guid.NewGuid(),
                OneTimePassword = "blabla"
            };
            return Task.FromResult(result);
        }

        public Task<RetireResult> RetireSmartcardAsync(Guid smartcardId, IDictionary<string, string> dataCollectionItems, string comment, byte priority)
        {
            var result = new RetireResult
            {
                RequestId = Guid.NewGuid(),
                OneTimePassword = "passwordone"
            };
            return Task.FromResult(result);
        }

        public Task<RevokeResult> RevokeAsync(Guid uuid, IDictionary<string, string> dataCollectionItems, string comment, int? effectiveRevocationInHours)
        {
            throw new NotImplementedException();
        }

        public Task<RevokeResult> RevokeSoftprofileAsync(Guid profileId, IDictionary<string, string> dataCollectionItems, string comment,
            int? effectiveRevocationInHours)
        {
            throw new NotImplementedException();
        }

        public Task<ProfileStub[]> SelfServiceFindCertificateAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<Profile[]> SelfServiceProfileAsync()
        {
            var m2tUi001 = (new Messaging.OpenFim.Entities.Profile
            {
                EnrollSubmitted = System.DateTime.UtcNow,
                IsSmartcard = false,
                UserId = System.Guid.NewGuid(),
                ProfileTemplate = await ProfileTemplateByIdAsync(new System.Guid("5aeeedd0-0bc1-4a0f-a331-21793779fba2")).ConfigureAwait(false),
                RequestStatus = Messaging.OpenFim.Enumerations.ReqStatus.Approved,
                Status = Messaging.OpenFim.Enumerations.ProfileStatus.Active,
                Id = System.Guid.NewGuid()
            });

            var baseAuth = (new Messaging.OpenFim.Entities.Profile
            {
                EnrollSubmitted = System.DateTime.UtcNow,
                IsSmartcard = false,
                UserId = System.Guid.NewGuid(),
                ProfileTemplate = await ProfileTemplateByIdAsync(new System.Guid("7e4b3521-aa0d-4b5f-89b0-042793103a00")).ConfigureAwait(false),
                RequestStatus = Messaging.OpenFim.Enumerations.ReqStatus.Approved,
                Status = Messaging.OpenFim.Enumerations.ProfileStatus.Active,
                Id = System.Guid.NewGuid()
            });

            var m2tUi006 = (new Messaging.OpenFim.Entities.Profile
            {
                EnrollSubmitted = System.DateTime.UtcNow,
                IsSmartcard = false,
                UserId = System.Guid.NewGuid(),
                ProfileTemplate = await ProfileTemplateByIdAsync(new System.Guid("db33b07a-9780-46e5-ab93-4bf39e3e33a0")).ConfigureAwait(false),
                RequestStatus = Messaging.OpenFim.Enumerations.ReqStatus.Executing,
                Status = Messaging.OpenFim.Enumerations.ProfileStatus.Disabled,
                Id = System.Guid.NewGuid()
            });

            var response = new Messaging.OpenFim.Entities.Profile[] { m2tUi001, baseAuth, m2tUi006 };

            return response;
        }

        public Task<ProfileStub[]> SelfServiceProfileStubAsync()
        {
            throw new NotImplementedException();
        }

        public Task<SelfServiceRevokeResult> SelfServiceRevokeAsync(Guid uuid, IDictionary<string, string> dataCollectionItems, string comment,
            int? effectiveRevocationInHours)
        {
            throw new NotImplementedException();
        }

        public Task<IDictionary<string, string>> SubscriberDciAsync(Guid requestId, string otp)
        {
            throw new NotImplementedException();
        }

        public Task<SuspendResult> SuspendAsync(Guid uuid, bool isSmartcard, IDictionary<string, string> dataCollectionItems, string comment, int? effectiveRevocationInHours)
        {
            throw new NotImplementedException();
        }

        public Task<SuspendResult> SuspendSmartcardAsync(Guid smartcardId, IDictionary<string, string> dataCollectionItems, string comment, int? effectiveRevocationInHours)
        {
            var result = new SuspendResult()
            {
                OneTimePassword = "test",
                RequestId = Guid.NewGuid()
            };
            return Task.FromResult(result);
        }

        //public Task<ProfileRequestResult> SuspendSmartcardAsync(SuspendReinstateRequest profileRequest)
        //{
        //    var profile = new ProfileRequestResult
        //    {
        //        BaseRequest = new ProfileRequest
        //        {
        //            ProfileId = Guid.NewGuid(),
        //            Id = Guid.NewGuid(),
        //            Comment = "commentString"
        //        }
        //    };
        //    return Task.FromResult(profile);
        //}

        public Task<TempEnrollResult> TempEnrollAsync(Guid uuid, Guid profileTemplateId, IDictionary<string, string> dataCollectionItems, string comment, Guid? smartCardId)
        {
            throw new NotImplementedException();
        }

        public Task<ValidateOtpResult> ValidateOtpAsync(string otp)
        {
            var result = new ValidateOtpResult()
            {
                RequestId = Guid.NewGuid(),
                CookieContainer = null
            };
            return Task.FromResult(result);
        }

        #endregion Methods
    }
}
﻿using AutoMapper;
using m2trust.Frontend.Common.Enums;
using m2trust.Frontend.Model;
using m2trust.Frontend.Service.Common;
using m2trust.Messaging.OpenFim.API;
using m2trust.Messaging.OpenFim.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Service
{
    public class SmartcardService : ServiceBase, ISmartcardService
    {
        #region Fields

        private readonly int defaultPriority;
        private readonly IMapper mapper;
        private readonly IMimAccessApi mimAccessApi;
        private readonly IProfileService profileService;
        private readonly IReinstateDCIConfigurationManager reinstateDCIConfigurationManager;

        #endregion Fields

        #region Constructors

        public SmartcardService(IMimAccessApi mimAccessApi, IMapper mapper, IProfileService profileService, IReinstateDCIConfigurationManager reinstateDCIConfigurationManager)
        {
            this.mimAccessApi = mimAccessApi;
            this.mapper = mapper;
            this.profileService = profileService;
            this.reinstateDCIConfigurationManager = reinstateDCIConfigurationManager;
            AutomaticReinstateEnabled = true;
        }

        #endregion Constructors

        #region Properties

        public bool AutomaticReinstateEnabled { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Asynchronously disables smartcard
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<OtpResult> DisableSmartcardAsync(AuditRequestEntry request)
        {
            var response = await mimAccessApi.DisableSmartcardAsync(request.ProfileId, request.DataCollectionItems, request.Comment, request.EffectiveRevocationInHours).ConfigureAwait(false);
            if (response != null)
            {
                return mapper.Map<OtpResult>(response);
            }
            return null;
        }

        /// <summary>
        /// Gets the permanent smartcard Id asynchronous
        /// </summary>
        /// <param name="smartcardId"></param>
        /// <returns></returns>
        public async Task<Guid?> GetPermanentSmartcardIdAsync(Guid smartcardId)
        {
            Guid? result = null;

            var smartCard = await profileService.GetSmartcardProfileAsync(smartcardId).ConfigureAwait(false);

            if (await IsSmartcardPermanentAsync(smartCard))
            {
                result = smartCard.PermanentSmartcardId;
            }

            return result;
        }

        public Task<bool> IsSmartcardPermanentAsync(SmartcardProfileEntry smartcard)
        {
            return Task.FromResult(smartcard != null &&
                    smartcard.TargetType == TargetForm.TemporarySmartcard &&
                    smartcard.ProfileTemplate.TemporaryCardsPolicy.ImmediatelySuspendLinkedCard);
        }

        /// <summary>
        /// Asynchronously unblocks smartcard
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<RequestDetails> InitiateOfflineUnblockAsync(AuditRequestEntry request)
        {
            request.Priority = defaultPriority;
            var response = await mimAccessApi.InitiateOfflineUnblockAsync(request.DataCollectionItems, request.ProfileId).ConfigureAwait(false);
            if (response != null)
            {
                return mapper.Map<RequestDetails>(response);
            }
            return null;
        }

        /// <summary>
        /// Asynchronously unblock challenge
        /// </summary>
        /// <param name="requestEntry"></param>
        /// <returns></returns>
        public async Task<string> OfflineUnblockChallengeAsync(EnterChallengeRequestEntry requestEntry)
        {
            var response = await mimAccessApi.OfflineUnblockChallengeAsync(requestEntry.ProfileId, requestEntry.RequestGuid, requestEntry.Challenge).ConfigureAwait(false);
            if (response != null)
            {
                return response;
            }
            return null;
        }

        /// <summary>
        /// Asyncronously Reinstate Smartcard
        /// </summary>
        /// <param name="requestEntry"></param>
        /// <returns></returns>
        public async Task<OtpResult> ReinstateSmartcardAsync(AuditRequestEntry requestEntry)
        {
            if (requestEntry != null)
            {
                var response = await mimAccessApi.ReinstateSmartcardAsync(requestEntry.ProfileId, requestEntry.DataCollectionItems, requestEntry.Comment, requestEntry.EffectiveRevocationInHours).ConfigureAwait(false);
                var result = mapper.Map<OtpResult>(response);
                return result;
            }
            return null;
        }

        /// <summary>
        /// Retires the smartcard asynchronous
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<OtpResult> RetireSmartcardAsync(AuditRequestEntry request)
        {
            if (request != null)
            {
                var response = await mimAccessApi.RetireSmartcardAsync(request.ProfileId, request.DataCollectionItems, request.Comment, (byte)request.Priority).ConfigureAwait(false);
                if (response != null)
                {
                    var result = mapper.Map<OtpResult>(response);
                    return result;
                }
            }
            return null;
        }

        /// <summary>
        /// Asynchronously Suspend Smartcard
        /// </summary>
        /// <param name="requestEntry"></param>
        /// <returns></returns>
        public async Task<OtpResult> SuspendSmartcardAsync(AuditRequestEntry requestEntry)
        {
            if (requestEntry != null)
            {
                var response = await mimAccessApi.SuspendSmartcardAsync(requestEntry.ProfileId, requestEntry.DataCollectionItems, requestEntry.Comment, requestEntry.EffectiveRevocationInHours).ConfigureAwait(false);
                var result = mapper.Map<OtpResult>(response);
                return result;
            }
            return null;
        }

        public async Task<AuditRequestEntry> GetSuspendReinstateRequestAsync(SmartcardProfileEntry smartcard)
        {
            if (smartcard.PermanentSmartcardId == Guid.Empty || smartcard.PermanentSmartcardId == null)
            {
                throw new Exception("No Permanent smartcard found.");
            }

            var result = new AuditRequestEntry();
            result.ProfileId = smartcard.PermanentSmartcardId.Value;
            result.DataCollectionItems = await reinstateDCIConfigurationManager.GetDCIs(smartcard.ProfileTemplate.Id);
            return result;
        }

        #endregion Methods
    }
}
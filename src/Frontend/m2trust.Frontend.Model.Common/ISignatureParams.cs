﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Model.Common
{
    public interface ISignatureParams
    {
        string SignatureCabClassId { get; set; }
        string EkuOid { get; set; }
        string TemplateOid { get; set; }
        bool UseActivexInIe { get; set; }
        string SignatureDataCollectionItemName { get; set; }
    }
}
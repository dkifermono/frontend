﻿using m2trust.Messaging.OpenFim.API;
using m2trust.Messaging.OpenFim.Entities;
using m2trust.Messaging.OpenFim.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace m2trust.Frontend.Model.Common
{
    public interface ISignedRequest
    {
        Guid Id { get; }
        Request Request { get; }
        SignatureStatus SignatureStatus { get; }
        String Signature { get; }
    }
}
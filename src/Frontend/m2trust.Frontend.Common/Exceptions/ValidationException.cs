﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace m2trust.Frontend.Common
{
    public class ValidationException : Exception
    {
        public ValidationException()
        {
        }

        public ValidationException(string message) : base(message)
        {
        }

        public ValidationException(List<ValidationError> errors)
        {
            ValidationErrors = errors;
        }

        public List<ValidationError> ValidationErrors { get; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("errors", ValidationErrors);
        }
    }

    public class ValidationError
    {
        public ValidationError()
        {

        }

        public ValidationError(string error)
        {
            ErrorMessage = error;
        }
        public string ErrorMessage { get; set; }
    }
}

﻿using m2trust.Messaging.Configuration.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace m2trust.Frontend.Common.Configuration
{
    public interface IFrontendConfigurationService
    {
        #region FrontendConfiguration
        Task<Models.Configuration> GetFrontendConfigurationAsync();
        Task SaveFrontendConfigurationAsync(Models.Configuration configuration, Guid userId);
        #endregion FrontendConfiguration

        #region ServiceConfiguration
        Task<ServiceConfiguration> GetServiceConfigurationAsync();
        Task SaveServiceConfigurationAsync(ServiceConfiguration configuration);
        #endregion ServiceConfiguration
    }
}

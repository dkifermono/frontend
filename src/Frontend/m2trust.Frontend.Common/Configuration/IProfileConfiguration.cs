﻿using System.Threading.Tasks;

namespace m2trust.Frontend.Common.Configuration
{
    /// <summary>
    /// Profile configuration contract.
    /// </summary>
    public interface IProfileConfiguration
    {
        #region Properties

        /// <summary>
        /// Gets a value indicating whether [select default profile].
        /// </summary>
        /// <value><c>true</c> if [select default profile]; otherwise, <c>false</c>.</value>
        Task<bool> SelectDefaultProfile { get; }

        #endregion Properties
    }
}
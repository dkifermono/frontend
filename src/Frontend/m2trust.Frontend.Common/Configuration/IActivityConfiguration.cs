﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Common.Configuration
{
    public interface IActivityConfiguration
    {
        #region Methods

        Task<string[]> GetActivitiesAsync();

        Task<bool?> GetShowCommentsAsync(string activityName);

        Task<bool?> GetContinueAsync(string activityName);

        /// <summary>
        /// Gets the names of AD Groups that are authorized to perform the given activity.
        /// </summary>
        /// <param name="activityName">The name of the activity.</param>
        /// <returns></returns>
        Task<IEnumerable<string>> GetAuthorizedADGroups(string activityName);

        /// <summary>
        /// Returns a value indicating whether or not the activity is registered.
        /// </summary>
        /// <param name="activityName">The name of the activity to check</param>
        /// <returns></returns>
        Task<bool> IsActivityPermissionRegistered(string activityName);

        #endregion Methods
    }
}
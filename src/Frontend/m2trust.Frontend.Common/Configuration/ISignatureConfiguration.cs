﻿using System.Threading.Tasks;

namespace m2trust.Frontend.Common.Configuration
{
    /// <summary>
    /// Signature configuration contract.
    /// </summary>
    public interface ISignatureConfiguration
    {
        #region Properties

        /// <summary>
        /// Gets the reject invalid signing certificate.
        /// </summary>
        /// <value>The reject invalid signing certificate.</value>
        Task<bool> RejectInvalidSigningCertificate { get; }

        /// <summary>
        /// Gets the name of the signature data collection item.
        /// </summary>
        /// <value>The name of the signature data collection item.</value>
        Task<string> SignatureDataCollectionItemName { get; }

        #endregion Properties
    }
}
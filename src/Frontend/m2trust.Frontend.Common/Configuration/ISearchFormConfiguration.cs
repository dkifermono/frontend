﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Common.Configuration
{
    /// <summary>
    /// Search form configuration contract.
    /// </summary>
    public interface ISearchFormConfiguration
    {
        #region Properties

        /// <summary>
        /// Gets the size of the search users page.
        /// </summary>
        /// <value>The size of the search users page.</value>
        Task<int> SearchUsersPageSize { get; }

        /// <summary>
        /// Gets the user display properties.
        /// </summary>
        /// <value>The user display properties.</value>
        Task<IEnumerable<string>> UserDisplayProperties { get; }

        /// <summary>
        /// Gets the user search properties.
        /// </summary>
        /// <returns>The user search properties.</returns>
        Task<IEnumerable<string>> UserSearchProperties { get; }

        #endregion Properties
    }
}
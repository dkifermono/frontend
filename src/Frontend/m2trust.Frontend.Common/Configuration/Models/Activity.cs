﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Common.Configuration.Models
{
    public class BaseActivity
    {
        #region Properties

        public IEnumerable<string> AuthorizedGroups { get; set; }
        public string Name { get; set; }
        public bool? ShowComments { get; set; }
        public bool? Continue { get; set; }

        #endregion Properties
    }

    public class Activity
    {
        #region Properties

        public BaseActivity DisableSmartcard { get; set; }

        public BaseActivity Enroll { get; set; }

        public BaseActivity ManageRequests { get; set; }

        public BaseActivity RecoverCertificate { get; set; }

        public BaseActivity ReinstateSmartcard { get; set; }

        public BaseActivity RenewCertificate { get; set; }

        public BaseActivity RetireTemporarySmartcard { get; set; }

        public BaseActivity RevokeCertificate { get; set; }

        public BaseActivity RewriteSmartcard { get; set; }

        public BaseActivity SuspendSmartcard { get; set; }

        public BaseActivity UnblockSmartcard { get; set; }

        public BaseActivity Administration { get; set; }


        #endregion Properties

        #region Indexers

        public BaseActivity this[string propertyName]
        {
            get { return (BaseActivity)this.GetType().GetProperty(propertyName).GetValue(this, null); }
            set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
        }

        #endregion Indexers
    }
}

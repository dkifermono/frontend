﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Common.Configuration.Models
{
    public class PageTextItem
    {
        public string En { get; set; }
        public string De { get; set; }
    }
}

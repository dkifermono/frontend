﻿using System;
using System.Collections.Generic;

namespace m2trust.Frontend.Common.Models
{
    public class ProfileTemplate
    {
        #region Properties
        
        public Guid ProfileTemplateId { get; set; }
        
        public List<DCI> ReinstateDCIs { get; set; }

        #endregion Properties
    }
}
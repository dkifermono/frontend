﻿namespace m2trust.Frontend.Common.Models
{
    public class DCI
    {
        #region Properties
        
        public string Name { get; set; }
        
        public string Value { get; set; }

        #endregion Properties
    }
}
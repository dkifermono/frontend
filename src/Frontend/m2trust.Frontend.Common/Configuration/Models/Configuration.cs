﻿using System;
using System.Collections.Generic;
using System.Text;
using m2trust.Frontend.Common.Models;

namespace m2trust.Frontend.Common.Configuration.Models
{
    public class Configuration
    {
        public bool SelectDefaultProfile { get; set; } = true;

        public bool ShowDisabledProfiles { get; set; } = true;

        public IEnumerable<string> BlacklistedProfileTemplates { get; set; } = new string[0];

        public IEnumerable<string> ExcludedProfileTemplates { get; set; } = new string[0];

        public int SearchUsersPageSize { get; set; } = 50;

        public IEnumerable<string> UserSearchProperties { get; set; } = new string[] { "displayName", "cn", "mail", "description", "name", "userPrincipalName", "memberOf", "msNPCallingStationID", "manager", "distinguishedName", "businessCategory" };

        public IEnumerable<string> UserDisplayProperties { get; set; } = new string[] { "objectclass", "cn", "mail", "description", "name", "userPrincipalName", "msNPCallingStationID", "manager", "distinguishedName", "businessCategory" };

        public IEnumerable<ProfileTemplate> DCIProfileTemplates { get; set; } = new ProfileTemplate[0];

        public PageTextItem WelcomeText { get; set; } = new PageTextItem { En = "eCom Service GmbH m2trust.Frontend", De = "eCom Service GmbH m2trust.Frontend" };

        public PageTextItem FooterText { get; set; } = new PageTextItem { En = "2019 eCom Service GmbH", De = "2019 eCom Service GmbH" };

        public PageTextItem HeaderText { get; set; } = new PageTextItem { En = "DemoCorp Certificate Management Portal", De = "DemoCorp Zertifikatverwaltung Portal" };

        public PageTextItem ContactAddressText { get; set; } = new PageTextItem { En = "DemoCorp Helpdesk", De = "DemoCorp Helpdesk" };

        public PageTextItem HelpDeskInstruction { get; set; } = new PageTextItem { En = "Please contact your DemoCorp Helpdesk.", De = "Bei Fragen wenden Sie sich bitte an den DemoCorp-Helpdesk." };

        public Activity Activities { get; set; } = new Activity
        {
            DisableSmartcard = new BaseActivity { AuthorizedGroups = new string[0], Name = "DisableSmartcard" },
            Enroll = new BaseActivity { AuthorizedGroups = new string[0], Name = "Enroll" },
            ManageRequests = new BaseActivity { AuthorizedGroups = new string[0], Name = "ManageRequests" },
            RecoverCertificate = new BaseActivity { AuthorizedGroups = new string[0], Name = "RecoverCertificate" },
            ReinstateSmartcard = new BaseActivity { AuthorizedGroups = new string[0], Name = "ReinstateSmartcard" },
            RenewCertificate = new BaseActivity { AuthorizedGroups = new string[0], Name = "RenewCertificate" },
            RetireTemporarySmartcard = new BaseActivity { AuthorizedGroups = new string[0], Name = "RetireTemporarySmartcard" },
            RevokeCertificate = new BaseActivity { AuthorizedGroups = new string[0], Name = "RevokeCertificate" },
            RewriteSmartcard = new BaseActivity { AuthorizedGroups = new string[0], Name = "RewriteSmartcard" },
            SuspendSmartcard = new BaseActivity { AuthorizedGroups = new string[0], Name = "SuspendSmartcard" },
            UnblockSmartcard = new BaseActivity { AuthorizedGroups = new string[0], Name = "UnblockSmartcard" },
        };
    }
}

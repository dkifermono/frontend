﻿using System.Threading.Tasks;
using m2trust.Frontend.Common.Configuration.Models;

namespace m2trust.Frontend.Common.Configuration
{
    /// <summary>
    /// Page text configuration contract
    /// </summary>
    public interface IPageTextConfiguration
    {
        #region Properties

        /// <summary>
        /// Gets the contact address.
        /// </summary>
        /// <value>The contact address.</value>
        Task<PageTextItem> ContactAddress { get; }

        /// <summary>
        /// Gets the footer text.
        /// </summary>
        /// <value>The footer text.</value>
        Task<PageTextItem> FooterText { get; }

        /// <summary>
        /// Gets the header text.
        /// </summary>
        /// <value>The header text.</value>
        Task<PageTextItem> HeaderText { get; }

        /// <summary>
        /// Gets the help desk instruction.
        /// </summary>
        /// <value>The help desk instruction.</value>
        Task<PageTextItem> HelpDeskInstruction { get; }

        /// <summary>
        /// Gets the welcome text.
        /// </summary>
        /// <value>The welcome text.</value>
        Task<PageTextItem> WelcomeText { get; }

        #endregion Properties
    }

    
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using m2trust.Frontend.Common.Models;

namespace m2trust.Frontend.Common.Configuration
{
    /// <summary>
    /// Reinstate dci configuration contract.
    /// </summary>
    public interface IReinstateDCIConfiguration
    {
        #region Properties

        /// <summary>
        /// Gets the dci profile templates.
        /// </summary>
        /// <value>The dci profile templates.</value>
        Task<IEnumerable<ProfileTemplate>> DCIProfileTemplates { get; }

        #endregion Properties
    }
}
﻿using System.Threading.Tasks;

namespace m2trust.Frontend.Common.Configuration
{
    /// <summary>
    /// Profile state filter configuration contract.
    /// </summary>
    public interface IProfileStateFilterConfiguration
    {
        #region Properties

        /// <summary>
        /// Gets a value indicating whether [show disabled profiles].
        /// </summary>
        /// <value><c>true</c> if [show disabled profiles]; otherwise, <c>false</c>.</value>
        Task<bool> ShowDisabledProfiles { get; }

        #endregion Properties
    }
}
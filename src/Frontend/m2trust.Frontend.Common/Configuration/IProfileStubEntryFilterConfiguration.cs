﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace m2trust.Frontend.Common.Configuration
{
    /// <summary>
    /// Profile stub entry filter configuration contract.
    /// </summary>
    public interface IProfileStubEntryFilterConfiguration
    {
        #region Properties
        
        Task<IEnumerable<string>> BlacklistedProfileTemplates { get; }

        Task<IEnumerable<string>> ExcludedProfileTemplates { get; }

        #endregion Properties
    }
}
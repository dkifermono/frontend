﻿using System;

namespace m2trust.Frontend.Common.Parameters
{
    public class PagingParameters
    {
        private const int MaxPageSize = 1000;

        /// <summary>
        /// Initializes a new instance of the <see cref="PagingParameters"/> class.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">The page size.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if page number or size are not greater than 0.</exception>
        public PagingParameters(int pageNumber, int pageSize)
        {
            if(pageNumber < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(pageNumber), "Page number must be greater than 0");
            }
            if(pageSize < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(pageSize), "Page size must be greater than 0");
            }

            this.PageNumber = pageNumber;
            this.PageSize = pageSize <= MaxPageSize ? pageSize : MaxPageSize;
        }

        /// <summary>
        /// Gets the page number.
        /// </summary>
        public int PageNumber { get; }

        /// <summary>
        /// Gets the page size.
        /// </summary>
        public int PageSize { get; }
    }
}

﻿using m2trust.Frontend.Common.Enums;
using System;

namespace m2trust.Frontend.Common.Parameters
{
    /// <summary>
    /// Approve activity filter parameters.
    /// </summary>
    public class RequestFilterParameters
    {
        /// <summary>
        /// Gets or sets the type of the request.
        /// </summary>
        /// <value>
        /// The type of the request.
        /// </value>
        public RequestForm? RequestForm { get; set; }

        /// <summary>
        /// Gets or sets the request status.
        /// </summary>
        /// <value>
        /// The request status.
        /// </value>
        public RequestState? RequestState { get; set; }

        /// <summary>
        /// Gets or sets the submitted from date.
        /// </summary>
        /// <value>
        /// The submitted from.
        /// </value>
        public DateTime? SubmittedFrom { get; set; }

        /// <summary>
        /// Gets or sets the sumbitted to date.
        /// </summary>
        /// <value>
        /// The sumbitted to.
        /// </value>
        public DateTime? SumbittedTo { get; set; }

        /// <summary>
        /// Gets or sets the name of the originator user.
        /// </summary>
        /// <value>
        /// The name of the originator user.
        /// </value>
        public string OriginatorUserName { get; set; }

        /// <summary>
        /// Gets or sets the name of the target user.
        /// </summary>
        /// <value>
        /// The name of the target user.
        /// </value>
        public string TargetUserName { get; set; }
    }
}
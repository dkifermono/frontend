﻿using m2trust.Frontend.Common.Enums;
using System;

namespace m2trust.Frontend.Common.Parameters
{
    public class LoggingFilterParameters
    {
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public string SearchTerm { get; set; }
        public LogLevelType? LogLevel { get; set; }
        public bool? ShowAuditLogs { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Common.Parameters.Sorting
{
    /// <summary>
    /// Sorting parameters.
    /// </summary>
    public class SortingParameters
    {
        /// <summary>
        /// Gets or sets the order by.
        /// </summary>
        /// <value>
        /// The order by.
        /// </value>
        public string OrderBy { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="SortingParameters"/> is ascending.
        /// </summary>
        /// <value>
        ///   <c>true</c> if ascending; otherwise, <c>false</c>.
        /// </value>
        public bool Ascending { get; set; }
    }
}
﻿namespace m2trust.Frontend.Common.Enums
{
    /// <summary>
    /// Target type enum.
    /// </summary>
    public enum TargetForm
    {
        Smartcard = 0,
        PermanentSmartcard = 1,
        TemporarySmartcard = 2,
        SoftProfile = 3
    }
}
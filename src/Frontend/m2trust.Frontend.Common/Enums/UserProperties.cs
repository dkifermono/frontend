﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Common.Enums
{
    public enum UserDisplayProperties
    {
        displayName = 0,
        cn = 1,
        mail = 2, 
        description = 3,
        name = 4,
        userPrincipalName = 5,
        memberOf = 6,
        msNPCallingStationID = 7,
        operatingSystem = 8,
        manager = 9,
        distinguishedName = 10,
        businessCategory = 11
    }

    public enum UserSearchProperties
    {
        displayName = 0,
        cn = 1,
        mail = 2,
        description = 3,
        name = 4,
        userPrincipalName = 5,
        memberOf = 6,
        msNPCallingStationID = 7,
        manager = 8, 
        distinguishedName = 9,
        businessCategory = 10
    }
}

﻿namespace m2trust.Frontend.Common.Enums
{
    /// <summary>
    /// data type enum
    /// </summary>
    public enum DataTypeDci
    {
        String = 0,
        Number = 1,
        Date = 2,
        None = 3
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Common.Enums
{
    public enum SectionType
    {
        TempEnroll = 0,
        Enroll = 1,
        Revoke = 2,
        SelfServiceRevoke = 3,
        SelfServiceRenew = 4,
        Retire = 5,
        Suspend = 6,
        Reinstate = 7,
        Recover = 8,
        OfflineUnblock = 9,
        Renew = 10,
        Unknown = 11
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Common.Enums
{
    public enum ProfileState
    {
        None = 0,
        New = 1,
        Active = 2,
        Disabled = 3,
        Suspended = 4,
        Retired = 5,
        Assigned = 6
    }
}

﻿namespace m2trust.Frontend.Common.Enums
{
    public enum Activity
    {
        Enroll,
        ManageRequests,
        DisableSmartcard,
        UnblockSmartcard,
        RecoverCertificate,
        ReinstateSmartcard,
        RetireTemporarySmartcard,
        RevokeCertificate,
        SuspendSmartcard,
        RewriteSmartcard,
        RenewCertificate,
        Logging,
        UiSettings,
        Administration
    }
}
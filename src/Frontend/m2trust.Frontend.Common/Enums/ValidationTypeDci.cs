﻿namespace m2trust.Frontend.Common.Enums
{
    /// <summary>
    /// validation type enum
    /// </summary>
    public enum ValidationTypeDci
    {
        Custom = 0,
        DataType = 1,
        None = 2,
        RegularExpression = 3
    }
}

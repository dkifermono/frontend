﻿namespace m2trust.Frontend.Common.Enums
{
    /// <summary>
    /// Request status enum.
    /// </summary>
    public enum RequestState
    {
        Approved = 0,
        Canceled = 1,
        Completed = 2,
        Denied = 3,
        Executing = 4,
        Failed = 5,
        Pending = 6
    }
}
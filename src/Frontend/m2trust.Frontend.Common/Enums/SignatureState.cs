﻿namespace m2trust.Frontend.Common.Enums
{
    /// <summary>
    /// Signature status enum.
    /// </summary>
    public enum SignatureState
    {
        Unsigned = 0,
        ValidSignature = 1,
        InvalidHash = 2,
        ExpiredCert = 4,
        InvalidCert = 8,
        NoCert = 16,
        RevokedCert = 32,
        NoTrustedRoot = 64,
        UserNameMismatch = 128
    }
}
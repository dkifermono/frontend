﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Common.Enums
{
    public enum LogLevelType
    {
        Trace = 0,
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Common.Enums
{
    /// <summary>
    /// Request action enum.
    /// </summary>
    public enum RequestAction
    {
        DistributeSecrets = 0,
        Approve = 1,
        Cancel = 2,
        Abandon = 3,
        Deny = 4
    }
}
﻿namespace m2trust.Frontend.Common.Enums
{
    /// <summary>
    /// Request form enum.
    /// </summary>
    public enum RequestForm
    {
        None = 0,
        Enroll = 1,
        Recover = 2,
        Renew = 3,
        Disable = 4,
        Unblock = 5,
        Duplicate = 6,
        Retire = 7,
        RecoverOnBehalf = 8,
        SuspendOrReinstate = 9,
        OnlineUpdate = 10,
        TemporaryCardEnroll = 11,
        TemporaryCardRetire = 12,
        TemporaryCardDisable = 13,
        OfflineUnblock = 14
    }
}
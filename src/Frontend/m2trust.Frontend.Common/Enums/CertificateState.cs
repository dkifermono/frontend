﻿using System;
using System.Collections.Generic;
using System.Text;

namespace m2trust.Frontend.Common.Enums
{
    public enum CertificateState
    {
        Unknown = -1,
        Valid = 0,
        Expired = 1,
        NotYetValid = 2,
        Revoked = 3
    }
}

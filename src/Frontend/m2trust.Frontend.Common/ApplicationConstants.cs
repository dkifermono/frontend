﻿namespace m2trust.Frontend.Common
{
    public static class Permissions
    {
        #region Fields

        public const string DisableSmartcard = "DisableSmartcard";
        public const string Enroll = "Enroll";
        public const string Logging = "Logging";
        public const string ManageRequests = "ManageRequests";
        public const string RecoverCertificate = "RecoverCertificate";
        public const string ReinstateSmartcard = "ReinstateSmartcard";
        public const string RenewCertificates = "RenewCertificates";
        public const string RequestAudit = "RequestAudit";
        public const string RequestReinstate = "RequestReinstate";
        public const string RetireTemporarySmartcard = "RetireTemporarySmartcard";
        public const string RevokeCertificate = "RevokeCertificate";
        public const string RevokeProfile = "RevokeProfile";
        public const string RewriteSmartcard = "RewriteSmartcard";
        public const string SuspendSmartcard = "SuspendSmartcard";
        public const string TemporaryCardEnroll = "TemporaryCardEnroll";
        public const string UiSettings = "UiSettings";
        public const string UnblockSmartcard = "UnblockSmartcard";

        #endregion Fields
    }

    public class SettingKeys
    {
        #region Fields
        public const string FrontendConfigKey = "FrontendConfigKey";
        public const string ContactAddressKey = "ContactAddressText";
        public const string DCIProfileTemplatesKey = "DCIProfileTemplates";
        public const string FooterTextKey = "FooterText";
        public const string HeaderTextKey = "HeaderText";
        public const string WelcomeTextKey = "WelcomeText";
        public const string HelpDeskInstructionKey = "HelpDeskInstruction";
        public const string Activities = "Activities";
        public const string BlacklistedProfileTemplatesKey = "BlacklistedProfileTemplates";
        public const string ExcludedProfileTemplatesKey = "ExcludedProfileTemplates";
        
        public const string SearchUsersPageSizeKey = "SearchUsersPageSize";
        public const string SelectDefaultProfileKey = "SelectDefaultProfile";
        public const string ShowDisabledProfilesKey = "ShowDisabledProfiles";

        public const string SignatureEkuOidKey = "signatureEkuOid";
        public const string SignatureTempOidKey = "signatureTempOid";
        public const string SkipSingleProfileKey = "skipSingleProfile";
        public const string UserDisplayPropertiesKey = "UserDisplayProperties";
        public const string UserSearchPropertiesKey = "UserSearchProperties";


        #endregion Fields
    }

    public class AdministrationConfig
    {
        public string Name { get; set; }
        public string[] Groups { get; set; }
    }
}
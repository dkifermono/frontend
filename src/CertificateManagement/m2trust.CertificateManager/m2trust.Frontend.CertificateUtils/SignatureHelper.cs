﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace m2trust.Frontend.CertificateUtils
{
    //TODO: this is only a "forwarding" class - should investigate how to "use the package directly" (always got runtime errors - forwarding works)
    public sealed class SignatureHelper
    {
        public static string BuildSignatureString(string base64Certificate, string base64SignatureDigest)
        {
            return m2trust.RequestSigning.Common.SignatureHelper.BuildSignatureString(base64Certificate,
                base64SignatureDigest);
        }

        public static string GetDataToSignForProfileTemplateRequest(string signatureDciName, Guid userId, Guid profileTemplateId, Guid originatorId, IDictionary<string, object> dataCollectionItems)
        {
            return m2trust.RequestSigning.Common.SignatureHelper.GetDataToSignForProfileTemplateRequest(
                signatureDciName, userId, profileTemplateId, originatorId, dataCollectionItems);
        }

        public static string GetDataToSignForProfileRequest(string signatureDciName, Guid profileId, Guid originatorId, IDictionary<string, object> dataCollectionItems)
        {
            return m2trust.RequestSigning.Common.SignatureHelper.GetDataToSignForProfileRequest(signatureDciName,
                profileId, originatorId, dataCollectionItems);
        }
    }
}
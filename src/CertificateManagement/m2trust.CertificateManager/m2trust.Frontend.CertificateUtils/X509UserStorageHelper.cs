﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace m2trust.Frontend.CertificateUtils
{
    public sealed class X509UserStorageHelper
    {
        public static IReadOnlyList<string> GetCertificateThumbprintHexStringsByTemplateOid(string certTemplateOid)
        {
            return GetCertificateThumbprintEnumerableByTemplateOid(certTemplateOid)
                .ToList();
        }

        private static IEnumerable<string> GetCertificateThumbprintEnumerableByTemplateOid(string certTemplateOid)
        {
            using (X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser))
            {
                try
                {
                    store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                }
                catch (CryptographicException ex)
                {
                    //LogManager.LogCritical($"Store is unreadable: {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
                    throw new Exception(ex.ToString());
                }
                catch (SecurityException ex)
                {
                    //LogManager.LogCritical($"No permissions to open: {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
                    throw new Exception(ex.ToString());
                }
                catch (ArgumentException ex)
                {
                    //LogManager.LogCritical($"Invalid store values: {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
                    throw new Exception(ex.ToString());
                }

                X509Certificate2Collection certificates = store.Certificates;

                foreach (X509Certificate2 cert in certificates)
                {
                    var found = cert.Extensions.Cast<X509Extension>().Where(x => x.Oid.Value == "1.3.6.1.4.1.311.21.7")
                        .FirstOrDefault(c =>
                            c.Format(true)
                                .Contains(certTemplateOid));
                    if (found != null)
                    {
                        yield return cert.Thumbprint;
                    }
                }
            }
        }

        public static byte[] HexStringToByteArray(string hex)
        {
            if (hex.Length % 2 == 1)
            {
                throw new Exception("The binary key cannot have an odd number of digits");
            }

            byte[] arr = new byte[hex.Length >> 1];

            for (int i = 0; i < hex.Length >> 1; ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        public static int GetHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            return val - (val < 58 ? 48 : 55);
            //For lowercase a-f letters:
            //return val - (val < 58 ? 48 : 87);
            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }
    }
}
